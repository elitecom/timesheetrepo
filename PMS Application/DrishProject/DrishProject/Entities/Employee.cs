﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface IEmployee
    {

        string UserName { get; set; }
        string Password { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string EmployeeCode { get; set; }
        DateTime DateOfBirth { get; set; }
        DateTime DateOfJoining { get; set; }
        DateTime DateOfLeaving { get; set; }
        string CorrespondanceAddress { get; set; }
        string PermanentAddress { get; set; }
        string PhoneNumber { get; set; }
        string MobileNumber { get; set; }
        string EmailId { get; set; }
        int DepartmentId { get; set; }
        int DesgID { get; set; }
        string EducationalQualification { get; set; }
        string SkillSet { get; set; }
        string Hobbies { get; set; }
        string Remarks { get; set; }
        string Status { get; set; }
        string SeatLocation { get; set; }
        string IdentityCard { get; set; }
        int IsReportingManager { get; set; }
        int ProjectId { get; set; }
        int RoleId { get; set; }
        string ShortName { get; set; }
        int IsAministrator { get; set; }
        string DepartmentName { get; set; }
        string Designation { get; set; }
        int RM_ID { get; set; }
        int EmployeeId { get; set; }
        string OldPassword { get; set; }
        string NewPassword { get; set; }
    }
    public class Employee : IEmployee
    {
        public string UserName { get; set; }
        public int IsAministrator { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmployeeCode { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime DateOfJoining { get; set; }
        public DateTime DateOfLeaving { get; set; }
        public string Designation { get; set; }
        public string DepartmentName { get; set; }
        public string CorrespondanceAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string EmailId { get; set; }
        public int DepartmentId { get; set; }
        public int DesgID { get; set; }
        public string EducationalQualification { get; set; }
        public string SkillSet { get; set; }
        public string Hobbies { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }
        public string SeatLocation { get; set; }
        public string IdentityCard { get; set; }
        public int IsReportingManager { get; set; }
        public int ProjectId { get; set; }
        public int RoleId { get; set; }
        public string ShortName { get; set; }
        public int RM_ID { get; set; }
        public int EmployeeId { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
