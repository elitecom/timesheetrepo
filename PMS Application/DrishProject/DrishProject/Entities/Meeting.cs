﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface IMeeting
    {
        int MeetingId { get; set; }
        int ProjectId { get; set; }
        string Recap { get; set; }
        string MeetingDate { get; set; }
        string MeetingTime { get; set; }
        string Duration { get; set; }
        int MediaId { get; set; }
        string Media { get; set; }
        string Subject { get; set; }
        int CalledBy { get; set; }
        string CalledByName { get; set; }
        string MeetingHr { get; set; }
        string MeetingMin { get; set; }
        string MeetingAmPm { get; set; }
        int MeetingStatusId { get; set; }
        int MemberId { get; set; }
        int IsClient { get; set; }
        string Criteria { get; set; }
        string SortCriteria { get; set; }
        string SortDir { get; set; }
        string To { get; set; }
        string Cc { get; set; }
        string Attachment { get; set; }
        int VenueId { get; set; }
    }
    public class Meeting : IMeeting
    {
        public int MeetingId { get; set; }
        public int ProjectId { get; set; }
        public string Recap { get; set; }
        public string MeetingDate { get; set; }
        public string MeetingTime { get; set; }
        public string Duration { get; set; }
        public int MediaId { get; set; }
        public string Media { get; set; }
        public string Subject { get; set; }
        public int CalledBy { get; set; }
        public string CalledByName { get; set; }
        public string MeetingHr { get; set; }
        public string MeetingMin { get; set; }
        public string MeetingAmPm { get; set; }
        public int MeetingStatusId { get; set; }
        public int MemberId { get; set; }
        public int IsClient { get; set; }
        public string Criteria { get; set; }
        public string SortCriteria { get; set; }
        public string SortDir { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Attachment { get; set; }
        public int VenueId { get; set; }
    }
}
