﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface IClients
    {
        int ClientID { get; set; }
        string ClientName { get; set; }
        string CompanyName { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zip { get; set; }
        string Notes { get; set; }
        string Country { get; set; }
        int ProjectID { get; set; }
        int ContactId { get; set; }
        int MapId { get; set; }
        #region "Unused Code"        
        //int intMapId { get; set; }
        //int intProjectId { get; set; }
        //int intClientId { get; set; }
        //int intContactId { get; set; }
        //string strContactFirstName { get; set; }
        //string strContactMiddleName { get; set; }
        //string strContactLastName { get; set; }
        //string strContactEmail { get; set; }
        //string strContactPhone { get; set; }
        //string strContactMobile { get; set; }
        //string strContactFax { get; set; }
        //string strContactRemarks { get; set; }
        //string strSortCriteria { get; set; }
        //string strSortDir { get; set; }
        #endregion
    }
    public class Clients :IClients
    {
        public int ClientID { get; set; }
        public string ClientName { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Notes { get; set; }
        public string Country { get; set; }
        public int ProjectID { get; set; }
        public int ContactId { get; set; }
        public int MapId { get; set; }
        #region "Unused Code"        
        //public int intMapId { get; set; }
        //public int intProjectId { get; set; }
        //public int intClientId { get; set; }
        //public int intContactId { get; set; }
        //public string strContactFirstName { get; set; }
        //public string strContactMiddleName { get; set; }
        //public string strContactLastName { get; set; }
        //public string strContactEmail { get; set; }
        //public string strContactPhone { get; set; }
        //public string strContactMobile { get; set; }
        //public string strContactFax { get; set; }
        //public string strContactRemarks { get; set; }
        //public string strSortCriteria { get; set; }
        //public string strSortDir { get; set; }
        #endregion
    }
}
