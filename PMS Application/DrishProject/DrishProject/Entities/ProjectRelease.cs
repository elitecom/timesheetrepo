﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface IProjectRelease
    {
        int ReleaseId{ get;set;}
        int TaskId{ get;set;}
        int ProjectId{ get;set;}
        string ReleaseStatus{ get;set;}
        DateTime ReleaseDate{ get;set;}
        DateTime ReleaseDateF{ get;set;}
        string Deliverables{ get;set;}
        string ReleaseTarget{ get;set;}
        string DeliveryIssues{ get;set;}
        int EnteredBy{ get;set;}
        DateTime EnteredOn{ get;set;}
        int LastModifiedBy{ get;set;}
        DateTime LastModifiedOn{ get;set;}
        string Description{ get;set;}
        int ReleaseStatusId{ get;set;}
        string SortCriteria{ get;set;}
        string SortDir{ get;set;}
        DateTime ActualDate{ get;set;}
        string ReleaseIssues { get; set; }
    }
    public class ProjectRelease :IProjectRelease
    {
        public int ReleaseId { get; set; }
        public int TaskId { get; set; }
        public int ProjectId { get; set; }
        public string ReleaseStatus { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime ReleaseDateF { get; set; }
        public string Deliverables { get; set; }
        public string ReleaseTarget { get; set; }
        public string DeliveryIssues { get; set; }
        public int EnteredBy { get; set; }
        public DateTime EnteredOn { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public string Description { get; set; }
        public int ReleaseStatusId { get; set; }
        public string SortCriteria { get; set; }
        public string SortDir { get; set; }
        public DateTime ActualDate { get; set; }
        public string ReleaseIssues { get; set; }
    }
}
