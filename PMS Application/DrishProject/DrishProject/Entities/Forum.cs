﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface IForum
    {
        int TopicId { get; set; }
        string TopicSummary { get; set; }
        string TopicDesc { get; set; }
        string NameCreatedBy { get; set; }
        DateTime CreatedOn { get; set; }
        string ReplyDesc { get; set; }
        DateTime ReplyOn { get; set; }
        int ReplyBy { get; set; }
        string ReplyAttachment { get; set; }
        int ReplyID { get; set; }
        int CreatedBy { get; set; }
        int LastReplyBy { get; set; }
        string NameLastReplyBy { get; set; }
        DateTime LastReployOn { get; set; }
        string Attachment { get; set; }        
    }

    public class Forum : IForum
    {
        public int TopicId { get; set; }
        public string TopicSummary { get; set; }
        public string TopicDesc { get; set; }
        public string NameCreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ReplyDesc { get; set; }
        public DateTime ReplyOn { get; set; }
        public int ReplyBy { get; set; }
        public string ReplyAttachment { get; set; }
        public int ReplyID { get; set; }
        public int CreatedBy { get; set; }
        public int LastReplyBy { get; set; }
        public string NameLastReplyBy { get; set; }
        public DateTime LastReployOn { get; set; }
        public string Attachment { get; set; }
    }
}
