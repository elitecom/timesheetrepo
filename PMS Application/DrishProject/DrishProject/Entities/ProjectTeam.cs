﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface IProjectTeam
    {
        string MemberName { get; set; }
        string MemberRole { get; set; }
        string MemberWorking { get; set; }
        string MemberAvailability { get; set; }
        string ReportingManager { get; set; }

        int ProjectId { get; set; }
        int MemberId { get; set; }
        int EmpId { get; set; }
        int MemberNameId { get; set; }
        int MemberRoleId { get; set; }
        string Availability { get; set; }
        int ModifiedBy { get; set; }
        int ReportTo { get; set; }
        DateTime ModifiedOn { get; set; }
    }
    public class ProjectTeam : IProjectTeam
    {
        public string MemberName { get; set; }
        public string MemberRole { get; set; }
        public string MemberWorking { get; set; }
        public string MemberAvailability { get; set; }
        public string ReportingManager { get; set; }

        public int ProjectId { get; set; }
        public int MemberId { get; set; }
        public int EmpId { get; set; }
        public int MemberNameId { get; set; }
        public int MemberRoleId { get; set; }
        public string Availability { get; set; }
        public int ModifiedBy { get; set; }
        public int ReportTo { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
