﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface IProjectNotes
    {
        int NoteId { get; set; }
        int ProjectId { get; set; }
        int NoteTypeId { get; set; }
        string Note { get; set; }
        int PostedBy { get; set; }
        DateTime PostedOn { get; set; }
        string Reference { get; set; }
        int LastModifiedBy { get; set; }
        DateTime LastModifiedOn { get; set; }
        string SortCriteria { get; set; }
        string SortDir { get; set; }
    }
     
    public class ProjectNotes : IProjectNotes
    {
        public int NoteId { get; set; }
        public int ProjectId { get; set; }
        public int NoteTypeId { get; set; }
        public string Note { get; set; }
        public int PostedBy { get; set; }
        public DateTime PostedOn { get; set; }
        public string Reference { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public string SortCriteria { get; set; }
        public string SortDir { get; set; }
    }
}
