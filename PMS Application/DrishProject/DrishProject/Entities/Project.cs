﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface IProject
    {
        int ProjectID { get; set; }
        string ProjectName { get; set; }
        string ProjectCode { get; set; }
        string ProjectCategory { get; set; }
        string ProjectType { get; set; }
        string ProjectStatus { get; set; }
        DateTime ProjectStartDate { get; set; }
        DateTime ProjectEndDate { get; set; }
        string ProjectLevel { get; set; }
        int ProjectPlan { get; set; }
        int MantisSetup { get; set; }
        string ProjectEmail { get; set; }
        int ProjectTypeID { get; set; }
        int ProjectStatusID { get; set; }
        string ProjectPhase { get; set; }
        int ProjectLevelID { get; set; }
        DateTime ProjectStartDateF { get; set; }
        DateTime ProjectEndDateF { get; set; }
        string ProjectEmailId { get; set; }
    }
    public class Project : IProject
    {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectCategory { get; set; }
        public string ProjectType { get; set; }
        public string ProjectStatus { get; set; }
        public DateTime ProjectStartDate { get; set; }
        public DateTime ProjectEndDate { get; set; }
        public string ProjectLevel { get; set; }
        public int ProjectPlan { get; set; }
        public int MantisSetup { get; set; }
        public string ProjectEmail { get; set; }
        public int ProjectTypeID { get; set; }
        public int ProjectStatusID { get; set; }
        public string ProjectPhase { get; set; }
        public int ProjectLevelID { get; set; }
        public DateTime ProjectStartDateF { get; set; }
        public DateTime ProjectEndDateF { get; set; }
        public string ProjectEmailId { get; set; }
    }
}
