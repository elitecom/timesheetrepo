﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface IDSR
    {
        int DSRHistID { get; set; }
        int tID { get; set; }
        int ProjectID { get; set; }
        int ModuleID { get; set; }
        int ActivityID { get; set; }
        decimal HrsSpent { get; set; }
        int tMonth	{ get; set; }
        int tDay	{ get; set; }
        int tYear	{ get; set; }
        DateTime tDate	{get;set;}
        string WorkDone	{get;set;}
        int LastModifiedBy {get;set;}
        DateTime LastModifiedOn	{get;set;}
        int IfDSRSent{get;set;}
        int EmpID { get; set; }
    }

    public class DSR : IDSR
    {
        public int DSRHistID { get; set; }
        public int tID { get; set; }
        public int ProjectID { get; set; }
        public int ModuleID { get; set; }
        public int ActivityID { get; set; }
        public decimal HrsSpent { get; set; }
        public string WorkDone { get; set; }
        public int tMonth { get; set; }
        public int tDay { get; set; }
        public int tYear { get; set; }
        public DateTime tDate { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public int IfDSRSent { get; set; }
        public int EmpID {get;set;}
    }
}
