﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface IDocuments
    {
        int DocId { get; set; }
        int DocTypeId { get; set; }
        string ProjectCategory { get; set; }
        string DocType { get; set; }
        string DocDesc { get; set; }
        int ProjectId { get; set; }
        string ProjectCode { get; set; }
        int ProjectLevelId { get; set; }
        string DocName { get; set; }
        string DocFileName { get; set; }
        int UploadedByID { get; set; }
        string UploadedBy { get; set; }
        DateTime UploadedOn {get;set;}
        string FolderName { get; set; }
        string FolderPath { get; set; }
        string SortCriteria { get; set; }
        string SortDir { get; set; }
        string Comments { get; set; }
        int CommentID { get; set; }
        string Description { get; set; }
        bool UpLoadFile { get; set; }
    }
    public class Documents :IDocuments
    {
        public int DocId { get; set; }
        public int DocTypeId { get; set; }
        public string ProjectCategory { get; set; }
        public string DocType { get; set; }
        public string DocDesc { get; set; }
        public int ProjectId { get; set; }
        public string ProjectCode { get; set; }
        public int ProjectLevelId { get; set; }
        public string DocName { get; set; }
        public string DocFileName { get; set; }
        public int UploadedByID { get; set; }
        public string UploadedBy { get; set; }
        public DateTime UploadedOn { get; set; }
        public string FolderName { get; set; }
        public string FolderPath { get; set; }
        public string SortCriteria { get; set; }
        public string SortDir { get; set; }
        public string Comments { get; set; }
        public int CommentID { get; set; }
        public string Description { get; set; }
        public bool UpLoadFile { get; set; }
    }
}
