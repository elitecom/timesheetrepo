﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface ISkillSet
    {
        int Quarter { get; set; }
        int EmpId { get; set; }
        int Year { get; set; }
        int Approach {get;set;}
        int Analysis {get;set;}
        int Confidence {get;set;}
        int Spoken {get;set;}
        int Listenning {get;set;}
        int Written {get;set;}
        int TeamWork {get;set;}
        int LeaderShip {get;set;}
        int Planning {get;set;}
        int Willingness {get;set;}
        int VB {get;set;}
        int VbDotNet {get;set;}
        int ASP {get;set;}
        int AspDotNet {get;set;}
        int CSharp {get;set;}
        int C {get;set;}
        int Php {get;set;}
        int SqlServer {get;set;}
        int Oracle {get;set;}
        int MySql {get;set;}
        int ManualTesting {get;set;}
        int AutomatedTesting {get;set;}
        int TestPlanMaking {get;set;}
        int ISOProcess {get;set;}
        int WhiteBox {get;set;}
        int Windows {get;set;}
        int Linux {get;set;}
        int Flash {get;set;}
        int Phhop {get;set;}
        int DreamWeaver {get;set;}
        int Nunit {get;set;}
        int Kentico {get;set;}
        int XSLT {get;set;}
        int SqlServerReports {get;set;}
        int Kentico0 {get;set;}
    }

    public class SkillSet : ISkillSet
    {
        public int Quarter { get; set; }
        public int EmpId { get; set; }
        public int Year { get; set; }
        public int Approach {get;set;}
        public int Analysis {get;set;}
        public int Confidence {get;set;}
        public int Spoken {get;set;}
        public int Listenning {get;set;}
        public int Written {get;set;}
        public int TeamWork {get;set;}
        public int LeaderShip {get;set;}
        public int Planning {get;set;}
        public int Willingness {get;set;}
        public int VB {get;set;}
        public int VbDotNet {get;set;}
        public int ASP {get;set;}
        public int AspDotNet {get;set;}
        public int CSharp {get;set;}
        public int C {get;set;}
        public int Php {get;set;}
        public int SqlServer {get;set;}
        public int Oracle {get;set;}
        public int MySql {get;set;}
        public int ManualTesting {get;set;}
        public int AutomatedTesting {get;set;}
        public int TestPlanMaking {get;set;}
        public int ISOProcess {get;set;}
        public int WhiteBox {get;set;}
        public int Windows {get;set;}
        public int Linux {get;set;}
        public int Flash {get;set;}
        public int Phhop {get;set;}
        public int DreamWeaver {get;set;}
        public int Nunit {get;set;}
        public int Kentico {get;set;}
        public int XSLT {get;set;}
        public int SqlServerReports {get;set;}
        public int Kentico0 {get;set;}
    }
}
