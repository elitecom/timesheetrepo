﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface ICodeReview
    {
        int ReviewId { get; set; }
        int ProjectId { get; set; }
        string ReviewDate { get; set; }
        string ReviewDateF { get; set; }
        DateTime dtReviewDate { get; set; }
        string ReviewRemarks { get; set; }
        int Acceptability { get; set; }
        int ReviewerId { get; set; }
        int LastModifiedBy { get; set; }
        DateTime dtLastModifiedOn { get; set; }
        string ReviewerName { get; set; }
        int ObservationId { get; set; }
        string Observation { get; set; }
        string SLId { get; set; }
        string FollowUpPlan { get; set; }
        int ResponsibilityID { get; set; }
        string Responsibility { get; set; }
        string PlanDate { get; set; }
        string PlanDateF { get; set; }
        string Status { get; set; }
        string ReviewerRemarks { get; set; }
        int AgendaId { get; set; }
        string AgendaPoint { get; set; }
        int SLID { get; set; }
    }
    public class CodeReview : ICodeReview
    {
        public int ReviewId { get; set; }
        public int ProjectId { get; set; }
        public string ReviewDate { get; set; }
        public string ReviewDateF { get; set; }
        public DateTime dtReviewDate { get; set; }
        public string ReviewRemarks { get; set; }
        public int Acceptability { get; set; }
        public int ReviewerId { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime dtLastModifiedOn { get; set; }
        public string ReviewerName { get; set; }
        public int ObservationId { get; set; }
        public string Observation { get; set; }
        public string SLId { get; set; }
        public string FollowUpPlan { get; set; }
        public int ResponsibilityID { get; set; }
        public string Responsibility { get; set; }
        public string PlanDate { get; set; }
        public string PlanDateF { get; set; }
        public string Status { get; set; }
        public string ReviewerRemarks { get; set; }
        public int AgendaId { get; set; }
        public string AgendaPoint { get; set; }
        public int SLID { get; set; }
    }
}
