﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface IProjectModule
    {
        int ModuleId { get; set; }
        int ProjectId { get; set; }
        string ModuleName { get; set; }
        int ModuleStatusId { get; set; }
        string ModuleStatus { get; set; }
        int HideStatusId { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        DateTime StartDateF { get; set; }
        DateTime EndDateF { get; set; }
        int AssignedToId { get; set; }
        string AssignedToName { get; set; }
        int AssignedBy { get; set; }
        string AssignedOn { get; set; }
        string LastModifiedOn { get; set; }
        int LastModifiedBy { get; set; }
        decimal dEstimateHours { get; set; }
        string SortCriteria { get; set; }
        string SortDir { get; set; }
        int ModuleHideStatusId { get; set; }
    }
    public class ProjectModule : IProjectModule
    {
        public int ModuleId { get; set; }
        public int ProjectId { get; set; }
        public string ModuleName { get; set; }
        public int ModuleStatusId { get; set; }
        public string ModuleStatus { get; set; }
        public int HideStatusId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDateF { get; set; }
        public DateTime EndDateF { get; set; }
        public int AssignedToId { get; set; }
        public string AssignedToName { get; set; }
        public int AssignedBy { get; set; }
        public string AssignedOn { get; set; }
        public string LastModifiedOn { get; set; }
        public int LastModifiedBy { get; set; }
        public decimal dEstimateHours { get; set; }
        public string SortCriteria { get; set; }
        public string SortDir { get; set; }
        public int ModuleHideStatusId { get; set; }        
    }
}
