﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public interface IProjectModuleTasks
    {
        int TaskId { get; set; }
        int ProjectId { get; set; }
        int ModuleId { get; set; }
        int AssignedTo { get; set; }
        string TaskDesc { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        int TaskStatusId { get; set; }
        int AssignedBy { get; set; }
        DateTime AssignedOn { get; set; }
        int LastModifiedBy { get; set; }
        DateTime LastModifiedOn { get; set; }
        decimal EstimateHours { get; set; }
        int QAAssigned { get; set; }
        decimal EstimateQaHours { get; set; }
        string MajorIssues { get; set; }
        string MinorIssues { get; set; }

        string Assigned { get;set; }
        string Closed { get; set; }
        string Completed { get; set; }
        string Maintenance { get; set; }
        string New { get; set; }
        string PipeLine { get; set; }
        string InProcess { get; set; }
        string QAed { get; set; }
        string SignOff { get; set; }

        int HideTaskStatusId { get; set; }
    }
    public class ProjectModuleTasks : IProjectModuleTasks
    {
        public int TaskId { get; set; }
        public int ProjectId { get; set; }
        public int ModuleId { get; set; }
        public int AssignedTo { get; set; }
        public string TaskDesc { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int TaskStatusId { get; set; }
        public int AssignedBy { get; set; }
        public DateTime AssignedOn { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public decimal EstimateHours { get; set; }
        public int QAAssigned { get; set; }
        public decimal EstimateQaHours { get; set; }
        public string MajorIssues { get; set; }
        public string MinorIssues { get; set; }

        public string Assigned { get; set; }
        public string Closed { get; set; }
        public string Completed { get; set; }
        public string Maintenance { get; set; }
        public string New { get; set; }
        public string PipeLine { get; set; }
        public string InProcess { get; set; }
        public string QAed { get; set; }
        public string SignOff { get; set; }

        public int HideTaskStatusId { get; set; }
    }
}
