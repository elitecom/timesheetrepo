﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using Entities;

namespace DrishProject.ProjectNotes
{
    public partial class Notes : System.Web.UI.Page
    {
        int ProjectID;
        Entities.ProjectNotes objNotes = new Entities.ProjectNotes();
        Entities.Project objProject = new Entities.Project();
        Entities.Employee objUser = new Entities.Employee();

        const int EditColIndex = 5;

        protected void Page_Load(object sender, EventArgs e)
        {
            objUser = (Entities.Employee)Session["emp"];

            #region "Unused Code"
            if (objUser.IsAministrator == 0)
            {
                //if (User.IsInRole("AddNotes"))
                //{
                //    RegisterStartupScript("strShow", "<script language=\'javascript\'>this.document.all.imgAddNotes.style.display=\'block\';</script>");
                //}
                //else
                //{
                //    RegisterStartupScript("strHide", "<script language=\'javascript\'>this.document.all.imgAddNotes.style.display=\'none\';</script>");
                //}
            }
            #endregion

            ProjectID = Convert.ToInt32(Session["ProjectID"]);
            //Code to hide the Release, CheckList, Client and Code Review Tabs for Non Engineering Projects.
            objProject.ProjectID = ProjectID;
            objProject = new ProjectBAL().GetProject(ProjectID);
            lblProjectCode.Text = objProject.ProjectCode;
            if (!Page.IsPostBack)
            {
                try
                {
                    if (objUser.IsAministrator == 0)
                    {
                        if (User.IsInRole("EditNotes"))
                        {
                            dgNotes.Columns[EditColIndex].Visible = true;
                        }
                        else
                        {
                            dgNotes.Columns[EditColIndex].Visible = false;
                        }
                    }
                    else
                    {
                        dgNotes.Columns[EditColIndex].Visible = true;
                    }            
                    GetNotesPerProject();
                    }
                catch (Exception ex)
                {
                    LblException.Text = ex.Message;
                }
            }
        }

        protected void dgNotes_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Header)
            {
                RegisterStartupScript("Declare", "<script language=\'javascript\'>var realColor;</script>");
                e.Item.Attributes.Add("OnMouseOver", "realColor = this.style.backgroundColor; this.style.backgroundColor = \'#cccccc\';");
                e.Item.Attributes.Add("OnMouseOut", "this.style.backgroundColor = realColor;");
            }            
        }

        protected void dgNotes_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgNotes.CurrentPageIndex = e.NewPageIndex;
            GetNotesPerProject();
        }

        protected void dgNotes_SortCommand(object source, DataGridSortCommandEventArgs e)
        {

        }

        private void GetNotesPerProject()
        {
            try
            {
                DataSet DS = new DataSet();
                objNotes.ProjectId = ProjectID;
                //objNotes.SortCriteria = this.SortCriteria;
                //objNotes.SortDir = this.SortDir;
                DS = new ProjectNotesBAL().GetNotesPerProject(objNotes);
                dgNotes.DataSource = DS;
                //Code to check whether the CurrentPageIndex value is not less than 0
                if (DS.Tables[0].Rows.Count > 0)
                {
                    if (Math.Ceiling(double.Parse(DS.Tables[0].Rows.Count.ToString()) / dgNotes.PageSize) - 1 < dgNotes.PageSize)
                    {
                        dgNotes.CurrentPageIndex = 0;
                    }
                }
                else
                {
                    dgNotes.CurrentPageIndex = 0;
                }
                dgNotes.DataBind();
                if (DS.Tables[0].Rows.Count == 0)
                {
                    dgNotes.PagerStyle.Visible = false;
                    LblException.Text = "No Notes Present.";
                }
                else
                {
                    dgNotes.PagerStyle.Visible = true;
                    LblException.Text = "";
                }
                if (DS.Tables[0].Rows.Count <= dgNotes.PageSize)
                {
                    dgNotes.PagerStyle.Visible = false;
                }
                else
                {
                    dgNotes.PagerStyle.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            GetNotesPerProject();
        }
    }
}