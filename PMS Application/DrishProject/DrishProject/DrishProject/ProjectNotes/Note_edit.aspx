﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Note_edit.aspx.cs" Inherits="DrishProject.ProjectNotes.Note_edit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript">
        function EnterVal(event) {
            if (window.event.keyCode == 13) {
                event.returnValue = false;
                event.cancel = true;
                document.getElementById('imgBtnSave').click();
            }
        }
    </script>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="FrmEditModule" method="post" runat="server">
    <table id="MAINTABLE" height="90%" width="100%">
        <tr>
            <td width="100%" height="1%">
            </td>
        </tr>
        <tr>
            <td width="100%">
                <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                    align="center" border="0">
                    <tr height="100%">
                        <td>
                        </td>
                        <td align="left" valign="top" width="100%">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td>
                                        <img id="imgTop" src="../Images/add_note_tab.gif" border="0">
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr class="center-bg">
                                        <td valign="top" align="left" width="96%" style="height: 1px">
                                            <img height="10" src="../Images/spacer.gif" width="10">
                                        </td>
                                        <td valign="top" align="right" width="3%" style="height: 1px">
                                            <img height="10" src="../Images/right_top_corner.gif" width="10">
                                        </td>
                                    </tr>
                                    <tr class="center-bg" valign="top" align="left">
                                        <td valign="top" height="100%" width="100%" colspan="2">
                                            <table cellspacing="0" cellpadding="2" width="100%" align="left" border="0">
                                                <tbody>
                                                    <tr valign="middle" height="21">
                                                        <td class="Errortxt" width="100%" valign="top" colspan="4">
                                                            <asp:Label CssClass="Errortxt" ID="LblException" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td class="Gridtxt" width="1%">
                                                        </td>
                                                        <td class="Labeltxt" valign="top">
                                                            Project Code
                                                        </td>
                                                        <td class="Gridtxt" width="1%">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt">
                                                            <asp:Label CssClass="Gridtxt" ID="lblProjectCode" runat="server" Width="200"></asp:Label>
                                                            <asp:Label CssClass="Gridtxt" ID="lblNoteId" runat="server" Width="0px" Visible="False"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td class="Mendatoryfld" valign="top">
                                                            *
                                                        </td>
                                                        <td class="Labeltxt" valign="top">
                                                            Note Type
                                                        </td>
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt">
                                                            <asp:DropDownList CssClass="Gridtxt" ID="ddlNoteType" runat="server" Width="275">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td>
                                                        </td>
                                                        <td class="Labeltxt" valign="top">
                                                            Reference
                                                        </td>
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt" valign="top">
                                                            <asp:TextBox CssClass="Gridtxt" ID="txtReference" runat="server" Width="275" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td class="Mendatoryfld" valign="top">
                                                            *
                                                        </td>
                                                        <td class="Labeltxt" valign="top">
                                                            Note
                                                        </td>
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt" valign="top">
                                                            <asp:TextBox CssClass="Gridtxt" ID="txtNote" runat="server" Width="275" TextMode="MultiLine"
                                                                Rows="5" MaxLength="4000"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr class="Gridtxt">
                                                        <td height="20">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt" valign="top">
                                                            <asp:ImageButton ID="imgBtnSave" ImageUrl="../Images/save_bt.gif" runat="server" OnClick="imgBtnSave_Click">
                                                            </asp:ImageButton>&nbsp;&nbsp;<img style="cursor: pointer" onclick="window.close();"
                                                                src="../Images/cancel_bt.gif">&nbsp;&nbsp;<asp:ImageButton ID="imgAddNew" ImageUrl="../Images/add_btn.gif"
                                                                    runat="server" OnClick="imgAddNew_Click"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="center-bg">
                                        <td valign="top" align="left">
                                            <img height="10" src="../Images/spacer.gif" width="10">
                                        </td>
                                        <td valign="bottom" align="right">
                                            <img height="10" src="../Images/right_bottom_corner.gif" width="10">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr class="Gridtxt">
                        <td colspan="3" height="3%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
