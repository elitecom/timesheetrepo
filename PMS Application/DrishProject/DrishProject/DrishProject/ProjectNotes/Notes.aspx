﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="Notes.aspx.cs" Inherits="DrishProject.ProjectNotes.Notes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript">
        function NoteAddFunction() {
            var leftPos = (screen.width / 2) - 225;
            var topPos = (screen.height / 2) - 135;
            window.open("Note_edit.aspx?NoteId=0", "NoteAdd", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=450, height=270");
        }
        function NoteEditFunction(ID) {
            var leftPos = (screen.width / 2) - 225;
            var topPos = (screen.height / 2) - 135;
            window.open("Note_edit.aspx?NoteId=" + ID, "ModuleEdit", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=450, height=270");
        }

        function PageRefresh() {
            __doPostBack('btnRefresh', '');
        }
    </script>
    <div style="width: 1030px">
        <table id="tblMain" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tbody>
                <tr height="10%">
                    <td valign="top" width="100%">
                        <!-- HEADER -->
                        <table id="tblHeader" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td valign="top" align="left">
                                    <!---- Header --->
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr height="80%">
                    <td valign="top" width="100%">
                        <!-- CENTER -->
                        <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                            align="center" border="0">
                            <tr>
                                <td width="1%">
                                </td>
                                <td width="98%" height="100%" align="left" valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" align="top" border="0">
                                                    <tr>
                                                        <td>
                                                            <a href="Projects.aspx">
                                                                <img src="../Images/details_new.gif" border="0"></a>
                                                        </td>
                                                        <td>
                                                            <a href="TeamMember.aspx">
                                                                <img src="../Images/tabs_bt_02.gif" border="0"></a>
                                                        </td>
                                                        <td>
                                                            <a href="Module.aspx">
                                                                <img src="../Images/tabs_bt_06.gif" border="0"></a>
                                                        </td>
                                                        <td>
                                                            <a href="Tasks.aspx">
                                                                <img src="../Images/tasks.gif" border="0"></a>
                                                        </td>
                                                        <td>
                                                            <a href="Documents.aspx">
                                                                <img src="../Images/tabs_bt_05.gif" border="0"></a>
                                                        </td>
                                                        
                                                        <td id="tdClients" runat="Server">
                                                            <a href="Clients.aspx">
                                                                <img src="../Images/tabs_bt_03.gif" border="0"></a>
                                                        </td>                                                        
                                                        <td>
                                                            <a href="Meetings.aspx">
                                                                <img src="../Images/tabs_bt_10.gif" border="0"></a>
                                                        </td>
                                                        <td>
                                                            <img src="../Images/notes_active.gif">
                                                        </td>
                                                        <td align="left">
                                                            <a href="Project_History.aspx">
                                                                <img src="../Images/tabs_bt_07.gif" border="0"></a>
                                                        </td>
                                                        <td id="tdCodeReview" runat="Server">
                                                            <a href="CodeReviews.aspx">
                                                                <img src="../Images/review_active.gif" border="0"></a>
                                                        </td>
                                                        <td id="tdPTimeSheet" runat="server">
                                                            <a href="ProjectTimeSheet.aspx">
                                                                <img src="../Images/timesheet_tab_active.gif" border="0" /></a>
                                                        </td>
                                                        <%--<td id="tdRisk" runat="server" style="height: 20px"><A href="ProjectRisk.aspx"><IMG src="../Images/risk_active.gif" border="0"></A></td>--%>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table height="400px" cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
                                                    <tbody>
                                                        <tr class="center-bg" valign="top">
                                                            <td align="left" colspan="2">
                                                                <table height="38" width="100%" border="0">
                                                                    <tr>
                                                                        <td colspan="4">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="Gridtxt" align="left" width="50%">
                                                                            &nbsp;&nbsp;<b>Project&nbsp;Code&nbsp;:&nbsp;</b>
                                                                            <asp:Label ID="lblProjectCode" CssClass="Gridtxt" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td class="Gridtxt" align="right" width="49.5%">
                                                                            <asp:Button ID="btnRefresh" CssClass="gridtxt" runat="server" Width="0px" Visible="false"
                                                                                OnClick="btnRefresh_Click"></asp:Button>
                                                                            <asp:DropDownList CssClass="Gridtxt" ID="DropDownList1" runat="server" Width="0px"
                                                                                AutoPostBack="True">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td class="Gridtxt">
                                                                            <img id="imgAddNotes" style="cursor: pointer" onclick="NoteAddFunction();" src="../Images/add_new_notes_bt.gif"
                                                                                border="0">
                                                                        </td>
                                                                        <td class="Gridtxt" width=".5%">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr class="center-bg" valign="top">
                                                            <td valign="top" class="GridBG" height="100%" width="98%">
                                                                <asp:DataGrid ID="dgNotes" runat="server" Width="100%" AllowPaging="True" AutoGenerateColumns="False"
                                                                    BackColor="#F7F7F7" BorderColor="White" DataKeyField="noteid" CellPadding="3"
                                                                    GridLines="Vertical" BorderStyle="None" AllowSorting="True" OnItemDataBound="dgNotes_ItemDataBound"
                                                                    OnPageIndexChanged="dgNotes_PageIndexChanged" OnSortCommand="dgNotes_SortCommand">
                                                                    <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                                    <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                                    <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                                    <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                                    <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="NoteType" SortExpression="NoteType" ReadOnly="True" HeaderText="Note Type">
                                                                            <HeaderStyle Width="15%"></HeaderStyle>
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Note" SortExpression="Note" ReadOnly="True" HeaderText="Note">
                                                                            <HeaderStyle Width="40%"></HeaderStyle>
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="reference" SortExpression="reference" ReadOnly="True"
                                                                            HeaderText="Reference">
                                                                            <HeaderStyle Width="20px"></HeaderStyle>
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Datepostedon" SortExpression="postedon" HeaderText="Posted On">
                                                                            <HeaderStyle Width="10%"></HeaderStyle>
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="DateLastModifiedOn" SortExpression="LastModifiedOn" HeaderText="Modified On">
                                                                            <HeaderStyle Width="10%"></HeaderStyle>
                                                                        </asp:BoundColumn>
                                                                        <asp:TemplateColumn HeaderText="Edit">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <img id="imageEdit" align="middle" src="../Images/pen.gif" onclick='NoteEditFunction(<%# Eval("Noteid")%>)' style="CURSOR: hand" alt="">
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                    <PagerStyle NextPageText="&lt;img src=Images/next_bt.gif border=0&gt;" PrevPageText="&lt;img src=Images/prev_bt.gif border=0&gt;"
                                                                        CssClass="Pagertxt" Mode="NumericPages"></PagerStyle>
                                                                </asp:DataGrid>
                                                                <asp:Label CssClass="Errortxt" ID="LblException" runat="server"></asp:Label>
                                                                <div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <img height="10" src="../Images/spacer.gif" width="1">
                                                            </td>
                                                        </tr>
                                                        <tr class="center-bg" height="10%">
                                                            <td class="Gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt">
                                                            </td>
                                                        </tr>
                                                        <tr class="center-bg">
                                                            <td valign="top" align="left">
                                                                <img height="10" src="../Images/spacer.gif" width="10">
                                                            </td>
                                                            <td valign="bottom" align="right">
                                                                <img height="10" src="../Images/right_bottom_corner.gif" width="10">
                                                            </td>
                                                        </tr>
                                                        <tr class="Gridtxt">
                                                            <td colspan="2" height="3%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="1%">
                                    <img height="10" src="../Images/spacer.gif" width="10">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!--FOOTER -->
                <tr>
                    <td width="100%">
                        <table id="tblFooter" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td valign="top" align="left">
                                    <!--     Footer    --->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
</asp:Content>
