﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using Entities;

namespace DrishProject.ProjectNotes
{
    public partial class Note_edit : System.Web.UI.Page
    {
        int intProjectId;
        int intNoteId;
        Entities.Project objProject = new Entities.Project();
        Entities.ProjectNotes objNotes = new Entities.ProjectNotes();

        protected void Page_Load(object sender, EventArgs e)
        {
            intProjectId = Convert.ToInt32(Session["ProjectID"]);
            objProject.ProjectID = intProjectId;
            objProject = new ProjectBAL().GetProject(intProjectId);
            lblProjectCode.Text = objProject.ProjectCode;
            intNoteId = Convert.ToInt32(Request.QueryString["NoteId"]);
            if (intNoteId != 0)
            {
                if (new ProjectNotesBAL().IfAccessible(intNoteId, intProjectId, "Note") == false)
                {
                    Response.Redirect("Default.aspx?Authority=Deny");
                }
            }

            if (!Page.IsPostBack)
            {
                lblNoteId.Text = intNoteId.ToString();
                GetNoteTypes();
                if (intNoteId > 0)
                {
                    imgAddNew.Visible = false;
                    GetNoteInfo(intNoteId);
                }
                //GetTopImage();
                ddlNoteType.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                txtReference.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                SetFocus(ddlNoteType);
            }		
        }

        private void GetNoteTypes()
        {
            try
            {
                DataSet DS = new DataSet();
                DS = new ProjectNotesBAL().GetNoteTypes();
                DataRow dRow;
                dRow = DS.Tables[0].NewRow();
                dRow["notetypeid"] = 0;
                dRow["notetype"] = "--Select--";
                DS.Tables[0].Rows.InsertAt(dRow, 0);
                ddlNoteType.DataSource = DS;
                ddlNoteType.DataTextField = "NoteType";
                ddlNoteType.DataValueField = "NoteTypeId";
                ddlNoteType.DataBind();
                //ddlNoteType.SelectedValue = 0;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private void GetNoteInfo(int intNoteId)
        {
            int i;
            intNoteId = Convert.ToInt32(lblNoteId.Text);
            if (intNoteId > 0)
            {
                objNotes.NoteId = intNoteId;
                objNotes = new ProjectNotesBAL().GetNote(objNotes);
                txtNote.Text = objNotes.Note;
                for (i = 0; i <= ddlNoteType.Items.Count - 1; i++)
                {
                    ddlNoteType.SelectedIndex = i;
                    if (Convert.ToInt32(ddlNoteType.SelectedItem.Value) == objNotes.NoteTypeId)
                    {
                        break;
                    }
                }
                txtReference.Text = objNotes.Reference;
            }
        }

        protected void imgBtnSave_Click(object sender, ImageClickEventArgs e)
        {
            string strNote = "";
            int intNoteTypeId = 0;
            string strReference = "";

            int ModifiedBy = 0;
            DateTime ModifiedOn = System.DateTime.Now;
            int intPostedBy = 0;
            DateTime dtPostedOn = System.DateTime.Now;
            string strRetVal = "";
            intNoteId = System.Convert.ToInt32(lblNoteId.Text);
            Entities.Employee objUser = new Entities.Employee();
            objUser = (Entities.Employee)Session["emp"];

            if (Convert.ToInt32(ddlNoteType.SelectedItem.Value) == 0)
            {
                LblException.Text = "Note type required !";
                SetFocus(ddlNoteType);
                return;
            }
            else
            {
                LblException.Text = "";
            }

            if (txtNote.Text == "")
            {
                LblException.Text = "Note description required !";
                SetFocus(txtNote);
                return;
            }
            else
            {
                if (txtNote.Text.Length > 4000)
                {
                    LblException.Text = "Note description can not be more than 4000 characters !";
                    SetFocus(txtNote);
                    return;
                }
                LblException.Text = "";
            }
            strNote = txtNote.Text.TrimStart();
            if ((txtReference.Text) != "")
            {
                strReference = txtReference.Text.TrimStart();
            }
            if (ddlNoteType.SelectedValue != "0")
            {
                intNoteTypeId = Convert.ToInt32(ddlNoteType.SelectedItem.Value);
            }
            if (intNoteId > 0)
            {                
                ModifiedBy = objUser.EmployeeId;
                ModifiedOn = DateTime.Now;

                //update module                
                objNotes.ProjectId = intProjectId;
                objNotes.NoteId = intNoteId;
                objNotes.NoteTypeId = intNoteTypeId;
                objNotes.Note = strNote;
                objNotes.Reference = strReference;
                objNotes.LastModifiedBy = ModifiedBy;
                objNotes.LastModifiedOn = ModifiedOn;
                bool res;
                res = new ProjectNotesBAL().UpdateNote(objNotes);
                if (res == true)
                {
                    strRetVal = "Note Modified Successfully";
                }
                //GetTopImage();
            }
            else
            {                
                intPostedBy = objUser.EmployeeId;
                dtPostedOn = DateTime.Now;

                objNotes.ProjectId = intProjectId;
                objNotes.NoteId = intNoteId;
                objNotes.NoteTypeId = intNoteTypeId;
                objNotes.Note = strNote;
                objNotes.Reference = strReference;
                objNotes.PostedBy = intPostedBy;
                objNotes.PostedOn = dtPostedOn;
                bool res;
                res = new ProjectNotesBAL().InsertNote(objNotes);
                if (res == true)
                {
                    strRetVal = "Note Added Successfully";
                    lblNoteId.Text = strRetVal;                    
                }
                //GetTopImage();
            }
            LblException.Text = strRetVal;
            RegisterStartupScript("strRefresh", "<script language=\'javascript\'>window.opener.PageRefresh();</script>");
        }

        protected void imgAddNew_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Note_edit.aspx?NoteId=0");
        }   
    }
}