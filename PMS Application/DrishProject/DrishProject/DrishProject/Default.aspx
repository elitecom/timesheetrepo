﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DrishProject.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Elite Web Technologies - PMS Control Tool</title>
    <style type="text/css">
        .WhiteLabeltxt
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #ffffff;
            font-weight: bold;
            text-decoration: none;
        }
        .Errortxt
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: red;
            text-decoration: none;
        }
        .Labeltxt
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000000;
            font-weight: bold;
            text-decoration: none;
        }
        .Gridtxt
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000000;
            text-decoration: none;
        }
        .grytxt
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10pt;
            color: #666666;
            text-decoration: none;
        }
        
        .style1
        {
            height: 500px;
        }
        
        .style2
        {
            color: #FF9966;
            font-weight: bold;
        }
    </style>
    <script language="javascript" type="text/javascript">
    <!--
        function MaximizeWindow() {
            document.getElementById('txtUserName').focus();
            window.resizeTo(screen.width, screen.height);
            window.moveTo(0, 0);
        }
        setInterval("window.status='Drish-PMS -  Press CTRL+ D to bookmark this site !'", 10000);
    -->
    </script>
    <script src="Scripts/jquery-2.1.3.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#ImageButton1').click(function () {
                var username, password;
                var ErrMsg = '';
                username = $("#TextBox1").val();
                password = $("#TextBox2").val();
                if (username == '') {
                    ErrMsg += 'Must Enter User Name\n';
                }
                if (password == '') {
                    ErrMsg += 'Must Enter User Password';
                }
                if (ErrMsg != '') {
                    alert(ErrMsg);
                }
            });
        });
    </script>
</head>
<body leftmargin="0" topmargin="0" onload="MaximizeWindow();">
    <form id="frmLogin" method="post" runat="server">
    <table id="Default" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <!-- HEADER -->
    </table>
    <table id="tblHeader" height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td rowspan="2">
                <asp:ImageButton ID="imgLogo" runat="server" ImageUrl="~/Images/logo.png" Style="width: 171px;
                    height: 80px" />
            </td>
            <td width="100%">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <th valign="top" scope="row" align="left" width="1%">
                            <img height="46" src="images/index_02.gif" width="9">
                        </th>
                        <td align="right" class="style2" valign="bottom">
                            Elite Web Technologies - Project Management System&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="topbar" valign="top" align="left" width="100%">
                <table cellspacing="0" cellpadding="0" width="100%" border="0" style="margin-top: 0px"
                    bgcolor="#FF9900">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <div class="admin" align="right">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- CENTRAL -->
    <table id="tblCentral" style="background-position: center center; background-attachment: fixed;
        background-image: url('Images/login_bg1.gif'); background-repeat: no-repeat;
        height: 436px;" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr valign="middle" align="center">
            <td class="style1">
                <table id="tblCenter" style="border-right: #d96526 thin solid; border-top: #d96526 thin solid;
                    border-left: #d96526 thin solid; border-bottom: #d96526 thin solid" cellspacing="1"
                    cellpadding="2">
                    <tr>
                        <td bgcolor="#d96526" colspan="2" class="WhiteLabeltxt">
                            &nbsp;Login Here
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="Errortxt">
                            &nbsp;<asp:Label CssClass="Errortxt" ID="LblMessage" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ff9966" class="Labeltxt">
                            Username
                        </td>
                        <td valign="top" bgcolor="#ffcc66" class="Gridtxt">
                            &nbsp;<asp:TextBox CssClass="Gridtxt" ID="TextBox1" runat="server" MaxLength="15"></asp:TextBox><asp:RequiredFieldValidator
                                ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ff9966" class="Labeltxt">
                            Password
                        </td>
                        <td bgcolor="#ffcc66" class="Gridtxt">
                            &nbsp;<asp:TextBox CssClass="Gridtxt" ID="TextBox2" runat="server" MaxLength="15"
                                TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                    runat="server" ControlToValidate="TextBox2" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ff9966" class="Labeltxt">
                            Save Login
                        </td>
                        <td bgcolor="#ffcc66" class="Gridtxt">
                            &nbsp;<asp:CheckBox CssClass="Gridtxt" ID="chkSaveLogin" runat="server"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="Gridtxt">
                            &nbsp;
                        </td>
                        <td class="Gridtxt">
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="images/login_bt.gif"
                                OnClick="ImageButton1_Click" Style="height: 19px" />
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="grytxt" align="center" colspan="2" bgcolor="#FF9966">
                            <asp:HyperLink ID="hlAdmin" runat="server">Contact Admin for New Account</asp:HyperLink>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
      </table>
      <table width ="100%">
        <tr>
            <td align="center">
                <p class="grytxt">Version&nbsp;<asp:Label id="lblVersion" runat="server"></asp:Label>
													&nbsp;© Elite Web Technologies.</p>
            </td>
        </tr>
      </table>
    </form>
</body>
</html>
