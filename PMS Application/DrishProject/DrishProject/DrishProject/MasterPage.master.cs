﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                Entities.Employee emp = new Entities.Employee();
                emp = (Entities.Employee)(Session["emp"]);
                ((Label)this.FindControl("Label1")).Text = "Welcome, " + emp.FirstName + ' ' + emp.LastName;
            }
            catch (Exception)
            { }
        }
    }

    protected void ImgSignOut_Click(object sender, ImageClickEventArgs e)
    {
        Session.Abandon();
        Response.Redirect("~/Default.aspx");
    }
    protected void imgProjects_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/Projects/ProjectSearch.aspx");
    }
    protected void imgTasks_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/Tasks/MyTasks.aspx");
    }
    protected void imgEmployees_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/Employee/EmployeeSearch.aspx");
    }
    protected void imgClients_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/Clients/Clients.aspx");
    }
    protected void imgTools_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/Tools/Tools.aspx");
    }
    protected void ImgMyProfile_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/Employee/MyProfile.aspx");
    }
    protected void imgTimeSheet_Click(object sender, ImageClickEventArgs e)
    {
        //Response.Redirect("~/TimeSheets/TimeSheet.aspx");
        Entities.Employee emp = new Entities.Employee();
        emp = (Entities.Employee)(Session["emp"]);
        if (emp.Designation == "Project Manager")
        {
            Response.Redirect("~/TimeSheets/TimeSheet.aspx");
        }
        else
        {
            Response.Redirect("~/DSR/MyDSRNew.aspx");
        }
    }
    protected void imgForum_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/Forum/Forum.aspx");
    }
}
