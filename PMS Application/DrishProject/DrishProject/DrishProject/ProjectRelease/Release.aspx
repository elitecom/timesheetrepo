﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="Release.aspx.cs" Inherits="DrishProject.Release.Releases" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table id="tblMain" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tbody>
            <tr height="10%">
                <td valign="top" width="100%">
                    <!-- HEADER -->
                    <table id="tblHeader" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="top" align="left">
                                <!---- Header --->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="80%">
                <td valign="top" width="100%">
                    <!-- CENTER -->
                    <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                        align="center" border="0">
                        <tr>
                            <td width="1%">
                            </td>
                            <td valign="top" align="left">
                                <table cellspacing="0" cellpadding="0" align="top" border="0">
                                    <tr>
                                        <td>
                                            <a href="../Release/Projects.aspx">
                                                <img src="../Images/details_new.gif" border="0"></a>
                                        </td>
                                        <td>
                                            <a href="../Release/Team_Members.aspx">
                                                <img src="../images/tabs_bt_02.gif" border="0"></a>
                                        </td>
                                        <td>
                                            <a href="../Release/Modules.aspx">
                                                <img src="../images/tabs_bt_06.gif" border="0"></a>
                                        </td>
                                        <td>
                                            <a href="../Release/Tasks.aspx">
                                                <img src="../images/tasks.gif" border="0"></a>
                                        </td>
                                        <td>
                                            <a href="../Release/Documents.aspx">
                                                <img src="../images/tabs_bt_05.gif" border="0"></a>
                                        </td>                                       
                                        <td id="tdClients" runat="Server">
                                            <a href="../Release/Clients.aspx">
                                                <img src="../images/tabs_bt_03.gif" border="0"></a>
                                        </td>
                                       
                                        <td>
                                            <a href="../Release/Meetings.aspx">
                                                <img src="../images/tabs_bt_10.gif" border="0"></a>
                                        </td>
                                        <td>
                                            <a href="../Release/ProjectNotes.aspx">
                                                <img src="../images/tabs_bt_09.gif" border="0"></a>
                                        </td>
                                        <td align="left">
                                            <a href="../Release/Project_History.aspx">
                                                <img src="../images/tabs_bt_07.gif" border="0"></a>
                                        </td>
                                       
                                        <td>
                                            <img src="../images/Releases_active.gif"></td>
                                        <td>
                                            <img src="../Images/review_active.gif"></td>
                                        <td>
                                            <img src="../Images/timesheet_tab_active.gif"></td>
                                    </tr>
                                </table>
                                <table height="98%" cellspacing="0" cellpadding="0" width="117%" align="left" border="0">
                                    <tbody>
                                        <tr class="center-bg" valign="top" align="left">
                                            <td align="right" colspan="2">
                                                <table height="38" width="100%" border="0">
                                                    <tr>
                                                        <td class="gridtxt" align="left" width="30%">
                                                            &nbsp;&nbsp; <strong>Project&nbsp;Code&nbsp;:&nbsp;</strong><asp:Label CssClass="gridtxt"
                                                                ID="lblProjectCode" runat="server"></asp:Label>
                                                            <asp:Button ID="btnRefresh" runat="server"
                                                                    Width="0px" onclick="btnRefresh_Click"></asp:Button><asp:DropDownList ID="DropDownList1" runat="server" Width="0px"
                                                                        AutoPostBack="True">
                                                                    </asp:DropDownList>
                                                        </td>
                                                        <td class="gridtxt" align="right" width="65%">
                                                            <img id="imgAddRelease" style="cursor: hand" onclick="ReleaseAddFunction();" src="../Images/add_new_release_bt.gif"
                                                                border="0">
                                                        </td>
                                                        <td class="gridtxt" align="right" width="14.5%">
                                                            &nbsp;&nbsp;<asp:HyperLink runat="server" ID="imgSurveyRelease" ImageUrl="~/Images/survey_bt.gif"></asp:HyperLink>
                                                        </td>
                                                        <td class="gridtxt" width="0.5%">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="center-bg" valign="top">
                                            <td class="GridBG" height="100%" width="100%">
                                                <asp:DataGrid ID="dgReleases" runat="server" Width="100%" BorderStyle="None" GridLines="Vertical"
                                                    CellPadding="3" DataKeyField="releaseid" BorderColor="White" BackColor="#F7F7F7"
                                                    AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" 
                                                    onitemdatabound="dgReleases_ItemDataBound" 
                                                    onpageindexchanged="dgReleases_PageIndexChanged" 
                                                    onsortcommand="dgReleases_SortCommand">
                                                    <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                    <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                    <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                    <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                    <Columns>
                                                        <asp:BoundColumn DataField="Description" SortExpression="description" HeaderText="Description">
                                                            <HeaderStyle Width="20%"></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ReleaseDateFormated" SortExpression="releasedate" ReadOnly="True"
                                                            HeaderText="Release Date">
                                                            <HeaderStyle Width="13%"></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="releasestatus" SortExpression="releasestatus" HeaderText="Status">
                                                            <HeaderStyle Width="10%"></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Deliverable" SortExpression="deliverable" ReadOnly="True"
                                                            HeaderText="Deliverables">
                                                            <HeaderStyle Width="20%"></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ReleaseTarget" SortExpression="ReleaseTarget" HeaderText="Release Target">
                                                            <HeaderStyle Width="20%"></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="DeliveryIssues" SortExpression="DeliveryIssues" HeaderText="Delivery Issues">
                                                            <HeaderStyle Width="18%"></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:TemplateColumn HeaderText="Edit">
                                                            <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <img id="imageEdit" align="middle" src="../images/pen.gif" onclick='ReleaseEditFunction(<%# Eval("Releaseid")%>)'
                                                                    style="cursor: hand">
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Features">
                                                            <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <img id="imageFeatures" align="middle" src="../images/releasefeatures_icon.gif" onclick='ReleaseFeaturesFunction(<%# Eval("Releaseid")%>)'
                                                                    style="cursor: hand">
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <PagerStyle NextPageText="&lt;img src=../Images/next_bt.gif border=0&gt;" PrevPageText="&lt;img src=../Images/prev_bt.gif border=0&gt;"
                                                        CssClass="Pagertxt" Mode="NumericPages"></PagerStyle>
                                                </asp:DataGrid><asp:Label CssClass="Errortxt" ID="LblException" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <img height="10" src="../images/spacer.gif" width="10">
                                            </td>
                                        </tr>
                                        <tr class="gridtxt">
                                            <td colspan="2" height="3%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <img height="10" src="../images/spacer.gif" width="10">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--FOOTER -->
            <tr>
                <td width="100%">
                    <table id="tblFooter" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="top" align="left">
                                <!--     Footer    --->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <script language="javascript" type="text/javascript">
        function ReleaseAddFunction() {
            var leftPos = (screen.width / 2) - 225;
            var topPos = (screen.height / 2) - 200;
            window.open("Release_Edit.aspx?ReleaseId=0", "ReleaseAdd", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=550, height=400");
        }
        function ReleaseEditFunction(ID) {
            var leftPos = (screen.width / 2) - 225;
            var topPos = (screen.height / 2) - 200;
            window.open("Release_Edit.aspx?ReleaseId=" + ID, "ReleaseEdit", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=550, height=400");
        }

        function ReleaseFeaturesFunction(ID) {
            var leftPos = (screen.width / 2) - 450;
            var topPos = (screen.height / 2) - 300;
            //window.navigate("Release_Features.aspx?ReleaseId=" + ID)
            //window.open("Release_features.aspx?ReleaseId=" + ID ,"ReleaseFeatures");

            window.open("Release_features.aspx?ReleaseId=" + ID ,"ReleaseFeatures","toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=625, height=600");
        }

        function PageRefresh() {
            __doPostBack('btnRefresh', '');
        }
    </script>
</asp:Content>
