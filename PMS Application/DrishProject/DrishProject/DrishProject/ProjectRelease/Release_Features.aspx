﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Release_Features.aspx.cs"
    Inherits="DrishProject.ProjectRelease.Release_Features" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function PageRefresh() {
            __doPostBack('btnRefresh', '');
        }
        function Export() {
            var leftPos = (screen.width / 2) - 400
            var topPos = (screen.height / 2) - 250;
            window.open("ExportExcel.aspx?DataSet=Release", "ExportExcel", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=yes, scrollbars=yes resizable=yes, copyhistory=yes, width=800, height=500");
        }
        function TaskAddFunc() {
            var leftPos = (screen.width / 2) - 325;
            var topPos = (screen.height / 2) - 270;
            window.open("Tasks_edit.aspx?TaskId=0&Action=Release&ModuleId=0", "TaskAdd", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=650, height=540");
            //		window.location.href="Tasks_Add.aspx?Action=Release";
        }
        function SelectAllCheckboxes(spanChk) {
            // For click attribute added to the header checkbox
            // Added as ASPX uses SPAN for checkbox
            var oItem = spanChk.children;
            var theBox = (spanChk.type == "checkbox") ? spanChk : spanChk.children.item[0];
            xState = theBox.checked;

            elm = theBox.form.elements;
            for (i = 0; i < elm.length; i++)
                if (elm[i].type == "checkbox" && elm[i].id != theBox.id) {
                    //elm[i].click();
                    if (elm[i].checked != xState)
                    //elm[i].click();
                        elm[i].checked = xState;
                }
        }


        function SetCheckbox(spanChk) {
            // For click attribute added to the header checkbox
            // Added as ASPX uses SPAN for checkbox
            var oItem = spanChk.children;
            var theBox = (spanChk.type == "checkbox") ? spanChk : spanChk.children.item[0];
            elm = theBox.form.elements;
            for (i = 0; i < elm.length; i++)
                if (elm[i].type == "checkbox" && elm[i].id != theBox.id) {
                    //elm[i].click();
                    if (theBox.checked == false) {
                        elm[i].checked = false;
                    }
                    i = i + 1000;
                }
        }

    </script>
    <style type="text/css">
        .style1
        {
            width: 553px;
        }
        .style2
        {
            height: 352px;
            width: 553px;
        }
        .style3
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000000;
            text-decoration: none;
            width: 553px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="tblMain" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tbody>
                <tr height="1%">
                    <td>
                    </td>
                </tr>
                <tr height="80%">
                    <td valign="top" width="100%">
                        <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                            align="center" border="0">
                            <tr>
                                <td width="1%">
                                </td>
                                <td valign="top" align="left">
                                    <table cellspacing="0" cellpadding="0" align="top" border="0">
                                        <tr>
                                            <td>
                                                <a href="Release.aspx">
                                                    <img alt="" src="../Images/tabs_bt_08.gif" border="0" /></a>
                                            </td>
                                            <td>
                                                <img alt="" src="../Images/release_features_tab.gif" border="0">
                                            </td>
                                        </tr>
                                    </table>
                                    <table height="98%" cellspacing="0" cellpadding="0" border="0" valign="top" style="width: 99%">
                                        <tbody>
                                            <tr class="center-bg">
                                                <td valign="top" align="left" style="width: 1024px">
                                                    <img alt="" src="../Images/spacer.gif" width="10" style="height: 9px" />
                                                </td>
                                            </tr>
                                            <tr class="center-bg" valign="top" align="left">
                                                <td style="height: 36px" valign="top" height="36">
                                                    <table cellspacing="0" cellpadding="2" width="100%" align="left" border="0">
                                                        <tbody>
                                                            <tr height="2">
                                                                <td colspan="6" height="2">
                                                                    <img alt="" height="2" src="../Images/spacer.gif" width="2" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Labeltxt" valign="top" width="12%">
                                                                    &nbsp;&nbsp;Release Date :
                                                                </td>
                                                                <td class="Gridtxt" valign="top" align="left" width="12%">
                                                                    <asp:Label ID="lblReleaseDate" runat="server"></asp:Label>
                                                                </td>
                                                                <td class="Labeltxt" valign="top" width="15%">
                                                                    Release Status :
                                                                </td>
                                                                <td class="Gridtxt" valign="top" width="12%">
                                                                    <asp:Label ID="lblReleaseStatus" runat="server"></asp:Label>
                                                                </td>
                                                                <td class="gridtxt" align="right">
                                                                    <img alt="" id="imgAddTask" style="cursor: pointer" onclick="TaskAddFunc();" src="../Images/add_new_task_bt.gif"
                                                                        border="0" />
                                                                </td>
                                                                <td class="gridtxt" width="0.5%">
                                                                    <asp:Button ID="btnRefresh" runat="server" Width="0px"></asp:Button><asp:DropDownList
                                                                        ID="DropDownList1" runat="server" Width="0px" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="center-bg" valign="top">
                                                <td class="GridBG" valign="top" align="left" style="height: 352px;">
                                                    <div style="overflow: auto; width: 100%; height: 290px">
                                                        <asp:DataGrid ID="dgFeatures" runat="server" Width="100%" OnItemDataBound="dgFeatures_ItemDataBound"
                                                            BorderStyle="None" GridLines="Vertical" CellPadding="3" DataKeyField="taskid"
                                                            BorderColor="White" BackColor="#F7F7F7" AutoGenerateColumns="False" AllowSorting="true"
                                                            Height="111px">
                                                            <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                            <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                            <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                            <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                            <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                            <Columns>
                                                                <asp:TemplateColumn>
                                                                    <HeaderStyle Width="3%"></HeaderStyle>
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkAll" runat="server" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:HyperLinkColumn DataNavigateUrlField="ModuleId" DataNavigateUrlFormatString="Tasks.aspx?ModuleId={0}"
                                                                    DataTextField="Tasks" SortExpression="taskdesc" HeaderText="Tasks"></asp:HyperLinkColumn>
                                                                <asp:BoundColumn DataField="modulename" SortExpression="modulename" HeaderText="Module">
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="TaskStatus" SortExpression="modulestatus" HeaderText="Status">
                                                                    <HeaderStyle Width="10%"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="Startdate" SortExpression="startdate" HeaderText="Start Date">
                                                                    <HeaderStyle Width="10%"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="EndDate" SortExpression="enddate" HeaderText="End Date">
                                                                    <HeaderStyle Width="10%" />
                                                                    <%--</HeaderStyle>--%>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="developer" SortExpression="developer" HeaderText="Developer">
                                                                    <HeaderStyle Width="10%"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="estimatehours" SortExpression="A.estimatehours" HeaderText="Dev. Hours">
                                                                    <HeaderStyle Width="8%"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="QA" SortExpression="QA" HeaderText="QA">
                                                                    <HeaderStyle Width="8%"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="estimateqahours" SortExpression="A.estimateqahours" HeaderText="QA Hours">
                                                                    <HeaderStyle Width="8%"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                            </Columns>
                                                        </asp:DataGrid></div>
                                                    <asp:Label ID="LblException" runat="server" Width="50%" CssClass="Errortxt"></asp:Label>
                                                    <div align="right">
                                                        <img id="imgExcel" style="cursor: pointer" onclick="Export()" title="Export to Excel"
                                                            alt="Export to Excel" src="../Images/exp_excel_ico.gif" border="0" />&nbsp;</div>
                                                </td>
                                            </tr>
                                            <tr class="center-bg" height="10%">
                                                <td class="Gridtxt" style="width: 1024px">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr class="Center-bg">
                                                <td class="Gridtxt" valign="top" align="center">
                                                    <asp:ImageButton ID="imgBtnSave" runat="server" ImageUrl="~/Images/save_big_bt.gif"
                                                        Style="height: 19px" OnClick="imgBtnSave_Click"></asp:ImageButton>
                                                </td>
                                            </tr>
                                            <tr class="center-bg">
                                                <td valign="top" align="left" style="width: 1024px">
                                                    <img alt="" height="10" src="../Images/spacer.gif" width="10" />
                                                </td>
                                            </tr>
                                            <tr class="gridtxt">
                                                <td height="3%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="1%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" colspan="3">
                                    <table id="tblFooter" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td valign="top" align="left">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>
