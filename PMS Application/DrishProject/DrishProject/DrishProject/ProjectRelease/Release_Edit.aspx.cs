﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using Entities;

namespace DrishProject.Release
{
    public partial class Release_Edit : System.Web.UI.Page
    {
        int intProjectId = -1;
        int intReleaseId = 0;
        string strProjectCode = string.Empty;
        Entities.Employee objUser = new Entities.Employee();
        Entities.Project objProject = new Entities.Project();
        Entities.ProjectRelease objRelease = new Entities.ProjectRelease();
        DateTime dtOldDate;
        string addedit;
        bool bln_AddEdit;
        DataSet ds = new DataSet();

        protected void Page_Load(object sender, EventArgs e)
        {
            intProjectId = Convert.ToInt32(Session["ProjectID"].ToString());
            objProject.ProjectID = intProjectId;
            objProject = new ProjectBAL().GetProject(intProjectId);
            strProjectCode = objProject.ProjectCode;
            lblProjectCode.Text = strProjectCode;
            if (!Page.IsPostBack)
            {
                intReleaseId = Convert.ToInt32(Request.QueryString["ReleaseId"].ToString());
                addedit = intReleaseId.ToString();
                lblReleaseId.Text = intReleaseId.ToString();
                GetReleaseStatusList();
                if (intReleaseId > 0)
                {
                    ImgAddNew.Visible = false;
                    GetReleaseInfo(intReleaseId);
                }
                GetVisibleTrueFalse(intReleaseId);
            }
        }

        protected void imgBtnSave_Click(object sender, ImageClickEventArgs e)
        {
            string strDeliverable = string.Empty;
            string strDescription = string.Empty;
            string strReleaseTarget = string.Empty;
            string strDeliveryIssues = string.Empty;
            string strReleaseIssues = string.Empty;
            DateTime dtReleaseDate = Convert.ToDateTime("1/1/1900");
            string strRetVal = string.Empty;
            int ModifiedBy = 0;
            int intreleaseId = 0;
            DateTime ModifiedOn = Convert.ToDateTime("1/1/1900");
            int EnteredBy = 0;
            DateTime EnteredOn = Convert.ToDateTime("1/1/1900");
            int intReleaseStatusId = 0;
            DateTime dtActualDate = Convert.ToDateTime("1/1/1900");
            bool res;
            intreleaseId = Convert.ToInt32(lblReleaseId.Text);
            if (txtReleaseDate.Text.Trim() == "")
            {
                LblException.Text = "Plan Date required !";
            }
            else
            {
                LblException.Text = "";
            }
            if (txtDescription.Text.Trim() == "")
            {
                LblException.Text = "Release Description required !";
            }
            else
            {
                LblException.Text = "";
            }
            if (txtDeliverable.Text.Trim() == "")
            {
                LblException.Text = "Deliverable required !";
            }
            else
            {
                LblException.Text = "";
            }
            strReleaseTarget = txtTarget.Text;
            strDeliveryIssues = txtIssues.Text;
            if (strDeliveryIssues.Length > 500)
            {
                LblException.Text = "Delivery issues exceeded. Max length (500) !";
            }
            else
            {
                strDeliveryIssues = txtIssues.Text;
            }
            if (intreleaseId > 0)
            {
                GetOldInformation(intreleaseId);
            }
            if (txtDeliverable.Text == "")
            {
                strDeliverable = "";
            }
            else
            {
                strDeliverable = txtDeliverable.Text;
            }
            if (txtDescription.Text.TrimEnd() == "")
            {
                strDescription = "";
            }
            else
            {
                strDescription = txtDescription.Text;
            }
            if (txtReleaseIssues.Text.TrimEnd() == "")
            {
                strReleaseIssues = "";
            }
            else
            {
                strReleaseIssues = txtReleaseIssues.Text;
            }
            if (txtReleaseDate.Text.Trim() != "")
            {
                dtReleaseDate = Convert.ToDateTime(txtReleaseDate.Text.Trim());
                if (dtReleaseDate != dtOldDate && intreleaseId > 0)
                {
                    if (txtChangeReason.Text.Trim() == "")
                    {
                        LblException.Text = "Reason for change of date required !";
                    }
                    else
                    {
                        LblException.Text = "";
                    }
                }
            }
            if (txtActualDate.Text.Trim() != "")
            {
                dtActualDate = Convert.ToDateTime(txtActualDate.Text.Trim());
            }
            if (Convert.ToInt32(ddlstatus.SelectedItem.Value) > 0)
            {
                intReleaseStatusId = Convert.ToInt32(ddlstatus.SelectedItem.Value);
            }
            objUser = (Entities.Employee)(Session["emp"]);
            if (intReleaseId > 0)
            {
                ModifiedBy = objUser.EmployeeId;
                ModifiedOn = DateTime.Now;
                if (txtActualDate.Text.Trim() == "")
                {
                    LblException.Text = "Actual Date required !";
                }
                else
                {
                    LblException.Text = "";
                }
                Entities.ProjectRelease objRelease = new Entities.ProjectRelease();
                objRelease.ProjectId = intProjectId;
                objRelease.ReleaseId = intReleaseId;
                objRelease.Description = strDescription;
                objRelease.Deliverables = strDeliverable;
                objRelease.ReleaseTarget = strReleaseTarget;
                objRelease.DeliveryIssues = strDeliveryIssues;
                objRelease.ReleaseDate = dtReleaseDate;
                objRelease.LastModifiedBy = ModifiedBy;
                objRelease.LastModifiedOn = ModifiedOn;
                objRelease.ReleaseStatusId = intReleaseStatusId;
                objRelease.ActualDate = dtActualDate;
                res = new ProjectReleaseBAL().UpdateRelease(objRelease);

                //if (dtReleaseDate != dtOldDate)
                //{
                //    strRetVal += new ProjectReleaseBAL().AddHistory(intReleaseId, "R", "ReleaseDate", CStr(dtOldDate), CStr(dtReleaseDate), txtChangeReason.Text, ModifiedBy, ModifiedOn)
                //}
                if (res == true)
                {
                    strRetVal = "Release Modified Successfully";
                }
                GetVisibleTrueFalse(intReleaseId);
            }
            else
            {
                EnteredBy = objUser.EmployeeId;
                EnteredOn = DateTime.Now;
                objRelease.ProjectId = intProjectId;
                objRelease.ReleaseId = 0;
                objRelease.Description = strDescription;
                objRelease.Deliverables = strDeliverable;
                objRelease.DeliveryIssues = strDeliveryIssues;
                objRelease.ReleaseTarget = strReleaseTarget;
                objRelease.ReleaseDate = dtReleaseDate;
                objRelease.EnteredBy = EnteredBy;
                objRelease.EnteredOn = EnteredOn;
                objRelease.ReleaseStatusId = intReleaseStatusId;
                objRelease.ActualDate = dtActualDate;
                objRelease.ReleaseIssues = strReleaseIssues;
                objRelease.LastModifiedBy = ((Entities.Employee)(Session["emp"])).EmployeeId;
                res = new ProjectReleaseBAL().InsertRelease(objRelease);
            }
            if (res == true)
            {
                lblReleaseId.Text = (res == true ? "Created" : "Not Created");
                strRetVal = "Release Added Successfully";
            }
            //GetVisibleTrueFalse(intReleaseId);
            LblException.Text = strRetVal;
            RegisterStartupScript("strRefresh", "<script language='javascript'>window.opener.PageRefresh();</script>");

        }

        protected void ImgAddNew_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Release_edit.aspx?ReleaseId=0");
            addedit = "add";
        }

        private void GetReleaseStatusList()
        {
            ds = new DataSet();
            ds = new ProjectReleaseBAL().GetReleaseStatus();
            ddlstatus.DataSource = ds.Tables[0];
            ddlstatus.DataMember = ds.Tables[0].TableName;
            ddlstatus.DataTextField = "ReleaseStatus";
            ddlstatus.DataValueField = "ReleaseStatusId";
            ddlstatus.DataBind();
        }

        private void GetReleaseInfo(int intReleaseId)
        {
            int intreleaseId;
            intreleaseId = Convert.ToInt32(lblReleaseId.Text);
            if (intreleaseId > 0)
            {
                objRelease.ReleaseId = intreleaseId;
                objRelease = new ProjectReleaseBAL().GetRelease(objRelease);
                txtReleaseDate.Text = objRelease.ReleaseDate.ToShortDateString();
                txtActualDate.Text = objRelease.ActualDate.ToShortDateString();
                txtDescription.Text = objRelease.Description;
                txtDeliverable.Text = objRelease.Deliverables;
                txtTarget.Text = objRelease.ReleaseTarget;
                txtIssues.Text = objRelease.DeliveryIssues;
                ddlstatus.SelectedValue = objRelease.ReleaseStatusId.ToString();
            }
        }

        private void GetVisibleTrueFalse(int? intReleaseId)
        {
            if (intReleaseId > 0)
            {
                RegisterStartupScript("strShow", "<script language='javascript'>this.document.all.imgTop.src='Images/edit_release_tab.gif';</script>");
                lblReason.Enabled = true;
                lblReason.Visible = true;
                lblReason.Text = "Reason";
                txtChangeReason.Visible = true;
                txtChangeReason.Enabled = true;
                lblIssues.Visible = true;
                lblIssues.Text = "Major Delivery Issues";
                txtIssues.Visible = true;
            }
            else
            {
                RegisterStartupScript("strHode", "<script language='javascript'>this.document.all.imgTop.src='Images/add_release_tab.gif';</script>");
                lblReason.Enabled = false;
                lblReason.Visible = false;
                txtChangeReason.Visible = false;
                txtChangeReason.Enabled = false;
                lblIssues.Visible = false;
                txtIssues.Visible = false;
            }
        }

        private void GetOldInformation(int intReleaseId)
        {
            if (intReleaseId > 0)
            {
                objRelease.ReleaseId = intReleaseId;
                objRelease = new ProjectReleaseBAL().GetRelease(objRelease);
                if (objRelease.ReleaseDate.ToShortDateString() != "")
                {
                    dtOldDate = Convert.ToDateTime(objRelease.ReleaseDate);
                }
            }
        }
    }
}