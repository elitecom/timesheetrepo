﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectBAL;

namespace DrishProject.ProjectRelease
{
    public partial class Release_Features : System.Web.UI.Page
    {
        Entities.Employee objUser = new Entities.Employee();
        Entities.ProjectRelease objRelease = new Entities.ProjectRelease();
        int intProjectId;
        int intReleaseId;
        Entities.Project objProject = new Entities.Project();
    
        protected void Page_Load(object sender, EventArgs e)
        {
            string strProjectcode= string.Empty;                
            objUser = (Entities.Employee)(Session["emp"]);
            if (objUser.IsAministrator == 0)
            {
                #region "Unused Code"                
                //RoleAuthenticate()
                //        If User.IsInRole("AddReleaseFeatures") Then
                //        If Not Request("Action") = Nothing Then
                //            imgBtnSave.Visible = False
                //            RegisterStartupScript("strShowTask", "<script language='javascript'>this.document.all.imgAddTask.style.display='none';</script>")
                //        Else
                //            imgBtnSave.Visible = True
                //            RegisterStartupScript("strShowTask", "<script language='javascript'>this.document.all.imgAddTask.style.display='block';</script>")
                //        End If
                //    Else
                //        imgBtnSave.Visible = False
                //        RegisterStartupScript("strHideTask", "<script language='javascript'>this.document.all.imgAddTask.style.display='none';</script>")
                //    End If
                //End If
                #endregion
                intProjectId = Convert.ToInt32(Session["ProjectID"].ToString());
                objProject.ProjectID = intProjectId;
                objProject = new ProjectBAL().GetProject(intProjectId);
                strProjectcode = objProject.ProjectCode;
                Session["ReleaseID"] = Request.QueryString["ReleaseId"].ToString();
                intReleaseId = Convert.ToInt32(Session["ReleaseID"].ToString());

                #region "Unused Code"                
                //if ( IfAccessible(intReleaseId, intProjectId, "Release") = False Then
                //    Response.Redirect("Default.aspx?Authority=Deny")
                //End If
                #endregion
            }
            if (!Page.IsPostBack)
            {
                //this.SortCriteria = "A.EndDate";
                //this.SortDir = "desc";
                GetRelease(intReleaseId);
                ManageDisplay();
                GetFeatures();
                GetPlannedFeatures(intReleaseId);
                ExportDataSetToExcel(intReleaseId);
            }
        }

        protected void imgBtnSave_Click(object sender, ImageClickEventArgs e)
        {

        }

        private void GetRelease(int intReleaseId)
        {
            if (intReleaseId > 0)
            {
                objRelease.ReleaseId = intReleaseId;
                objRelease = new ProjectReleaseBAL().GetRelease(objRelease);
                lblReleaseDate.Text = objRelease.ReleaseDateF.ToShortDateString();
                lblReleaseStatus.Text = objRelease.ReleaseStatus;
            }
        }

        private void ManageDisplay()
        {
             if (lblReleaseStatus.Text.Trim().ToUpper() == "DELIVERED")
             {
                 imgBtnSave.Visible = false;
                 RegisterStartupScript("strHideTask", "<script language='javascript'>this.document.all.imgAddTask.style.display='none';</script>");
             }
        }

        private void GetFeatures()
        {
            if (lblReleaseStatus.Text.Trim().ToUpper() == "DELIVERED" || Request["Action"] != null)
            {
                dgFeatures.Columns[0].Visible = false;
            }
            else
            {
                if (objUser.IsAministrator == 0)
                {
                    //    If User.IsInRole("AddReleaseFeatures") Then
                    //        dgFeatures.Columns(0).Visible = True
                    //    Else
                    //        dgFeatures.Columns(0).Visible = False
                    //    End If
                    //Else
                    //    dgFeatures.Columns(0).Visible = True
                    //End If
                }
            }
            try
            {
                objRelease = new Entities.ProjectRelease();
                DataSet ds = new DataSet ();
                if (lblReleaseStatus.Text.Trim().ToUpper() == "DELIVERED")
                {
                    objRelease.ProjectId = intProjectId;
                    objRelease.ReleaseId = intReleaseId;
                    ds = new DataSet();
                    ds = new ProjectReleaseBAL().GetReleaseFeaturesDelivered(objRelease);
                }
                else
                {
                    if (!String.IsNullOrEmpty(Request["Action"].ToString()))
                    {
                        objRelease = new Entities.ProjectRelease();
                        objRelease.ProjectId = intProjectId;
                        objRelease.ReleaseId = intReleaseId;
                        ds = new DataSet ();
                        ds = new ProjectReleaseBAL().GetReleaseFeatures(objRelease);
                    }
                    else
                    {
                        if (objUser.IsAministrator == 0)
                        {
                            objRelease = new Entities.ProjectRelease();
                            objRelease.ProjectId = intProjectId;
                            objRelease.ReleaseId = intReleaseId;
                            ds = new DataSet();
                            ds = new ProjectReleaseBAL().GetReleaseFeatures(objRelease);
                            #region "Unused Code"

                //        If User.IsInRole("AddReleaseFeatures") Then
                //            With objRelease
                //                .ProjectId = intProjectId
                //                .SortCriteria = Me.SortCriteria
                //                .SortDir = Me.SortDir
                //                DS = .GetFeatures
                //            End With
                //        Else
                //            With objRelease
                //                .ProjectId = intProjectId
                //                .ReleaseId = intReleaseId
                //                .SortCriteria = Me.SortCriteria
                //                .SortDir = Me.SortDir
                //                DS = .GetReleaseFeatures
                //            End With
                //        End If
                //    Else
                //        With objRelease
                //            .ProjectId = intProjectId
                //            .SortCriteria = Me.SortCriteria
                //            .SortDir = Me.SortDir
                //            DS = .GetFeatures
                //        End With
                //    End If
                //End If
                        #endregion
                        }
                    }
                }
                dgFeatures.DataSource = ds;
                dgFeatures.DataMember = ds.Tables[0].TableName;
                dgFeatures.DataBind();
                
                if (ds.Tables[0].Rows.Count == 0)
                {
                    dgFeatures.PagerStyle.Visible = false;
                    LblException.Text = "No Open Tasks Present.";
                    RegisterStartupScript("strHide", "<script language='javascript'>this.document.all.imgExcel.style.display='none';</script>");
                }
                else
                {
                    RegisterStartupScript("strShow", "<script language='javascript'>this.document.all.imgExcel.style.display='block';</script>");
                    dgFeatures.PagerStyle.Visible = true;
                    LblException.Text = "";
                }
                if (ds.Tables[0].Rows.Count <= dgFeatures.PageSize)
                {
                    dgFeatures.PagerStyle.Visible = false;
                }
                else
                {
                    dgFeatures.PagerStyle.Visible = true;
                }
            }
            catch(Exception  ex)
            {
                throw ex;
            }         
        }

        private void GetPlannedFeatures(int intReleaseId)
        {
            DataGridItem dgItem;
            int intTaskId;
            CheckBox chkSelected;
            try
            {
                SqlDataReader ds;
                objRelease.ReleaseId = intReleaseId;
                ds = new ProjectReleaseBAL().GetPlannedFeatures(objRelease);
                if (ds.HasRows)
                {
                    while (ds.Read())
                    {
                        //foreach()
                        //For Each dgItem In dgFeatures.Items
                        //intTaskId = dgFeatures.DataKeys.Item(dgItem.ItemIndex)
                        //If intTaskId = DS("taskid") Then
                        //    chkSelected = dgItem.FindControl("chkSelect")
                        //    chkSelected.Checked = True
                        //End If
                    }
                }
            }
            catch (Exception ee)
            {
            
            }
            //DS.Close();
            
        }

        private void ExportDataSetToExcel(int intReleaseId)
        {}

        protected void dgFeatures_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

        }
    }
}