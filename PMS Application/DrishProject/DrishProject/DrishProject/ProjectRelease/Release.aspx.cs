﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using Entities;

namespace DrishProject.Release
{
    public partial class Releases : System.Web.UI.Page
    {
        int ProjectID;        
        Entities.Employee objUser = new Entities.Employee();
        Entities.ProjectRelease release = new Entities.ProjectRelease();
        Entities.Project objProject = new Entities.Project();
  
        const int ReleaseColIndex = 7;
        const int EditColIndex = 6;
        DataSet ds;

        protected void Page_Load(object sender, EventArgs e)
        {
            objUser = (Entities.Employee)(Session["emp"]);
            if (Request["ProjectID"] == "")
            {
                ProjectID = Convert.ToInt32(Session["ProjectID"].ToString());
            }
            else
            {
                //Session["ProjectID"] = Request["ProjectID"];
                ProjectID = Convert.ToInt32(Session["ProjectID"].ToString());
            }
            //Code to hide the Release, CheckList, Client and Code Review Tabs for Non Engineering Projects.
            objProject.ProjectID = ProjectID;
            objProject = new ProjectBAL().GetProject(ProjectID);

            if (objUser.IsAministrator == 1)
            {
                //If the user is not the member of that project then deny his access
                bool res;
                res = new EmployeeBAL().IsActiveMember(objUser.EmployeeId, ProjectID);
                if (res == false)
                {
                    Response.Redirect("Default.aspx?Authority=Deny");
                }
                #region "Unused Code"
                //RoleAuthenticate()
                //If User.IsInRole("AddRelease") Then
                //    RegisterStartupScript("strShow", "<script language='javascript'>this.document.all.imgAddRelease.style.display='block';</script>")
                //Else
                //    RegisterStartupScript("strHide", "<script language='javascript'>this.document.all.imgAddRelease.style.display='none';</script>")
                //End If
                #endregion
            }

            lblProjectCode.Text = objProject.ProjectCode;
            if (!Page.IsPostBack)
            {
                //this.SortCriteria = "ReleaseDate";
                //this.SortDir = "desc";
                getReleasePerProject(ProjectID);
            }
            #region "Unused Code"
            //if (new EmployeeBAL().isClient() == true)
            //{
            //    imgSurvey.NavigateUrl = objConfig.GetSurvey_Project
            //    imgSurveyRelease.NavigateUrl = objConfig.GetSurvey_Release
            //Else
            //    imgSurvey.NavigateUrl = objConfig.GetSurvey_Results
            //    imgSurveyRelease.NavigateUrl = objConfig.GetSurvey_Results
            //End If
            //}
            #endregion
        }

        protected void dgReleases_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            getReleasePerProject(ProjectID);
        }

        private void getReleasePerProject(int ProjectID)
        {
            try
            {
                ds = new DataSet();
                release.ProjectId = ProjectID;
                ds = new DrishProjectBAL.ProjectReleaseBAL().GetReleasesPerProject(release);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    dgReleases.DataSource = ds.Tables[0];
                    dgReleases.DataMember = ds.Tables[0].TableName;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        int count;
                        count = Convert.ToInt32((ds.Tables[0].Rows.Count - 1) / dgReleases.PageSize);
                        if (count < dgReleases.CurrentPageIndex)
                        {
                            dgReleases.CurrentPageIndex = 0;
                        }
                    }
                    else
                    {
                        dgReleases.CurrentPageIndex = 0;
                    }
                    dgReleases.DataBind();
                }
                if (ds.Tables[0].Rows.Count == 0)
                {
                    dgReleases.PagerStyle.Visible = false;
                    LblException.Text = "No Releases Present.";
                }
                else
                {
                    dgReleases.PagerStyle.Visible = true;
                    LblException.Text = "";
                }
                if (ds.Tables[0].Rows.Count <= dgReleases.PageSize)
                {
                    dgReleases.PagerStyle.Visible = false;
                }
                else
                {
                    dgReleases.PagerStyle.Visible = true;
                }
            }
            catch (Exception)
            { }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            getReleasePerProject(ProjectID);
        }

        protected void dgReleases_SortCommand(object source, DataGridSortCommandEventArgs e)
        {

        }

        protected void dgReleases_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Header)
            {
                RegisterStartupScript("Declare", "<script language='javascript'>var realColor;</script>");
                e.Item.Attributes.Add("OnMouseOver", "realColor = this.style.backgroundColor; this.style.backgroundColor = '#cccccc';");
                e.Item.Attributes.Add("OnMouseOut", "this.style.backgroundColor = realColor;");
            }
        }
    }
}