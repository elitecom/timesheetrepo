﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Release_Edit.aspx.cs" Inherits="DrishProject.Release.Release_Edit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000000;
            font-weight: bold;
            text-decoration: none;
            width: 23%;
        }
        .style2
        {
            width: 23%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="MAINTABLE" height="100%" width="100%">
            <tr>
                <td width="100%" height="1%">
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                        align="center" border="0">
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr height="100%">
                            <td>
                            </td>
                            <td align="left" height="100%">
                                <table cellspacing="0" cellpadding="0" align="top" border="0">
                                    <tr>
                                        <td>
                                            <img id="imgTop" src="../Images/add_release_tab.gif" border="0">
                                        </td>
                                    </tr>
                                </table>
                                <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0" valign="top">
                                    <tbody>
                                        <tr>
                                            <td valign="top" align="center" height="100%">
                                                <table height="100%" cellspacing="0" cellpadding="0" width="100%" align="center"
                                                    border="0">
                                        </tr>
                                        <tr class="center-bg">
                                            <td valign="top" align="left" width="96%">
                                                <img height="10" src="../Images/spacer.gif" width="10" alt="" />
                                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                                </asp:ScriptManager>
                                            </td>
                                            <td valign="top" align="right" width="3%">
                                                <img height="10" src="../Images/right_top_corner.gif" width="10">
                                            </td>
                                        </tr>
                                        <tr class="center-bg" valign="top" align="left">
                                            <td valign="top" width="99%" colspan="2" height="100%">
                                                <table cellspacing="0" cellpadding="2" align="left" border="0">
                                                    <tbody>
                                                        <tr valign="middle" height="21">
                                                            <td class="Errortxt" valign="top" colspan="4">
                                                                <asp:Label ID="LblException" runat="server" Text="Label"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td class="Gridtxt" width="1%">
                                                            </td>
                                                            <td class="style1" valign="top">
                                                                Project Code
                                                            </td>
                                                            <td class="gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt">
                                                                <asp:Label ID="lblProjectCode" runat="server" CssClass="gridtxt" Width="200"></asp:Label>
                                                                <asp:Label ID="lblReleaseId" CssClass="gridtxt" Visible="False" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td class="mendatoryfld" valign="top" width="1%">
                                                                *
                                                            </td>
                                                            <td class="style1" valign="top">
                                                                Plan Date
                                                            </td>
                                                            <td class="Gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt">
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:TextBox ID="txtReleaseDate" Width="150" runat="server" MaxLength="10"></asp:TextBox>
                                                                        <cc1:CalendarExtender ID="txtReleaseDate_CalendarExtender" runat="server" BehaviorID="txtReleaseDate_CalendarExtender"
                                                                            TargetControlID="txtReleaseDate" Format="dd/MM/yyyy"></cc1:CalendarExtender>
                                                                        &nbsp;<asp:Image ID="ImgReleaseDate" title="Calendar" Style="cursor: pointer" runat="server"
                                                                            ImageUrl="~/Images/selectall.gif"></asp:Image>&nbsp;&nbsp;
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td class="mendatoryfld" width="1%">
                                                            </td>
                                                            <td class="style1" valign="top">
                                                                <asp:Label ID="lblReason" CssClass="Labeltxt" runat="server"></asp:Label>
                                                            </td>
                                                            <td class="gridtxt">
                                                            </td>
                                                            <td class="Gridtxt" valign="top">
                                                                <asp:TextBox ID="txtChangeReason" Width="240px" runat="server" MaxLength="500"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle" id="tr1">
                                                           <%-- <%if (addedit > "0")
                                                              {%>--%>
                                                            <td class="mendatoryfld" valign="top" width="1%">
                                                                *
                                                            </td>
                                                           <%-- <% }
                                                              else
                                                              { %>--%>
                                                            <td class="style1" valign="top">
                                                                Actual Date
                                                            </td>
                                                           <%-- <%}%>--%>
                                                            <td class="Labeltxt" valign="top">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt">
                                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:TextBox ID="txtActualDate" Width="150" runat="server" MaxLength="10"></asp:TextBox>
                                                                        <cc1:CalendarExtender ID="txtActualDate_CalendarExtender" runat="server" BehaviorID="txtActualDate_CalendarExtender"
                                                                            TargetControlID="txtActualDate" Format="dd/MM/yyyy"></cc1:CalendarExtender>
                                                                        <asp:Image ID="ImgActualDate" title="Calendar" Style="cursor: pointer" runat="server"
                                                                            ImageUrl="~/Images/selectall.gif"></asp:Image>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                            <td class="Gridtxt">
                                                                &nbsp;&nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                        <%--<tr vAlign="middle">
																<td class="mendatoryfld" width="1%"></td>
																<td class="Labeltxt" vAlign="top"><asp:label id="lblReason" CssClass="Labeltxt" Runat="server"></asp:label></td>
																<td class="gridtxt"></td>
																<td class="Gridtxt" vAlign="top"><asp:textbox id="txtChangeReason" Width="240px" Runat="server" MaxLength="500"></asp:textbox></td>
															</tr>--%>
                                                        <tr valign="middle">
                                                            <td class="mendatoryfld" valign="top" width="1%">
                                                                *
                                                            </td>
                                                            <td class="style1" valign="top">
                                                                Description
                                                            </td>
                                                            <td class="gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt" valign="top">
                                                                <asp:TextBox ID="txtDescription" runat="server" CssClass="Gridtxt" Width="300" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td class="mendatoryfld" valign="top" width="1%">
                                                                *
                                                            </td>
                                                            <td class="style1" valign="top">
                                                                Deliverable
                                                            </td>
                                                            <td class="gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt" valign="top">
                                                                <asp:TextBox ID="txtDeliverable" runat="server" CssClass="Gridtxt" Width="300" MaxLength="500"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td class="mendatoryfld" valign="top" width="1%">
                                                            </td>
                                                            <td class="style1" valign="top">
                                                                Release Target
                                                            </td>
                                                            <td class="gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt" valign="top">
                                                                <asp:TextBox ID="txtTarget" runat="server" CssClass="Gridtxt" Width="300" MaxLength="500"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td class="mendatoryfld" valign="top" width="1%">
                                                                &nbsp;</td>
                                                            <td class="style1" valign="top">
                                                                Release Issues</td>
                                                            <td class="gridtxt">
                                                                &nbsp;</td>
                                                            <td class="Gridtxt" valign="top">
                                                                <asp:TextBox ID="txtReleaseIssues" runat="server" CssClass="Gridtxt" Width="300" MaxLength="500"></asp:TextBox>
                                                             </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mendatoryfld" width="1%">
                                                            </td>
                                                            <td class="style1" valign="top">
                                                                Release Status
                                                            </td>
                                                            <td class="Gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="gridtxt">
                                                                <asp:DropDownList ID="ddlstatus" CssClass="Gridtxt" Width="141px" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td class="mendatoryfld" valign="top" width="1%">
                                                            </td>
                                                            <td class="style1" valign="top">
                                                                <asp:Label ID="lblIssues" runat="server"></asp:Label>
                                                            </td>
                                                            <td class="gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt" valign="top">
                                                                <asp:TextBox ID="txtIssues" runat="server" CssClass="Gridtxt" Width="300" MaxLength="500"
                                                                    TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="gridtxt" width="100%" colspan="4" height="20">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td class="mendatoryfld" width="1%">
                                                            </td>
                                                            <td class="style2">
                                                                &nbsp;
                                                            </td>
                                                            <td class="gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt" valign="top">
                                                                <asp:ImageButton ID="imgBtnSave" runat="server" ImageUrl="~/Images/save_bt.gif" 
                                                                    OnClick="imgBtnSave_Click" style="height: 19px">
                                                                </asp:ImageButton>&nbsp;&nbsp;<img style="cursor: pointer" onclick="window.close();"
                                                                    src="../Images/cancel_bt.gif">&nbsp;&nbsp;<asp:ImageButton ID="ImgAddNew" runat="server"
                                                                        ImageUrl="~/Images/add_btn.gif" OnClick="ImgAddNew_Click"></asp:ImageButton>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="center-bg">
                                            <td valign="top" align="left">
                                                <img height="10" src="../Images/spacer.gif" width="10">
                                            </td>
                                            <td valign="bottom" align="right">
                                                <img height="10" src="../Images/right_bottom_corner.gif" width="10">
                                            </td>
                                        </tr>
                                        <tr class="gridtxt">
                                            <td colspan="2" height="3%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                </table>
                            </td>
                            <td>
                                <img height="10" src="../Images/spacer.gif" width="2" />
                            </td>
                        </tr>
                        <tr class="gridtxt">
                            <td height="3%">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </TD></TR></TBODY></TABLE></TD></TD><tr>
            <td width="100%">
            </td>
        </tr>
    </TBODY></TABLE></form>
    <script language="javascript" type="text/javascript">
        var dtRelease = document.getElementById('txtReleaseDate').value;
        new DateInput("txtReleaseDate", "ImgReleaseDate", null);
        document.getElementById('txtReleaseDate').value = dtRelease;
        var dtActual = document.getElementById('txtActualDate').value;
        new DateInput("txtActualDate", "ImgActualDate", null);
        document.getElementById('txtActualDate').value = dtActual;
				
    </script>
    </div> </form>
</body>
</html>
