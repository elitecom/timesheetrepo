﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="NewJoinee2.aspx.cs" Inherits="DrishProject.Employee.NewJoinee2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table id="MAINtable" height="100%" width="100%">
        <tr>
            <td width="100%" height="1%">
            </td>
        </tr>
        <tr>
            <td width="100%">
                <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                    align="center" border="0">
                    <tr height="100%">
                        <td>
                        </td>
                        <td align="left" valign="top" width="100%">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td>
                                        <img id="imgTop" src="../Images/new_joiner_1.gif" border="0">
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr class="center-bg">
                                        <td valign="top" align="left" width="96%" style="height: 1px">
                                            <img height="10" src="../images/spacer.gif" width="10">
                                        </td>
                                        <td valign="top" align="right" width="3%" style="height: 1px">
                                            <img height="10" src="../images/right_top_corner.gif" width="10">
                                        </td>
                                    </tr>
                                    <tr class="center-bg" valign="top" align="left">
                                        <td valign="top" height="100%" width="100%" colspan="2">
                                            <table cellspacing="0" cellpadding="2" width="100%" align="left" border="0">
                                                <tbody>
                                                    <tr valign="middle" height="21">
                                                        <td class="Errortxt" width="100%" valign="top" colspan="4">
                                                            <asp:Label CssClass="Errortxt" ID="LblException" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Mendatoryfld" valign="top">
                                                        </td>
                                                        <td class="Labeltxt" valign="top">
                                                            Joinee Name
                                                        </td>
                                                        <td class="Gridtxt">
                                                        </td>
                                                        <td class="Gridtxt">
                                                            <asp:Label ID="lblName" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Mendatoryfld" valign="top">
                                                        </td>
                                                        <td class="Labeltxt" valign="top">
                                                            Assigned Project
                                                        </td>
                                                        <td class="Gridtxt">
                                                        </td>
                                                        <td class="Gridtxt">
                                                            <asp:Label ID="lblProject" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td class="Mendatoryfld" valign="top">
                                                        </td>
                                                        <td class="Labeltxt" valign="top">
                                                            Printer Access
                                                        </td>
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt">
                                                            <asp:CheckBox ID="chkPrinter" runat="server"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td class="Mendatoryfld" valign="top">
                                                            *
                                                        </td>
                                                        <td class="Labeltxt" valign="top">
                                                            Softwares Required
                                                        </td>
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt" valign="top">
                                                            <asp:TextBox CssClass="Gridtxt" ID="txtSoftware" runat="server" Width="535px" TextMode="MultiLine"
                                                                Rows="5" MaxLength="500"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr class="Gridtxt">
                                                        <td height="20">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt" valign="top" height="90%">
                                                            <asp:ImageButton ID="imgBtnSave" ImageUrl="../Images/send_bt.gif" 
                                                                runat="server" onclick="imgBtnSave_Click1">
                                                            </asp:ImageButton>&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <asp:ImageButton ID="imgBtnClose" runat="server" 
                                                                ImageUrl="../Images/canc_bt.gif" onclick="imgBtnClose_Click1">
                                                            </asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="center-bg" height="100%">
                                        <td valign="top" align="left">
                                            <img height="10" src="../images/spacer.gif" width="10">
                                        </td>
                                        <td valign="bottom" align="right">
                                            <img height="10" src="../images/right_bottom_corner.gif" width="10">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr class="Gridtxt">
                        <td colspan="3" height="3%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
