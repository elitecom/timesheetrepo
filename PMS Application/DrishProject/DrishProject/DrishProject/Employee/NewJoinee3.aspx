﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="NewJoinee3.aspx.cs" Inherits="DrishProject.Employee.NewJoinee3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script language="javascript">
<!--
    function HideDiv() {
        var obj = document.getElementById('divDENY');
        obj.style.display = 'none';
    }
    function ShowDiv(layer) {
        this.document.all.divDENY.style.display = 'block';
        return false;
    }
//-->
		</script>
	<TABLE id="MAINTABLE" height="100%" width="100%">
				<tr>
					<td width="100%">
						<TABLE id="tblCentral" height="100%" cellSpacing="0" cellPadding="0" width="100%" align="center"
							border="0">
							<TR height="100%">
								<td></td>
								<TD vAlign="top" align="left" width="100%">
									<table cellSpacing="0" cellPadding="0" border="0">
										<tr>
											<TD><IMG id="imgTop" src="../Images/add_note_tab.gif" border="0"></TD>
										</tr>
									</table>
									<table cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TBODY>
											<TR class="center-bg">
												<TD style="HEIGHT: 1px" vAlign="top" align="left" width="96%"><IMG height="10" src="../images/spacer.gif" width="10"></TD>
												<TD style="HEIGHT: 1px" vAlign="top" align="right" width="3%"><IMG height="10" src="../images/right_top_corner.gif" width="10"></TD>
											</TR>
											<TR class="center-bg" vAlign="top" align="left">
												<TD vAlign="top" width="100%" colSpan="2" height="100%">
													<TABLE cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
														<TBODY>
															<tr vAlign="middle" height="11">
																<td class="Errortxt" vAlign="top" width="100%" colSpan="8"><asp:label id="LblException" runat="server" CssClass="Errortxt"></asp:label>&nbsp;
                                                                    <asp:HyperLink ID="hlForm" runat="server" ForeColor="Blue" Visible="False" Target="_blank">Click Here to download</asp:HyperLink></td>
															</tr>
															<TR>
																<TD class="Mendatoryfld" vAlign="top"></TD>
																<TD class="Labeltxt" vAlign="top">Joinee Name</TD>
																<TD class="Gridtxt"></TD>
																<TD colspan="5" class="Gridtxt" style="WIDTH: 375px" width="100%"><asp:label id="lblName" runat="server"></asp:label></TD>
															</TR>
															<TR>
																<TD class="Mendatoryfld" vAlign="top"></TD>
																<TD class="Labeltxt" vAlign="top">Date of Joining</TD>
																<TD class="Gridtxt"></TD>
																<TD colspan="5" class="Gridtxt" style="WIDTH: 375px"><asp:label id="lblDOJ" runat="server"></asp:label></TD>
															</TR>
                                                            <tr>
                                                                <td class="Mendatoryfld" valign="top">
                                                                </td>
                                                                <td class="Labeltxt" valign="top">
                                                                    Designation</td>
                                                                <td class="Gridtxt">
                                                                </td>
                                                                <td class="Gridtxt" colspan="5" style="width: 375px">
                                                                    <asp:Label ID="lblDesg" runat="server"></asp:Label></td>
                                                            </tr>
															<TR>
																<TD class="Mendatoryfld" vAlign="top"></TD>
																<TD class="Labeltxt" vAlign="top">Seat Location</TD>
																<TD class="Gridtxt"></TD>
																<TD colspan="5" class="Gridtxt" style="WIDTH: 375px"><asp:label id="lblSeat" runat="server"></asp:label></TD>
															</TR>
															<TR>
																<TD class="Mendatoryfld" vAlign="top"></TD>
																<TD class="Labeltxt" vAlign="top">Reporting Manager</TD>
																<TD class="Gridtxt"></TD>
																<TD colspan="5" class="Gridtxt" style="WIDTH: 375px"><asp:label id="lblRM_Name" runat="server"></asp:label></TD>
															</TR>
															<TR>
																<TD class="Mendatoryfld" vAlign="top"></TD>
																<TD class="Labeltxt" vAlign="top">Assigned Project</TD>
																<TD class="Gridtxt"></TD>
																<TD colspan="5" class="Gridtxt" style="WIDTH: 375px"><asp:label id="lblProject" runat="server"></asp:label></TD>
															</TR>
															<tr vAlign="middle">
																<td class="Mendatoryfld" vAlign="top" width="1%" colSpan="1" rowSpan="1"></td>
																<td class="Labeltxt" vAlign="top" width="15%" colSpan="1" rowSpan="1">Softwares 
																	Required</td>
																<td class="Gridtxt">&nbsp;</td>
																<td colspan="5" class="Gridtxt" style="WIDTH: 375px" vAlign="top"><asp:label id="lblSoftwares" runat="server"></asp:label>
                                                                    <asp:TextBox ID="TextBox14" runat="server" TextMode="MultiLine" Width="524px"></asp:TextBox></td>
															</tr>
															<tr vAlign="middle">
																<td class="Mendatoryfld" vAlign="top"></td>
																<td class="Labeltxt" vAlign="top">Printer Access</td>
																<td class="Gridtxt">&nbsp;</td>
																<td colspan="5" class="Gridtxt" style="WIDTH: 375px"><asp:label id="lblPrinter" runat="server"></asp:label>
                                                                    <asp:CheckBox ID="CheckBox10" runat="server" /></td>
															</tr>
															<TR>
																<TD class="Mendatoryfld" vAlign="top" align="right">*</TD>
																<TD class="Labeltxt" vAlign="top">M/C Login ID</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" style="WIDTH: 375px" vAlign="top" width="375" colSpan="1" rowSpan="1"><asp:textbox id="TextBox1" runat="server" MaxLength="50"></asp:textbox></TD>
																<TD class="Mendatoryfld" vAlign="top" align="right">*</TD>
																<TD class="Labeltxt" style="WIDTH: 138px" vAlign="top">M/C Login Password</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" vAlign="top"><asp:textbox id="TextBox2" runat="server" MaxLength="50"></asp:textbox></TD>
															</TR>
															<TR>
																<TD class="Mendatoryfld" vAlign="top" align="right">*</TD>
																<TD class="Labeltxt" vAlign="top">Mailing ID</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" vAlign="top" width="15%"><asp:textbox id="TextBox3" runat="server" MaxLength="50"></asp:textbox></TD>
																<TD class="Mendatoryfld" vAlign="top" align="right">*</TD>
																<TD class="Labeltxt" style="WIDTH: 138px" vAlign="top">Mailing Password</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" vAlign="top"><asp:textbox id="Textbox4" runat="server" MaxLength="50"></asp:textbox></TD>
															</TR>
															<%--<TR>
																<TD class="Mendatoryfld" vAlign="top" align="right"></TD>
																<TD class="Labeltxt" vAlign="top">VSS ID</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" style="WIDTH: 375px" vAlign="top"><asp:textbox id="Textbox5" runat="server" MaxLength="50"></asp:textbox></TD>
																<TD class="Mendatoryfld" vAlign="top" align="right"></TD>
																<TD class="Labeltxt" style="WIDTH: 138px" vAlign="top">VSS Password</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" vAlign="top"><asp:textbox id="Textbox6" runat="server" MaxLength="50"></asp:textbox></TD>
															</TR>--%>
															<TR>
																<TD class="Mendatoryfld" vAlign="top" align="right"></TD>
																<TD class="Labeltxt" vAlign="top">Skype ID</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" style="WIDTH: 375px" vAlign="top"><asp:textbox id="Textbox7" runat="server" MaxLength="50"></asp:textbox></TD>
																<TD class="Mendatoryfld" vAlign="top" align="right"></TD>
																<TD class="Labeltxt" style="WIDTH: 138px" vAlign="top">Skype Password</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" vAlign="top"><asp:textbox id="Textbox8" runat="server" MaxLength="50"></asp:textbox></TD>
															</TR>
															<TR>
																<TD class="Mendatoryfld" vAlign="top" align="right"></TD>
																<TD class="Labeltxt" vAlign="top">MSN ID</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" style="WIDTH: 375px" vAlign="top"><asp:textbox id="Textbox9" runat="server" MaxLength="50"></asp:textbox></TD>
																<TD class="Mendatoryfld" vAlign="top" align="right"></TD>
																<TD class="Labeltxt" vAlign="top" width="16%" colSpan="1" rowSpan="1">MSN Password</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" vAlign="top"><asp:textbox id="Textbox10" runat="server" MaxLength="50"></asp:textbox></TD>
															</TR>
															<TR>
																<TD class="Mendatoryfld" vAlign="top" align="right">*</TD>
																<TD class="Labeltxt" vAlign="top">M/C Name.</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" style="WIDTH: 375px" vAlign="top"><asp:textbox id="Textbox11" runat="server" MaxLength="50"></asp:textbox></TD>
																<TD class="Mendatoryfld" vAlign="top" align="right">*</TD>
																<TD class="Labeltxt" style="WIDTH: 138px" vAlign="top">M/C No.</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" vAlign="top"><asp:textbox id="Textbox12" runat="server" MaxLength="50"></asp:textbox></TD>
															</TR>
															<TR>
																<TD class="Mendatoryfld" vAlign="top" align="right"></TD>
																<TD class="Labeltxt" vAlign="top">VSS Permissions</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" style="WIDTH: 375px" vAlign="top"><asp:checkbox id="CheckBox1" runat="server"></asp:checkbox></TD>
																<TD class="Mendatoryfld" vAlign="top" align="right">*</TD>
																<TD class="Labeltxt" style="WIDTH: 138px" vAlign="top">Internet Line</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" vAlign="top"><asp:textbox id="Textbox13" runat="server" MaxLength="15"></asp:textbox><asp:regularexpressionvalidator id="regValidate" runat="server" ControlToValidate="Textbox13" ErrorMessage='Invalid input, Valid input "192.168.143.200"'
																		ValidationExpression="\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b"></asp:regularexpressionvalidator></TD>
															</TR>
															<TR>
																<TD class="Mendatoryfld" vAlign="top"></TD>
																<TD class="Labeltxt" vAlign="top">Softwares Installed</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" style="WIDTH: 375px" vAlign="top"><asp:checkbox id="CheckBox2" runat="server"></asp:checkbox></TD>
																<TD class="Mendatoryfld" vAlign="top"></TD>
																<TD class="Labeltxt" style="WIDTH: 138px" vAlign="top" width="138">External Mail 
																	Access</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" vAlign="top"><asp:checkbox id="CheckBox3" runat="server"></asp:checkbox></TD>
															</TR>
															<%--<TR>
																<TD class="Mendatoryfld" vAlign="top"></TD>
																<TD class="Labeltxt" vAlign="top">Safalchd Access</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" style="WIDTH: 375px" vAlign="top"><asp:checkbox id="CheckBox4" runat="server"></asp:checkbox></TD>
																<TD class="Mendatoryfld" vAlign="top"></TD>
																<TD class="Labeltxt" style="WIDTH: 138px" vAlign="top">Pegasus Access</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" vAlign="top"><asp:checkbox id="CheckBox5" runat="server"></asp:checkbox></TD>
															</TR>--%>
															<TR>
																<TD class="Mendatoryfld" vAlign="top"></TD>
																<TD class="Labeltxt" vAlign="top">Project Mailing List</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" style="WIDTH: 375px" vAlign="top"><asp:checkbox id="CheckBox6" runat="server"></asp:checkbox></TD>
																<TD class="Mendatoryfld" vAlign="top"></TD>
																<TD class="Labeltxt" style="WIDTH: 138px" vAlign="top">Mantis Setup</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" vAlign="top"><asp:checkbox id="CheckBox7" runat="server"></asp:checkbox></TD>
															</TR>
															<%--<TR>
																<TD class="Mendatoryfld" vAlign="top"></TD>
																<TD class="Labeltxt" vAlign="top">Outlook Configured</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" style="WIDTH: 375px" vAlign="top"><asp:checkbox id="Checkbox8" runat="server"></asp:checkbox></TD>
																<TD class="Mendatoryfld" vAlign="top"></TD>
																<TD class="Labeltxt" style="WIDTH: 138px" vAlign="top">Local Admin Setup</TD>
																<TD class="Gridtxt"></TD>
																<TD class="Gridtxt" vAlign="top"><asp:checkbox id="Checkbox9" runat="server"></asp:checkbox></TD>
															</TR>--%>
															<tr class="Gridtxt">
																<td height="20">&nbsp;</td>
															</tr>
															<tr vAlign="middle">
																<TD class="Mendatoryfld" vAlign="top" style="height: 25px"></TD>
																<TD class="Gridtxt" style="height: 25px"></TD>
																<TD class="Mendatoryfld" vAlign="top" style="height: 25px"></TD>
																<TD class="Gridtxt" align="right" style="height: 25px">
                                                                    &nbsp;&nbsp;
                                                                    <asp:ImageButton ID="ImgBtnApprove" runat="server" 
                                                                        ImageUrl="~/Images/approve_bt.gif" CausesValidation="False" 
                                                                        onclick="ImgBtnApprove_Click" />
                                                                    &nbsp;&nbsp;
                                                                    <asp:imagebutton id="imgBtnSave" valigImageUrl="../Images/send_bt.gif" 
                                                                        Runat="server" ImageUrl="~/Images/save_bt.gif" onclick="imgBtnSave_Click"></asp:imagebutton></TD>
																<td class="Gridtxt" style="height: 25px">&nbsp;</td>
																<td class="Gridtxt" style="WIDTH: 138px; height: 25px;" align="left" width="138">
                                                                    <asp:ImageButton ID="ImageButton1" runat="server" 
                                                                        ImageUrl="~/Images/deny_bt.gif" onclick="ImageButton1_Click" />
                                                                    &nbsp;&nbsp;
                                                                    <asp:imagebutton id="imgBtnClose" runat="server" 
                                                                        valigImageUrl="~/Images/canc_bt.gif" CausesValidation="False" 
                                                                        ImageUrl="~/Images/canc_bt.gif" onclick="imgBtnClose_Click"></asp:imagebutton></td>
																<td class="Gridtxt" style="height: 25px">&nbsp;</td>
																<td class="Gridtxt" style="WIDTH: 375px; height: 25px;" vAlign="top" align="left" colSpan="1">&nbsp;
                                                                    <div id="divDENY" style="z-index: 1500; left: 599px; display:NONE; width: 295px;
                                                                        position: absolute; top: 428px; height: 87px">
                                                                        <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="DIVtablestyle"
                                                                            style="height: 100%" width="100%">
                                                                            <tr>
                                                                                <td align="left" colspan="2" width="95%" style="height: 15px">
                                                                                    <strong>Reason:</strong></td>
                                                                                <td align="center" onclick="HideDiv();" style="cursor: pointer; height: 15px;" title="Close">
                                                                                    <strong style="font-size: 12px; color: #ff9966">X</strong></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top" style="height: 115px">
                                                                                    <asp:TextBox ID="txtRemarks" runat="server" Height="102px" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top">
                                                                                    <asp:Label ID="lblDenyError" runat="server" CssClass="Errortxt"></asp:Label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center" valign="top">
                                                                                    <asp:ImageButton ID="imgBtnDenySend" runat="server" valigImageUrl="~/Images/send_bt.gif" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
															</tr>
														</TBODY>
													</TABLE>
												</TD>
											</TR>
										</TBODY>
									</table>
								</TD>
								<TD></TD>
							</TR>
							<tr class="Gridtxt">
								<td colSpan="3" height="3%">&nbsp;</td>
							</tr>
						</TABLE>
					</td>
				</tr>
			</TABLE>
			
</asp:Content>
