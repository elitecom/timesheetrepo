﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using DrishProjectBAL;
using System.Data;
using System.Data.SqlClient;
namespace DrishProject
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Entities.Employee emp = new Entities.Employee();
            if (!Page.IsPostBack)
            {
                try
                {
                    emp = (Entities.Employee)(Session["emp"]);
                    DataSet ds = new DataSet();
                    ds = new DrishProjectBAL.EmployeeBAL().GetDesignations();
                    drpDesignation.DataSource = ds.Tables[0];
                    drpDesignation.DataMember = ds.Tables[0].TableName;
                    drpDesignation.DataTextField = "Designation";
                    drpDesignation.DataValueField = "DesgID";
                    drpDesignation.DataBind();
                    drpDesignation.Items.Insert(0, " == Select ==");
                    ds = new DrishProjectBAL.EmployeeBAL().getdata();
                    gridview.DataSource = ds;
                    gridview.DataBind();
                    string role = new EmployeeBAL().GetUserRole(emp.UserName);
                    //if (role == "Project Manager" || role == "Team Leader")
                    if (role.Equals("Human Resource Manager"))
                    {
                        gridview.Columns[10].Visible = true;
                    }
                    else
                    {
                        gridview.Columns[10].Visible = false;
                    }
                    lblResult.Text = "Total Record : " + ds.Tables[0].Rows.Count.ToString();

                    // Show/Hide the Add Employee Button
                    if (role.Equals("Human Resource Manager"))
                    {
                        ImageButton3.Visible = true;
                    }
                    else
                    {
                        ImageButton3.Visible = false;
                    }                
                }
                catch (Exception)
                {
                    Response.Redirect("../Default.aspx");
                }
            }
        }
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            int desgId = -1;
            string name = string.Empty;
            string dob = string.Empty;
            string doj = string.Empty;
            if (drpDesignation.SelectedIndex > 0)
            {
                desgId = Convert.ToInt32(drpDesignation.SelectedValue.ToString());
            }
            else
            {
                desgId = -1;
            }
            if (TextBox1.Text.Length > 0)
            {
                name = TextBox1.Text;
            }
            else
            {
                name = null;
            }
            if (!String.IsNullOrEmpty(TextBox2.Text))
            {
                dob = TextBox2.Text;
            }            
            if (!String.IsNullOrEmpty(TextBox3.Text))
            {
                doj = TextBox3.Text;
            }           
            DataSet ds = new DataSet();
            ds = new DrishProjectBAL.EmployeeBAL().SearchEmployee(desgId, name, dob, doj);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gridview.DataSource = ds;
                gridview.DataBind();
                lblResult.Text = "Total Record : " + ds.Tables[0].Rows.Count.ToString();
            }
            else
            {
                gridview.DataSource = null;
                gridview.DataBind();
                lblResult.Text = "<b>No Record Found ....</b>";
            }
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Entities.Employee emp = new Entities.Employee();
            emp = (Entities.Employee)(Session["emp"]);
            DataSet ds = new DataSet();
            ds = new DrishProjectBAL.EmployeeBAL().GetDesignations();
            drpDesignation.DataSource = ds.Tables[0];
            drpDesignation.DataMember = ds.Tables[0].TableName;
            drpDesignation.DataTextField = "Designation";
            drpDesignation.DataValueField = "DesgID";
            drpDesignation.DataBind();
            drpDesignation.Items.Insert(0, " == Select ==");
            ds = new DrishProjectBAL.EmployeeBAL().getdata();
            gridview.DataSource = ds;
            gridview.DataBind();
            lblResult.Text = "Total Record : " + ds.Tables[0].Rows.Count.ToString();
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Employee/AddEmployee.aspx");
        }

        protected void btnExport_Click(object sender, ImageClickEventArgs e)
        {
            Entities.Employee emp = new Entities.Employee();
            emp = (Entities.Employee)(Session["emp"]);
            DataSet ds = new DataSet();
            ds = new DrishProjectBAL.EmployeeBAL().GetDetailedEmployeeList();
            DataSetToExcel.Convert(ds, Response);
        }
    }
}