﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Entities;
using DrishProjectBAL;
namespace DrishProject
{
    public partial class WebForm9 : System.Web.UI.Page
    {
        Entities.Employee emp = new Entities.Employee();
        EmployeeBAL bal = new EmployeeBAL();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    DataSet ds = new DataSet();
                    ds = new DrishProjectBAL.EmployeeBAL().GetDepartments();
                    drpDepartment.DataSource = ds.Tables[0];
                    drpDepartment.DataMember = ds.Tables[0].TableName;
                    drpDepartment.DataTextField = "DeptName";
                    drpDepartment.DataValueField = "DeptID";
                    drpDepartment.DataBind();
                    drpDepartment.Items.Insert(0, " == Select ==");

                    ds = new DataSet();
                    ds = new DrishProjectBAL.EmployeeBAL().GetDesignations();
                    drpDesignation.DataSource = ds.Tables[0];
                    drpDesignation.DataMember = ds.Tables[0].TableName;
                    drpDesignation.DataTextField = "Designation";
                    drpDesignation.DataValueField = "DesgID";
                    drpDesignation.DataBind();
                    drpDesignation.Items.Insert(0, " == Select ==");

                    ds = new DataSet();
                    ds = new DrishProjectBAL.EmployeeBAL().GetReportingManagers();
                    drpReportingManagers.DataSource = ds.Tables[0];
                    drpReportingManagers.DataMember = ds.Tables[0].TableName;
                    drpReportingManagers.DataTextField = "Name";
                    drpReportingManagers.DataValueField = "EmpID";
                    drpReportingManagers.DataBind();
                    drpReportingManagers.Items.Insert(0, " == Select ==");

                    ds = new DataSet();
                    ds = new DrishProjectBAL.EmployeeBAL().GetProjects();
                    drpProjects.DataSource = ds.Tables[0];
                    drpProjects.DataMember = ds.Tables[0].TableName;
                    drpProjects.DataTextField = "ProjectName";
                    drpProjects.DataValueField = "ProjectID";
                    drpProjects.DataBind();
                    drpProjects.Items.Insert(0, " == Select ==");

                    string empCode = Convert.ToString(Request.QueryString["EmpCode"].ToString());
                    if (!String.IsNullOrEmpty(empCode))
                    {                        
                        emp = new EmployeeBAL().SearchEmployeeByCode(empCode);
                        TextBox1.Text = emp.FirstName;
                        TextBox2.Text = emp.LastName;
                        TextBox3.Text = emp.EmployeeCode;
                        TextBox4.Text = emp.DateOfJoining.ToShortDateString();
                        TextBox5.Text = emp.UserName;
                        TextBox6.Text = emp.SeatLocation;
                        TextBox7.Text = emp.IdentityCard;
                        TextBox8.Text = emp.DateOfBirth.ToShortDateString();
                        TextBox9.Text = emp.CorrespondanceAddress;
                        TextBox10.Text = emp.PermanentAddress;
                        TextBox11.Text = emp.PhoneNumber;
                        TextBox12.Text = emp.MobileNumber;
                        TextBox13.Text = emp.EducationalQualification;
                        TextBox14.Text = emp.SkillSet;
                        TextBox15.Text = emp.Hobbies;
                        TextBox16.Text = emp.Remarks;
                        drpDepartment.SelectedValue = emp.DepartmentId.ToString();
                        drpDesignation.SelectedValue = emp.DesgID.ToString();
                        drpReportingManagers.SelectedValue = emp.RM_ID.ToString();
                        drpProjects.SelectedValue = emp.ProjectId.ToString();
                    }
                }
                catch (Exception)
                {

                }
            }


        }

        protected void Button1_Click(object sender, ImageClickEventArgs e)
        {
            bool res;
            try
            {
                emp.UserName = TextBox5.Text;
                emp.DepartmentId = Convert.ToInt32(drpDepartment.SelectedValue.ToString());
                emp.DesgID = Convert.ToInt32(drpDesignation.SelectedValue.ToString());
                emp.SeatLocation = TextBox6.Text;
                emp.CorrespondanceAddress = TextBox9.Text;
                emp.PermanentAddress = TextBox10.Text;
                emp.PhoneNumber = TextBox11.Text;
                emp.MobileNumber = TextBox12.Text;
                emp.SkillSet = TextBox14.Text;
                emp.Hobbies = TextBox15.Text;
                emp.Remarks = TextBox16.Text;
                res = bal.updatedata(emp);
                if (res == true)
                {
                    Response.Write("<b>Employee  Data Updated Successfully");
                }
                else
                {
                    Response.Write("<b>Employee  Data Not Update Successfully");
                }
            }

            catch (Exception ee)
            {
                Response.Write("Message : " + ee.Message);
                Response.Write(ee.StackTrace);
            }

        }

    }
}