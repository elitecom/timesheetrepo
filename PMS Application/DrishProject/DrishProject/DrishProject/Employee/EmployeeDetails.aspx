﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="EmployeeDetails.aspx.cs" Inherits="DrishProject.WebForm18" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <p/>
    <div>
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/my_profile_active.gif" />
        <asp:ImageButton ID="ImageButton2" runat="server" 
            ImageUrl="~/Images/skill_set_active.gif" onclick="ImageButton2_Click" />
        <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/training_feedback_active.gif" />
        <%--<a href="#" style="float: right;">Change Password</a>--%>
    </div>
    <table class="center-bg" width="100%">
        <tr>
            <td style="width:250px"><asp:Label ID="Label2" runat="server" Text="First Name" 
                    Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblFirstName" runat="server" Text="Label"></asp:Label></td>
            <td style="width:100px"></td>
            <td style="width:250px"><asp:Label ID="Label3" runat="server" Text="Last Name" 
                    Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblLastName" runat="server" Text="Label"></asp:Label></td>
        </tr>
         <tr>
            <td colspan="5"><br /></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label4" runat="server" Text="Employee Code" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblEmployeeCode" runat="server" Text="Label"></asp:Label></td>
            <td></td>
            <td><asp:Label ID="Label5" runat="server" Text="EmailId" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblEmailId" runat="server" Text="Label"></asp:Label></td>
        </tr>
         <tr>
            <td colspan="5"><br /></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label8" runat="server" Text="Department" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblDepartment" runat="server" Text="Label"></asp:Label></td>
            <td></td>
            <td><asp:Label ID="Label9" runat="server" Text="Designation" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblDesignation" runat="server" Text="Label"></asp:Label></td>
        </tr>
         <tr>
            <td colspan="5"><br /></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label14" runat="server" Text="Date Of Birth" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblDOB" runat="server" Text="Label"></asp:Label></td>
            <td></td>
            <td><asp:Label ID="Label1" runat="server" Text="Date Of Joining" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblDOJ" runat="server" Text="Label"></asp:Label></td>
        </tr>
         <tr>
            <td colspan="5"><br /></td>
        </tr>
         <tr>
            <td><asp:Label ID="Label15" runat="server" Text="Correspondance Address" 
                    Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblCoressAddress" runat="server" Text="Label"></asp:Label></td>
            <td></td>
            <td><asp:Label ID="Label16" runat="server" Text="Permanent Address" 
                    Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblPermaAddress" runat="server" Text="Label"></asp:Label></td>
        </tr>
         <tr>
            <td colspan="5"><br /></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label17" runat="server" Text="Phone Number" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblPhNo" runat="server" Text="Label"></asp:Label></td>
            <td></td>
            <td><asp:Label ID="Label18" runat="server" Text="Mobile Number" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblMobNo" runat="server" Text="Label"></asp:Label></td>
        </tr>
         <tr>
            <td colspan="5"><br /></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label19" runat="server" Text="Educational Qualification" 
                    Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblEduqual" runat="server" Text="Label"></asp:Label></td>
            <td></td>
            <td><asp:Label ID="Label20" runat="server" Text="Skillset" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblSkillSet" runat="server" Text="Label"></asp:Label></td>
        </tr>
         <tr>
            <td colspan="5"><br /></td>
        </tr>
         <tr>
            <td><asp:Label ID="Label21" runat="server" Text="Hobbies" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="lblHobbies" runat="server" Text="Label"></asp:Label></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
         <tr>
            <td colspan="5"><br /></td>
        </tr>
        <tr>
            <td colspan="5" align="center">
                <asp:ImageButton ID="ImageButton4" runat="server" 
                    ImageUrl="~/Images/send_request_bt.gif" onclick="ImageButton4_Click" />    
            </td>
        </tr>
         <tr>
            <td colspan="5"><br /></td>
        </tr>
    </table>   
</asp:Content>
