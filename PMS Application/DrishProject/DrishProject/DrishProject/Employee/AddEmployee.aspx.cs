﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using Entities;


namespace DrishProject
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        EmployeeBAL bal = new EmployeeBAL();
        Entities.Employee emp = new Entities.Employee();
        public void Page_PreInit()
        {
            bool res;
            res = new DrishProjectBAL.RBACScreenBAL().IsValidRole("AddEmployee");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    DataSet ds = new DataSet();
                    ds = new DrishProjectBAL.EmployeeBAL().GetDepartments();
                    drpDepartment.DataSource = ds.Tables[0];
                    drpDepartment.DataMember = ds.Tables[0].TableName;
                    drpDepartment.DataTextField = "DeptName";
                    drpDepartment.DataValueField = "DeptID";
                    drpDepartment.DataBind();
                    drpDepartment.Items.Insert(0, " == Select ==");

                    ds = new DataSet();
                    ds = new DrishProjectBAL.EmployeeBAL().GetDesignations();
                    drpDesignation.DataSource = ds.Tables[0];
                    drpDesignation.DataMember = ds.Tables[0].TableName;
                    drpDesignation.DataTextField = "Designation";
                    drpDesignation.DataValueField = "DesgID";
                    drpDesignation.DataBind();
                    drpDesignation.Items.Insert(0, " == Select ==");

                    ds = new DataSet();
                    ds = new DrishProjectBAL.EmployeeBAL().GetReportingManagers();
                    drpReportingManagers.DataSource = ds.Tables[0];
                    drpReportingManagers.DataMember = ds.Tables[0].TableName;
                    drpReportingManagers.DataTextField = "Name";
                    drpReportingManagers.DataValueField = "EmpID";
                    drpReportingManagers.DataBind();
                    drpReportingManagers.Items.Insert(0, " == Select ==");

                    ds = new DataSet();
                    ds = new DrishProjectBAL.EmployeeBAL().GetProjects();
                    drpProjects.DataSource = ds.Tables[0];
                    drpProjects.DataMember = ds.Tables[0].TableName;
                    drpProjects.DataTextField = "ProjectName";
                    drpProjects.DataValueField = "ProjectID";
                    drpProjects.DataBind();
                    drpProjects.Items.Insert(0, " == Select ==");
                }
                catch (Exception)
                {

                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddEmployee.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            bool res;
            try
            {
                emp.FirstName = TextBox1.Text;
                emp.LastName = TextBox2.Text;
                emp.EmployeeCode = TextBox3.Text;
                emp.DateOfJoining = Convert.ToDateTime(TextBox4.Text);
                emp.UserName = TextBox5.Text;
                emp.Password = emp.FirstName.Substring(0, 1).ToUpper() + emp.LastName + "@123";
                emp.SeatLocation = TextBox6.Text;
                emp.IdentityCard = TextBox7.Text;
                emp.DateOfBirth = Convert.ToDateTime(TextBox8.Text);
                emp.CorrespondanceAddress = TextBox9.Text;
                emp.PermanentAddress = TextBox10.Text;
                emp.PhoneNumber = TextBox11.Text;
                emp.MobileNumber = TextBox12.Text;
                emp.EducationalQualification = TextBox13.Text;
                emp.SkillSet = TextBox14.Text;
                emp.Hobbies = TextBox15.Text;
                emp.Remarks = TextBox16.Text;
                emp.EmailId = emp.FirstName + "." + emp.LastName + "@drishinfo.com";
                emp.DepartmentId = Convert.ToInt32(drpDepartment.SelectedValue.ToString());
                emp.DesgID = Convert.ToInt32(drpDesignation.SelectedValue.ToString());
                emp.Status = "A";
                emp.IsReportingManager = Convert.ToInt32(drpReportingManagers.SelectedValue.ToString());
                emp.ShortName = emp.FirstName.Substring(0, 1) + emp.LastName.Substring(0, 2);
                res = bal.AddEmployee(emp);
                if (res == true)
                {
                    Response.Write("<b>Employee Added Successfully</b>");
                    //Response.Redirect("NewJoinee2.aspx");
                }
                else
                {
                    Response.Write("<b>Employee Not Added Successfully</b>");
                }
            }
            catch (Exception)
            {
                //Response.Write("Message : " + ee.Message);
                //Response.Write(ee.StackTrace);
            }
        }
    }
}