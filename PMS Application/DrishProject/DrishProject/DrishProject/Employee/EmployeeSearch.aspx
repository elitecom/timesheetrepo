﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="EmployeeSearch.aspx.cs" Inherits="DrishProject.WebForm3" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table class="searchtablestyle" cellspacing="0" cellpadding="0" width="115%" align="left">
        <tr>
            <td class="SearchTblHead" width="100%" align="left" colspan="8">
                &nbsp;Employee Search
            </td>
        </tr>
        <tr>
            <td colspan="8" align="right">
                <asp:ImageButton ID="ImageButton3" runat="server" 
                    ImageUrl="~/Images/add_mem_bt.gif" onclick="ImageButton3_Click" />
            </td>
        </tr>
        <tr class="Gridtxt" height="10">
            <td class="Gridtxt" colspan="8" height="10">
                <img src="../Images/spacer.gif" height="10" width="10">
            </td>
        </tr>
        <tr>
            <td colspan="8">
    <div style="float: left;">
        <asp:Label ID="Label1" runat="server" Text="Designation" Style="margin-left: 20px;"></asp:Label>
        <asp:DropDownList ID="drpDesignation" runat="server" Style="margin-left: 20px;">
            <asp:ListItem>== Select==</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div style="float: left;">
        <asp:Label ID="Label2" runat="server" Text="Name / Code" Style="margin-left: 20px;"></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server" Style="margin-left: 10px;"></asp:TextBox>
    </div>
    <div style="float: left;">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:Label ID="Label3" runat="server" Text="Date Of Birth" Style="margin-left: 20px;"></asp:Label>
                <asp:TextBox ID="TextBox2" runat="server" Style="margin-left: 10px;" ></asp:TextBox>
                <asp:CalendarExtender ID="TextBox2_CalendarExtender" runat="server" TargetControlID="TextBox2" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="float: left;">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Label ID="Label4" runat="server" Text="Date Of Joining" Style="margin-left: 20px;" ></asp:Label>
                <asp:TextBox ID="TextBox3" runat="server" Style="margin-left: 10px;" ></asp:TextBox>
                <asp:CalendarExtender ID="TextBox3_CalendarExtender" runat="server" TargetControlID="TextBox3" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="float: left;">
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/searc_bt.gif"
            OnClick="ImageButton1_Click" Style="margin-right: 20px; margin-left: 20px;" />
        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/display_all_bt.gif"
            OnClick="ImageButton2_Click" />
    </div>
    <br />
    <br />
    <br />
    <asp:GridView ID="gridview" runat="server" AutoGenerateColumns="False" DataKeyNames="EmpCode"
        CellPadding="3" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" 
                    BorderWidth="1px" CellSpacing="2" Width="1300px">
        <Columns>
            <asp:HyperLinkField HeaderText="Emp Code" DataNavigateUrlFields="EmpCode" DataNavigateUrlFormatString="EmployeeDetails.aspx?EmpCode={0}"
                DataTextField="EmpCode" >
            <ItemStyle Width="75px" />
            </asp:HyperLinkField>
            <asp:BoundField HeaderText="First Name" DataField="FirstName" />
            <asp:BoundField HeaderText="Last Name" DataField="LastName" />
            <asp:BoundField HeaderText="DOB" DataField="DOB" 
                DataFormatString="{0:dd-MM-yyyy}" >
            <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Phone No" DataField="Phone" >
            <ItemStyle Width="85px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Mobile No" DataField="Mobile" >
            <ItemStyle Width="85px" />
            </asp:BoundField>
           <%-- <asp:BoundField HeaderText="Email Id" DataField="EmailId" />--%>
            <asp:HyperLinkField DataTextField="EmailId" HeaderText="E-Mail" DataNavigateUrlFormatString="SendMail.aspx?EmpCode={0}"
                DataNavigateUrlFields="EmpCode" >
            <ItemStyle Width="90px" />
            </asp:HyperLinkField>
            <asp:BoundField HeaderText=" Department" DataField="DeptName" >
            <ItemStyle Width="95px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Designation" DataField="Designation" >
            <ItemStyle Width="95px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="DOJ" DataField="DOJ" 
                DataFormatString="{0:dd-MM-yyyy}" >
            <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:HyperLinkField HeaderText="Edit" DataNavigateUrlFields="EmpCode" DataNavigateUrlFormatString="EditEmployee.aspx?EmpCode={0}"
                Text="Edit" />
        </Columns>
        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#FFF1D4" />
        <SortedAscendingHeaderStyle BackColor="#B95C30" />
        <SortedDescendingCellStyle BackColor="#F1E5CE" />
        <SortedDescendingHeaderStyle BackColor="#93451F" />
    </asp:GridView>
                <br />
                <asp:ImageButton ID="btnExport" runat="server" ImageUrl="../Images/exp_excel_ico.gif" 
                        style="width: 30px; height: 16px; margin-left: 1260px;" 
                    onclick="btnExport_Click"  />
                </td>
        </tr>
    </table>
    <br />
    <div>
        <asp:Label ID="lblResult" runat="server" Text="Label"></asp:Label>
    </div>
    <br />

    <script language="javascript" type="text/javascript">
        $(function () {
            $("._datepicker").datepicker({ dateFormat: 'dd/mm/yy' });
        });
    </script>
</asp:Content>
