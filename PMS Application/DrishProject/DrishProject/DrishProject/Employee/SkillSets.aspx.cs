﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using System.Collections;

namespace DrishProject.Employee
{
    public partial class SkillSets : System.Web.UI.Page
    {
        DataSet ds;
        Entities.SkillSet objOfBusSkillSet = new Entities.SkillSet();
        Entities.Employee objUser = new Entities.Employee();
        int Value;
        int j;
        Entities.Employee objEmployee = new Entities.Employee();
        //int DatasetRowsCount;
        int A;
        int EmpId;

        protected void Page_Load(object sender, EventArgs e)
        {
            objUser = (Entities.Employee)(Session["emp"]);
            EmpId = objUser.EmployeeId;


            if (!Page.IsPostBack)
            {
                lblSuccessfully.Visible = false;
                try
                {
                    GetQuarter();
                    // get quarter data
                    GetDropDownValue();
                    GetYear();
                    // get year data
                    ddlSelectedIndexChange();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + "\n" + ex.StackTrace);
                }
            }
        }

        public void ddlSelectedIndexChange()
        {
            try
            {
                Hashtable HsTbl = null;
                AssignValue();
                ds = new DataSet();

                HsTbl = new SkillSetsBAL().GetSkillSetGeneralValue();
                ds = (DataSet)HsTbl["Dataset"];
                //Value = HsTbl["Value"];
                Value = 1;

                if (Value == 1)
                {
                    AssignDataSetValue();
                }
                else
                {
                    AssignValueToDdl(ddlApproach);
                    AssignValueToDdl(ddlAnalysis);
                    AssignValueToDdl(ddlConfidence);
                    AssignValueToDdl(ddlSpoken);
                    AssignValueToDdl(ddlListening);
                    AssignValueToDdl(ddlWritten);
                    AssignValueToDdl(ddlTeamWork);
                    AssignValueToDdl(ddlLeaderShip);
                    AssignValueToDdl(ddlPlanning);
                    AssignValueToDdl(ddlWillingness);

                    AssignValueToDdl(ddlVb);
                    AssignValueToDdl(ddlVbDotNet);
                    AssignValueToDdl(ddlAsp);
                    AssignValueToDdl(ddlAspDotNet);
                    AssignValueToDdl(ddlCsharp);
                    AssignValueToDdl(ddlC);
                    AssignValueToDdl(ddlPhp);
                    AssignValueToDdl(ddlSqlServer);
                    AssignValueToDdl(ddlOracle);
                    AssignValueToDdl(ddlMysql);
                    AssignValueToDdl(ddlManualTesting);
                    AssignValueToDdl(ddlAutomatedTesting);
                    AssignValueToDdl(ddlTestPlanMaking);
                    AssignValueToDdl(ddlIsoProcess);
                    AssignValueToDdl(ddlWhiteBox);
                    AssignValueToDdl(ddlWindows);
                    AssignValueToDdl(ddlLinux);
                    AssignValueToDdl(ddlFlash);
                    AssignValueToDdl(ddlPhhop);
                    AssignValueToDdl(ddlDream);
                    AssignValueToDdl(ddlNunit);
                    AssignValueToDdl(ddlKentico);
                    AssignValueToDdl(ddlXSLT);
                    AssignValueToDdl(ddlSqlReports);
                    GetDropDownValue();
                }
            }
            catch (Exception ex)
            {
                //ExceptionMess(ex.Message)
                throw ex;
            }
        }

        public void AssignValueToDdl(DropDownList ddl)
        {
            try
            {
                A = 0;
                do
                {
                    ddl.SelectedItem.Text = A.ToString();
                }
                while (!(A == A));
            }
            catch (Exception ex)
            {
                //   ExceptionMess(ex.Message)
                throw ex;
            }
        }

        public void AssignDataSetValue()
        {
            try
            {
                DataTable DataSetTbl = null;
                DataTable DataSetTbl1 = null;

                DataSetTbl = ds.Tables["table"];
                DataSetTbl = ds.Tables["table1"];

                DropDownvalue(ddlApproach, DataSetTbl, 0);
                DropDownvalue(ddlAnalysis, DataSetTbl, 1);
                DropDownvalue(ddlConfidence, DataSetTbl, 2);
                DropDownvalue(ddlSpoken, DataSetTbl, 3);
                DropDownvalue(ddlListening, DataSetTbl, 4);
                DropDownvalue(ddlWritten, DataSetTbl, 5);
                DropDownvalue(ddlTeamWork, DataSetTbl, 6);
                DropDownvalue(ddlLeaderShip, DataSetTbl, 7);
                DropDownvalue(ddlPlanning, DataSetTbl, 8);
                DropDownvalue(ddlWillingness, DataSetTbl, 9);

                DropDownvalue(ddlVb, DataSetTbl1, 0);
                DropDownvalue(ddlVbDotNet, DataSetTbl1, 1);
                DropDownvalue(ddlAsp, DataSetTbl1, 2);
                DropDownvalue(ddlAspDotNet, DataSetTbl1, 3);
                DropDownvalue(ddlCsharp, DataSetTbl1, 4);
                DropDownvalue(ddlC, DataSetTbl1, 5);
                DropDownvalue(ddlPhp, DataSetTbl1, 6);
                DropDownvalue(ddlSqlServer, DataSetTbl1, 7);
                DropDownvalue(ddlOracle, DataSetTbl1, 8);
                DropDownvalue(ddlMysql, DataSetTbl1, 9);
                DropDownvalue(ddlManualTesting, DataSetTbl1, 10);
                DropDownvalue(ddlAutomatedTesting, DataSetTbl1, 11);
                DropDownvalue(ddlTestPlanMaking, DataSetTbl1, 12);
                DropDownvalue(ddlIsoProcess, DataSetTbl1, 13);
                DropDownvalue(ddlWhiteBox, DataSetTbl1, 14);
                DropDownvalue(ddlWindows, DataSetTbl1, 15);
                DropDownvalue(ddlLinux, DataSetTbl1, 16);
                DropDownvalue(ddlFlash, DataSetTbl1, 17);
                DropDownvalue(ddlPhhop, DataSetTbl1, 18);
                DropDownvalue(ddlDream, DataSetTbl1, 19);
                DropDownvalue(ddlNunit, DataSetTbl1, 20);
                DropDownvalue(ddlKentico, DataSetTbl1, 21);
                DropDownvalue(ddlXSLT, DataSetTbl1, 22);
                DropDownvalue(ddlSqlReports, DataSetTbl1, 23);

                GetDropDownValue();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DropDownvalue(DropDownList ddl, DataTable dsTable, int j)
        {
            try
            {
                do
                {
                    ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(dsTable.Rows[j][1].ToString()));
                } while (!(j == j));
            }
            catch (Exception ex)
            {
                //  ExceptionMess(ex.Message)
                throw ex;
            }
        }

        public void AssignValue()
        {
            try
            {
                objOfBusSkillSet.Quarter = Convert.ToInt32(ddlQuarter.SelectedValue.ToString());
                objOfBusSkillSet.EmpId = EmpId;
                objOfBusSkillSet.Year = Convert.ToInt32(ddlYear.SelectedValue.ToString());

                objOfBusSkillSet.Approach = 1;
                objOfBusSkillSet.Analysis = 2;
                objOfBusSkillSet.Confidence = 3;
                objOfBusSkillSet.Spoken = 4;
                objOfBusSkillSet.Listenning = 5;
                objOfBusSkillSet.Written = 6;
                objOfBusSkillSet.TeamWork = 7;
                objOfBusSkillSet.LeaderShip = 8;
                objOfBusSkillSet.Planning = 9;
                objOfBusSkillSet.Willingness = 10;
                objOfBusSkillSet.VB = 1;
                objOfBusSkillSet.VbDotNet = 2;
                objOfBusSkillSet.ASP = 3;
                objOfBusSkillSet.AspDotNet = 4;
                objOfBusSkillSet.CSharp = 5;
                objOfBusSkillSet.C = 6;
                objOfBusSkillSet.Php = 7;
                objOfBusSkillSet.SqlServer = 8;
                objOfBusSkillSet.Oracle = 9;
                objOfBusSkillSet.MySql = 10;
                objOfBusSkillSet.ManualTesting = 11;
                objOfBusSkillSet.AutomatedTesting = 12;
                objOfBusSkillSet.TestPlanMaking = 13;
                objOfBusSkillSet.ISOProcess = 14;
                objOfBusSkillSet.WhiteBox = 15;
                objOfBusSkillSet.Windows = 16;
                objOfBusSkillSet.Linux = 17;
                objOfBusSkillSet.Flash = 18;
                objOfBusSkillSet.Phhop = 19;
                objOfBusSkillSet.DreamWeaver = 20;
                objOfBusSkillSet.Nunit = 21;
                objOfBusSkillSet.Kentico = 22;
                objOfBusSkillSet.XSLT = 23;
                objOfBusSkillSet.SqlServerReports = 26;
                objOfBusSkillSet.Kentico0 = 24;
                objOfBusSkillSet.SqlServerReports = 25;

                #region "Unused Code"
                
                //if (!(ddlApproach.SelectedItem.Text == ""))
                //{
                //objOfBusSkillSet.Approach = Convert.ToInt32(ddlApproach.SelectedValue.ToString());
                //objOfBusSkillSet.Analysis = Convert.ToInt32(ddlAnalysis.SelectedValue.ToString());
                //objOfBusSkillSet.Confidence = Convert.ToInt32(ddlConfidence.SelectedValue.ToString());
                //objOfBusSkillSet.Spoken = Convert.ToInt32(ddlSpoken.SelectedValue.ToString());
                //objOfBusSkillSet.Listenning = Convert.ToInt32(ddlListening.SelectedValue.ToString());
                //objOfBusSkillSet.Written = Convert.ToInt32(ddlWritten.SelectedValue.ToString());
                //objOfBusSkillSet.TeamWork = Convert.ToInt32(ddlTeamWork.SelectedValue.ToString());
                //objOfBusSkillSet.LeaderShip = Convert.ToInt32(ddlLeaderShip.SelectedValue.ToString());
                //objOfBusSkillSet.Planning = Convert.ToInt32(ddlPlanning.SelectedValue.ToString());
                //objOfBusSkillSet.Willingness = Convert.ToInt32(ddlWillingness.SelectedValue.ToString());
                //objOfBusSkillSet.VB = Convert.ToInt32(ddlVb.SelectedValue.ToString());
                //objOfBusSkillSet.VbDotNet = Convert.ToInt32(ddlVbDotNet.SelectedValue.ToString());
                //objOfBusSkillSet.ASP = Convert.ToInt32(ddlAsp.SelectedValue.ToString());
                //objOfBusSkillSet.AspDotNet = Convert.ToInt32(ddlAspDotNet.SelectedValue.ToString());
                //objOfBusSkillSet.CSharp = Convert.ToInt32(ddlCsharp.SelectedValue.ToString());
                //objOfBusSkillSet.C = Convert.ToInt32(ddlC.SelectedValue.ToString());
                //objOfBusSkillSet.Php = Convert.ToInt32(ddlPhp.SelectedValue.ToString());
                //objOfBusSkillSet.SqlServer = Convert.ToInt32(ddlSqlServer.SelectedValue.ToString());
                //objOfBusSkillSet.Oracle = Convert.ToInt32(ddlOracle.SelectedValue.ToString());
                //objOfBusSkillSet.MySql = Convert.ToInt32(ddlMysql.SelectedValue.ToString());
                //objOfBusSkillSet.ManualTesting = Convert.ToInt32(ddlManualTesting.SelectedValue.ToString());
                //objOfBusSkillSet.AutomatedTesting = Convert.ToInt32(ddlAutomatedTesting.SelectedValue.ToString());
                //objOfBusSkillSet.TestPlanMaking = Convert.ToInt32(ddlTestPlanMaking.SelectedValue.ToString());
                //objOfBusSkillSet.ISOProcess = Convert.ToInt32(ddlIsoProcess.SelectedValue.ToString());
                //objOfBusSkillSet.WhiteBox = Convert.ToInt32(ddlWhiteBox.SelectedValue.ToString());
                //objOfBusSkillSet.Windows = Convert.ToInt32(ddlWindows.SelectedValue.ToString());
                //objOfBusSkillSet.Linux = Convert.ToInt32(ddlLinux.SelectedValue.ToString());
                //objOfBusSkillSet.Flash = Convert.ToInt32(ddlFlash.SelectedValue.ToString());
                //objOfBusSkillSet.Phhop = Convert.ToInt32(ddlPhhop.SelectedValue.ToString());
                //objOfBusSkillSet.DreamWeaver = Convert.ToInt32(ddlDream.SelectedValue.ToString());
                //objOfBusSkillSet.Nunit = Convert.ToInt32(ddlNunit.SelectedValue.ToString());
                //objOfBusSkillSet.Kentico = Convert.ToInt32(ddlKentico.SelectedValue.ToString());
                //objOfBusSkillSet.XSLT = Convert.ToInt32(ddlXSLT.SelectedValue.ToString());
                ////objOfBusSkillSet.Kentico00 = ddlKentico1.SelectedItem.Text
                //objOfBusSkillSet.SqlServerReports = Convert.ToInt32(ddlSqlReports.SelectedValue.ToString());
                //}
                #endregion
            }
            catch (Exception ex)
            {
                // ExceptionMess(ex.Message)
                throw ex;
            }
        }

        public void GetEmployeeName()
        {
            try
            {
                ds = new DataSet();
                ds = new EmployeeBAL().GetAllEmployees();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetQuarter()
        {
            try
            {
                //get quarter data and bind with the ddlquarter dropdownlist
                ds = new DataSet();
                ds = new SkillSetsBAL().GetQuarter();
                ddlQuarter.DataSource = ds.Tables[0];
                ddlQuarter.DataMember = ds.Tables[0].TableName;
                ddlQuarter.DataTextField = "Quarter";
                ddlQuarter.DataValueField = "QuartlyId";
                ddlQuarter.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetDropDownValue()
        {
            try
            {
                ds = new DataSet();
                ds = new SkillSetsBAL().GetDropDownValue();

                DataBoundToddl(ddlApproach, ds);
                DataBoundToddl(ddlAnalysis, ds);
                DataBoundToddl(ddlConfidence, ds);
                DataBoundToddl(ddlSpoken, ds);
                DataBoundToddl(ddlListening, ds);
                DataBoundToddl(ddlWritten, ds);
                DataBoundToddl(ddlTeamWork, ds);
                DataBoundToddl(ddlLeaderShip, ds);
                DataBoundToddl(ddlPlanning, ds);
                DataBoundToddl(ddlWillingness, ds);

                DataBoundToddl(ddlVb, ds);
                DataBoundToddl(ddlVbDotNet, ds);
                DataBoundToddl(ddlAsp, ds);
                DataBoundToddl(ddlAspDotNet, ds);
                DataBoundToddl(ddlCsharp, ds);
                DataBoundToddl(ddlC, ds);
                DataBoundToddl(ddlPhp, ds);
                DataBoundToddl(ddlSqlServer, ds);
                DataBoundToddl(ddlOracle, ds);
                DataBoundToddl(ddlMysql, ds);
                DataBoundToddl(ddlManualTesting, ds);
                DataBoundToddl(ddlAutomatedTesting, ds);
                DataBoundToddl(ddlTestPlanMaking, ds);
                DataBoundToddl(ddlIsoProcess, ds);
                DataBoundToddl(ddlWhiteBox, ds);
                DataBoundToddl(ddlWindows, ds);
                DataBoundToddl(ddlLinux, ds);
                DataBoundToddl(ddlFlash, ds);
                DataBoundToddl(ddlPhhop, ds);
                DataBoundToddl(ddlDream, ds);
                DataBoundToddl(ddlNunit, ds);
                DataBoundToddl(ddlKentico, ds);
                DataBoundToddl(ddlXSLT, ds);
                //DataBoundToddl(ddlKentico1, ds)
                DataBoundToddl(ddlSqlReports, ds);
            }
            catch (Exception ex)
            {
                //  ExceptionMess(ex.Message)
                throw ex;
            }
        }

        public void DataBoundToddl(DropDownList ddl, DataSet ds)
        {
            try
            {
                do
                {
                    ddl.DataSource = ds;
                    ddl.DataTextField = "value";
                    ddl.DataBind();
                } while (!(A == A));
            }
            catch (Exception ex)
            {
                //  ExceptionMess(ex.Message)
                throw ex;
            }
        }

        public void GetYear()
        {
            try
            {
                ds = new DataSet();
                ds = new DrishProjectBAL.SkillSetsBAL().GetYear();
                ddlYear.DataSource = ds;
                ddlYear.DataTextField = "year";
                ddlYear.DataValueField = "yearid";
                ddlYear.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ExceptionMess(string ExceptionMessage)
        {
            try
            {
                lblException.Visible = true;
                lblException.Text = ExceptionMessage;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            bool res;
            try
            {
                AssignValue();
                res = new DrishProjectBAL.SkillSetsBAL().InsertTblSkillset(objOfBusSkillSet);
                lblSuccessfully.Visible = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void imgGo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ddlSelectedIndexChange();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddlQuarter_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Page_Error(object sender, System.EventArgs e)
        {
            Exception ex = null;
            ex = Server.GetLastError();
            Session["Error"] = ex.Message;
            Session["InnerError"] = ex.StackTrace;
            Session["ErrorPage"] = Request.Url.AbsoluteUri;
            Server.ClearError();
            Response.Redirect("PageError.aspx");
        }
    }
}