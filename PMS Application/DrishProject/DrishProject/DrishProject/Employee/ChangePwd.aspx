﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePwd.aspx.cs" Inherits="DrishProject.Employee.ChangePwd" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="FrmChangePassword" method="post" runat="server">
    <table id="MAINTABLE" height="100%" width="100%">
        <tr class="Gridtxt">
            <td width="100%" height="1%">
            </td>
        </tr>
        <tr>
            <td width="100%">
                <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                    align="center" border="0">
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr height="100%">
                        <td>
                        </td>
                        <td valign="top" align="left" height="100%">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td>
                                        <img src="../Images/change_password_tab.gif" border="0">
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td valign="top" align="center" height="100%">
                                            <table height="100%" cellspacing="0" cellpadding="0" width="100%" align="center"
                                                border="0">
                                    </tr>
                                    <tr class="center-bg">
                                        <td valign="top" align="left" width="96%">
                                            <img height="10" src="../Images/spacer.gif" width="10">
                                        </td>
                                        <td valign="top" align="right" width="3%">
                                            <img height="10" src="../images/right_top_corner.gif" width="10">
                                        </td>
                                    </tr>
                                    <tr class="center-bg" valign="top" align="left">
                                        <td valign="top" height="100%">
                                            <table cellspacing="0" cellpadding="0" align="left" border="0">
                                                <tbody>
                                                    <tr valign="middle">
                                                        <td colspan="4" class="Gridtxt" valign="top">
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle" height="21">
                                                        <td class="Errortxt" valign="top" colspan="4" width="100%">
                                                            <asp:Label CssClass="Errortxt" ID="LblException" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr height="5">
                                                        <td colspan="4" height="5">
                                                            <img height="5" width="5" src="../images/spacer.gif">
                                                        </td>
                                                    </tr>
                                                    <tr class="Gridtxt" valign="middle">
                                                        <td valign="top" class="MendatoryFld">
                                                            *
                                                        </td>
                                                        <td class="Labeltxt" width="40%" valign="top">
                                                            Old Password
                                                        </td>
                                                        <td>
                                                            <img src="../images/spacer.gif" width="5">
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="Gridtxt" ID="txtOldPwd" Width="120px" TextMode="Password"
                                                                MaxLength="15" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr height="5">
                                                        <td colspan="4" height="5">
                                                            <img height="5" width="5" src="../images/spacer.gif">
                                                        </td>
                                                    </tr>
                                                    <tr class="Gridtxt" valign="middle">
                                                        <td valign="top" class="MendatoryFld">
                                                            *
                                                        </td>
                                                        <td class="Labeltxt" width="40%" valign="top">
                                                            New Password
                                                        </td>
                                                        <td>
                                                            <img src="../images/spacer.gif" width="5">
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="Gridtxt" ID="txtNewPwd" Width="120px" TextMode="Password"
                                                                MaxLength="15" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr height="5">
                                                        <td colspan="4" height="5">
                                                            <img height="5" width="5" src="../images/spacer.gif">
                                                        </td>
                                                    </tr>
                                                    <tr class="Gridtxt" valign="middle">
                                                        <td valign="top" class="MendatoryFld">
                                                            *
                                                        </td>
                                                        <td class="Labeltxt" width="40%" valign="top">
                                                            Confirm Password
                                                        </td>
                                                        <td>
                                                            <img src="../images/spacer.gif" width="5">
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="Gridtxt" ID="txtConfirmPwd" TextMode="Password" Width="120px"
                                                                MaxLength="15" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr height="10">
                                                        <td colspan="4" class="Gridtxt" height="25">
                                                            <img height="25" width="25" src="../Images/spacer.gif">
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td colspan="4" align="center" class="Gridtxt" valign="top">
                                                            <asp:ImageButton CssClass="Gridtxt" ID="imgBtnSave" runat="server" ImageUrl="~/Images/save_bt.gif"
                                                                OnClick="imgBtnSave_Click"></asp:ImageButton>&nbsp;&nbsp;
                                                            <img style="cursor: POINTER" onclick="window.close();" 
                                                                src="../Images/cancel_bt.gif">
                                                        </td>
                                                    </tr>
                                        </td>
                                    </tr>
                            </table>
                        </td>
                        <td class="Gridtxt">
                            <img height="10" src="../Images/spacer.gif" width="10">
                        </td>
                    </tr>
                    <tr class="center-bg">
                        <td valign="top" class="Gridtxt" align="left">
                            <img height="10" src="../Images/spacer.gif" width="10">
                        </td>
                        <td valign="bottom" class="Gridtxt" align="right">
                            <img height="10" src="../Images/right_bottom_corner.gif" width="10">
                        </td>
                    </tr>
                    </table>
            </td>
        </tr>
    </table>
    </TD></TR></TBODY></TABLE></TD></TD>
    <tr class="Gridtxt">
        <td class="Gridtxt" width="100%">
        </td>
    </tr>
    </TBODY></TABLE></form>
</body>
</html>
