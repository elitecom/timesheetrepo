﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using DrishProjectBAL;

namespace DrishProject
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Entities.Employee emp = new Entities.Employee();
            string str = string.Empty;
            EmployeeBAL bal = new EmployeeBAL();

            //if (!String.IsNullOrEmpty(Request.QueryString["EmpCode"].ToString()))
            //{
            //    str = Convert.ToString(Request.QueryString["EmpCode"].ToString());
            //}
            //else
            //{
            str = ((Entities.Employee)(Session["emp"])).UserName;
            //}
            emp.UserName = str;
            if (!string.IsNullOrEmpty(str))
            {
                //emp = (Entities.Employee)(Session["emp"]);
                emp = bal.GetProfileDetailsByUserName(emp);

                lblFirstName.Text = emp.FirstName;
                lblLastName.Text = emp.LastName;
                lblEmployeeCode.Text = emp.EmployeeCode;
                lblEmailId.Text = emp.EmailId;
                lblDepartment.Text = emp.DepartmentName;
                lblDesignation.Text = emp.Designation;
                lblDOB.Text = emp.DateOfBirth.ToShortDateString();
                lblDOJ.Text = emp.DateOfJoining.ToShortDateString();
                lblPhNo.Text = emp.PhoneNumber;
                lblMobNo.Text = emp.MobileNumber;
                lblEduqual.Text = emp.EducationalQualification;
                lblSkillSet.Text = emp.SkillSet;
                lblCoressAddress.Text = emp.CorrespondanceAddress;
                lblPermaAddress.Text = emp.PermanentAddress;
                lblHobbies.Text = emp.Hobbies;
            }
            
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("SendRequest.aspx");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("SkillSets.aspx");
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {

        }
    }
}