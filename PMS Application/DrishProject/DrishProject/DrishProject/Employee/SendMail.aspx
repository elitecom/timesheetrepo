﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="SendMail.aspx.cs" Inherits="DrishProject.WebForm12" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Send Mail</h2>
    <div>
        <div>
            <asp:Label ID="Label1" runat="server" Text="Mail To"></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server" style="margin-left:100px;" 
                Height="18px" Width="463px"></asp:TextBox>
        </div>
        <div>
             <asp:Label ID="Label2" runat="server" Text="Subject"></asp:Label>
            <asp:TextBox ID="TextBox2" runat="server" style="margin-left:100px;" 
                 Height="17px" Width="460px"></asp:TextBox>
        </div>
        <div>
             <asp:Label ID="Label3" runat="server" Text="CC"></asp:Label>
            <asp:TextBox ID="TextBox3" runat="server" style="margin-left:125px;" 
                 Height="23px" Width="459px"></asp:TextBox>
        </div>
        <div>
             <asp:Label ID="Label4" runat="server" Text="BCC"></asp:Label>
            <asp:TextBox ID="TextBox4" runat="server" style="margin-left:110px;" 
                 Height="25px" Width="461px"></asp:TextBox>
        </div>
        <div>
             <asp:Label ID="Label5" runat="server" Text="Text"></asp:Label>
            <asp:TextBox ID="TextBox5" runat="server" style="margin-left:120px;" 
                 TextMode="MultiLine" Height="130px" Width="463px"></asp:TextBox>
             <br />
        </div>
        <div>
            <asp:Button ID="btnSend"  runat="server" Text="Send Mail" 
                style="margin-left:50px;" onclick="btnSend_Click"/>
            <asp:Button ID="btnReset"  runat="server" Text="Reset" style="margin-left:20px;"/>
        </div>
    </div>
</asp:Content>
