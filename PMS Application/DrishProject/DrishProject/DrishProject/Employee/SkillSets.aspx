﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="SkillSets.aspx.cs" Inherits="DrishProject.Employee.SkillSets" %>
       


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        #Site_footer
        {
            display:none;
        }
    </style>
    <table id="tblMain" cellspacing="0" cellpadding="0" width="115%" border="0" style="margin-top:0px;">
        <tbody>            
            <tr>
                <td width="100%">
                    <!-- CENTER -->
                    <table id="tblCentral" cellspacing="0" cellpadding="0" width="100%" align="center" border="0" valign="top">
                        <tbody>
                            <tr>
                                <td align="left">
                                    &nbsp;</td>
                                <tr>
                                <td align="left">
                                    <table align="left" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <a href="MyProfile.aspx">
                                                    <img src="../Images/my_profile_active1.gif" border="0"></a>
                                            </td>
                                            <td id="tdSkillset" runat="server">
                                                <img src="../Images/skill_set_tab.gif" border="0"></a>
                                            </td>
                                            <td id="tdTrainingFeedback" runat="server">
                                                <a href="TrainingFeedback.aspx">
                                                    <img src="../Images/training_feedback_active.gif" border="0"></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                                            <tbody>
                                                <tr class="center-bg">
                                                    <td valign="top" align="left" style="height: 10px; width: 1034px;">
                                                        <img height="10" src="../Images/spacer.gif" width="10">
                                                        <br />
                                                        <b><asp:Label ID="lblSuccessfully" runat="server" ForeColor="Red" Text="Saved Successfully" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblException" runat="server" Text="Label" Visible="False"></asp:Label>
                                                        </b>
                                                    </td>
                                                    <td valign="top" align="right" width="3%" style="height: 10px">
                                                        <img height="10" src="../Images/right_top_corner.gif" width="10">
                                                    </td>
                                                </tr>
                                                <tr valign="top" align="left" class="center-bg">
                                                    <td>                                                        
                                                        <table width="100%" cellpadding="0" cellspacing="0" class="searchtablestyle">
                                                            <tr>
                                                                <td class="SearchTblHead" width="100%" colspan="6">
                                                                    &nbsp;Skill Set
                                                                </td>
                                                            </tr>
                                                            <tr height="5">
                                                                <td colspan="6" width="100%" height="5">
                                                                    <img height="5" src="../Images/spacer.gif">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="2%">
                                                                </td>
                                                                <td width="15%">
                                                                    <span><strong>Year &nbsp;&nbsp; </strong>
                                                                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="Gridtxt" 
                                                                        Width="70px" onselectedindexchanged="ddlYear_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </span>
                                                                </td>
                                                                <td width="5%">
                                                                </td>
                                                                <td width="25%">
                                                                    <strong>Quarter</strong> &nbsp; &nbsp;
                                                                    <asp:DropDownList ID="ddlQuarter" CssClass="Gridtxt" runat="server" 
                                                                        Width="120px" onselectedindexchanged="ddlQuarter_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:ImageButton ID="imgGo" ImageUrl="../Images/go_bt.gif" runat="server" 
                                                                        onclick="imgGo_Click" />
                                                                </td>
                                                                <td width="40">
                                                                </td>
                                                            </tr>
                                                            <tr height="5">
                                                                <td colspan="6" width="100%" height="5">
                                                                    <img height="5" src="../Images/spacer.gif">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="height: 10px">
                                                                    <fieldset>
                                                                        <legend><b>Skill Set General</b></legend>
                                                                        <table width="100%" cellpadding="0" cellspacing="0" style="height: 105px">
                                                                            <tr>
                                                                                <td valign="top" style="height: 120px; width: 364px;">
                                                                                    <fieldset>
                                                                                        <legend>Problem Solving Skills</legend>
                                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td style="width: 76px">
                                                                                                    <span>Approach:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlApproach" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 76px">
                                                                                                    <span>Analysis:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlAnalysis" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 76px">
                                                                                                    <span>Confidence:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlConfidence" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>                                                                                           
                                                                                        </table>
                                                                                    </fieldset>
                                                                                </td>
                                                                                <td valign="top" style="height: 115px; width: 151px;">
                                                                                    &nbsp;</td>
                                                                                <td valign="top" style="height: 115px; width: 290px;">
                                                                                    <fieldset>
                                                                                        <legend>Language(English)</legend>
                                                                                        <table cellpadding="0" cellspacing="0" style="width: 78%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <span>Spoken:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlSpoken" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    &nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <span>Listening:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlListening" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    &nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <span>Written:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlWritten" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    &nbsp;</td>
                                                                                            </tr>                                                                                         
                                                                                        </table>
                                                                                    </fieldset>
                                                                                </td>
                                                                                <td valign="top" style="height: 115px">
                                                                                    <fieldset>
                                                                                        <legend>Communication</legend>
                                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td class="style2" style="width: 103px">
                                                                                                    &nbsp;</td>
                                                                                                <td>
                                                                                                    <span>Team Work:</span>
                                                                                                </td>
                                                                                                <td style="width: 51px">
                                                                                                    <asp:DropDownList ID="ddlTeamWork" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="style2" style="width: 103px">
                                                                                                    &nbsp;</td>
                                                                                                <td>
                                                                                                    <span>Leadership:</span>
                                                                                                </td>
                                                                                                <td style="width: 51px">
                                                                                                    <asp:DropDownList ID="ddlLeaderShip" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="style2" style="width: 103px">
                                                                                                    &nbsp;</td>
                                                                                                <td>
                                                                                                    <span>Planning:</span>
                                                                                                </td>
                                                                                                <td style="width: 51px">
                                                                                                    <asp:DropDownList ID="ddlPlanning" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="style2" style="width: 103px">
                                                                                                    &nbsp;</td>
                                                                                                <td>
                                                                                                    <span>Willingness:</span>
                                                                                                </td>
                                                                                                <td style="width: 51px">
                                                                                                    <asp:DropDownList ID="ddlWillingness" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>                                                                                          
                                                                                        </table>
                                                                                    </fieldset>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <fieldset>
                                                                        <legend><b>Skill Set Technical</b></legend>
                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td valign="top" width="36%">
                                                                                    <fieldset>
                                                                                        <legend>Programming I</legend>
                                                                                        <table cellpadding="0" cellspacing="0" style="width: 71%">
                                                                                            <tr width="50%">
                                                                                                <td style="width: 75px">
                                                                                                    <span>VB:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlVb" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 75px">
                                                                                                    <span>Vb.net:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlVbDotNet" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 75px">
                                                                                                    <span>Asp:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlAsp" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 75px">
                                                                                                    <span>Asp.net:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlAspDotNet" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 75px">
                                                                                                    <span>C#:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlCsharp" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </fieldset>
                                                                                </td>
                                                                                <td valign="top" style="width: 23%">
                                                                                    <fieldset>
                                                                                        <legend>Programming II</legend>
                                                                                        <table cellpadding="0" cellspacing="0" style="width: 91%">
                                                                                            <tr>
                                                                                                <td style="width: 1px">
                                                                                                    &nbsp;</td>
                                                                                                <td style="width: 112px">
                                                                                                    <span>C:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlC" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 1px">
                                                                                                    &nbsp;</td>
                                                                                                <td style="width: 112px">
                                                                                                    <span>Php:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlPhp" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 1px">
                                                                                                    &nbsp;</td>
                                                                                                <td style="width: 112px">
                                                                                                    <span>Sql Server:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlSqlServer" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 1px">
                                                                                                    &nbsp;</td>
                                                                                                <td style="width: 112px">
                                                                                                    <span>Oracle:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlOracle" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 1px">
                                                                                                    &nbsp;</td>
                                                                                                <td style="width: 112px">
                                                                                                    <span>MySql:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlMysql" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </fieldset>
                                                                                </td>
                                                                                <td valign="top" width="31%">
                                                                                    <fieldset>
                                                                                        <legend>QA Skills</legend>
                                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td style="width: 70px">
                                                                                                    &nbsp;</td>
                                                                                                <td style="width: 226px">
                                                                                                    <span>Manual Testing:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlManualTesting" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 70px">
                                                                                                    &nbsp;</td>
                                                                                                <td style="width: 226px">
                                                                                                    <span>Automated Testing:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlAutomatedTesting" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 70px">
                                                                                                    &nbsp;</td>
                                                                                                <td style="width: 226px">
                                                                                                    <span>Test Plan Making:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlTestPlanMaking" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 70px">
                                                                                                    &nbsp;</td>
                                                                                                <td style="width: 226px">
                                                                                                    <span>ISO Processes Understanding:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlIsoProcess" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 70px">
                                                                                                    &nbsp;</td>
                                                                                                <td style="width: 226px">
                                                                                                    <span>White Box Testing:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlWhiteBox" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </fieldset>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <fieldset>
                                                                                        <legend>Miscellaneous</legend>
                                                                                        <table cellpadding="0" cellspacing="0" style="width: 87%">
                                                                                            <tr>
                                                                                                <td style="width: 132px">
                                                                                                    <span>Nunit:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlNunit" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                                <td align="right" style="width:80px">
                                                                                                    &nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 132px">
                                                                                                    <span>Kentico:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlKentico" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    &nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 132px">
                                                                                                    <span>XSLT & XSD:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlXSLT" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    &nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 132px">
                                                                                                    <span>Sql Server Reports:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlSqlReports" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    &nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </fieldset>
                                                                                </td>
                                                                                <td valign="top" style="width: 23%">
                                                                                    <fieldset>
                                                                                        <legend>O/S</legend>
                                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <span>Windows:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlWindows" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <span>Linux:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlLinux" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </fieldset>
                                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td valign="top">
                                                                                                <fieldset>
                                                                                                    <legend>Script Editing</legend>
                                                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <span>Dream Weaver/Front Page:</span>
                                                                                                            </td>
                                                                                                            <td align="right">
                                                                                                                <asp:DropDownList ID="ddlDream" runat="server" Width="100px" Height="16px">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </fieldset>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <br />
                                                                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/save_bt.gif" 
                                                                            onclick="btnSave_Click"></asp:ImageButton>
                                                                                </td>
                                                                                <td valign="top" width="34%">
                                                                                    <fieldset>
                                                                                        <legend>Animation</legend>
                                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td style="width: 71px">
                                                                                                    &nbsp;</td>
                                                                                                <td style="width: 231px">
                                                                                                    <span>Flash:</span>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:DropDownList ID="ddlFlash" runat="server" Width="100px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>                                                                                            
                                                                                        </table>
                                                                                    </fieldset>
                                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td valign="top" style="height: 90px">
                                                                                                <fieldset>
                                                                                                    <legend>Designing</legend>
                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="TABLE1" language="javascript"
                                                                                                        onclick="return TABLE1_onclick()">
                                                                                                        <tr>
                                                                                                            <td style="width: 70px">
                                                                                                                &nbsp;</td>
                                                                                                            <td>
                                                                                                                &nbsp;</td>
                                                                                                            <td>
                                                                                                                <span>Pshop/CorelDraw:</span>
                                                                                                            </td>
                                                                                                            <td align="right">
                                                                                                                <asp:DropDownList ID="ddlPhhop" runat="server" Width="100px">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </fieldset>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>                                                                
                                                            </tr>
                                                          
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <img height="10" src="../Images/spacer.gif" width="10">
                                                    </td>
                                                </tr>
                                                <tr class="center-bg">
                                                    <td valign="top" align="left">
                                                        <img height="10" src="../Images/spacer.gif" width="10">
                                                    </td>
                                                    <td valign="bottom" align="right">
                                                        <img height="10" src="../Images/right_bottom_corner.gif" width="10">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
