﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="EditEmployee.aspx.cs" Inherits="DrishProject.WebForm9" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <b>
            <asp:Label ID="Label1" runat="server" Text="Edit Employee"></asp:Label></b>
    </div>
    <br />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <asp:Label ID="Label2" runat="server" Text="First Name"></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server" Style="margin-left: 104px;" Enabled="False"></asp:TextBox>
        <asp:Label ID="Label3" runat="server" Text="Last Name" Style="margin-left: 61px;"></asp:Label>
        <asp:TextBox ID="TextBox2" runat="server" Style="margin-left: 80px;" Enabled="False"></asp:TextBox>
    </div>
    <div>
        <br />
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:Label ID="Label4" runat="server" Text="Employee Code"></asp:Label>
                <asp:TextBox ID="TextBox3" runat="server" Style="margin-left: 72px;" Enabled="False"></asp:TextBox>
                <asp:Label ID="Label5" runat="server" Text="Date Of Joining" Style="margin-left: 61px;"></asp:Label>
                <asp:TextBox ID="TextBox4" runat="server" Style="margin-left: 48px;" Enabled="False"></asp:TextBox>
                <asp:CalendarExtender ID="TextBox4_CalendarExtender" runat="server" TargetControlID="TextBox4"
                    Format="dd/MM/yyyy"></asp:CalendarExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
        <br />
        <asp:Label ID="Label6" runat="server" Text="User Name"></asp:Label>
        <asp:TextBox ID="TextBox5" runat="server" Style="margin-left: 102px;"></asp:TextBox>
        <asp:Label ID="Label7" runat="server" Text="Is Administrator" Style="margin-left: 61px;"></asp:Label>
        <asp:CheckBox ID="CheckBox1" runat="server" Style="margin-left: 40px;" />
    </div>
    <div>
        <br />
        <asp:Label ID="Label8" runat="server" Text="Department"></asp:Label>
        <asp:DropDownList ID="drpDepartment" runat="server" Style="margin-left: 99px;" Height="31px"
            Width="174px">
            <asp:ListItem>==Select==</asp:ListItem>
        </asp:DropDownList>
        <asp:Label ID="Label9" runat="server" Text="Designation" Style="margin-left: 60px;"></asp:Label>
        <asp:DropDownList ID="drpDesignation" runat="server" Style="margin-left: 75px;" Height="25px"
            Width="170px">
            <asp:ListItem>==Select==</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div>
        <br />
        <asp:Label ID="Label10" runat="server" Text="Seat Location"></asp:Label>
        <asp:TextBox ID="TextBox6" runat="server" Style="margin-left: 86px;"></asp:TextBox>
        <asp:Label ID="Label11" runat="server" Text="Identity Card" Style="margin-left: 60px;"></asp:Label>
        <asp:TextBox ID="TextBox7" runat="server" Style="margin-left: 70px;" Enabled="False"></asp:TextBox>
    </div>
    <div>
        <br />
        <asp:Label ID="Label12" runat="server" Text=" Reporting Manager"></asp:Label>
        <asp:DropDownList ID="drpReportingManagers" runat="server" Style="margin-left: 49px;"
            Height="31px" Width="173px">
            <asp:ListItem>==Select==</asp:ListItem>
        </asp:DropDownList>
        <asp:Label ID="Label13" runat="server" Text="Project assigned" Style="margin-left: 60px;"></asp:Label>
        <asp:DropDownList ID="drpProjects" runat="server" Style="margin-left: 52px;" Height="21px"
            Width="179px">
            <asp:ListItem>==Select==</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div>
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Label ID="Label14" runat="server" Text="Date Of Birth"></asp:Label>
                <asp:TextBox ID="TextBox8" runat="server" Style="margin-left: 115px;" Enabled="False"></asp:TextBox>
                <asp:CalendarExtender ID="TextBox8_CalendarExtender" runat="server" TargetControlID="TextBox8"
                    Format="dd/MM/yyyy"></asp:CalendarExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
        <br />
        <asp:Label ID="Label15" runat="server" Text="Correspondance Address"></asp:Label>
        <asp:TextBox ID="TextBox9" runat="server" Style="margin-left: 15px;"></asp:TextBox>
        <asp:Label ID="Label16" runat="server" Text="Permanent Address" Style="margin-left: 55px;"></asp:Label>
        <asp:TextBox ID="TextBox10" runat="server" Style="margin-left: 35px;"></asp:TextBox>
    </div>
    <div>
        <br />
        <asp:Label ID="Label17" runat="server" Text="Phone Number"></asp:Label>
        <asp:TextBox ID="TextBox11" runat="server" Style="margin-left: 78px;"></asp:TextBox>
        <asp:Label ID="Label18" runat="server" Text="Mobile Number" Style="margin-left: 55px;"></asp:Label>
        <asp:TextBox ID="TextBox12" runat="server" Style="margin-left: 58px;"></asp:TextBox>
    </div>
    <div>
        <br />
        <asp:Label ID="Label19" runat="server" Text="Educational Qualification"></asp:Label>
        <asp:TextBox ID="TextBox13" runat="server" Style="margin-left: 10px;" Enabled="False"></asp:TextBox>
        <asp:Label ID="Label20" runat="server" Text="Skillset" Style="margin-left: 58px;"></asp:Label>
        <asp:TextBox ID="TextBox14" runat="server" Style="margin-left: 109px;"></asp:TextBox>
    </div>
    <div>
        <br />
        <asp:Label ID="Label21" runat="server" Text="Hobbies"></asp:Label>
        <asp:TextBox ID="TextBox15" runat="server" Style="margin-left: 120px;"></asp:TextBox>
        <asp:Label ID="Label22" runat="server" Text="Remarks" Style="margin-left: 60px;"></asp:Label>
        <asp:TextBox ID="TextBox16" runat="server" Style="margin-left: 100px;"></asp:TextBox>
    </div>
    <div>
    </div>
    <br />
    <asp:ImageButton ID="Button1" runat="server" ImageUrl="~/Images/edit_bt.gif" OnClick="Button1_Click"
        Style="height: 19px" />
    <asp:ImageButton ID="Button2" runat="server" ImageUrl="~/Images/reset_bt.gif" Style="margin-left: 100px" />
    <script language="javascript" type="text/javascript">
        $(function () {
            $("._datepicker").datepicker({ dateFormat: 'dd/mm/yy' });
        });
    </script>
</asp:Content>
