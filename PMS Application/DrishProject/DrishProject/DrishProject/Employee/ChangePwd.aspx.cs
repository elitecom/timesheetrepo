﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using DrishProjectBAL;
using System.Data;

namespace DrishProject.Employee
{
    public partial class ChangePwd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void imgBtnSave_Click(object sender, ImageClickEventArgs e)
        {
            Entities.Employee objUser = new Entities.Employee();
           
            if (txtOldPwd.Text == "")
            {
                LblException.Text = "Old Password required !";
                SetFocus(txtOldPwd);
                return;
            }
            else
            {
                LblException.Text = "";
            }
            if (txtNewPwd.Text == "")
            {
                LblException.Text = "New Password required !";
                SetFocus(txtNewPwd);
                return;
            }
            else
            {
                LblException.Text = "";
            }
            if (txtConfirmPwd.Text == "")
            {
                LblException.Text = "Confirm Password required !";
                SetFocus(txtConfirmPwd);
                return;
            }
            else
            {
                LblException.Text = "";
            }
            if (txtNewPwd.Text != txtConfirmPwd.Text)
            {
                LblException.Text = "Password not confirmed !";
                SetFocus(txtNewPwd);
                return;
            }
            else
            {
                LblException.Text = "";
            }

            objUser = (Entities.Employee)Session["emp"];
            int empid = new EmployeeBAL().GetEmployeeId(objUser);
            objUser.EmployeeId = empid;

            objUser.OldPassword = txtOldPwd.Text;
            objUser.NewPassword = txtNewPwd.Text;
            bool res;
            res = new EmployeeBAL().ChangePassword(objUser);
            if (res)
            {
                LblException.Text = "Password changed successfully !!";


                Entities.Employee objNewUser = new Entities.Employee();
                objNewUser = (Entities.Employee)Session["emp"];
                objNewUser.Password = txtNewPwd.Text;
                Session["emp"] = objNewUser;
                LblException.Text = "Password Changed Successfully";
            }
            else
            {
                LblException.Text = "Password Not Changed";
            }
            RegisterStartupScript("strRefresh", "<script language=\'javascript\'>window.opener.PageRefresh();</script>");
        }
    }
}