﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectBAL;

namespace DrishProject.DSR
{
    public partial class DSR : System.Web.UI.Page
    {
        Entities.Employee objUser = new Entities.Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Put user code to initialize the page here
            objUser = (Entities.Employee)(Session["emp"]);
            if (objUser.IsAministrator == 0)
            {
                //EmployeeAuthenticate();
                if (User.IsInRole("ProjectTimeSheets"))
                {
                    RegisterStartupScript("strShowTimeSheet", "<script language='javascript'>this.document.all.imgTimeSheets.style.display='block';</script>");
                }
                else
                {
                    RegisterStartupScript("strHideTimeSheet", "<script language='javascript'>this.document.all.imgTimeSheets.style.display='none';</script>");
                }
                if (User.IsInRole("ProjectWorkPlan"))
                {
                    RegisterStartupScript("strShowWorkPlan", "<script language='javascript'>this.document.all.imgWorkPlan.style.display='block';</script>");
                }
                else
                {
                    RegisterStartupScript("strHideWorkPlan", "<script language='javascript'>this.document.all.imgWorkPlan.style.display='none';</script>");
                }
                if (User.IsInRole("AddResourceHours"))
                {
                    RegisterStartupScript("strAddRes", "<script language='javascript'>this.document.all.imgAddRes.style.display='block';</script>");
                }
                else
                {
                    RegisterStartupScript("strAddRes", "<script language='javascript'>this.document.all.imgAddRes.style.display='none';</script>");
                }
                lblEmployee.Visible = false;
                ddlEmployee.Visible = false;
                RegisterStartupScript("strShowResDetail", "<script language='javascript'>this.document.all.imgResAllDetail.style.display='none';</script>");
            }
            else
            {
                RegisterStartupScript("strShowResDetail", "<script language='javascript'>this.document.all.imgResAllDetail.style.display='block';</script>");
            }
            if (!Page.IsPostBack)
            {
                try
                {
                    GetMonthsList();
                    GetEmployeeList();
                    //To make the Timesheet button Active
                    ImageButton img = new ImageButton();
                    img = (ImageButton)Page.FindControl("UC_Header1").FindControl("imgTimesheets");
                    img.ImageUrl = "..\\Images\\timesheet_active.gif";
                }
                catch (Exception ex)
                {
                    LblException.Text = ex.Message;
                }
            }
        }

        private void GetMonthsList()
        {
            try
            {
                DataSet DS = new DataSet();
                DS = new DsrBAL().GetDSRMonths(((Entities.Employee)(Session["emp"])).EmployeeId);
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        GrdDSR.DataSource = DS;
                    }
                }

                //Code to check whether the CurrentPageIndex value is not less than 0
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        if ((int) (DS.Tables[0].Rows.Count / GrdDSR.PageSize) - 1 < GrdDSR.CurrentPageIndex)
                        {
                            GrdDSR.CurrentPageIndex = 0;
                        }
                    }
                    else
                    {
                        GrdDSR.CurrentPageIndex = 0;
                    }
                }
                GrdDSR.DataBind();
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count == 0)
                    {
                        GrdDSR.PagerStyle.Visible = false;
                        LblException.Text = "No Month.";
                    }
                    else
                    {
                        GrdDSR.PagerStyle.Visible = true;
                        LblException.Text = "";
                    }
                }
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count <= GrdDSR.PageSize)
                    {
                        GrdDSR.PagerStyle.Visible = false;
                    }
                    else
                    {
                        GrdDSR.PagerStyle.Visible = true;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetMonthsListAccToDropdownValue()
        {
            try
            {
                DataSet DS = new DataSet();
                DS = new DsrBAL().GetDSRMonths(((Entities.Employee)(Session["emp"])).EmployeeId);
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        GrdDSR.DataSource = DS;
                    }
                }
                //Code to check whether the CurrentPageIndex value is not less than 0
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        if ((int)(DS.Tables[0].Rows.Count / GrdDSR.PageSize) - 1 < GrdDSR.CurrentPageIndex)
                        {
                            GrdDSR.CurrentPageIndex = 0;
                        }
                    }
                    else
                    {
                        GrdDSR.CurrentPageIndex = 0;
                    }
                }
                GrdDSR.DataBind();
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count == 0)
                    {
                        GrdDSR.PagerStyle.Visible = false;
                        LblException.Text = "No Month.";
                    }
                    else
                    {
                        GrdDSR.PagerStyle.Visible = true;
                        LblException.Text = "";
                    }
                }
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count <= GrdDSR.PageSize)
                    {
                        GrdDSR.PagerStyle.Visible = false;
                    }
                    else
                    {
                        GrdDSR.PagerStyle.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetEmployeeList()
        {
            try
            {
                DataSet DS = new DataSet();
                DS = new EmployeeBAL().GetEmployeeList();
                if (DS.Tables.Count > 0)
                {
                    if ((DS.Tables[0].Rows.Count > 0))
                    {
                        ddlEmployee.DataSource = DS;
                        ddlEmployee.DataValueField = "EmpID";
                        ddlEmployee.DataTextField = "Name";
                        ddlEmployee.DataBind();
                        ddlEmployee.SelectedValue = objUser.EmployeeId.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ImgbtnMonth_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //DataSet DS = new DataSet();
                //if (objTimesheet.MonthExists(objUser.EmpId, System.DateTime.Now) == true)
                //{
                //    //DS = objTimesheet.InsertMonth(objUser.EmpId)
                //    DS = objTimesheet.InsertMonthV2(objUser.EmpId);
                //    if (DS.Tables.Count > 0)
                //    {
                //        if ((DS.Tables[0].Rows.Count > 0))
                //        {
                //            GrdDSR.DataSource = DS;
                //            GrdDSR.DataBind();
                //            Response.Redirect("DSR.aspx");
                //        }
                //    }
                //}
                //else
                //{
                //    LblException.Text = "Current month exists.";
                //    return;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}