﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using DrishProjectBAL;
using System.Data;
using System.Data.SqlClient;

namespace DrishProject.DSR
{
    public partial class MyDSR : System.Web.UI.Page
    {
        Entities.Employee objUser = new Entities.Employee();
        Entities.Project objProjects = new Entities.Project();
        Entities.ProjectModule objModule = new Entities.ProjectModule();
        Entities.DSR objTs = new Entities.DSR();

        bool blnShouldRun;
        bool blnIsOld;
        DataSet dsDates = new DataSet();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Put user code to initialize the page here
            blnShouldRun = false;
            blnIsOld = false;
            objUser = (Entities.Employee)(Session["emp"]);
            //Page.SmartNavigation = True
            if (objUser.IsAministrator == 0)
            {
                //EmployeeAuthenticate();
                if (User.IsInRole("ProjectTimeSheets"))
                {
                    RegisterStartupScript("strShowTimeSheet", "<script language='javascript'>this.document.all.imgTimeSheets.style.display='block';</script>");
                }
                else
                {
                    RegisterStartupScript("strHideTimeSheet", "<script language='javascript'>this.document.all.imgTimeSheets.style.display='none';</script>");
                }
                if (User.IsInRole("ProjectWorkPlan"))
                {
                    RegisterStartupScript("strShowWorkPlan", "<script language='javascript'>this.document.all.imgWorkPlan.style.display='block';</script>");
                }
                else
                {
                    RegisterStartupScript("strHideWorkPlan", "<script language='javascript'>this.document.all.imgWorkPlan.style.display='none';</script>");
                }
            }
            int UserID = objUser.EmployeeId;
            if ((Request["EmpID"] != null))
            {
                objUser.EmployeeId = Convert.ToInt32(Request["EmpID"].ToString());
            }


            if (!Page.IsPostBack)
            {
                this.txtMonth.Text = Request["Month"];
                this.txtYear.Text = Request["Year"];
                System.DateTime dCurrent = default(System.DateTime);
                if (string.IsNullOrEmpty(this.txtMonth.Text))
                {
                    dCurrent = System.DateTime.Today;
                    this.ClndMonth.VisibleDate = dCurrent;
                    dtlProjects.Visible = false;
                    btnSaveDSR.Visible = false;
                    btnSaveDSR1.Visible = false;
                    btnSendDSR.Visible = false;
                    btnSendDSR1.Visible = false;
                    this.lblException.Text = "No Project Assigned. Contact Administrator !";
                    this.lblDate.Text = dCurrent.ToLongDateString();
                    this.ClndMonth.SelectedDate = dCurrent;
                    this.lblDate.Text = DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Day.ToString() + ", " + DateTime.Now.Year.ToString();
                    this.txtYear.Text = DateTime.Now.Year.ToString();
                    ClndMonth_SelectionChanged(sender, e);
                }
                else
                {
                    dCurrent = System.DateTime.Now.AddDays(1);
                    //dCurrent = txtMonth.Text + "1" + txtYear.Text;
                    this.ClndMonth.VisibleDate = dCurrent;
                    dtlProjects.Visible = false;
                    btnSaveDSR.Visible = false;
                    btnSaveDSR1.Visible = false;
                    btnSendDSR.Visible = false;
                    btnSendDSR1.Visible = false;
                    this.lblException.Text = "No Project Assigned. Contact Administrator !";
                    this.lblDate.Text = DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Day.ToString() + ", " + DateTime.Now.Year.ToString();
                    this.ClndMonth.SelectedDate = dCurrent;
                    ClndMonth_SelectionChanged(sender, e);
                }
                //To make the Timesheet button Active
                //ImageButton img = new ImageButton();
                //img = (ImageButton)Page.FindControl("UC_Header1").FindControl("imgTimesheets");
                //img.ImageUrl = "..\\Images\\timesheet_active.gif";
            }
            btnSendDSR.Attributes.Add("OnClick", "return ConfirmSendDSR(this);");
            btnSendDSR1.Attributes.Add("OnClick", "return ConfirmSendDSR(this);");
        }

        private void ClndMonth_SelectionChanged(System.Object sender, System.EventArgs e)
        {
            GetDatedDSR();
        }

        protected void btnSaveDSR_Click(object sender, ImageClickEventArgs e)
        {
            #region "Unused Code"

            //bool blnIsError = false;
            //blnIsError = false;
            //bool blnIsFilled = false;
            //blnIsFilled = false;
            //string strErrorTrace = null;
            //strErrorTrace = "";

            //int iProjectID = 0;
            //int iModuleID = 0;
            //string strHrsSpent = null;
            //string strWorkDone = null;
            //System.DateTime dtDSRDate = default(System.DateTime);
            //System.DateTime dtModifiedOn = default(System.DateTime);
            //TextBox txtTemp = new TextBox();
            //Label lblProjects = new Label();
            //Label lblModules = new Label();
            //Label lblProjectName = new Label();
            //Label lblModuleName = new Label();
            //string HrsSpent = null;
            //string WorkDone = null;
            //int iCount = 0;
            //string SelectName = null;

            //for (iCount = 0; iCount <= dtlProjects.Items.Count - 1; iCount++) 
            //{
            //    lblProjects = (Label)(dtlProjects.Items[iCount].FindControl("msgProjects"));				

            //    txtTemp = (TextBox)(dtlProjects.Items[iCount].FindControl("txtHrsSpentP"));
            //    HrsSpent = txtTemp.Text;
            //    txtTemp = (TextBox)(dtlProjects.Items[iCount].FindControl("txtWorkDoneP"));
            //    WorkDone = txtTemp.Text;

            //    txtTemp = (TextBox)(dtlProjects.Items[iCount].FindControl("txtProjectIdP"));
            //    iProjectID = Convert.ToInt32(txtTemp.Text);
            //    txtTemp = (TextBox)(dtlProjects.Items[iCount].FindControl("txtModuleIdP"));
            //    iModuleID = Convert.ToInt32(txtTemp.Text);

            //    objProjects.ProjectID = iProjectID;
            //    objProjects = new ProjectBAL().GetProject(iProjectID);
            //    SelectName = "project - " + objProjects.ProjectCode + " !";
            //    //lblProjectName.Text = objProjects.ProjectCode
            //    lblProjectName = (Label)(dtlProjects.Items[iCount].FindControl("lblProject"));
            //    if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone)) 
            //    {       
            //        int result;
            //        if (int.TryParse(HrsSpent, out result))
            //        //if ( !Information.IsNumeric(HrsSpent)) 
            //        {
            //            lblProjects.Text = "Please fill valid Hrs for the " + SelectName + "";
            //            blnIsError = true;
            //            if (string.IsNullOrEmpty(strErrorTrace)) 
            //            {
            //                strErrorTrace = lblProjectName.Text;
            //            } 
            //            else 
            //            {
            //                strErrorTrace = strErrorTrace + ", " + lblProjectName.Text;
            //            }
            //            goto Modules;
            //        }
            //        //Save DSR for Project
            //        bool res;
            //        res = new DsrBAL().saveDSR(objUser.EmployeeId, iProjectID, iModuleID, Convert.ToDouble(HrsSpent), WorkDone, ClndMonth.SelectedDate, System.DateTime.Now);
            //        lblProjects.Text = "";
            //        blnIsFilled = true;

            //    } 
            //    else if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & string.IsNullOrEmpty(WorkDone)) 
            //    {
            //        lblProjects.Text = "Please fill Work Done.";
            //        blnIsError = true;
            //        if (string.IsNullOrEmpty(strErrorTrace)) 
            //        {
            //            strErrorTrace = lblProjectName.Text;
            //        } 
            //        else 
            //        {
            //            strErrorTrace = strErrorTrace + ", " + lblProjectName.Text;
            //        }
            //        goto Modules;
            //    } 
            //    else if ((string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone)) 
            //    {
            //        lblProjects.Text = "Please fill Hrs Spent !";
            //        blnIsError = true;
            //        if (string.IsNullOrEmpty(strErrorTrace)) 
            //        {
            //            strErrorTrace = lblProjectName.Text;
            //        } 
            //        else 
            //        {
            //            strErrorTrace = strErrorTrace + ", " + lblProjectName.Text;
            //        }
            //        goto Modules;
            //    } 
            //    else 
            //    {
            //        //delete
            //        new DsrBAL().DeleteDSR(objUser.EmployeeId, iProjectID, iModuleID, ClndMonth.SelectedDate);
            //    }
            //    lblProjects.Text = "";
            //}
            //Modules:
            ////This is the GoTo destination
            //DataList dtlrunMod = new DataList();
            //dtlrunMod = (DataList)(dtlProjects.Items[iCount].FindControl("dtlModules"));
            //try 
            //{
            //    int iCountM = 0;
            //    for (iCountM = 0; iCountM <= dtlrunMod.Items.Count - 1; iCountM++) 
            //    {
            //        lblModuleName = (Label)(dtlrunMod.Items[iCountM].FindControl("lblModule"));
            //        lblModules = (Label)(dtlrunMod.Items[iCountM].FindControl("msgModules"));

            //        txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtHrsSpentM"));
            //        HrsSpent = txtTemp.Text;
            //        txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtWorkDoneM"));
            //        WorkDone = txtTemp.Text;
            //        txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtProjectIdM"));
            //        iProjectID = Convert.ToInt32(txtTemp.Text);
            //        txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtModuleIdM"));
            //        iModuleID = Convert.ToInt32(txtTemp.Text);

            //        lblModules.Text = "Please fill valid Hrs spent.";
            //        objModule.ModuleId = iModuleID;
            //        objModule = new ProjectModuleBAL().GetModule(objModule);
            //        SelectName = "module - " + objModule.ModuleName + " !";
            //        //lblModuleName.Text = objModule.ModuleName

            //        if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone)) 
            //        {                            
            //            //if (!Information.IsNumeric(HrsSpent)) {
            //                int result;
            //                if (int.TryParse(HrsSpent, out result))
            //                {
            //                    lblModules.Text = "Please fill valid Hrs for the " + SelectName + "";
            //                    blnIsError = true;
            //                    if (string.IsNullOrEmpty(strErrorTrace)) {
            //                        strErrorTrace = lblModuleName.Text;
            //                    } else {
            //                        strErrorTrace = strErrorTrace + ", " + lblModuleName.Text;
            //                    }
            //                    goto Projects;
            //                }

            //                //Save DSR for Module
            //                bool res = new DsrBAL().saveDSR(objUser.EmployeeId, iProjectID, iModuleID, Convert.ToDouble(HrsSpent), WorkDone, ClndMonth.SelectedDate, System.DateTime.Now);
            //                lblModules.Text = "";
            //                blnIsFilled = true;
            //            } 
            //            else 
            //            {
            //                lblModules.Text = "";
            //                lblModuleName = (Label)(dtlrunMod.Items[iCountM].FindControl("lblModule"));
            //                if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & string.IsNullOrEmpty(WorkDone)) 
            //                {
            //                    lblModules.Text = "Please fill Work Done.";
            //                    blnIsError = true;
            //                    if (string.IsNullOrEmpty(strErrorTrace)) 
            //                    {
            //                        strErrorTrace = lblModuleName.Text;
            //                    } 
            //                    else 
            //                    {
            //                        strErrorTrace = strErrorTrace + ", " + lblModuleName.Text;
            //                    }
            //                    goto Projects;
            //                } 
            //                else if ((string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone)) 
            //                {
            //                    lblModules.Text = "Please fill Hrs Spent !";
            //                    blnIsError = true;
            //                    if (string.IsNullOrEmpty(strErrorTrace)) 
            //                    {
            //                        strErrorTrace = lblModuleName.Text;
            //                    } 
            //                    else 
            //                    {
            //                        strErrorTrace = strErrorTrace + ", " + lblModuleName.Text;
            //                    }
            //                    goto Projects;
            //                } 
            //                else 
            //                {
            //                    //delete
            //                    bool res;
            //                    res = new DsrBAL().DeleteDSR(objUser.EmployeeId, iProjectID, iModuleID, ClndMonth.SelectedDate);
            //                }                           
            //            }
            //        Projects:
            //        }
            //            //This is the GoTo destination		
            //         //Projects:

            //    catch (Exception ex) 
            //    {
            //    }
            //}
            //if (blnIsError == true) 
            //{
            //    lblException.Text = "DSR saved! Please note the comments below for " + strErrorTrace + ".";
            //} 
            //else 
            //{
            //    if (blnIsFilled == true) 
            //    {
            //        lblException.Text = "DSR successfully saved for today !";
            //        blnIsFilled = false;
            //    } 
            //    else 
            //    {
            //        lblException.Text = "No data is available to save !";

            //    }
            //}
            #endregion
            bool blnIsError = false;
            blnIsError = false;
            bool blnIsFilled = false;
            blnIsFilled = false;
            string strErrorTrace = null;
            strErrorTrace = "";

            int iProjectID = 0;
            int iModuleID = 0;
            string strHrsSpent = null;
            string strWorkDone = null;
            System.DateTime dtDSRDate = System.DateTime.Now;
            System.DateTime dtModifiedOn = System.DateTime.Now;
            TextBox txtTemp = new TextBox();
            Label lblProjects = new Label();
            Label lblModules = new Label();
            Label lblProjectName = new Label();
            Label lblModuleName = new Label();
            string HrsSpent = null;
            string WorkDone = null;

            int iCount = 0;
            for (iCount = 0; iCount <= dtlProjects.Items.Count - 1; iCount++)
            {
                lblProjects = (Label)(dtlProjects.Items[iCount].FindControl("msgProjects"));

                txtTemp = (TextBox)(dtlProjects.Items[iCount].FindControl("txtHrsSpentP"));
                HrsSpent = txtTemp.Text;
                txtTemp = (TextBox)(dtlProjects.Items[iCount].FindControl("txtWorkDoneP"));
                WorkDone = txtTemp.Text;

                txtTemp = (TextBox)(dtlProjects.Items[iCount].FindControl("txtProjectIdP"));
                iProjectID = Convert.ToInt32(txtTemp.Text);
                txtTemp = (TextBox)(dtlProjects.Items[iCount].FindControl("txtModuleIdP"));
                iModuleID = Convert.ToInt32(txtTemp.Text);

                string SelectName = null;
                objProjects.ProjectID = iProjectID;
                objProjects = new ProjectBAL().GetProject(iProjectID);
                SelectName = "Project - " + objProjects.ProjectCode + " !";
                //lblProjectName.Text = objProjects.ProjectCode
                lblProjectName = (Label)(dtlProjects.Items[iCount].FindControl("lblProject"));
                if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone))
                {
                    int result;
                    if (int.TryParse(HrsSpent, out result))
                    {
                        lblProjects.Text = "Please fill valid Hrs for the " + SelectName + "";
                        blnIsError = true;
                        if (string.IsNullOrEmpty(strErrorTrace))
                        {
                            strErrorTrace = (lblProjectName.Text);
                        }
                        else
                        {
                            strErrorTrace = strErrorTrace + ", " + lblProjectName.Text;
                        }
                        goto Modules;
                    }
                    //Save DSR for Project
                    bool res;
                    res = new DsrBAL().saveDSR(objUser.EmployeeId, iProjectID, iModuleID, Convert.ToDouble(HrsSpent), WorkDone, ClndMonth.SelectedDate, System.DateTime.Now);
                    lblProjects.Text = "";
                    blnIsFilled = true;
                }
                else
                {
                    if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & string.IsNullOrEmpty(WorkDone))
                    {
                        lblProjects.Text = "Please fill Work Done.";
                        blnIsError = true;
                        if (string.IsNullOrEmpty(strErrorTrace))
                        {
                            strErrorTrace = lblProjectName.Text;
                        }
                        else
                        {
                            strErrorTrace = strErrorTrace + ", " + lblProjectName.Text;
                        }
                        goto Modules;
                    }
                    else if ((string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone))
                    {
                        lblProjects.Text = "Please fill Hrs Spent !";
                        blnIsError = true;
                        if (string.IsNullOrEmpty(strErrorTrace))
                        {
                            strErrorTrace = lblProjectName.Text;
                        }
                        else
                        {
                            strErrorTrace = strErrorTrace + ", " + lblProjectName.Text;
                        }
                        goto Modules;
                    }
                    else
                    {
                        //delete
                        bool res;
                        res = new DsrBAL().DeleteDSR(objUser.EmployeeId, iProjectID, iModuleID, ClndMonth.SelectedDate);
                    }
                    lblProjects.Text = "";
                }
            Modules:
                //This is the GoTo destination
                DataList dtlrunMod = new DataList();
                dtlrunMod = (DataList)(dtlProjects.Items[iCount].FindControl("dtlModules"));
                try
                {
                    int iCountM = 0;
                    for (iCountM = 0; iCountM <= dtlrunMod.Items.Count - 1; iCountM++)
                    {
                        lblModuleName = (Label)(dtlrunMod.Items[iCountM].FindControl("lblModule"));
                        lblModules = (Label)(dtlrunMod.Items[iCountM].FindControl("msgModules"));

                        txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtHrsSpentM"));
                        HrsSpent = (txtTemp.Text);
                        txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtWorkDoneM"));
                        WorkDone = (txtTemp.Text);

                        txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtProjectIdM"));
                        iProjectID = Convert.ToInt32(txtTemp.Text);
                        txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtModuleIdM"));
                        iModuleID = Convert.ToInt32(txtTemp.Text);

                        lblModules.Text = "Please fill valid Hrs spent.";
                        objModule.ModuleId = iModuleID;
                        objModule = new ProjectModuleBAL().GetModule(objModule);
                        SelectName = ("module - " + objModule.ModuleName) + " !";
                        //lblModuleName.Text = objModule.ModuleName

                        if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone))
                        {
                            //if (!Information.IsNumeric(HrsSpent)) {
                            int result;
                            if (int.TryParse(HrsSpent, out result))
                            {
                                lblModules.Text = "Please fill valid Hrs for the " + SelectName + "";
                                blnIsError = true;
                                if (string.IsNullOrEmpty(strErrorTrace))
                                {
                                    strErrorTrace = (lblModuleName.Text);
                                    // & "(" & Trim(objProjects.ProjectCode) & ")"
                                }
                                else
                                {
                                    strErrorTrace = strErrorTrace + ", " + lblModuleName.Text;
                                }
                                goto Projects;
                            }

                            //Save DSR for Module
                            bool res;
                            res = new DsrBAL().saveDSR(objUser.EmployeeId, iProjectID, iModuleID, Convert.ToDouble(HrsSpent), WorkDone, ClndMonth.SelectedDate, System.DateTime.Now);
                            lblModules.Text = "";
                            blnIsFilled = true;
                        }
                        else
                        {
                            lblModules.Text = "";
                            lblModuleName = (Label)(dtlrunMod.Items[iCountM].FindControl("lblModule"));
                            if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & string.IsNullOrEmpty(WorkDone))
                            {
                                lblModules.Text = "Please fill Work Done.";
                                blnIsError = true;
                                if (string.IsNullOrEmpty(strErrorTrace))
                                {
                                    strErrorTrace = lblModuleName.Text;
                                }
                                else
                                {
                                    strErrorTrace = strErrorTrace + ", " + lblModuleName.Text;
                                }
                                goto Projects;
                            }
                            else if ((string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone))
                            {
                                lblModules.Text = "Please fill Hrs Spent !";
                                blnIsError = true;
                                if (string.IsNullOrEmpty(strErrorTrace))
                                {
                                    strErrorTrace = lblModuleName.Text;
                                }
                                else
                                {
                                    strErrorTrace = strErrorTrace + ", " + lblModuleName.Text;
                                }
                                goto Projects;
                            }
                            else
                            {
                                //delete
                                bool res;
                                res = new DsrBAL().DeleteDSR(objUser.EmployeeId, iProjectID, iModuleID, ClndMonth.SelectedDate);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            Projects:
                //This is the GoTo destination
                if (blnIsError == true)
                {
                    lblException.Text = "DSR saved! Please note the comments below for " + strErrorTrace + ".";
                }
                else
                {
                    if (blnIsFilled == true)
                    {
                        lblException.Text = "DSR successfully saved for today !";
                        blnIsFilled = false;
                    }
                    else
                    {
                        lblException.Text = "No data is available to save !";
                    }
                }
            }
        }

        protected void btnSendDSR_Click(object sender, ImageClickEventArgs e)
        {
            try 
            {
				bool blnIsError = false;
				blnIsError = false;
				bool blnIsFilled = false;
				blnIsFilled = false;
				string strErrorTrace = null;
				strErrorTrace = "";

				int iProjectID = 0;
				int iModuleID = 0;
				string strHrsSpent = null;
				string strWorkDone = null;
				System.DateTime dtDSRDate = default(System.DateTime);
				System.DateTime dtModifiedOn = default(System.DateTime);
				TextBox txtTemp = new TextBox();
				ImageButton imgTemp = new ImageButton();
				Label lblProjects = new Label();
				Label lblModules = new Label();
				Label lblProjectName = new Label();
				Label lblModuleName = new Label();
				Label lblModText = new Label();
				int iCount = 0;
				string strBody = null;
				string strCompleteDSR = null;

                for (int iCountM = 0; iCountM <= dtlProjects.Items.Count - 1; iCountM++)
                {
                    strBody = "";
                    strCompleteDSR = "";

                    lblProjects = (Label)(dtlProjects.Items[iCountM].FindControl("msgProjects"));
                    string HrsSpent = null;
                    string WorkDone = null;

                    txtTemp = (TextBox)(dtlProjects.Items[iCountM].FindControl("txtHrsSpentP"));
                    HrsSpent = txtTemp.Text;
                    txtTemp = (TextBox)(dtlProjects.Items[iCountM].FindControl("txtWorkDoneP"));
                    WorkDone = txtTemp.Text;

                    txtTemp = (TextBox)(dtlProjects.Items[iCountM].FindControl("txtProjectIdP"));
                    iProjectID = Convert.ToInt32(txtTemp.Text);
                    txtTemp = (TextBox)(dtlProjects.Items[iCountM].FindControl("txtModuleIdP"));
                    iModuleID = Convert.ToInt32(txtTemp.Text);

                    string SelectName = null;
                    objProjects.ProjectID = iProjectID;
                    objProjects = new ProjectBAL().GetProject(iProjectID);
                    SelectName = ("project - " + objProjects.ProjectCode) + " !";
                    //lblProjectName.Text = objProjects.ProjectCode
                    lblProjectName = (Label)(dtlProjects.Items[iCountM].FindControl("lblProject"));
                    if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone))
                    {
                        //if (!Information.IsNumeric(HrsSpent)) {
                        int result;
                        if (int.TryParse(HrsSpent, out result))
                        {
                            lblProjects.Text = "Please fill valid Hrs for the " + SelectName + "";
                            blnIsError = true;
                            if (string.IsNullOrEmpty(strErrorTrace))
                            {
                                strErrorTrace = lblProjectName.Text;
                            }
                            else
                            {
                                strErrorTrace = strErrorTrace + ", " + lblProjectName.Text;
                            }
                            goto Modules1;
                        }
                        //Save DSR for Project
                        bool res;
                        res = new DsrBAL().saveDSR(objUser.EmployeeId, iProjectID, iModuleID, Convert.ToDouble(HrsSpent), WorkDone, ClndMonth.SelectedDate, System.DateTime.Now);

                        //imgTemp = dtlProjects.Items[iCountM].FindControl("cmdDeleteDSRP")
                        imgTemp.Enabled = false;

                        lblProjects.Text = "";
                        blnIsFilled = true;

                    }
                    else
                    {
                        if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & string.IsNullOrEmpty(WorkDone))
                        {
                            lblProjects.Text = "Please fill Work Done.";
                            blnIsError = true;
                            if (string.IsNullOrEmpty(strErrorTrace))
                            {
                                strErrorTrace = lblProjectName.Text;
                            }
                            else
                            {
                                strErrorTrace = strErrorTrace + ", " + lblProjectName.Text;
                            }
                            goto Modules1;
                        }
                        else if ((string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone))
                        {
                            lblProjects.Text = "Please fill Hrs Spent !";
                            blnIsError = true;
                            if (string.IsNullOrEmpty(strErrorTrace))
                            {
                                strErrorTrace = lblProjectName.Text;
                            }
                            else
                            {
                                strErrorTrace = strErrorTrace + ", " + lblProjectName.Text;
                            }
                            goto Modules1;
                        }
                        else
                        {
                            //delete
                            bool res;
                            res = new DsrBAL().DeleteDSR(objUser.EmployeeId, iProjectID, iModuleID, ClndMonth.SelectedDate);
                        }
                        lblProjects.Text = "";
                    }
                Modules1:
                    //This is the GoTo destination

                    DataList dtlrunMod = new DataList();
                    dtlrunMod = (DataList)(dtlProjects.Items[iCountM].FindControl("dtlModules"));
                    try
                    {
                        //int iCountM = 0;
                        for (iCountM = 0; iCountM <= dtlrunMod.Items.Count - 1; iCountM++)
                        {
                            lblModuleName = (Label)(dtlrunMod.Items[iCountM].FindControl("lblModule"));
                            lblModules = (Label)(dtlrunMod.Items[iCountM].FindControl("msgModules"));

                            txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtHrsSpentM"));
                            HrsSpent = txtTemp.Text;
                            txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtWorkDoneM"));
                            WorkDone = txtTemp.Text;

                            txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtProjectIdM"));
                            iProjectID = Convert.ToInt32(txtTemp.Text);
                            txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtModuleIdM"));
                            iModuleID = Convert.ToInt32(txtTemp.Text);

                            lblModules.Text = "Please fill valid Hrs spent.";
                            objModule.ModuleId = iModuleID;
                            objModule = new ProjectModuleBAL().GetModule(objModule);
                            SelectName = "module - " + objModule.ModuleName + " !";

                            if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone))
                            {
                                //if (!Information.IsNumeric(HrsSpent)) {
                                int result;
                                if (int.TryParse(HrsSpent, out result))
                                {
                                    lblModules.Text = "Please fill valid Hrs for the " + SelectName + "";
                                    blnIsError = true;
                                    if (string.IsNullOrEmpty(strErrorTrace))
                                    {
                                        strErrorTrace = (lblModuleName.Text);
                                    }
                                    else
                                    {
                                        strErrorTrace = strErrorTrace + ", " + lblModuleName.Text;
                                    }
                                    goto Projects1;
                                }

                                //Save DSR for Module
                                bool res;
                                res = new DsrBAL().saveDSR(objUser.EmployeeId, iProjectID, iModuleID, Convert.ToDouble(HrsSpent), WorkDone, ClndMonth.SelectedDate, System.DateTime.Now);
                                imgTemp.Enabled = false;
                                lblModules.Text = "";
                                blnIsFilled = true;
                            }
                            else
                            {
                                lblModules.Text = "";
                                lblModuleName = (Label)(dtlrunMod.Items[iCountM].FindControl("lblModule"));
                                if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & string.IsNullOrEmpty(WorkDone))
                                {
                                    lblModules.Text = "Please fill Work Done.";
                                    blnIsError = true;
                                    if (string.IsNullOrEmpty(strErrorTrace))
                                    {
                                        strErrorTrace = lblModuleName.Text;
                                    }
                                    else
                                    {
                                        strErrorTrace = strErrorTrace + ", " + lblModuleName.Text;
                                    }
                                    goto Projects1;
                                }
                                else if ((string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone))
                                {
                                    lblModules.Text = "Please fill Hrs Spent !";
                                    blnIsError = true;
                                    if (string.IsNullOrEmpty(strErrorTrace))
                                    {
                                        strErrorTrace = lblModuleName.Text;
                                    }
                                    else
                                    {
                                        strErrorTrace = strErrorTrace + ", " + lblModuleName.Text;
                                    }
                                    goto Projects1;
                                }
                                else
                                {
                                    //delete
                                    bool res;
                                    res = new DsrBAL().DeleteDSR(objUser.EmployeeId, iProjectID, iModuleID, ClndMonth.SelectedDate);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                Projects1:
                    //This is the GoTo destination

                    if (blnIsError == true)
                    {
                        if (blnIsFilled == false)
                        {
                            lblException.Text = "No data to send in DSR ";
                            return;
                        }
                        else
                        {
                            lblException.Text = "DSR saved! Please note the comments below for " + strErrorTrace + ".";
                            return;
                        }
                    }
                    else
                    {
                        if (blnIsFilled == false)
                        {
                            lblException.Text = "No data to send in DSR !";
                            return;
                        }
                        else
                        {
                            //Send mail case
                            System.IO.StreamReader objReader = null;
                            //File objFile = null;
                            string FullPath = null;
                            string strBodyContents = null;
                            FullPath = Server.MapPath("./Mails/SendDSR.txt");
                            objReader = System.IO.File.OpenText(FullPath);
                            strBodyContents = objReader.ReadToEnd();
                            objReader.Close();

                            for (iCountM = 0; iCountM <= dtlProjects.Items.Count - 1; iCountM++)
                            {
                                strBody = "";
                                strCompleteDSR = "";

                                lblProjects = (Label)(dtlProjects.Items[iCountM].FindControl("msgProjects"));
                                HrsSpent = null;
                                WorkDone = null;

                                txtTemp = (TextBox)(dtlProjects.Items[iCountM].FindControl("txtHrsSpentP"));
                                HrsSpent = (txtTemp.Text);
                                txtTemp = (TextBox)(dtlProjects.Items[iCountM].FindControl("txtWorkDoneP"));
                                WorkDone = (txtTemp.Text);
                                WorkDone = WorkDone.Replace("vbCrLf", "<BR>");
                                WorkDone = Server.HtmlEncode(WorkDone);
                                WorkDone = WorkDone.Replace("&lt;BR&gt;", "<BR>");

                                txtTemp = (TextBox)(dtlProjects.Items[iCountM].FindControl("txtProjectIdP"));
                                iProjectID = Convert.ToInt32(txtTemp.Text);
                                txtTemp = (TextBox)(dtlProjects.Items[iCountM].FindControl("txtModuleIdP"));
                                iModuleID = Convert.ToInt32(txtTemp.Text);

                                SelectName = null;
                                objProjects.ProjectID = iProjectID;
                                objProjects = new ProjectBAL().GetProject(iProjectID);
                                SelectName = ("project - " + objProjects.ProjectCode) + " !";
                                //lblProjectName.Text = objProjects.ProjectCode
                                lblProjectName = (Label)(dtlProjects.Items[iCountM].FindControl("lblProject"));
                                if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone))
                                {
                                    strBody = strBodyContents;
                                    strBody = strBody.Replace("<<Name>>", "Project");
                                    strBody = strBody.Replace("<<Project/ModuleName>>", lblProjectName.Text);
                                    strBody = strBody.Replace("<<HrsSpent>>", HrsSpent);
                                    strBody = strBody.Replace("<<WorkDone>>", WorkDone);
                                    strCompleteDSR = strCompleteDSR + strBody;

                                    lblProjects.Text = "";
                                    blnIsFilled = true;
                                }
                                else
                                {
                                    lblProjects.Text = "";
                                }
                                imgTemp = (ImageButton)(dtlProjects.Items[iCountM].FindControl("cmdDeleteDSRP"));
                                imgTemp.Visible = false;
                                //imgbtnModules
                                imgTemp = (ImageButton)(dtlProjects.Items[iCountM].FindControl("imgbtnModules"));
                                imgTemp.Visible = false;
                                lblModText = (Label)(dtlProjects.Items[iCountM].FindControl("lblModuleText"));
                                lblModText.Text = "";
                            Modules:

                                //This is the GoTo destination

                                dtlrunMod = new DataList();
                                dtlrunMod = (DataList)(dtlProjects.Items[iCountM].FindControl("dtlModules"));
                                try
                                {
                                    iCountM = 0;
                                    for (iCountM = 0; iCountM <= dtlrunMod.Items.Count - 1; iCountM++)
                                    {
                                        lblModuleName = (Label)(dtlrunMod.Items[iCountM].FindControl("lblModule"));
                                        lblModules = (Label)(dtlrunMod.Items[iCountM].FindControl("msgModules"));

                                        txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtHrsSpentM"));
                                        HrsSpent = (txtTemp.Text);
                                        txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtWorkDoneM"));
                                        WorkDone = (txtTemp.Text);
                                        WorkDone = WorkDone.Replace("vbCrLf", "<BR>");
                                        WorkDone = Server.HtmlEncode(WorkDone);
                                        WorkDone = WorkDone.Replace("&lt;BR&gt;", "<BR>");
                                        txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtProjectIdM"));
                                        iProjectID = Convert.ToInt32(txtTemp.Text);
                                        txtTemp = (TextBox)(dtlrunMod.Items[iCountM].FindControl("txtModuleIdM"));
                                        iModuleID = Convert.ToInt32(txtTemp.Text);

                                        lblModules.Text = "Please fill valid Hrs spent.";
                                        objModule.ModuleId = iModuleID;
                                        objModule = new ProjectModuleBAL().GetModule(objModule);
                                        SelectName = ("module - " + objModule.ModuleName) + " !";
                                        //lblModuleName.Text = objModule.ModuleName

                                        if (!(string.IsNullOrEmpty(HrsSpent) | HrsSpent == "0") & !string.IsNullOrEmpty(WorkDone))
                                        {
                                            lblModules.Text = "";
                                            blnIsFilled = true;
                                            strBody = strBodyContents;
                                            strBody = strBody.Replace("<<Name>>", "Module");
                                            strBody = strBody.Replace("<<Project/ModuleName>>", lblModuleName.Text);
                                            strBody = strBody.Replace("<<HrsSpent>>", HrsSpent);
                                            strBody = strBody.Replace("<<WorkDone>>", WorkDone);
                                            strCompleteDSR = strCompleteDSR + strBody;
                                        }
                                        else
                                        {
                                            lblModules.Text = "";
                                            lblModuleName = (Label)(dtlrunMod.Items[iCountM].FindControl("lblModule"));
                                        }
                                        //imgTemp = dtlrunMod.Items[iCountM].FindControl("cmdDeleteDSRM");
                                        //imgTemp.Visible = false;
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                            Projects:
                                //This is the GoTo destination	
                                //Send Mail here
                                SendMail(strCompleteDSR, iProjectID);
                            }

                        }

                    }
                    this.lblException.Text = "DSR saved and sent succesfully !";
                    bool res1;
                    res1 = new DsrBAL().UpdateIfDSRSent(objUser.EmployeeId, System.DateTime.Today);
                    this.btnSendDSR.Visible = false;
                    this.btnSaveDSR.Visible = false;
                    this.btnSendDSR1.Visible = false;
                    this.btnSaveDSR1.Visible = false;
                }
			} 
            catch (Exception ex) 
            {
				lblException.Text = ex.Message;
			}
        }

        protected void btnSaveDSR1_Click(object sender, ImageClickEventArgs e)
        {
            btnSaveDSR_Click(btnSaveDSR, e);
        }

        protected void btnSendDSR1_Click(object sender, ImageClickEventArgs e)
        {
            btnSendDSR_Click(btnSendDSR, e);
        }

        protected void ClndMonth_DayRender(object sender, DayRenderEventArgs e)
        {
            try
            {
                dynamic WhiteColor = null;
                dynamic RedColor = null;
                dynamic GreenColor = null;
                WhiteColor = System.Configuration.ConfigurationManager.AppSettings["White"].ToString();
                RedColor = System.Configuration.ConfigurationManager.AppSettings["Red"].ToString();
                GreenColor = System.Configuration.ConfigurationManager.AppSettings["Green"].ToString();
                //Entities.DSR objTS = new Entities.DSR();
                objUser = (Entities.Employee)(Session["emp"]);
                if (e.Day.Date > DateTime.Today | e.Day.IsOtherMonth == true)
                {
                    e.Cell.BackColor = System.Drawing.Color.FromArgb(WhiteColor(0), WhiteColor(1), WhiteColor(2));
                }
                else
                {
                    if (new DsrBAL().checkIfPresent(objUser.EmployeeId, e.Day.Date))
                    {
                        e.Cell.BackColor = System.Drawing.Color.FromArgb(GreenColor(0), GreenColor(1), GreenColor(2));
                        e.Cell.ForeColor = System.Drawing.Color.FromArgb(WhiteColor(0), WhiteColor(1), WhiteColor(2));
                    }
                    else
                    {
                        e.Cell.BackColor = System.Drawing.Color.FromArgb(RedColor(0), RedColor(1), RedColor(2));
                        e.Cell.ForeColor = System.Drawing.Color.FromArgb(WhiteColor(0), WhiteColor(1), WhiteColor(2));
                    }
                }
            }
            catch (Exception ex)
            {
                lblException.Text = ex.Message;
            }
        }

        protected void ClndMonth_SelectionChanged1(object sender, EventArgs e)
        {

        }

        protected void ClndMonth_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
            ClndMonth.SelectedDate = ClndMonth.VisibleDate;
            GetDatedDSR();
        }

        protected void dtlProjects_ItemCommand(object source, DataListCommandEventArgs e)
        {
            int projectid = 0;
            int moduleid = 0;
            TextBox txtTemp = new TextBox();
            DataSet dsDSR = new DataSet();
            TextBox txtWorkDoneP = new TextBox();
            txtWorkDoneP = (TextBox)(e.Item.FindControl("txtWorkDoneP"));
            TextBox txtHoursP = new TextBox();
            txtHoursP = (TextBox)(e.Item.FindControl("txtHrsSpentP"));

            Label lblmsgProject = new Label();
            lblmsgProject = (Label)(e.Item.FindControl("msgProjects"));

            //This code executes when Modules image is expanded or collapsed.
            if (e.CommandName == "ExpandModules")
            {
                ImageButton imgbtnMod = new ImageButton();
                imgbtnMod = (ImageButton)(e.Item.FindControl("imgbtnModules"));

                if (imgbtnMod.ToolTip == "Hide Modules")
                {
                    DataList dtlMod = new DataList();
                    dtlMod = (DataList)(e.Item.FindControl("dtlModules"));
                    dtlMod.Visible = false;
                    imgbtnMod.ToolTip = "Show Modules";
                    imgbtnMod.ImageUrl = "../Images/UP.gif";
                    //lblmsgModule.Text = ""
                    lblmsgProject.Text = "";
                }
                else
                {
                    txtTemp = (TextBox)(e.Item.FindControl("txtProjectIdP"));
                    projectid = Convert.ToInt32(txtTemp.Text);
                    txtTemp = (TextBox)(e.Item.FindControl("txtModuleIdP"));
                    moduleid = Convert.ToInt32(txtTemp.Text);
                    //objProjects.EmpId = objUser.EmpId;
                    dsDSR = new DsrBAL().GetDSRModule(objUser.EmployeeId, ClndMonth.SelectedDate, projectid);
                    DataList dtlMod = new DataList();
                    dtlMod = (DataList)(e.Item.FindControl("dtlModules"));
                    dtlMod.Visible = true;
                    dtlMod.DataSource = dsDSR;
                    dtlMod.DataBind();

                    if (dsDSR.Tables[0].Rows.Count == 0)
                    {
                        dtlMod.Visible = false;
                        //lblmsgModule.Text = "No Module Assigned under this project !"
                        lblmsgProject.Text = "No Module Assigned under this project !";
                        imgbtnMod.ToolTip = "Hide Modules";
                        imgbtnMod.ImageUrl = "../Images/DOWN.gif";
                    }
                    else
                    {
                        dtlMod.Visible = true;
                        //lblmsgModule.Text = ""
                        lblmsgProject.Text = "";
                        imgbtnMod.ToolTip = "Hide Modules";
                        imgbtnMod.ImageUrl = "../Images/DOWN.gif";
                    }
                }
            }


            if (e.CommandName == "DeleteDSRP")
            {
                bool res;
                lblException.Text = "";
                txtTemp = (TextBox)(e.Item.FindControl("txtProjectIdP"));
                projectid = Convert.ToInt32(txtTemp.Text);
                txtTemp = (TextBox)(e.Item.FindControl("txtModuleIdP"));
                moduleid = Convert.ToInt32(txtTemp.Text);
                string SelectName = null;
                objProjects.ProjectID = projectid;
                objProjects = new ProjectBAL().GetProject(projectid);
                SelectName = "project - " + objProjects.ProjectCode + " !";
                res = new DsrBAL().DeleteDSR(objUser.EmployeeId, projectid, moduleid, ClndMonth.SelectedDate);

                if (string.IsNullOrEmpty(txtWorkDoneP.Text) & (txtHoursP.Text == "0" | string.IsNullOrEmpty(txtHoursP.Text)))
                {
                    lblmsgProject.Text = "No DSR present for the " + SelectName + "";
                    //lblmsgModule.Text = ""
                    txtWorkDoneP.Text = "";
                    txtHoursP.Text = "";
                }
                else
                {
                    lblmsgProject.Text = "DSR successfully deleted for the " + SelectName + "";
                    //lblmsgModule.Text = ""
                    txtWorkDoneP.Text = "";
                    txtHoursP.Text = "";
                }
            }
        }

        protected void dtlProjects_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (blnShouldRun == true)
            {
                try
                {
                    int projectid = 0;
                    int moduleid = 0;
                    TextBox txtTemp = new TextBox();
                    txtTemp = (TextBox)(e.Item.FindControl("txtProjectIdP"));
                    projectid = Convert.ToInt32(txtTemp.Text);
                    txtTemp = (TextBox)(e.Item.FindControl("txtModuleIdP"));
                    moduleid = Convert.ToInt32(txtTemp.Text);
                    DataSet dsModule = new DataSet();
                    dsModule = new DsrBAL().getDSRModuleOld(objUser.EmployeeId, ClndMonth.SelectedDate, projectid);
                    if (dsModule.Tables[0].Rows.Count > 0)
                    {
                        dsModule = new DataSet();
                        dsModule = new DsrBAL().getDSRModule(objUser.EmployeeId, ClndMonth.SelectedDate, projectid);
                        DataList dtlMod = new DataList();
                        dtlMod = (DataList)(e.Item.FindControl("dtlModules"));
                        dtlMod.Visible = true;
                        dtlMod.DataSource = dsModule;
                        dtlMod.DataBind();
                        ImageButton imgbtnMod = new ImageButton();
                        imgbtnMod = (ImageButton)(e.Item.FindControl("imgbtnModules"));
                        imgbtnMod.ToolTip = "Hide Modules";
                        imgbtnMod.ImageUrl = "../Images/DOWN.gif";
                    }


                }
                catch (Exception ex)
                {
                }
            }

            if (blnIsOld == true)
            {

                try
                {
                    ImageButton imgbtnMod = new ImageButton();
                    imgbtnMod = (ImageButton)(e.Item.FindControl("imgbtnModules"));
                    imgbtnMod.Visible = false;

                    Label runModuleText = new Label();
                    runModuleText = (Label)(e.Item.FindControl("lblModuleText"));
                    runModuleText.Visible = false;

                    imgbtnMod = new ImageButton();
                    imgbtnMod = (ImageButton)(e.Item.FindControl("cmdDeleteDSRP"));
                    imgbtnMod.Visible = false;

                    int projectid = 0;
                    int moduleid = 0;
                    TextBox txtTemp = new TextBox();

                    txtTemp = (TextBox)(e.Item.FindControl("txtProjectIdP"));
                    projectid = Convert.ToInt32(txtTemp.Text);
                    txtTemp = (TextBox)(e.Item.FindControl("txtModuleIdP"));
                    moduleid = Convert.ToInt32(txtTemp.Text);

                    txtTemp = (TextBox)(e.Item.FindControl("txtWorkDoneP"));
                    txtTemp.ReadOnly = false;
                    if (string.IsNullOrEmpty(txtTemp.Text))
                    {
                        txtTemp.Visible = false;
                        txtTemp = (TextBox)(e.Item.FindControl("txtHrsSpentP"));
                        txtTemp.Visible = false;
                    }
                    else
                    {
                        txtTemp = (TextBox)(e.Item.FindControl("txtHrsSpentP"));
                        txtTemp.ReadOnly = false;
                    }

                    DataSet dsModule = new DataSet();
                    dsModule = new DsrBAL().getDSRModuleOld(objUser.EmployeeId, ClndMonth.SelectedDate, projectid);
                    if (dsModule.Tables[0].Rows.Count > 0)
                    {
                        DataList dtlMod = new DataList();
                        dtlMod = (DataList)(e.Item.FindControl("dtlModules"));
                        dtlMod.Visible = true;
                        dtlMod.DataSource = dsModule;
                        dtlMod.DataBind();
                    }
                    else
                    {
                        if (txtTemp.Visible == false)
                        {
                            Label runlblProject = new Label();
                            System.Web.UI.HtmlControls.HtmlTableRow runTRProject = new System.Web.UI.HtmlControls.HtmlTableRow();

                            runlblProject = (Label)(e.Item.FindControl("lblProject"));
                            runlblProject.Visible = false;
                            runTRProject = (System.Web.UI.HtmlControls.HtmlTableRow)(e.Item.FindControl("trProjectName"));
                            runTRProject.Visible = false;
                            runTRProject = (System.Web.UI.HtmlControls.HtmlTableRow)(e.Item.FindControl("trProjectDetails"));
                            runTRProject.Visible = false;
                            runTRProject = (System.Web.UI.HtmlControls.HtmlTableRow)(e.Item.FindControl("trErrorMessage"));
                            runTRProject.Visible = false;
                        }
                    }


                }
                catch (Exception ex)
                {
                }
            }
        }

        public void dtlModules_ItemCommand(object source, System.Web.UI.WebControls.DataListCommandEventArgs e)
        {
            //This code executes when DSR is deleted for a Project's Module.
            int projectid = 0;
            int moduleid = 0;
            TextBox txtTemp = new TextBox();
            DataSet dsDSR = new DataSet();
            TextBox txtWorkDoneM = new TextBox();
            txtWorkDoneM = (TextBox)(e.Item.FindControl("txtWorkDoneM"));
            TextBox txtHoursM = new TextBox();
            txtHoursM = (TextBox)(e.Item.FindControl("txtHrsSpentM"));
            Label lblmsgModule = new Label();
            lblmsgModule = (Label)(e.Item.FindControl("msgModules"));

            if (e.CommandName == "DeleteDSRM")
            {
                lblException.Text = "";
                txtTemp = (TextBox)(e.Item.FindControl("txtProjectIdM"));
                projectid = Convert.ToInt32(txtTemp.Text);
                txtTemp = (TextBox)(e.Item.FindControl("txtModuleIdM"));
                moduleid = Convert.ToInt32(txtTemp.Text);
                string SelectName = null;
                objModule.ModuleId = moduleid;
                objModule = new ProjectModuleBAL().GetModule(objModule);
                SelectName = "module - " + objModule.ModuleName + " !";

                bool res;
                res = new DsrBAL().DeleteDSR(objUser.EmployeeId, projectid, moduleid, ClndMonth.SelectedDate);

                if (string.IsNullOrEmpty(txtWorkDoneM.Text) & (txtHoursM.Text == "0" | string.IsNullOrEmpty(txtHoursM.Text)))
                {
                    lblmsgModule.Text = "No Data present for the " + SelectName + "";
                    txtWorkDoneM.Text = "";
                    txtHoursM.Text = "";
                }
                else
                {
                    lblmsgModule.Text = "Data successfully deleted for the " + SelectName + "";
                    txtWorkDoneM.Text = "";
                    txtHoursM.Text = "";
                }
            }
        }

        public string DeleteCheck()
        {
            //if (DateTime.Compare(DateTime.Day, ClndMonth.SelectedDate, System.DateTime.Today) == 0)
            //{
            //    if (btnSendDSR.Visible == false)
            //    {
            //        return "";
            //    }
            //    else
            //    {
            //        return "Delete";
            //    }
            //}
            //else
            //{
            //    return "";
            //}
            return "";
        }

        public void dtlModules_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
        {
            if (blnIsOld == true)
            {
                try
                {
                    TextBox txtTemp = new TextBox();
                    txtTemp = (TextBox)(e.Item.FindControl("txtWorkDoneM"));
                    txtTemp.ReadOnly = false;
                    txtTemp = (TextBox)(e.Item.FindControl("txtHrsSpentM"));
                    txtTemp.ReadOnly = false;
                    ImageButton imgbtnMod = new ImageButton();
                    imgbtnMod = (ImageButton)(e.Item.FindControl("cmdDeleteDSRM"));
                    imgbtnMod.Visible = false;
                }
                catch (Exception)
                {
                }
            }
        }

        private void SendMail(string Body, int ProjectID)
        {
            try
            {
                string strBody = null;
                string strSubject = null;
                string strFrom = null;
                string strTo = null;
                string strCc = null;
                if (!string.IsNullOrEmpty(Body))
                {
                    string strToProject = null;
                    string DSRSender = null;
                    System.Web.Mail.MailMessage Mail = new System.Web.Mail.MailMessage();
                    DSRSender = objUser.FirstName + " " + objUser.LastName;
                    objProjects.ProjectID = ProjectID;
                    objProjects = new ProjectBAL().GetProject(ProjectID);
                    strToProject = objProjects.ProjectEmailId;
                    strFrom = objUser.FirstName + " " + objUser.LastName + "<" + objUser.EmailId + ">";
                    strTo = strToProject;
                    //Get Email Addresss of All QA's to send the QA employee mail to all QA's.
                    DataSet QASet = new DataSet();
                    QASet = new ProjectBAL().IfUserIsQA(objUser.EmployeeId, ProjectID);
                    string EmailList = null;
                    if ((QASet != null))
                    {
                        int ncount = 0;
                        for (ncount = 0; ncount <= QASet.Tables[0].Rows.Count - 1; ncount++)
                        {
                            EmailList = EmailList + QASet.Tables[0].Rows[ncount]["Email"] + " ; ";
                        }
                        //Send Email to all the QA's of the company.
                        //strCc = Strings.Left(EmailList, Strings.Len(EmailList) - 3);
                        strCc = EmailList;
                    }
                    strSubject = "DSR(" + lblDate.Text + ")";
                    strBody = Body + "<BR>" + "Regards," + "<BR>" + DSRSender + "<BR>" + "<A HREF=mailto:" + (objUser.EmailId) + ">" + (objUser.EmailId) + "</A>" + "<BR>" + "<BR>" + "<BR>" + "<BR>" + "<BR>" + "<BR>" + "<BR>" + "<BR>" + "powered by SEPITS";
                    if (!string.IsNullOrEmpty(strTo))
                    {
                        // Send Mail
                        //if (SendDynamicMail(strFrom, strTo, strCc, strSubject, "", strBody, "HTML") == false) {
                        //    lblException.Text = "DSR saved! Mail not sent due to invalid Email Address";
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private object GetDatedDSR()
        {
            object functionReturnValue = null;
            this.lblDate.Text = ClndMonth.SelectedDate.ToString("MMMM") + " " + ClndMonth.SelectedDate.Day + ", " + ClndMonth.SelectedDate.Year;
            dtlProjects.Visible = true;

            if (objUser.IsAministrator == 0)
            {
                if (DateTime.Compare(ClndMonth.SelectedDate, System.DateTime.Today) == 0)
                {
                    btnSendDSR.Visible = true;
                    btnSaveDSR.Visible = true;
                    btnSendDSR1.Visible = true;
                    btnSaveDSR1.Visible = true;
                }
                else
                {
                    btnSendDSR.Visible = false;
                    btnSaveDSR.Visible = false;
                    btnSendDSR1.Visible = false;
                    btnSaveDSR1.Visible = false;
                }
            }

            Entities.Project objProjects = new Entities.Project();
            DataSet dsDSR = new DataSet();

            if (new DsrBAL().checkIfPresent(objUser.EmployeeId, ClndMonth.SelectedDate))
            {
                if (DateTime.Compare(ClndMonth.SelectedDate, System.DateTime.Today) == 0 && new DsrBAL().checkIfDSRSentNew(objUser.EmployeeId, ClndMonth.SelectedDate) == false)
                {
                    //This block will execute for the saved DSR of today atleast for one project.
                    dsDSR = new DsrBAL().getDSR(objUser.EmployeeId, ClndMonth.SelectedDate);
                    if (dsDSR.Tables[0].Rows.Count == 0)
                    {
                        dtlProjects.Visible = false;
                        btnSendDSR.Visible = false;
                        btnSaveDSR.Visible = false;
                        btnSendDSR1.Visible = false;
                        btnSaveDSR1.Visible = false;
                        lblException.Text = "No Project Assigned. Contact Administrator !";
                    }
                    else
                    {
                        dtlProjects.Visible = true;
                        btnSendDSR.Visible = true;
                        btnSaveDSR.Visible = true;
                        btnSendDSR1.Visible = true;
                        btnSaveDSR1.Visible = true;
                        lblException.Text = "";
                        blnShouldRun = true;
                        blnIsOld = false;
                        dtlProjects.DataSource = dsDSR;
                        dtlProjects.DataBind();
                    }
                }
                else
                {
                    //This block will execute to display previously saved DSR's.
                    dsDSR = new DsrBAL().getDSROldAvailable(objUser.EmployeeId, ClndMonth.SelectedDate);
                    if (dsDSR.Tables[0].Rows.Count > 0)
                    {
                        dtlProjects.Visible = true;
                        btnSendDSR.Visible = false;
                        btnSaveDSR.Visible = false;
                        btnSendDSR1.Visible = false;
                        btnSaveDSR1.Visible = false;
                        lblException.Text = "";
                        blnShouldRun = false;
                        blnIsOld = true;
                        dtlProjects.DataSource = dsDSR;
                        dtlProjects.DataBind();
                    }
                }
            }
            else
            {
                if (DateTime.Compare(ClndMonth.SelectedDate, System.DateTime.Today) == 0)
                {
                    //This block will execute when user has not saved today's DSR even once.
                    //objProjects.EmpId = objUser.EmpId;
                    dsDSR = new ProjectBAL().getEmployeeProjectsV2(objUser.EmployeeId);
                    //objProjects.getEmployeeProjects(;
                    this.dtlProjects.DataSource = dsDSR;
                    blnShouldRun = false;
                    blnIsOld = false;
                    dtlProjects.DataBind();
                    if (dsDSR.Tables[0].Rows.Count == 0)
                    {
                        dtlProjects.Visible = false;
                        btnSendDSR.Visible = false;
                        btnSaveDSR.Visible = false;
                        btnSendDSR1.Visible = false;
                        btnSaveDSR1.Visible = false;
                        lblException.Text = "No Project Assigned. Contact Administrator !";
                    }
                    else
                    {
                        dtlProjects.Visible = true;
                        btnSendDSR.Visible = true;
                        btnSaveDSR.Visible = true;
                        btnSendDSR1.Visible = true;
                        btnSaveDSR1.Visible = true;
                        lblException.Text = "";
                    }
                }
                else
                {
                    if (objUser.IsAministrator == 1)
                    {
                        //This block will execute when user has not saved today's DSR even once.
                        //objProjects.EmpId = objUser.EmpId;
                        dsDSR = new ProjectBAL().getEmployeeProjectsV2(objUser.EmployeeId);
                        //objProjects.getEmployeeProjects();
                        this.dtlProjects.DataSource = dsDSR;
                        blnShouldRun = false;
                        blnIsOld = false;
                        dtlProjects.DataBind();
                        if (dsDSR.Tables[0].Rows.Count == 0)
                        {
                            dtlProjects.Visible = false;
                            btnSendDSR.Visible = false;
                            btnSaveDSR.Visible = false;
                            btnSendDSR1.Visible = false;
                            btnSaveDSR1.Visible = false;
                            lblException.Text = "No Project Assigned. Contact Administrator !";
                        }
                        else
                        {
                            dtlProjects.Visible = true;
                            btnSendDSR.Visible = true;
                            btnSaveDSR.Visible = true;
                            btnSendDSR1.Visible = true;
                            btnSaveDSR1.Visible = true;
                            lblException.Text = "";
                        }
                    }
                    else
                    {
                        //This block will execute when no DSR is present for previous or future date.
                        lblException.Text = "No DSR Present !";
                        dtlProjects.Visible = false;
                        btnSendDSR.Visible = false;
                        btnSaveDSR.Visible = false;
                        btnSendDSR1.Visible = false;
                        btnSaveDSR1.Visible = false;
                        return functionReturnValue;
                    }
                }
            }
            return functionReturnValue;
        }
    }
}