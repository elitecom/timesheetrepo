﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="DSR.aspx.cs" Inherits="DrishProject.DSR.DSR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table id="tblMain" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr height="10%">
            <td width="100%">
                <!-- HEADER -->
                <table id="tblHeader" height="100%" cellspacing="0" cellpadding="0" width="100%"
                    border="0">
                    <tr>
                        <td valign="top" align="left" height="87px">
                            <table height="98%; vertical-align :top; width: 99%;" cellspacing="0" cellpadding="0"
                                align="top" border="0" id="TABLE1">
                                <tbody>
                                    <tr>
                                        <td valign="top" align="center" width=" 405px">
                                            <%--<table style="height:100%" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
											</table>--%>
                                        </td>
                                    </tr>
                                </tbody>
                                <tr>
                                    <td colspan="3" height=" 0px">
                                        <table cellspacing="0" cellpadding="0" align="top" border="0">
                                            <tr>
                                                <td height="21px">
                                                    <a href="MyDSRNew.aspx">
                                                        <img alt="" src="../Images/dsr_bt.gif" border="0" /></a>
                                                </td>
                                                <td height=" 21px">
                                                    <img alt="" src="../Images/dsr_history_active.gif" border="0" />
                                                </td>
                                                <td height=" 21px">
                                                    <a href="Timesheets.aspx">
                                                        <img alt="" id="imgTimeSheets" src="../Images/Project_Time.gif" border="0" /></a>
                                                </td>
                                                <td height=" 21px">
                                                    <a href="Project_WorkPlan.aspx">
                                                        <img alt="" id="imgWorkPlan" src="../Images/Project_work_plan.gif" border="0" /></a>
                                                </td>
                                                <td height="21px">
                                                    <a href="AddResourceHours.aspx">
                                                        <img alt="" id="imgAddRes" src="../Images/add_res_hrs_tab.gif" border="0" /></a>
                                                </td>
                                                <td height="21px">
                                                    <a href="ResourceAllocation_Detail.aspx">
                                                        <img alt="" id="imgResAllDetail" src="../Images/res_alloc_details_tab.gif" border="0" /></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="center-bg" valign="top" align="right">
                                    <td width=" 405px">
                                        <img alt="" src="../Images/spacer.gif" />
                                    </td>
                                    <td align="left">
                                        <table class="center-bg" height="38" valign="middle">
                                            <tbody>
                                                <tr class="Gridtxt">
                                                    <td valign="middle" align="left">
                                                        <asp:Label CssClass="Labeltxt" Text="Employee" ID="lblEmployee" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:DropDownList CssClass="Gridtxt" ID="ddlEmployee" runat="server" Width="164px"
                                                            AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td valign="middle" align="right" colspan="3" height="30">
                                                        <img alt="" style="cursor: pointer" onclick="ShowProjSummary()" src="../Images/dsr_summary_bt.gif"
                                                            border="0" />&nbsp;&nbsp;&nbsp; <asp:ImageButton ID="ImgbtnMonth" runat="server" ImageUrl="../Images/add_month.gif"
                                                            Visible="false" onclick="ImgbtnMonth_Click"></asp:ImageButton>&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <img alt="" src="../Images/spacer.gif" />
                                    </td>
                                </tr>
                                <tr class="center-bg" valign="top" align="left">
                                    <td width=" 405px">
                                        <img alt="" src="../Images/spacer.gif" width="10" />
                                    </td>
                                    <td valign="top" align="left" width="100%" class="GridBG" height="80%">
                                        <asp:DataGrid ID="GrdDSR" runat="server" BorderColor="White" BackColor="#F7F7F7"
                                            Width="100%" AutoGenerateColumns="False" CellPadding="3" GridLines="Vertical"
                                            BorderStyle="None" AllowPaging="True" PageSize="12">
                                            <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                            <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                            <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                            <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                            <FooterStyle CssClass="GridFoot"></FooterStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="MonthDisplay" HeaderText="Time Period"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="Monthly DSR">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <img alt="" id="imgDetails" src="../Images/Project_work_plan_icon3.gif" onclick='DetailDSR(<%# Eval("EmpID")%>,<%# Eval("Month")%>,<%# Eval("Year")%>);'
                                                            style="cursor: pointer" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="View DSR">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <img alt="" id="imgView" src="../Images/module_icon.gif" onclick='ViewMonthReport(<%# Eval("EmpID")%>,<%# Eval("Month")%>,<%# Eval("Year")%>);'
                                                            style="cursor: pointer" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Monthly DSR Summary">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <img alt="" id="imgSummary" src="../Images/Summary_img.gif" onclick='ShowMonthSummary(<%# Eval("EmpID")%>,<%# Eval("Month")%>,<%# Eval("Year")%>);'
                                                            style="cursor: pointer" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                            <PagerStyle CssClass="Pagertxt" NextPageText="&lt;img src=../Images/next_bt.gif border=0&gt;"
                                                PrevPageText="&lt;img src=../Images/prev_bt.gif border=0&gt;"></PagerStyle>
                                        </asp:DataGrid>
                                        <asp:Label ID="LblException" runat="server" CssClass="Errortxt">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <img alt="" src="../Images/spacer.gif" />
                                    </td>
                                </tr>
                                <tr class="center-bg">
                                    <td valign="bottom" align="left" height="10px" width=" 405px">
                                        <img alt="" height="10" src="../Images/left_bottom_corner.gif" width="10" />
                                    </td>
                                    <td valign="top" align="left" height="10px">
                                        <img alt="" height="80" src="../Images/spacer.gif" width="10" />
                                    </td>
                                    <td valign="bottom" align="right" height="10px">
                                        <img alt="" height="10" src="../Images/right_bottom_corner.gif" width="10" />
                                    </td>
                                </tr>
                                <tr class="Gridtxt">
                                    <td height="3%" width=" 405px">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="80%">
            <td valign="top" width="100%">
                <!-- CENTER -->
                <table id="tblCentral" height="15%" cellspacing="0" cellpadding="0" width="100%"
                    align="center" border="0">
                    <%--<tr>
							<td style="width: 15px"></td>
						</tr>--%>
                    <tr align="left">
                        <%--<td style="width:15px"></td>--%>
                        <td align="left" height=" 36px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script language="javascript" type ="text/javascript">
        function DetailDSR(EmpID, tMonth, tYear) {
            window.location.href = "MyDSRNew.aspx?EmpID=" + EmpID + "&Month=" + tMonth + "&Year=" + tYear;
        }
        function ViewMonthReport(EmpID, tMonth, tYear) {
            var leftPos = (screen.width / 2) - 500;
            var topPos = (screen.height / 2) - 300;
            window.open("View_MonthlyReport.aspx?EmpID=" + EmpID + "&Month=" + tMonth + "&Year=" + tYear, "MemberAdd", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=yes, width=1000, height=600");
        }
        function ShowProjSummary() {
            var leftPos = (screen.width / 2) - 275;
            var topPos = (screen.height / 2) - 200;
            window.open("Project_Summary.aspx", "ProjSummary", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=550, height=400");
        }
        function ShowMonthSummary(EmpID, tMonth, tYear) {
            var leftPos = (screen.width / 2) - 200;
            var topPos = (screen.height / 2) - 200;
            window.open("Month_Summary.aspx?EmpID=" + EmpID + "&Month=" + tMonth + "&Year=" + tYear, "MonthSummary", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=400, height=400");
        }

        

    </script>
</asp:Content>
