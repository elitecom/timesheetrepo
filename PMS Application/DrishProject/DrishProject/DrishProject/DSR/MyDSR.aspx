﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="MyDSR.aspx.cs" Inherits="DrishProject.DSR.MyDSR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        #Site_footer
        {
            display:none;
        }
    </style>
    <div>
        <script type="text/javascript" language="javascript">
<!--
            function ConfirmSendDSR(field) {
                field.style.cursor = 'Wait';
                //document.style.cursor = 'Wait';
                var conf = confirm("are you sure to send DSR?");
                if (conf == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
//-->
        </script>
        <table id="tblMain" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tbody>
                <tr height="10%">
                    <td width="100%">
                        <!-- HEaDER -->
                    </td>
                </tr>
                <tr height="80%">
                    <td width="100%">
                        <!-- CENTER -->
                        <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                            align="center" border="0">
                            <tbody>
                                <tr class="Gridtxt">
                                    <td width="1%">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="2%">
                                    </td>
                                    <td align="left">
                                        <table cellspacing="0" cellpadding="0" align="top" border="0">
                                            <tr height="5">
                                                <td>
                                                    <img src="../Images/DSR_active.gif" border="0" />
                                                </td>
                                                <td>
                                                    <a href="DSR.aspx">
                                                        <img src="../Images/dsr_history_tab.gif" border="0" /></a>
                                                </td>
                                                <td>
                                                    <a href="Timesheets.aspx">
                                                        <img id="imgTimeSheets" src="../Images/Project_Time.gif" border="0" /></a>
                                                </td>
                                                <td>
                                                    <a href="Project_WorkPlan.aspx">
                                                        <img id="imgWorkPlan" src="../Images/Project_work_plan.gif" border="0" /></a>
                                                </td>
                                            </tr>
                                        </table>
                                        <table height="99%" cellspacing="0" cellpadding="0" width="98%" align="top" border="0">
                                            <tbody>
                                                <tr class="center-bg">
                                                    <td valign="top" align="left" width="96%">
                                                        <img height="10" src="../Images/spacer.gif" width="10">
                                                    </td>
                                                    <td valign="top" align="right" width="3%">
                                                        <img height="10" src="../Images/right_top_corner.gif" width="10">
                                                    </td>
                                                </tr>
                                                <tr class="center-bg" valign="top" align="left">
                                                    <td valign="top" align="left" colspan="2">
                                                        <table width="100%" border="0">
                                                            <tr>
                                                                <td valign="top" width="22%">
                                                                    <table>
                                                                        <tr class="Gridtxt" height="5">
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <asp:Calendar ID="ClndMonth" runat="server" Width="200px" Height="169px" BackColor="White"
                                                                                    ShowGridLines="true" BorderColor="#003063" FirstDayOfWeek="Monday" ForeColor="#003063"
                                                                                    SelectMonthText=" " ondayrender="ClndMonth_DayRender" 
                                                                                    onselectionchanged="ClndMonth_SelectionChanged1" 
                                                                                    onvisiblemonthchanged="ClndMonth_VisibleMonthChanged">
                                                                                    <DayStyle CssClass="Gridtxt"></DayStyle>
                                                                                    <TodayDayStyle BackColor="White"></TodayDayStyle>
                                                                                    <SelectorStyle BackColor="White"></SelectorStyle>
                                                                                    <DayStyle BackColor="White"></DayStyle>
                                                                                    <DayHeaderStyle CssClass="Labeltxt" BackColor="White"></DayHeaderStyle>
                                                                                    <SelectedDayStyle ForeColor="Black" BackColor="#FFE2C5"></SelectedDayStyle>
                                                                                    <TitleStyle CssClass="Labeltxt" BackColor="#DDDDDD"></TitleStyle>
                                                                                    <WeekendDayStyle BackColor="White"></WeekendDayStyle>
                                                                                    <OtherMonthDayStyle HorizontalAlign="Center" ForeColor="#003063" VerticalAlign="Middle"
                                                                                        BackColor="White"></OtherMonthDayStyle>
                                                                                </asp:Calendar>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table width="100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td valign="top" width="100%">
                                                                                    <table class="SearchtableStyle" cellspacing="2" cellpadding="2" width="210">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="GridHead" width="25%">
                                                                                                    Colour
                                                                                                </td>
                                                                                                <td class="GridHead">
                                                                                                    DSR Status
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td bgcolor="red">
                                                                                                </td>
                                                                                                <td class="Gridtxt">
                                                                                                    Pending/Holiday
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td bgcolor="green">
                                                                                                </td>
                                                                                                <td class="Gridtxt">
                                                                                                    Updated
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td bgcolor="white">
                                                                                                </td>
                                                                                                <td class="Gridtxt">
                                                                                                    UpComing
                                                                                                </td>
                                                                                            </tr>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td valign="top">
                                                        <table id="dtList" width="100%">
                                                            <tbody>
                                                                <tr valign="top">
                                                                    <td class="LabelTxt" valign="top" nowrap="noWrap" align="left" width="50%">
                                                                        DSR Date:&nbsp;<asp:Label ID="lblDate" runat="server" CssClass="Labletxt"></asp:Label>
                                                                        <asp:TextBox ID="txtMonth" TabIndex="-1" runat="server" Width="0px"></asp:TextBox><asp:TextBox
                                                                            ID="txtYear" runat="server" Width="0px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Label ID="lblException" runat="server" CssClass="Errortxt"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="LabelTxt" valign="middle" nowrap="noWrap" align="center" width="50%">
                                                                        <asp:ImageButton ID="btnSaveDSR1" runat="server" 
                                                                            ImageUrl="../Images/save_big_bt.gif" onclick="btnSaveDSR1_Click">
                                                                        </asp:ImageButton>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:ImageButton ID="btnSendDSR1" runat="server" 
                                                                            ImageUrl="../Images/save_send_bt.gif" onclick="btnSendDSR1_Click">
                                                                        </asp:ImageButton>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" width="100%">
                                                                        <asp:DataList ID="dtlProjects" runat="server" BackColor="#F7F7F7" RepeatColumns="1"
                                                                            RepeatDirection="Horizontal" BorderWidth="1px" ShowFooter="False" 
                                                                            onitemcommand="dtlProjects_ItemCommand" 
                                                                            onitemdatabound="dtlProjects_ItemDataBound">
                                                                            <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                                            <HeaderTemplate>
                                                                                <tr>
                                                                                    <td class="tdStyle">
                                                                                        Hrs
                                                                                    </td>
                                                                                    <td class="tdStyle">
                                                                                        Work Done
                                                                                    </td>
                                                                                    <td runat="server" id="SaveColumn" class="tdStyle">
                                                                                        <%# DeleteCheck()%>
                                                                                    </td>
                                                                                </tr>
                                                                            </HeaderTemplate>
                                                                            <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <tr runat="server" id="trProjectName">
                                                                                    <td valign="top" class="tdStyle">
                                                                                    </td>
                                                                                    <td valign="top" class="tdStyle" align="left">
                                                                                        <asp:TextBox CssClass="Gridtxt" runat="server" ID="txtProjectIdP" Visible="False"
                                                                                            Text='<%# Eval("ProjectID")%>'>
                                                                                        </asp:TextBox>
                                                                                        <asp:TextBox CssClass="Gridtxt" runat="server" ID="txtModuleIDP" Visible="False"
                                                                                            Text='<%# Eval("ModuleID")%>'>
                                                                                        </asp:TextBox>
                                                                                        <asp:Label CssClass="Labeltxt" ID="lblProject" runat="server" Text='<%# Eval("ProjectName")%>'>
                                                                                        </asp:Label>
                                                                                    </td>
                                                                                    <td class="tdStyle" align="right">
                                                                                        <asp:Label ID="lblModuleText" Text="Modules" runat="server" CssClass="Labeltxt"></asp:Label>&nbsp;<asp:ImageButton
                                                                                            runat="server" CommandName="ExpandModules" ID="imgbtnModules" ToolTip="Show Modules"
                                                                                            ImageUrl="../Images/up.gif" autopostback="true"></asp:ImageButton>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr runat="server" id="trProjectdetails">
                                                                                    <td valign="top" align="left" class="tdStyle">
                                                                                        <asp:TextBox CssClass="Gridtxt" runat="server" ID="txtHrsSpentP" Width="50px" MaxLength="5"
                                                                                            Text='<%# Eval("HrsSpent")%>'>
                                                                                        </asp:TextBox>
                                                                                    </td>
                                                                                    <td align="left" class="tdStyle" width="100%">
                                                                                        <asp:TextBox CssClass="Gridtxt" ID="txtWorkDoneP" runat="server" Width="100%" TextMode="MultiLine"
                                                                                            Height="175px" Text='<%# Eval("WorkDone")%>'>
                                                                                        </asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdDeleteStyle" align="center" valign="middle">
                                                                                        &nbsp;
                                                                                        <asp:ImageButton ID="cmdDeleteDSRP" CommandName="DeleteDSRP" ImageUrl="../Images/DELETE.gif"
                                                                                            ToolTip="Delete Project DSR" runat="server"></asp:ImageButton>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr runat="server" id="trErrorMessage">
                                                                                    <td colspan="3" class="LabelTxt">
                                                                                        <asp:Label CssClass="Errortxt" ID="msgProjects" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3">
                                                                                        <asp:DataList ID="dtlModules" runat="server" Visible="False" OnItemCommand="dtlModules_ItemCommand"
                                                                                            OnItemDataBound="dtlModules_ItemDataBound">
                                                                                            <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                                                            <ItemTemplate>
                                                                                                <tr>
                                                                                                    <td valign="top" class="ModuletdStyle">
                                                                                                    </td>
                                                                                                    <td valign="top" class="ModuletdStyle" align="left">
                                                                                                        <asp:TextBox CssClass="Gridtxt" runat="server" ID="txtProjectIdM" Visible="False"
                                                                                                            Text='<%# Eval("ProjectID")%>'>
                                                                                                        </asp:TextBox>
                                                                                                        <asp:TextBox CssClass="Gridtxt" runat="server" ID="txtModuleIDM" Visible="False"
                                                                                                            Text='<%# Eval("ModuleID")%>'>
                                                                                                        </asp:TextBox>
                                                                                                        <asp:Label CssClass="Labeltxt" ID="lblModule" runat="server" Text='<%# Eval("ProjectName")%>'
                                                                                                            ForeColor="#ff9900">
                                                                                                        </asp:Label>
                                                                                                    </td>
                                                                                                    <td valign="top" class="ModuletdStyle">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td valign="top" class="ModuletdStyle" align="left" width="5%">
                                                                                                        <asp:TextBox class="Gridtxt" runat="server" ID="txtHrsSpentM" Width="50px" MaxLength="5"
                                                                                                            Text='<%# Eval("HrsSpent")%>'>
                                                                                                        </asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="left" class="ModuletdStyle" width="83%">
                                                                                                        <asp:TextBox class="Gridtxt" ID="txtWorkDoneM" runat="server" Width="100%" TextMode="MultiLine"
                                                                                                            Height="125px" Text='<%# Eval("WorkDone")%>'>
                                                                                                        </asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="ModuleDeletetdStyle" width="12%" align="center">
                                                                                                        <asp:ImageButton ID="cmdDeleteDSRM" CommandName="DeleteDSRM" ImageUrl="../Images/DELETE.gif"
                                                                                                            ToolTip="Delete Module DSR" runat="server"></asp:ImageButton>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="3" class="LabelTxt">
                                                                                                        <asp:Label class="Errortxt" ID="msgModules" runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                        </asp:DataList>
                                                                                    </td>
                                                                                </tr>
                                                                                </tr>
                                                                                <tr>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                                        </asp:DataList>
                                                                    </td>
                                                                </tr>
                                                                <tr class="Gridtxt">
                                                                    <td align="center">
                                                                        <asp:ImageButton ID="btnSaveDSR" runat="server" 
                                                                            ImageUrl="../Images/save_big_bt.gif" onclick="btnSaveDSR_Click">
                                                                        </asp:ImageButton>&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="btnSendDSR" runat="server"
                                                                            ImageUrl="../Images/save_send_bt.gif" onclick="btnSendDSR_Click"></asp:ImageButton>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr class="center-bg" height="4%">
                                    <td class="gridtxt">
                                        &nbsp;
                                    </td>
                                    <td class="gridtxt">
                                    </td>
                                </tr>
                                <tr class="center-bg" height="10%">
                                    <td valign="top" align="left">
                                        <img height="10" src="../Images/spacer.gif" width="10">
                                    </td>
                                    <td valign="bottom" align="right">
                                        <img height="10" src="../Images/right_bottom_corner.gif" width="10">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        </td> </tr> </tbody> </table> </td> </tr>
        <!--FOOTER -->
        <tr>
            <td width="100%">
            </td>
        </tr>
        </table> </table>
    </div>
</asp:Content>
