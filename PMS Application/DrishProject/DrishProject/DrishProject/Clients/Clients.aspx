﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="Clients.aspx.cs" Inherits="DrishProject.WebForm15" %>

<%@ Register Src="../UserControls/ProjectMenu.ascx" TagName="ProjectMenu" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <h2>Client Details</h2>
    </div>
    <table id="tblMain" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tbody>
            <tr height="80%">
                <td valign="top" width="100%">
                    <!-- CENTER -->
                    <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                        align="center" border="0">
                        <tr>
                            <td width="1%"></td>
                            <td align="left" height="100%">
                                <table cellspacing="0" cellpadding="0" width="100%" align="left" border="0"
                                    style="height: 91%">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <table cellspacing="0" cellpadding="0" align="top" border="0">
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <asp:ImageButton ID="ImageButton1" runat="server"
                                                                    ImageUrl="~/Images/details_new.gif" OnClick="ImageButton1_Click" />
                                                                <asp:ImageButton ID="ImageButton2" runat="server"
                                                                    ImageUrl="~/Images/tabs_bt_02.gif" OnClick="ImageButton2_Click" />
                                                                <asp:ImageButton ID="ImageButton3" runat="server"
                                                                    ImageUrl="~/Images/tabs_bt_06.gif" OnClick="ImageButton3_Click" />
                                                                <asp:ImageButton ID="ImageButton4" runat="server"
                                                                    ImageUrl="~/Images/tasks.gif" OnClick="ImageButton4_Click" />
                                                                <asp:ImageButton ID="ImageButton5" runat="server"
                                                                    ImageUrl="~/Images/tabs_bt_05.gif" OnClick="ImageButton5_Click" />
                                                                <asp:ImageButton ID="ImageButton6" runat="server"
                                                                    ImageUrl="~/Images/tabs_bt_08.gif" OnClick="ImageButton6_Click" />
                                                                <asp:ImageButton ID="ImageButton7" runat="server"
                                                                    ImageUrl="~/Images/client_contacts_tab.gif" OnClick="ImageButton7_Click" />
                                                                <asp:ImageButton ID="ImageButton8" runat="server"
                                                                    ImageUrl="~/Images/tabs_bt_10.gif" OnClick="ImageButton8_Click" />
                                                                <asp:ImageButton ID="ImageButton11" runat="server"
                                                                    ImageUrl="~/Images/tabs_bt_09.gif" OnClick="ImageButton11_Click" />
                                                                <asp:ImageButton ID="ImageButton9" runat="server"
                                                                    ImageUrl="~/Images/tabs_bt_07.gif" OnClick="ImageButton9_Click" />
                                                                <asp:ImageButton ID="ImageButton10" runat="server"
                                                                    ImageUrl="~/Images/tabs_bt_08.gif" OnClick="ImageButton10_Click" />
                                                                <asp:ImageButton ID="ImageButton13" runat="server"
                                                                    ImageUrl="~/Images/review_active.gif" OnClick="ImageButton13_Click" />
                                                                <asp:ImageButton ID="ImageButton14" runat="server"
                                                                    ImageUrl="~/Images/timesheet_tab_active.gif" OnClick="ImageButton14_Click" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="center-bg" valign="top">
                                            <td width="100%" colspan="2" style="height: 44px">
                                                <table height="38" width="100%" border="0">
                                                    <tr>
                                                        <td class="Gridtxt" align="left" width="50%" style="height: 19px">&nbsp;&nbsp;<strong> Project&nbsp;Code&nbsp;:&nbsp;</strong><asp:Label CssClass="Gridtxt"
                                                            ID="lblProjectCode" runat="server"></asp:Label><asp:DropDownList CssClass="Gridtxt"
                                                                ID="DropDownList1" runat="server" AutoPostBack="True" Width="0px">
                                                            </asp:DropDownList>
                                                            <asp:Button CssClass="Gridtxt" ID="btnRefresh" runat="server" Width="0px" Height="0px"
                                                                Visible="false"></asp:Button>
                                                        </td>
                                                        <td class="Gridtxt" align="right" width="50%" style="height: 19px">
                                                            <img id="imgContacts" style="cursor: pointer" onclick="ContactAddFunction();" src="../Images/select_contact_bt.gif"
                                                                border="0">
                                                        </td>
                                                        <td class="Gridtxt" width="0.5%" style="height: 19px">&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="center-bg" valign="top" align="left">
                                            <td class="GridBG" height="100%" width="100%">
                                                <asp:DataGrid ID="dgContacts" runat="server" Width="100%" AllowPaging="True" BorderStyle="None"
                                                    GridLines="Vertical" CellPadding="3" DataKeyField="clientid" BorderColor="White"
                                                    BackColor="#F7F7F7" AutoGenerateColumns="False" Style="margin-top: 0px">
                                                    <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                    <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                    <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                    <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn>
                                                            <HeaderStyle Width="9px"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="ImgBtn1" runat="server" Width="9px" AlternateText="Expand" Height="9px"
                                                                    ImageUrl="../Images/Plus-sign.gif" CommandName="Expand"></asp:ImageButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="companyname" HeaderText="Company Name"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="clientname" HeaderText="Client Name"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="address1" HeaderText="Address"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="place" HeaderText="Place"></asp:BoundColumn>
                                                        <asp:TemplateColumn>
                                                            <ItemTemplate>
                                                                <asp:PlaceHolder ID="Expanded" runat="server" Visible="False"></TD> </TR>
                                                                    <tr>
                                                                        <td width="9px">&nbsp;
                                                                        </td>
                                                                        <td colspan="6" width="100%">
                                                                            <asp:DataGrid ID="dgsub" PageSize="5" runat="server" Width="100%" BackColor="#F7F7F7"
                                                                                BorderWidth="1px" BorderColor="White" DataKeyField="contactid" CellPadding="2"
                                                                                GridLines="None" ForeColor="Black" AutoGenerateColumns="False">
                                                                                <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                                                <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                                                <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                                                <PagerStyle CssClass="Pagertxt" NextPageText="<img src=../Images/next_bt.gif border=0>"
                                                                                    PrevPageText="<img src=../Images/prev_bt.gif border=0>" HorizontalAlign="Center"
                                                                                    VerticalAlign="Middle"></PagerStyle>
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="name" HeaderText="Name"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Email" HeaderText="Email"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="phoneno" HeaderText="Phone"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="mobileno" HeaderText="Mobile"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="faxno" HeaderText="Fax"></asp:BoundColumn>
                                                                                    <asp:TemplateColumn>
                                                                                        <HeaderStyle Width="15px"></HeaderStyle>
                                                                                        <HeaderTemplate>
                                                                                            Delete
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="imgDel" runat="server" ImageUrl="../Images/cross.gif" CommandName="Delete"></asp:ImageButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                </Columns>
                                                                                <PagerStyle CssClass="Pagertxt" HorizontalAlign="Center"></PagerStyle>
                                                                            </asp:DataGrid>
                                                                </asp:PlaceHolder>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <PagerStyle VerticalAlign="Middle" NextPageText="&lt;img src=../Images/next_bt.gif border=0&gt;"
                                                        PrevPageText="&lt;img src=../Images/prev_bt.gif border=0&gt;" HorizontalAlign="Center"
                                                        CssClass="Pagertxt" Mode="NumericPages"></PagerStyle>
                                                </asp:DataGrid><asp:Label ID="LblException" CssClass="Errortxt" runat="server"></asp:Label>
                                                <div>
                                                </div>
                                            </td>
                                            <td width="1%">&nbsp;
                                            </td>
                                        </tr>
                                        <tr class="center-bg">
                                            <td style="height: 10px">&nbsp;
                                            </td>
                                            <td style="height: 10px"></td>
                                        </tr>
                                        <tr class="center-bg" height="10%">
                                            <td valign="top" align="left">
                                                <img height="10" src="../Images/spacer.gif" width="10">
                                            </td>
                                            <td valign="bottom" align="right">
                                                <img height="10" src="../Images/right_bottom_corner.gif" width="10" alt="" />
                                            </td>
                                        </tr>
                                        <tr class="gridtxt">
                                            <td colspan="2" style="height: 11%">&nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <img height="10" src="../Images/spacer.gif" width="10">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--FOOTER -->
            <tr>
                <td width="100%">
                    <table id="tblFooter" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="top" align="left">&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
