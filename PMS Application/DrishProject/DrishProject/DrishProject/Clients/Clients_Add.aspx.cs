﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using Entities;

namespace DrishProject.Clients
{
    public partial class Clients_Add : System.Web.UI.Page
    {
        Entities.Clients objClient = new Entities.Clients();
        Entities.Employee objUser = new Entities.Employee();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {                
                objUser = (Entities.Employee)(Session["emp"]);
                if (objUser.IsAministrator == 0)
                {
                    Response.Redirect("Default.aspx?Authority=Deny");
                }

                if (!Page.IsPostBack)
                {
                    txtAction.Text = Request["varAction"];
                    txtClientID.Text = Request["ClientID"];
                    GetCountryList();
                    if (txtAction.Text.ToLower().Trim() == "edit")
                    {
                        loadClientDetails();
                        RegisterStartupScript("strShow", "<script language='javascript'>this.document.all.imgClient.src='Images/edit_client_bt.gif';</script>");
                    }
                    else
                    {
                        RegisterStartupScript("strHide", "<script language='javascript'>this.document.all.imgClient.src='Images/add_client.gif';</script>");
                    }
                    txtClientName.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    txtCompanyName.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    txtAddress1.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    txtAddress2.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    txtCity.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    txtZip.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    txtState.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    cmbCountry.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    txtNotes.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                }
                if (txtAction.Text.ToLower().Trim() == "edit")
                {
                    imgAddNew.Visible = false;
                    RegisterStartupScript("strShow", "<script language='javascript'>this.document.all.imgClient.src='Images/edit_client_bt.gif';</script>");
                }
                else
                {
                    imgAddNew.Visible = true;
                    RegisterStartupScript("strHide", "<script language='javascript'>this.document.all.imgClient.src='Images/add_client.gif';</script>");
                }
            }
            catch (Exception ex)
            {
                LblException.Text = ex.Message;
            }
        }

        protected void imgBtnSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    if (!string.IsNullOrEmpty((txtClientName.Text)))
                    {
                        objClient.ClientName = txtClientName.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty((txtCompanyName.Text)))
                    {
                        objClient.CompanyName = txtCompanyName.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty((txtAddress1.Text)))
                    {
                        objClient.Address1 = txtAddress1.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty((txtAddress2.Text)))
                    {
                        objClient.Address2 = txtAddress2.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty((txtCity.Text)))
                    {
                        objClient.City = txtCity.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty((txtState.Text)))
                    {
                        objClient.State = txtState.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty((txtZip.Text)))
                    {
                        objClient.Zip = txtZip.Text.Trim();
                    }
                    if ((txtZip.Text.Length > 6))
                    {
                        LblException.Text = "Please Enter Zip Code upto 6 Characters.";
                        return;
                    }

                    if (!string.IsNullOrEmpty((txtNotes.Text)))
                    {
                        objClient.Notes = txtNotes.Text.Trim();
                    }
                    objClient.Country = cmbCountry.SelectedValue;

                    if (txtAction.Text.Trim().ToLower() == "edit")
                    {
                        objClient.ClientID = Convert.ToInt32(txtClientID.Text.Trim());
                        objClient.ClientName = txtClientName.Text.Trim();
                        if (new ClientsBAL().IfClientExists(objClient) == true)
                        {
                            LblException.Text = "Client Already Exists!";
                            return;
                        }
                        else
                        {
                            bool res;
                            res = new ClientsBAL().UpdateClientInfo(objClient);
                            if (res == true)
                            {
                                LblException.Text = "Client Updated Successfully";
                            }
                            else
                            {
                                LblException.Text = "Client Not Updated Successfully";
                            }
                        }
                    }
                    else
                    {
                        objClient.ClientID = Convert.ToInt32(txtClientID.Text.Trim());
                        objClient.ClientName = txtClientName.Text.Trim();
                        if (new ClientsBAL().IfClientExists(objClient) == true)
                        {
                            LblException.Text = "Client Already Exists!";
                            return;
                        }
                        else
                        {
                            txtAction.Text = "edit";
                            int res = new ClientsBAL().AddNewClient(objClient);
                            txtClientID.Text = res.ToString();
                            LblException.Text = "Client Added Successfully";
                        }
                    }
                }
                RegisterStartupScript("strRefresh", "<script language='javascript'>window.opener.PageRefresh();</script>");
            }
            catch (Exception ex)
            {
                LblException.Text = ex.Message;
            }
        }

        protected void imgAddNew_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Clients_Add.aspx?varAction=add");
        }

        private void loadClientDetails()
        {
            try
            {
                objClient.ClientID = Convert.ToInt32(txtClientID.Text.Trim());
                objClient = new ClientsBAL().GetClientDetails(objClient.ClientID);
                txtClientName.Text = objClient.ClientName;
                txtCompanyName.Text = objClient.CompanyName;
                txtAddress1.Text = objClient.Address1;
                txtAddress2.Text = objClient.Address2;
                txtCity.Text = objClient.City;
                txtState.Text = objClient.State;
                txtZip.Text = objClient.Zip;
                txtNotes.Text = objClient.Notes;
                cmbCountry.SelectedValue = objClient.Country;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetCountryList()
        {
            try
            {
                DataSet DS = new DataSet();
                DS = new DrishProjectBAL.ClientsBAL().GetCountryList();
                DataRow dRow = null;
                dRow = DS.Tables[0].NewRow();
                dRow["CountryID"] = "0";
                dRow["CountryName"] = "--Select--";
                DS.Tables[0].Rows.InsertAt(dRow, 0);
                cmbCountry.DataSource = DS;
                cmbCountry.DataValueField = "CountryID";
                cmbCountry.DataTextField = "CountryName";
                cmbCountry.DataBind();
                cmbCountry.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}