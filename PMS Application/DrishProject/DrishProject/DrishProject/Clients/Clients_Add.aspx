﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Clients_Add.aspx.cs" Inherits="DrishProject.Clients.Clients_Add" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    			<TABLE id="MAINTABLE" height="100%" width="100%">
				<tr class="Gridtxt">
					<td width="100%" height="1%"></td>
				</tr>
				<tr class="Gridtxt">
					<td class="Gridtxt" width="100%" valign="top">
						<TABLE id="tblCentral" height="100%" cellSpacing="0" cellPadding="0" width="100%" align="center"
							border="0">
							<TR class="Gridtxt">
								<td></td>
								<TD valign="top" align="left" height="100%">
									<table cellSpacing="0" cellPadding="0" border="0">
										<tr>
											<TD><IMG border="0" id="imgClient" src="../Images/add_client.gif"></TD>
										</tr>
									</table>
									<table cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TBODY>
											<TR class="center-bg">
												<TD vAlign="top" align="left" width="96%"><IMG height="10" src="../Images/spacer.gif" width="10"></TD>
												<TD vAlign="top" align="right" width="3%"><IMG height="10" src="../Images/right_top_corner.gif" width="10"></TD>
											</TR>
											<tr class="center-bg">
												<td height="20" class="Errortxt" colspan="2"><asp:label id="LblException" runat="server" CssClass="Errortxt"></asp:label></td>
											</tr>
											<TR class="center-bg" vAlign="top" align="left">
												<TD vAlign="top" height="100%">
													<TABLE cellSpacing="0" cellPadding="2" align="left" border="0">
														<TBODY>
															<tr class="Gridtxt" vAlign="middle">
																<td class="Mendatoryfld" valign="top">*</td>
																<td class="Labeltxt" vAlign="top">Client Name</td>
																<td class="Gridtxt"><asp:textbox CssClass="Gridtxt" id="txtClientName" tabIndex="1" runat="server" Width="288px"
																		MaxLength="50"></asp:textbox></td>
																<td class="Errortxt"><asp:requiredfieldvalidator id="rfvClientName" CssClass="Errortxt" runat="server" Width="128px" ErrorMessage="Name required"
																		ControlToValidate="txtClientName"></asp:requiredfieldvalidator></td>
															</tr>
															<tr class="Gridtxt" vAlign="middle">
																<td class="Mendatoryfld" valign="top">*</td>
																<td class="Labeltxt" vAlign="top">Company&nbsp;Name</td>
																<td class="Gridtxt"><asp:textbox CssClass="Gridtxt" id="txtCompanyName" tabIndex="2" runat="server" Width="288px"
																		MaxLength="200"></asp:textbox></td>
																<td class="Errortxt"><asp:requiredfieldvalidator CssClass="Errortxt" id="rfvCompanyName" runat="server" Width="128px" ErrorMessage="Name required"
																		ControlToValidate="txtCompanyName"></asp:requiredfieldvalidator></td>
															</tr>
															<tr class="Gridtxt" vAlign="middle">
																<td class="Mendatoryfld" valign="top">*</td>
																<td class="Labeltxt" vAlign="top">Address</td>
																<td class="Gridtxt"><asp:textbox CssClass="Gridtxt" id="txtAddress1" tabIndex="3" runat="server" Width="288px" MaxLength="500"></asp:textbox></td>
																<td class="Errortxt">
																	<asp:RequiredFieldValidator id="rfvAddress" runat="server" CssClass="Errortxt" ControlToValidate="txtAddress1"
																		ErrorMessage="Address required"></asp:RequiredFieldValidator></td>
															</tr>
															<tr class="Gridtxt" vAlign="middle">
																<td class="Gridtxt"></td>
																<td class="Labeltxt" vAlign="top" style="HEIGHT: 20px"></td>
																<td class="Gridtxt"><asp:textbox CssClass="Gridtxt" id="txtAddress2" tabIndex="4" runat="server" Width="288px" MaxLength="500"></asp:textbox></td>
																<td class="Gridtxt"></td>
															</tr>
															<tr class="Gridtxt" vAlign="middle">
																<td class="Mendatoryfld" valign="top">*</td>
																<td class="Labeltxt" vAlign="top">City</td>
																<td class="Gridtxt"><asp:textbox CssClass="Gridtxt" id="txtCity" tabIndex="5" runat="server" Width="141px" MaxLength="20"></asp:textbox></td>
																<td class="Errortxt">
																	<asp:RequiredFieldValidator id="rfvCity" runat="server" CssClass="Errortxt" ControlToValidate="txtCity" ErrorMessage="City required"></asp:RequiredFieldValidator></td>
															</tr>
															<TR class="Gridtxt">
																<td class="Gridtxt"></td>
																<TD class="Labeltxt" valign="top">Zip</TD>
																<TD class="Gridtxt"><asp:textbox CssClass="Gridtxt" id="txtZip" tabIndex="6" runat="server" Width="141px" MaxLength="20"></asp:textbox></TD>
																<TD class="Gridtxt"><asp:textbox CssClass="Gridtxt" id="txtClientID" runat="server" Width="0px" Visible="False"></asp:textbox>
																<asp:textbox id="txtAction" runat="server" Width="0px" Visible="False"></asp:textbox></TD>
															</TR>
															<TR class="Gridtxt">
																<td class="Mendatoryfld" valign="top">*</td>
																<TD class="Labeltxt" valign="top" style="HEIGHT: 25px">State</TD>
																<TD class="Gridtxt"><asp:textbox CssClass="Gridtxt" id="txtState" tabIndex="7" runat="server" Width="141px" MaxLength="20"></asp:textbox></TD>
																<TD class="Errortxt">
																	<asp:RequiredFieldValidator id="rfvState" runat="server" CssClass="Errortxt" ControlToValidate="txtState" ErrorMessage="State required"></asp:RequiredFieldValidator></TD>
															</TR>
															<tr class="Gridtxt" vAlign="middle">
																<td class="Mendatoryfld" valign="top">*</td>
																<td class="Labeltxt" vAlign="top" style="HEIGHT: 28px">Country</td>
																<td class="Gridtxt"><asp:dropdownlist CssClass="Gridtxt" id="cmbCountry" tabIndex="8" runat="server" Width="141px"></asp:dropdownlist></td>
																<td class="Gridtxt">
																	<asp:RequiredFieldValidator id="rfvCountry" runat="server" CssClass="Errortxt" ControlToValidate="cmbCountry"
																		ErrorMessage="Country required" InitialValue="0"></asp:RequiredFieldValidator></td>
															</tr>
															<TR class="Gridtxt">
																<td class="Gridtxt"></td>
																<TD class="Labeltxt" vAlign="top">Remarks</TD>
																<TD class="Gridtxt"><asp:textbox CssClass="Gridtxt" id="txtNotes" tabIndex="9" runat="server" Width="288px" Height="90px"
																		TextMode="MultiLine" MaxLength="4000"></asp:textbox></TD>
																<TD class="Gridtxt"></TD>
															</TR>
															<tr class="Gridtxt">
																<td colspan="4" class="Gridtxt" height="20">&nbsp;</td>
															</tr>
															<tr class="Gridtxt" vAlign="middle">
																<td class="Gridtxt" vAlign="top" align="center" colSpan="4">
                                                                    <asp:imagebutton id="imgBtnSave" tabIndex="10" ImageUrl="../Images/save_bt.gif" 
                                                                        Runat="server" onclick="imgBtnSave_Click"></asp:imagebutton>&nbsp;&nbsp;
																	<IMG style="CURSOR: POINTER" onclick="window.close();" src="../Images/cancel_bt.gif">&nbsp;&nbsp;<asp:imagebutton 
                                                                        id="imgAddNew" Runat="server" ImageUrl="../Images/add_btn.gif" 
                                                                        onclick="imgAddNew_Click"></asp:imagebutton>
																</td>
															</tr>
														</TBODY>
													</TABLE>
												</TD>
												<TD><IMG height="10" src="../Images/spacer.gif" width="10"></TD>
											</TR>
											<TR class="center-bg">
												<TD vAlign="top" align="left"><IMG height="10" src="../Images/spacer.gif" width="10"></TD>
												<TD vAlign="bottom" align="right"><IMG height="10" src="../Images/right_bottom_corner.gif" width="10"></TD>
											</TR>
											<tr class="Gridtxt">
												<td colspan="2" class="Gridtxt" height="3%">&nbsp;</td>
											</tr>
										</TBODY>
									</table>
								</TD>
								<TD><IMG height="10" src="../Images/spacer.gif" width="10"></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr class="Gridtxt">
					<td colspan="3" width="100%"></td>
				</tr>
			</TABLE>
    </div>
    </form>
</body>
</html>
