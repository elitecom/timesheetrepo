﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using DrishProjectBAL;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;

namespace DrishProject
{
    public partial class WebForm15 : System.Web.UI.Page
    {
        int ProjectID;
        Project objProject = new Project();
        //int intclientId;
        Entities.Employee objEmployee = new Entities.Employee();
        const int DelColumnIndex = 5;
        const int ExpandColIndex = 4;

        protected void Page_Load(object sender, EventArgs e)
        {
            #region "Unused Code"            
            //UserControls.ProjectMenu projectMenu = (UserControls.ProjectMenu)(this.ProjectMenu1);

            //ImageButton btnImage = (ImageButton)(projectMenu.FindControl("ImageButton7"));
            // btnImage.ImageUrl = "~/Images/Client_active.gif";
            #endregion

            if (Request["ProjectID"] == "")
            {
                objProject = (Project)(Session["Project"]);
                ProjectID = Convert.ToInt32(objProject.ProjectID);
            }
            else
            {
                //ProjectID = Convert.ToInt32(Request["ProjectID"].ToString());
                //Session["ProjectID"] = Request["ProjectID"];
                try
                {
                    objProject = (Project)(Session["Project"]);
                    ProjectID = objProject.ProjectID;
                }
                catch (Exception)
                { }
                //ProjectID = Convert.ToInt32(Session["ProjectID"]);
            }

            //objProject.ProjectID = ProjectID;
            //objProject.GetProject(ProjectID);
            Entities.Employee objEmployee = new Entities.Employee();
            objEmployee = (Entities.Employee)Session["emp"];
            LblException.Text = "";

            if (objEmployee.IsAministrator == 0)
            {
                //RoleAuthenticate;
                if (User.IsInRole("AddProjectClient"))
                {
                    RegisterStartupScript("strShow", "<script language=\'javascript\'>this.document.all.imgContacts.style.display=\'block\';</script>");
                }
                else
                {
                    RegisterStartupScript("strHide", "<script language=\'javascript\'>this.document.all.imgContacts.style.display=\'none\';</script>");
                }
                //System.Web.UI.HtmlControls.HtmlTableCell tdFLY;
                //tdFLY = (HtmlTableCell)FindControl("tdPTimeSheet");
                //tdFLY.Visible = false;
            }

            if (objProject.ProjectCategory != "ENGG")
            {
                #region "Unused Code"                
                //System.Web.UI.HtmlControls.HtmlTableCell tdFLY;
                //tdFLY = (HtmlTableCell)FindControl("tdRelease");
                //tdFLY.Visible = false;
                ////tdFLY =(HtmlTableCell) FindControl("tdClients");
                ////tdFLY.Visible = false;
                //tdFLY = (HtmlTableCell)FindControl("tdCheckList");
                //tdFLY.Visible = false;
                //tdFLY = (HtmlTableCell)FindControl("tdCodeReview");
                //tdFLY.Visible = false;
                #endregion
            }
            lblProjectCode.Text = objProject.ProjectCode;

            if (!Page.IsPostBack)
            {
                try
                {
                    BindGrid();

                    #region "Unused Code"                    
                    //To make the Project button Active
                    //ImageButton img = new ImageButton();
                    //img = (ImageButton) (Page.FindControl("UC_Header1").FindControl("imgProjects"));
                    //img.ImageUrl = "..\\Images\\Project_active.gif";
                    #endregion
                }
                catch (Exception ex)
                {
                    LblException.Text = ex.Message;
                }
            }
        }

        private void BindGrid()
        {
            Entities.Clients objProjectClientClass = new Entities.Clients();

            LblException.Text = "";
            try
            {
                DataSet DS = new DataSet();
                DS = new DrishProjectBAL.ClientsBAL().GetContactsPerProject(ProjectID);
                dgContacts.DataSource = DS;
                if (DS.Tables[0].Rows.Count > 0)
                {
                    if (Math.Ceiling(double.Parse(DS.Tables[0].Rows.Count.ToString()) / dgContacts.PageSize) - 1 < dgContacts.PageSize)
                    {
                        dgContacts.CurrentPageIndex = 0;
                    }
                }
                else
                {
                    dgContacts.CurrentPageIndex = 0;
                }
                dgContacts.DataBind();
                if (DS.Tables[0].Rows.Count == 0)
                {
                    dgContacts.PagerStyle.Visible = false;
                    LblException.Text = "No Contacts Present.";
                }
                else
                {
                    dgContacts.PagerStyle.Visible = true;
                    LblException.Text = "";
                }
                if (DS.Tables[0].Rows.Count <= dgContacts.PageSize)
                {
                    dgContacts.PagerStyle.Visible = false;
                }
                else
                {
                    dgContacts.PagerStyle.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Projects/ProjectDetail.aspx");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TeamMembers/ProjectTeamMembers.aspx");
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectModules/Module.aspx");
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Tasks/MyTasks.aspx");
        }

        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Documents/Documents.aspx");
        }

        protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectRelease/Release.aspx");
        }

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Clients/Clients.aspx");
        }

        protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectMeetings/Meeting.aspx");
        }

        protected void ImageButton11_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectNotes/Notes.aspx");
        }

        protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectHistory/History.aspx");
        }

        protected void ImageButton10_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectRelease/Release.aspx");
        }

        protected void ImageButton13_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectReview/Reviews.aspx");
        }

        protected void ImageButton14_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TimeSheets/TimeSheet.aspx");
        }
    }
}