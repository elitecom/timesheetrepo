﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="TrainingDetails.aspx.cs" Inherits="DrishProject.Training.TrainingDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script language="javascript" type="text/javascript">
    function EnterVal(event) {
        if (window.event.keyCode == 13) {
            event.returnValue = false;
            event.cancel = true;
            document.getElementById('imgBtnSearch').click();
        }
    }
    function EnterVal(event) {
        if (window.event.keyCode == 13) {
            event.returnValue = false;
            event.cancel = true;
            document.getElementById('imgBtnSearch').click();
        }
    }
    function MeetingEditFunction(ID) {
        var leftPos = (screen.width / 2) - 250;
        var topPos = (screen.height / 2) - 250;
        window.open("EditTraining.aspx?TrainingId=" + ID, "MeetingEdit", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=500, height=538");
    }
    function MeetingAddFunction() {
        var leftPos = (screen.width / 2) - 250;
        var topPos = (screen.height / 2) - 250;
        window.open("Addtraining.aspx?TrainingId=0", "MeetingEdit", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=500, height=538");
    }
    function Export() {
        var leftPos = (screen.width / 2) - 400
        var topPos = (screen.height / 2) - 250;
        window.open("ExportExcel.aspx?DataSet=TraniningDetails", "ExportExcel", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=yes, scrollbars=yes resizable=yes, copyhistory=yes, width=750, height=500");
    }
    function Refresh() {
        __doPostBack('btnRefresh', '');
    }	   
    </script>
        
     <table id="tblMain" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
		<tbody>
                <tr style="height:10%" >
						<td valign="top" >
                            &nbsp;</td>    
                </tr>
                
                <tr>
				  <td valign="top"><!-- CENTER -->
				    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top"><img src="../images/spacer.gif" width="10" height="10" /></td>
                        <td><table id="tblCentral" cellspacing="0" cellpadding="0" width="100%" align="center" height="100%"
								border="0">
                          <tbody>
                            <tr>
                              <td align="left"><table cellspacing="0" cellpadding="0" align="left" border="0">
                                  <tr>
                                    <td align="left"><a href ="../Employee/EmployeeSearch.aspx" ><img src="../images/search_bt.gif" border="0" /></a></td>
                                    <td align="left"><a href="../Employee/AddEmployee.aspx"><img id="imgAddEmployee" src="../images/add_bt.gif" border="0" /></a></td>
                                    <td align="left"><img src="../images/training_details_tab.gif" border="0" /></td>
                                    <td align="left"><a href="../Employee/SkillSets.aspx"><img id="imgDisplaySkillSet" src="../images/display_skillset_active.gif" border="0" /></a></td>
                                  </tr>
                              </table></td>
                            </tr>
                            <tr>
                              <td><table height="100%" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                                  <tbody>
                                    <tr>
                                      <td valign="top" align="center" style="height: 400px" ><table height="100%" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                                          <tr class="center-bg" height="2%">
                                            <td style="width: 1062px; height: 2%"><table class="searchtablestyle" cellspacing="0" cellpadding="0" width="100%" align="left">
                                                <tr>
                                                  <td class="SearchTblHead" align="left" colspan="23">&nbsp;Training Search</td>
                                                </tr>
                                                <tr class="Gridtxt" height="10">
                                                  <td class="Gridtxt" colspan="8" height="10"><img src="../images/spacer.gif" height="10" width="10" /></td>
                                                </tr>
                                                <tr>
                                                  <td width="6%" align="left" valign="middle" class="Labeltxt" style="height: 32px"> Trainer&nbsp;</td>
                                                  <td align="left" valign="middle" class="Gridtxt" style="height: 32px; width: 14%;"><asp:textbox CssClass="Gridtxt" id="txtTGiven" TabIndex="1"  Runat="server" MaxLength="15" Width="108px"></asp:textbox></td>
                                                 <td align="left" valign="middle" class="Labeltxt" style="height: 32px; width: 7%;">&nbsp;Attendee&nbsp;</td>
                                                  <td align="left" valign="middle" class="Gridtxt" style="height: 32px; width: 14%;">
                                                      <asp:textbox id="txtTAtended" TabIndex="2" CssClass="Gridtxt"  Runat="server" Width="108px"></asp:textbox>
                                                  </td>
                                                  <td align="left" valign="middle" class="Labeltxt" style="height: 32px; width: 6%;" >&nbsp;Training&nbsp;</td>
                                                  <td align="left" valign="middle" class="Gridtxt" style="height: 32px; width: 12%;"><asp:TextBox ID="txtTraining" TabIndex="3" CssClass="Gridtxt"  runat="server" Width="108px"></asp:TextBox>
                                                  </td>
                                                  <td style="height: 32px; width: 6px;">&nbsp;&nbsp;&nbsp;</td>
                                                    <td align="center" class="Gridtxt" colspan="2" style="height: 32px; width: 85px;" valign="middle" scope="row">
                                                        &nbsp; &nbsp;&nbsp;
                                                  <asp:imagebutton id="imgBtnSearch" runat="server" ImageUrl="../images/searc_bt.gif" 
                                                            Height="19px" style="CURSOR:HAND; left: -12px; position: relative; top: 6px;" 
                                                            ImageAlign="Middle" onclick="imgBtnSearch_Click">
                                                  </asp:imagebutton>
                                                        &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
                                                        </td>
                                                        <td style="width: 134px; height: 32px">
                                                        <img alt="" id="imgAddNewTraining" onclick="MeetingAddFunction()" <%--"MeetingAddFunction()"--%> src="../images/add new training.gif" ImageAlign="Middle" style="CURSOR:HAND; left: 1px; position: relative; top: 1px;" height="19"/>
                                                        </td>
                                                      <td style="height: 32px" align="center" valign="middle">  
                                                      &nbsp;<asp:DropDownList  AutoPostBack="true" ID="ddlPlanExecuted" CssClass="Gridtxt" runat="server">
                                                      <asp:ListItem Text="Planned Training" Value="1"></asp:ListItem>
                                                      <asp:ListItem Text="Executed Training" Value="2"></asp:ListItem>
                                                      <asp:ListItem Text="Cancelled Training" Value="3"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    </td>
                                                    
                                                </tr>
                                                <tr class="Gridtxt" height="5">
                                                  <td class="Gridtxt" align="right" colspan="8" style="height: 5px"><img src="../images/spacer.gif" height="5" width="5" /> </td>
                                                </tr>
                                            </table>
                                            </td>
                                            <td style="height: 2%"><asp:Button ID="btnRefresh" Width="0" Height="0" 
                                                    runat="server" Visible="false" onclick="btnRefresh_Click"/></td>
                                          </tr>
                                          <tr class="center-bg" >
                                            <td  colspan="2" style="height: 14px"><img height="1" src="../images/spacer.gif" /> </td>
                                          </tr>
                                          <tr class="center-bg" valign="top" align="left"  height="150px">
                                            <td valign="top" class="GridBG" style="width: 1062px"><asp:label Font-Bold ="true" ForeColor="Red" id="lblMessege" runat="server"></asp:label>
                                                <%-- <%=strFaketable%>--%>
                                                <asp:GridView  ID="grdDetails" RowStyle-CssClass ="Gridtxt" DataKeyNames="trainingid" AutoGenerateColumns="false" runat="server"   BorderColor="White"
                                                    BackColor="#F7F7F7" CellPadding="3" GridLines="Vertical" AllowSorting="true" PageSize="6" AllowPaging="true" 
                                                    BorderStyle="None" Height="298px" Width="1124px">
                                                  <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                  <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                  <AlternatingRowStyle BackColor="White"/>
                                                 
                                                  <Columns>
                                                  <asp:TemplateField Visible="False">
                                                    
                                                    <ItemTemplate>
                                                    <asp:Label runat="server" ID="lbl" Text='<%#Eval("trainingid")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    
                                                  </asp:TemplateField>
                                                  
                                                                                                   
                                                  <asp:HyperLinkField DataTextField="trainingname" SortExpression="trainingname" DataNavigateUrlFields="trainingid" DataNavigateUrlFormatString="TDetails.aspx?TrainingNameId={0}" HeaderText="Training Name" />
                                                  <asp:BoundField DataField="date" SortExpression="date" HeaderText="Date" />
                                                  <asp:BoundField DataField="Venuename" HeaderText="Venue"/>
                                                  <asp:BoundField  DataField="time" HeaderText="Time" SortExpression="time"  />
                                                  <asp:BoundField  DataField="duration" HeaderText="Duration"/>
                                                  <asp:TemplateField   HeaderText="Edit">
                                                    <HeaderStyle HorizontalAlign="Center" Width="4%"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate> <img id="imgEdit" align="middle" alt="Edit Meeting" title ="Edit Meeting"  src="../images/pen.gif" onclick="MeetingEditFunction(<%# Eval("trainingid")%>)" style="CURSOR:POINTER" /> </ItemTemplate>
                                                  </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                      <asp:ImageButton ID="ImgDel" AlternateText ="Delete" ToolTip ="Delete" CommandArgument='<%#Eval("trainingid")%>' runat="server" ImageUrl="../images/cross.gif" CommandName="Del"></asp:ImageButton>
                                                    </ItemTemplate>
                                                  </asp:TemplateField>
                                                  </Columns>
                                                <PagerStyle HorizontalAlign="center" CssClass="Pagertxt1" />                         
                                                  <PagerSettings NextPageText="&lt;img src=Images/next_bt.gif border=0&gt;" PreviousPageText="&lt;img src=Images/prev_bt.gif border=0&gt;" Mode="Numeric" />
                                               
                                                </asp:GridView>
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                  <tr>
                                                    <td class="Errortxt" align="left" width="80%">
                                                    <asp:label CssClass="Errortxt" id="LblException" runat="server"></asp:label>
                                                    </td>
                                                    <td align="right" class="Errortxt" width="20%">
                                                    <img id="imgExcel" alt="Export to Excel" src="../images/exp_excel_ico.gif" border="0" onclick='Export()' style="CURSOR:hand" />&nbsp;
                                                    </td>
                                                  </tr>
                                              </table></td>
                                            <td><img src="../images/spacer.gif"/></td>
                                          </tr>
                                          <tr class="center-bg" height="10%">
                                            <td class="gridtxt" style="height: 16%; width: 1062px;">&nbsp;</td>
                                            <td class="gridtxt" style="height: 16%"></td>
                                          </tr>
                                          </table></td>
                                    </tr>
                                  </tbody>
                              </table></td>
                            </tr>
                        </tbody>
                        </table></td>
                        <td valign="top"><img src="../images/spacer.gif" width="10" height="10" /></td>
                      </tr>
							<tr height="10%">
						<td colspan="3" >
							&nbsp;</td>
                </tr>
							</TD></TR></tbody></table>

        
        <%--</TD></TR></tbody></table>--%>
</asp:Content>
