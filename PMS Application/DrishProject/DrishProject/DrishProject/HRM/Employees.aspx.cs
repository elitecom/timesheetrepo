﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using DrishProjectBAL;
using System.Data;
using System.Data.SqlClient;

namespace DrishProject.HRM
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Entities.Employee emp = new Entities.Employee();
            if (!Page.IsPostBack)
            {
                emp = (Entities.Employee)(Session["emp"]);
                DataSet ds = new DataSet();
                ds = new DrishProjectBAL.EmployeeBAL().GetDesignations();
                drpDesignation.DataSource = ds.Tables[0];
                drpDesignation.DataMember = ds.Tables[0].TableName;
                drpDesignation.DataTextField = "Designation";
                drpDesignation.DataValueField = "DesgID";
                drpDesignation.DataBind();
                drpDesignation.Items.Insert(0, " == Select ==");
                ds = new DrishProjectBAL.EmployeeBAL().getdata();
                gridview.DataSource = ds;
                gridview.DataBind();
            }
        }
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            int desgId = -1;
            string name = string.Empty;
            string dob = string.Empty;
            string doj = string.Empty;
            if (drpDesignation.SelectedIndex > 0)
            {
                desgId = Convert.ToInt32(drpDesignation.SelectedValue.ToString());
            }
            if (TextBox1.Text.Length > 0)
            {
                name = TextBox1.Text;
            }
            dob = TextBox2.Text;
            doj = TextBox3.Text;
            DataSet ds = new DataSet();
            ds = new DrishProjectBAL.EmployeeBAL().SearchEmployee(desgId, name, dob, doj);
            gridview.DataSource = ds;
            gridview.DataBind();
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Entities.Employee emp = new Entities.Employee();
            emp = (Entities.Employee)(Session["emp"]);
            DataSet ds = new DataSet();
            ds = new DrishProjectBAL.EmployeeBAL().GetDesignations();
            drpDesignation.DataSource = ds.Tables[0];
            drpDesignation.DataMember = ds.Tables[0].TableName;
            drpDesignation.DataTextField = "Designation";
            drpDesignation.DataValueField = "DesgID";
            drpDesignation.DataBind();
            drpDesignation.Items.Insert(0, " == Select ==");
            ds = new DrishProjectBAL.EmployeeBAL().getdata();
            gridview.DataSource = ds;
            gridview.DataBind();
        }
    }
}