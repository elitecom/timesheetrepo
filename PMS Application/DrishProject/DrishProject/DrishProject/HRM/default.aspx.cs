﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using Entities;

namespace DrishProject.HRM
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Session.Abandon();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Abandon();
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            bool res;
            string role = string.Empty;
            Entities.Employee emp = new Entities.Employee();
            try
            {
                emp.UserName = TextBox1.Text;
                //emp.Password = new EmployeeBAL().Encrypt(TextBox2.Text);
                emp.Password = TextBox2.Text;
                res = new EmployeeBAL().AuthenticateUser(emp);
                if (res == true)
                {
                    role = new EmployeeBAL().GetUserRole(emp.UserName);
                    if (role.ToLower().Equals("Human Resource Manager".ToLower()))
                    {
                        Response.Redirect("Employees.aspx");
                    }
                    else
                    {
                        Response.Write("<script language='JavaScript'>alert('Unauthorised  Access');</script>");
                    }
                }
            }
            catch (Exception)
            {
            }
        }
    }
}