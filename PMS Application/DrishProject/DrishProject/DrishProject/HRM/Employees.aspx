﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HRM/HRM.Master" AutoEventWireup="true" CodeBehind="Employees.aspx.cs" Inherits="DrishProject.HRM.WebForm1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br />
<div style="float:left;">
    <asp:Label ID="Label1" runat="server" Text="Designation" style="margin-left:20px;"></asp:Label>
    <asp:DropDownList ID="drpDesignation" runat="server" style="margin-left:20px;">
        <asp:ListItem>== Select==</asp:ListItem>
    </asp:DropDownList>
    </div>
    <div style="float:left;">
    <asp:Label ID="Label2" runat="server" Text="Name" style="margin-left:20px;"></asp:Label>
    <asp:TextBox ID="TextBox1" runat="server" style="margin-left:10px;"></asp:TextBox>
    </div>
    <div style="float:left;">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
    <asp:Label ID="Label3" runat="server" Text="Date Of Birth" style="margin-left:20px;"></asp:Label>
    <asp:TextBox ID="TextBox2" runat="server" style="margin-left:10px;"></asp:TextBox>
   
        <asp:CalendarExtender ID="TextBox2_CalendarExtender" runat="server" 
            TargetControlID="TextBox2">
        </asp:CalendarExtender>
   </ContentTemplate>
   </asp:UpdatePanel>
  </div>
  <div style="float:left;">
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
     <ContentTemplate>
    <asp:Label ID="Label4" runat="server" Text="Date Of Joining" style="margin-left:20px;"></asp:Label>
    <asp:TextBox ID="TextBox3" runat="server" style="margin-left:10px;"></asp:TextBox>
    
      <asp:CalendarExtender ID="TextBox3_CalendarExtender" runat="server" 
          TargetControlID="TextBox3">
      </asp:CalendarExtender>
    </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    <div style="float:left;">
    <asp:ImageButton ID="ImageButton1" runat="server" 
        ImageUrl="~/Images/searc_bt.gif" onclick="ImageButton1_Click" style="margin-right:20px;margin-left:20px;" />
    <asp:ImageButton ID="ImageButton2" runat="server" 
            ImageUrl="~/Images/display_all_bt.gif" onclick="ImageButton2_Click" />
   </div>
   <br /><br />
    <br />
    <asp:GridView ID="gridview" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="EmpCode" CellPadding="3" BackColor="#DEBA84" 
        BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellSpacing="2">
    <Columns>
    <asp:HyperLinkField HeaderText="EmpCode" DataNavigateUrlFields="EmpCode" DataNavigateUrlFormatString="Myprofile.aspx?EmpCode={0}" DataTextField="EmpCode" />
    <asp:BoundField HeaderText="FirstName" DataField="FirstName" />
    <asp:BoundField HeaderText="LastName" DataField="LastName" />
    <asp:BoundField HeaderText="DOB" DataField="DOB" DataFormatString="{0:dd-MM-yyyy}" />
    <asp:BoundField HeaderText="Phone No" DataField="Phone" />
    <asp:BoundField HeaderText="Mobile No" DataField="Mobile" />    
    <asp:BoundField HeaderText="Email Id" DataField="EmailId" />
      <asp:HyperLinkField DataTextField="EmailId" HeaderText="E-Mail" DataNavigateUrlFormatString="SendMail.aspx?EmpCode={0}" DataNavigateUrlFields="EmpCode" />
    <asp:BoundField HeaderText=" Department" DataField="DeptName" />
    <asp:BoundField HeaderText="Designation" DataField="Designation" />
    <asp:BoundField HeaderText="DOJ" DataField="DOJ"  DataFormatString="{0:dd-MM-yyyy}" />
    <asp:HyperLinkField HeaderText="Edit" DataNavigateUrlFields="EmpCode" DataNavigateUrlFormatString="EditEmployee.aspx?EmpCode={0}"  Text="Edit" />
      
   </Columns>
        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#FFF1D4" />
        <SortedAscendingHeaderStyle BackColor="#B95C30" />
        <SortedDescendingCellStyle BackColor="#F1E5CE" />
        <SortedDescendingHeaderStyle BackColor="#93451F" />
</asp:GridView>
    <br />
    <br />
</asp:Content>

