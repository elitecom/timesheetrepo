﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Entities;
using DrishProjectBAL;

namespace DrishProject.ProjectModules
{
    public partial class Module_edit : System.Web.UI.Page
    {
        Entities.Employee emp = new Entities.Employee();
        Project project = new Project();
        DataSet ds;
        int strQuerystring = 0;
        ProjectModule module = new ProjectModule();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                strQuerystring = Convert.ToInt32(Request.QueryString["Moduleid"].ToString());
            }
            catch (Exception)
            {
                strQuerystring = 0;
            }
            emp = (Entities.Employee)(Session["emp"]);
            project = (Project)(Session["project"]);
            
            if (!Page.IsPostBack)
            {
                ds = new DataSet();
                ds = new ProjectModuleBAL().GetModuleStatus();
                drpModuleStatus.DataSource = ds.Tables[0];
                drpModuleStatus.DataMember = ds.Tables[0].TableName;
                drpModuleStatus.DataTextField = "ModuleStatus";
                drpModuleStatus.DataValueField = "ModuleStatusId";
                drpModuleStatus.DataBind();
                drpModuleStatus.Items.Insert(0, "== Select ==");

                ds = new DataSet();
                ds = new ProjectModuleBAL().GetProjectTeamMembers(project);
                drpAssignedTo.DataSource = ds.Tables[0];
                drpAssignedTo.DataMember = ds.Tables[0].TableName;
                drpAssignedTo.DataTextField = "Name";
                drpAssignedTo.DataValueField = "empid";
                drpAssignedTo.DataBind();
                drpAssignedTo.Items.Insert(0, "== Select ==");

                ds = new DataSet();
                ds = new ProjectModuleBAL().GetProjectTeamManagers(project);
                drpAssignedBy.DataSource = ds.Tables[0];
                drpAssignedBy.DataMember = ds.Tables[0].TableName;
                drpAssignedBy.DataTextField = "Name";
                drpAssignedBy.DataValueField = "empid";
                drpAssignedBy.DataBind();
                drpAssignedBy.Items.Insert(0, "== Select ==");

                ds = new DataSet();
                module.ProjectId = project.ProjectID;
                module.SortCriteria = "ModuleId";
                module.SortDir = "ASC";
                ds = new ProjectModuleBAL().GetSearchModule(module);

                if (strQuerystring == 0)
                {
                    Label8.Text = "Add New Module";
                    //txtEndDate.Text = "";
                    //txtEstimatedHours.Text = "";
                    //txtModuleName.Text = "";
                    //txtStartDate.Text = "";
                }
                else
                {
                    Label8.Text = "Edit Project Module";
                    ds = new DataSet();
                    module.ModuleId = strQuerystring;
                    module = new DrishProjectBAL.ProjectModuleBAL().GetModule(module);
                    txtModuleName.Text = module.ModuleName;
                    txtStartDate.Text = module.StartDate.ToShortDateString();
                    txtEndDate.Text = module.EndDate.ToShortDateString();
                    txtEstimatedHours.Text = module.dEstimateHours.ToString();
                    drpAssignedBy.SelectedValue = module.AssignedBy.ToString();
                    drpAssignedTo.SelectedValue = module.AssignedToId.ToString();
                    drpModuleStatus.SelectedValue = module.ModuleStatusId.ToString();
                }
            }
            
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            string res = string.Empty;            
            module = new ProjectModule();
            module.ProjectId = project.ProjectID;
            module.ModuleName = txtModuleName.Text;
            module.ModuleStatusId = Convert.ToInt32(drpModuleStatus.SelectedValue.ToString());
            module.StartDate = Convert.ToDateTime(txtStartDate.Text.ToString());
            module.EndDate = Convert.ToDateTime(txtEndDate.Text);
            module.AssignedToId = Convert.ToInt32(drpAssignedTo.SelectedValue.ToString());
            module.AssignedBy = Convert.ToInt32(drpAssignedBy.SelectedValue.ToString());
            module.AssignedOn = DateTime.Now.ToShortDateString();
            module.LastModifiedOn = DateTime.Now.ToShortDateString();
            module.dEstimateHours = Convert.ToInt32(txtEstimatedHours.Text);
            if (Label8.Text == "Add New Module")
            {
                res = new DrishProjectBAL.ProjectModuleBAL().InsertModule(module);
            }
            else
            {
                module.ModuleId = new DrishProjectBAL.ProjectModuleBAL().GetModuleIDByName(module.ModuleName);
                res = new DrishProjectBAL.ProjectModuleBAL().UpdateModule(module);
            }
            Label9.Text = res;
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Module_Edit.aspx");
        }
    }
}