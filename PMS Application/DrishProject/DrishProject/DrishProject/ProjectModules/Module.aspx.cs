﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectBAL;
using System.Web.UI.HtmlControls;

namespace DrishProject
{
    public partial class WebForm11 : System.Web.UI.Page
    {
        int ProjectID;
        bool bln_Delete;
        Entities.Employee objUser = new Entities.Employee();

        object objUser1;
        const int EditColIndex = 6;
        const int DelColIndex = 7;
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Project objProject = new Project();
                bln_Delete = false;
                objUser1 = (Entities.Employee)Session["emp"];
                if (Request["ProjectID"] == "")
                {
                    ProjectID = Convert.ToInt32(Session["ProjectID"]);
                }
                else
                {
                    objProject = (Project)(Session["project"]);
                    //Session["ProjectID"] = Request["ProjectID"];
                    //ProjectID = Convert.ToInt32(Session["ProjectID"]);
                    ProjectID = objProject.ProjectID;
                    
                }
                //Code to hide the Release, CheckList, Client and Code Review Tabs for Non Engineering Projects.
                objProject.ProjectID = ProjectID;
                objProject  = new ProjectBAL().GetProject(ProjectID);
                lblProjectCode.Text = objProject.ProjectCode;
                lblProjectName.Text = objProject.ProjectName;
                lblProjectStartDate.Text = objProject.ProjectStartDate.ToShortDateString();
                lblProjectEndDate.Text = objProject.ProjectEndDate.ToShortDateString();

                #region "Unused Code"
                
                //if (objProject.ProjectCategory != "ENGG")
                //{
                //    System.Web.UI.HtmlControls.HtmlTableCell tdFLY;
                //    tdFLY = (HtmlTableCell)FindControl("tdRelease");
                //    tdFLY.Visible = false;
                //    tdFLY = (HtmlTableCell)FindControl("tdClients");
                //    tdFLY.Visible = false;
                //    tdFLY = (HtmlTableCell)FindControl("tdCheckList");
                //    tdFLY.Visible = false;
                //    tdFLY = (HtmlTableCell)FindControl("tdCodeReview");
                //    tdFLY.Visible = false;
                //}
                //if (objUser.IsAministrator == 0)
                //{
                //    //If the user is not the member of that project then deny his access
                //    //if (objUser.IsActiveMember(objUser._EmpId, ProjectID))
                //    if (new EmployeeBAL().IsActiveMember(objUser.EmployeeId,ProjectID) == false)
                //    {
                //        Response.Redirect("Default.aspx?Authority=Deny");
                //    }
                    
                //    //System.Web.UI.HtmlControls.HtmlTableCell tdFLY;
                //    //tdFLY = (HtmlTableCell)FindControl("tdPTimeSheet");
                //    //tdFLY.Visible = false;
                //}
                #endregion
                LblException.Text = "";
                if (!Page.IsPostBack)
                {
                    ds = new DataSet();
                    objProject.ProjectID = ProjectID;
                    ds = new ProjectModuleBAL().GetModuleByProjectId(objProject);
                    dgModules.DataSource = ds.Tables[0];
                    dgModules.DataMember = ds.Tables[0].TableName;
                    dgModules.DataBind();

                    ds = new DataSet();
                    ds = new ProjectModuleBAL().GetModuleStatus();
                    ddlStatus.DataSource = ds.Tables[0];
                    ddlStatus.DataMember = ds.Tables[0].TableName;
                    ddlStatus.DataTextField = "ModuleStatus";
                    ddlStatus.DataValueField = "ModuleStatusId";
                    ddlStatus.DataBind();
                    ddlStatus.Items.Insert(0, "== Select ==");

                    ds = new DataSet();
                    DataRow dr;
                    ds = new ProjectModuleBAL().GetHideModuleStatus();
                    dr = ds.Tables[0].NewRow();
                    dr["priority"] = 0;
                    dr["modulestatus"] = "Hide None";
                    ds.Tables[0].Rows.InsertAt(dr, 0);
                    ddlHStatus.DataSource = ds;
                    ddlHStatus.DataTextField = "modulestatus";
                    ddlHStatus.DataValueField = "priority";
                    ddlHStatus.DataBind();
                }
                               
            }
            catch (Exception ee)
            {
                Response.Write("Message : " + ee.Message);
                Response.Write(ee.StackTrace);
            }
        }

        protected void cmdGo_Click(object sender, ImageClickEventArgs e)
        {
            dgModules.CurrentPageIndex = 0;
            GetModulesPerProject();
        }

        protected void cmdDisplayAll_Click(object sender, ImageClickEventArgs e)
        {
            txtModuleName.Text = "";
            ddlStatus.SelectedIndex = 0;
            dgModules.CurrentPageIndex = 0;
            GetModulesPerProject();
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgModules.CurrentPageIndex = 0;
            GetModulesPerProject();
        }

        protected void ddlHStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgModules.CurrentPageIndex = 0;
            GetModulesPerProject();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            GetModulesPerProject();
        }

        protected void dgModules_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            if (bln_Delete == false)
            {
                bln_Delete = true;
                string strRetVal;
                int intModuleId;
                ProjectModule objModules = new ProjectModule();
                intModuleId = 0;
                strRetVal = "";
                LblException.Text = "";
                intModuleId = Convert.ToInt32(dgModules.DataKeys[e.Item.ItemIndex]);
                objModules.ModuleId = intModuleId;
                strRetVal = new ProjectModuleBAL().DeleteModule(objModules);
                GetModulesPerProject();
                dgModules.EditItemIndex = -1;
                if (strRetVal == "")
                {
                    strRetVal = "Deleted Successfully !";
                }
                LblException.Text = strRetVal;
                //GetModulesPerProject();
            }
        }

        protected void dgModules_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            ImageButton imgbtn;
            imgbtn = (ImageButton)e.Item.FindControl("imgDel");
            if (imgbtn != null)
            {
                imgbtn.Attributes.Add("onclick", "return confirm(\'Are you sure you want to delete?\');");
            }

            if (e.Item.ItemType != ListItemType.Header)
            {
                RegisterStartupScript("Declare", "<script language=\'javascript\'>var realColor;</script>");
                e.Item.Attributes.Add("OnMouseOver", "realColor = this.style.backgroundColor; this.style.backgroundColor = \'#cccccc\';");
                e.Item.Attributes.Add("OnMouseOut", "this.style.backgroundColor = realColor;");
            }
        }

        protected void dgModules_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            if (this.SortCriteria == e.SortExpression)
            {
                if (this.SortDir == "asc")
                {
                    this.SortDir = "desc";
                }
                else
                {
                    this.SortDir = "asc";
                }
            }
            else
            {
                this.SortCriteria = e.SortExpression;
            }
            //rebind the grid
            GetModulesPerProject();
        }

        public string SortCriteria
        {
            get
            {
                return ViewState["SortCriteria"].ToString();
            }
            set
            {
                ViewState["SortCriteria"] = value;
            }
        }
        public string SortDir
        {
            get
            {
                return ViewState["SortDir"].ToString();
            }
            set
            {
                ViewState["SortDir"] = value;
            }
        }

        protected void dgModules_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            LblException.Text = "";
            dgModules.CurrentPageIndex = e.NewPageIndex;
            GetModulesPerProject();
        }

        private void GetModulesPerProject()
        {
            ProjectModule objModules = new ProjectModule();
            LblException.Text = "";

            if (objUser.IsAministrator != 0)
            {
                if (User.IsInRole("EditModules"))
                {
                    dgModules.Columns[EditColIndex].Visible = true;
                }
                else
                {
                    dgModules.Columns[EditColIndex].Visible = false;
                }
            }
            else
            {
                dgModules.Columns[EditColIndex].Visible = true;
            }
            if (objUser.IsAministrator != 0)
            {
                if (User.IsInRole("DeleteModules"))
                {
                    dgModules.Columns[DelColIndex].Visible = true;
                }
                else
                {
                    dgModules.Columns[DelColIndex].Visible = false;
                }
            }
            else
            {
                dgModules.Columns[DelColIndex].Visible = true;
            }

            try
            {
                DataSet ds = new DataSet();
                if (txtModuleName.Text.Trim() == "" && ddlStatus.SelectedIndex == 0)
                {
                    objModules.ProjectId = ProjectID;
                    //objModules.ProjectId = objModules.ProjectId;
                    //objModules.SortCriteria = this.SortCriteria;
                    //objModules.SortDir = this.SortDir;
                    //DS = objModules.GetModulesPerProject();
                    ds = new ProjectModuleBAL().GetModulesPerProject(objModules);
                }
                else
                {
                    ds = GetSearchResults();
                }
                dgModules.DataSource = ds;
                //Code to check whether the CurrentPageIndex value is not less than 0
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if ((Math.Ceiling(double.Parse(ds.Tables[0].Rows.Count.ToString()) / dgModules.PageSize) - 1 < dgModules.PageSize))
                    {
                        dgModules.CurrentPageIndex = 0;
                    }
                }
                else
                {
                    dgModules.CurrentPageIndex = 0;
                }
                dgModules.DataBind();
                // getExcelDataset(DS);

                if (ds.Tables[0].Rows.Count == 0)
                {
                    dgModules.PagerStyle.Visible = false;
                    LblException.Text = "No Module Present.";
                    RegisterStartupScript("strHide", "<script language=\'javascript\'>this.document.all.imgExcel.style.display=\'none\';</script>");
                }
                else
                {
                    RegisterStartupScript("strShow", "<script language=\'javascript\'>this.document.all.imgExcel.style.display=\'block\';</script>");
                    dgModules.PagerStyle.Visible = true;
                    LblException.Text = "";
                }
                if (ds.Tables[0].Rows.Count <= dgModules.PageSize)
                {
                    dgModules.PagerStyle.Visible = false;
                }
                else
                {
                    dgModules.PagerStyle.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private DataSet GetSearchResults()
        {
            //Validations objValidation = new Validations();
            string strRetVal;
            ProjectModule objModules = new ProjectModule();

            try
            {
                string strModuleName;
                string intStatus;
                int intHideStatusId;
                strModuleName = (txtModuleName.Text);
                if (ddlStatus.SelectedValue == "== Select ==")
                {
                    intStatus = "0";
                }
                else
                {
                    intStatus = ddlStatus.SelectedValue.ToString();
                }
                //if (Convert.ToInt32(ddlStatus.SelectedValue) > 0)
                //{
                //    intStatus = ddlStatus.SelectedValue;
                //}
                //else
                //{
                //    intStatus = "0";
                //}

                if (Convert.ToInt32(ddlHStatus.SelectedValue) > 0)
                {
                    intHideStatusId = Convert.ToInt32(ddlHStatus.SelectedValue);
                }
                else
                {
                    intHideStatusId = 0;
                }

                DataSet ds = new DataSet();
                objModules.ModuleName = strModuleName;
                objModules.ModuleStatusId = Convert.ToInt32(intStatus);
                objModules.ModuleHideStatusId = intHideStatusId;
                objModules.ProjectId = objModules.ProjectId;
                //objModules.SortCriteria = this.SortCriteria;
                //objModules.SortDir = this.SortDir;
                objModules.ProjectId = Convert.ToInt32(Session["ProjectID"]);
                ds = new ProjectModuleBAL().GetSearchModule(objModules);                

                return ds;
            }
            catch (Exception ex)
            {
                LblException.Text = ex.Message;
                return null;
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Projects/ProjectDetail.aspx");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TeamMembers/ProjectTeamMembers.aspx");
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectModules/Module.aspx");
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Tasks/MyTasks.aspx");
        }

        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Documents/Documents.aspx");
        }
        
        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Clients/Clients.aspx");
        }

        protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectMeetings/Meeting.aspx");
        }

        protected void ImageButton11_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectNotes/Notes.aspx");
        }

        protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectHistory/History.aspx");
        }

        protected void ImageButton10_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectRelease/Release.aspx");
        }

        protected void ImageButton13_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectReview/Reviews.aspx");
        }

        protected void ImageButton14_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TimeSheets/TimeSheet.aspx");
        }

        protected void btnExport_Click(object sender, ImageClickEventArgs e)
        {
            Entities.Project prj = new Entities.Project();
            Entities.ProjectModule module = new Entities.ProjectModule();
            Entities.Employee emp = new Entities.Employee();
            DataSet ds = new DataSet();
            prj.ProjectID = Convert.ToInt32(Session["ProjectID"]);
            emp = (Entities.Employee)(Session["emp"]); 
            ds = new DrishProjectBAL.ProjectModuleBAL().GetModuleByProjectForUser(prj,emp);
            DataSetToExcel.Convert(ds, Response);
        }
    }
}