﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module_edit.aspx.cs" Inherits="DrishProject.ProjectModules.Module_edit" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 128px;
        }
    </style>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
//        $(function () {
//            $("._datepicker").datepicker({ dateFormat: 'dd/mm/yy' });
//        });
    </script>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
        <table style="height: 282px; width: 323px">
            <caption>
                <asp:Label ID="Label8" runat="server" Text="Label"></asp:Label>               
            </caption>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label1" runat="server" Text="Module Name"></asp:Label>
                </td>
                <td class="style1">
                    <asp:TextBox ID="txtModuleName" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Module Status"></asp:Label>
                </td>
                <td class="style1">
                    <asp:DropDownList ID="drpModuleStatus" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Start Date"></asp:Label>
                </td>
                <td class="style1">
                     <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                    <asp:TextBox ID="txtStartDate" runat="server" CssClass="_datepicker"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" 
                        BehaviorID="txtStartDate_CalendarExtender" TargetControlID="txtStartDate" Format="dd/MM/yyyy">
                    </cc1:CalendarExtender>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="End Date"></asp:Label>
                </td>
                <td class="style1">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                    <asp:TextBox ID="txtEndDate" runat="server" CssClass="_datepicker"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" 
                        BehaviorID="txtEndDate_CalendarExtender" TargetControlID="txtEndDate" Format="dd/MM/yyyy">
                    </cc1:CalendarExtender>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Assigned To"></asp:Label>
                </td>
                <td class="style1">
                    <asp:DropDownList ID="drpAssignedTo" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Assigned By"></asp:Label>
                </td>
                <td class="style1">
                    <asp:DropDownList ID="drpAssignedBy" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Text="Estimated Hours"></asp:Label>
                </td>
                <td class="style1">
                    <asp:TextBox ID="txtEstimatedHours" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/save_bt.gif"
                        OnClick="ImageButton1_Click" />
                </td>
                <td class="style1">
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/reset_bt.gif"
                        OnClick="ImageButton2_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="Label9" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
