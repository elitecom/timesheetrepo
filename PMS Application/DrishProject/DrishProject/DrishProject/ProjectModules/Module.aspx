﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="Module.aspx.cs" Inherits="DrishProject.WebForm11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table id="tblMain" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tbody>
            <tr>
                <td valign="top" width="100%">
                    <!-- HEADER -->
                    <table id="tblHeader" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="top" align="left">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                                    align="center" border="0">
                                    <tr>
                                        <td valign="top">
                                        </td>
                                        <td height="100%" colspan="5" align="left" valign="top">
                                            <table cellspacing="0" cellpadding="0" align="top" border="0">
                                                <tr>
                                                    <td>
                                                        <div>
                                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/details_new.gif"
                                                                OnClick="ImageButton1_Click" />
                                                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/tabs_bt_02.gif"
                                                                OnClick="ImageButton2_Click" />
                                                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/modules_active.gif"
                                                                OnClick="ImageButton3_Click" />
                                                            <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/tasks.gif" OnClick="ImageButton4_Click" />
                                                            <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/tabs_bt_05.gif"
                                                                OnClick="ImageButton5_Click" />
                                                            <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/tabs_bt_03.gif"
                                                                OnClick="ImageButton7_Click" />
                                                            <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/Images/tabs_bt_10.gif"
                                                                OnClick="ImageButton8_Click" />
                                                            <asp:ImageButton ID="ImageButton11" runat="server" ImageUrl="~/Images/tabs_bt_09.gif"
                                                                OnClick="ImageButton11_Click" />
                                                            <asp:ImageButton ID="ImageButton9" runat="server" ImageUrl="~/Images/tabs_bt_07.gif"
                                                                OnClick="ImageButton9_Click" />
                                                            <asp:ImageButton ID="ImageButton10" runat="server" ImageUrl="~/Images/tabs_bt_08.gif"
                                                                OnClick="ImageButton10_Click" />
                                                            <asp:ImageButton ID="ImageButton13" runat="server" ImageUrl="~/Images/review_active.gif"
                                                                OnClick="ImageButton13_Click" />
                                                            <asp:ImageButton ID="ImageButton14" runat="server" ImageUrl="~/Images/timesheet_tab_active.gif"
                                                                OnClick="ImageButton14_Click" />
                                                        </div>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td width="1%">
                                        </td>
                                        <td align="left" valign="top">
                                            <table cellspacing="0" cellpadding="0" align="left" border="0" 
                                                style="height: 425px;width:1338px;">
                                                <tbody valign="top">
                                                    <tr class="center-bg" valign="top">
                                                        <td valign="top" width="100%" colspan="2">
                                                            <table class="center-bg" valign="middle">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="Gridtxt" valign="middle" align="left" style="width: 1391px">
                                                                            <b>Project&nbsp;Code&nbsp;:&nbsp;&nbsp;</b>
                                                                            <asp:Label ID="lblProjectCode" CssClass="Gridtxt" runat="server"></asp:Label>
                                                                            <b>&nbsp;&nbsp;&nbsp;&nbsp; Project&nbsp;Name&nbsp;:&nbsp;&nbsp;</b>
                                                                            <asp:Label ID="lblProjectName" CssClass="Gridtxt" runat="server"></asp:Label>
                                                                            <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Project&nbsp;Start Date&nbsp;:&nbsp;&nbsp;</b>
                                                                            <asp:Label ID="lblProjectStartDate" CssClass="Gridtxt" runat="server"></asp:Label>
                                                                            <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Project&nbsp;End Date&nbsp;:&nbsp;&nbsp;</b>
                                                                            <asp:Label ID="lblProjectEndDate" CssClass="Gridtxt" runat="server"></asp:Label>
                                                                            <asp:Button ID="Button1" runat="server" Visible="False" Width="0px" OnClick="btnRefresh_Click">
                                                                            </asp:Button>
                                                                            <%--<asp:DropDownList ID="DropDownList1" runat="server" Width="0px" AutoPostBack="True">
                         </asp:DropDownList>--%>
                                                                            <img id="addMember" style="cursor: POINTER; margin-left: 120px;" onclick="ModuleAddFunction()"
                                                                                src="../Images/add_module_bt.gif" border="0">
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <%--<table width="100%" align="left" border="0">
																<tr class="Gridtxt">
																	<td class="Gridtxt" width="60%">
																		<table border="0">
																			<tr class="Gridtxt">
																				<td class="Gridtxt" align="left">
																					&nbsp;&nbsp; <strong>Project Code:&nbsp;</strong>
																					<asp:Label CssClass="Gridtxt" ID="lblProjectCode" runat="server"></asp:Label>
																				</td>
																				<td class="Gridtxt" align="left">
																					&nbsp;&nbsp; <strong>Start Date&nbsp;:&nbsp;</strong>
																					<asp:Label CssClass="Gridtxt" ID="lblStartdate" runat="server"></asp:Label>
																				</td>
																				<td class="Gridtxt" align="left">
																					&nbsp;&nbsp; <strong>End Date&nbsp;:&nbsp;</strong>
																					<asp:Label CssClass="Gridtxt" ID="lblEndDate" runat="server"></asp:Label>
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td class="Gridtxt" valign="middle" align="right" width="40%">
																		<img id="imgAddModule" style="cursor: pointer" onclick="ModuleAddFunction();" src="../Images/add_module_bt.gif"
																			border="0">
																	</td>
																	<td class="Gridtxt" width="1%">
																		&nbsp;
																	</td>
																</tr>
															</table>--%>
                                                        </td>
                                                    </tr>
                                                    <tr class="center-bg">
                                                        <td valign="top" align="left" style="width: 96%">                                                            
                                                            <table  cellspacing="0" cellpadding="0" width="100%" align="left">
                                                                <tr>
                                                                    <td class="SearchTblHead" colspan="5">
                                                                        &nbsp;Module Search
                                                                    </td>
                                                                </tr>
                                                                <tr height="5" class="Center-bg">
                                                                    <td height="5" align="right" colspan="5">
                                                                        <img height="5" src="../Images/spacer.gif">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="Gridtxt" style="width: 23%">
                                                                        <b>
                                                                            <asp:Label ID="Label1" runat="server" Text="Module Status:   "></asp:Label></b>
                                                                        <asp:DropDownList CssClass="Gridtxt" ID="ddlStatus" runat="server" Width="130px"
                                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td align="left" class="Gridtxt" style="width: 372px">
                                                                        <b>
                                                                            <asp:Label ID="Label2" runat="server" Text="Show/Hide Status:"></asp:Label>&nbsp;</b><asp:DropDownList CssClass="Gridtxt" ID="ddlHStatus" runat="server" Width="175px"
                                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlHStatus_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                        <asp:Button CssClass="Gridtxt" ID="btnRefresh" runat="server" Width="0px" Visible="false"
                                                                            OnClick="btnRefresh_Click"></asp:Button>
                                                                        <%--<asp:dropdownlist CssClass="Gridtxt" id="Dropdownlist1" runat="server" Width="1px" AutoPostBack="true"
																		Height="1px"></asp:dropdownlist>--%>
                                                                    </td>
                                                                    <td width="12%" class="Labeltxt" align="right">
                                                                        Module Name&nbsp;
                                                                    </td>
                                                                    <td class="Gridtxt" style="width: 10%">
                                                                        <asp:TextBox CssClass="Gridtxt" ID="txtModuleName" runat="server" Width="141" MaxLength="50"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 237px">
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:ImageButton ID="cmdGo" runat="server" ImageUrl="../Images/searc_bt.gif" OnClick="cmdGo_Click">
                                                                        </asp:ImageButton>
                                                                        &nbsp;
                                                                        &nbsp;
                                                                        <asp:ImageButton ID="cmdDisplayAll" runat="server" ImageUrl="../Images/display_all_bt.gif"
                                                                            OnClick="cmdDisplayAll_Click"></asp:ImageButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="center-bg" align="right" colspan="5" style="height: 11px">
                                                                        <img height="5" src="../Images/spacer.gif">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <img src="../Images/spacer.gif">
                                                        </td>
                                                    </tr>
                                                    <tr class="center-bg" valign="top" align="left">
                                                        <td class="GridBG" height="100%" valign="top" style="width: 96%">
                                                            <asp:DataGrid ID="dgModules" runat="server" Width="95%" AutoGenerateColumns="False"
                                                                BackColor="#F7F7F7" BorderColor="White" DataKeyField="moduleid" CellPadding="3"
                                                                AllowSorting="true" GridLines="Vertical" BorderStyle="None" AllowPaging="true"
                                                                OnDeleteCommand="dgModules_DeleteCommand" OnItemDataBound="dgModules_ItemDataBound"
                                                                OnPageIndexChanged="dgModules_PageIndexChanged" OnSortCommand="dgModules_SortCommand">
                                                                <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                                <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                                <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                                <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                                <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="modulename" SortExpression="modulename" ReadOnly="true"
                                                                        HeaderText="Module Name">
                                                                        <HeaderStyle Width="25%"></HeaderStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="ModuleStatus" SortExpression="modulestatus" ReadOnly="true"
                                                                        HeaderText="Status">
                                                                        <HeaderStyle Width="8%"></HeaderStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Modulestartdate" SortExpression="startdate" ReadOnly="true"
                                                                        HeaderText="Start Date">
                                                                        <HeaderStyle Width="10%"></HeaderStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Moduleenddate" SortExpression="enddate" ReadOnly="true"
                                                                        HeaderText="End Date">
                                                                        <HeaderStyle Width="10%"></HeaderStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:HyperLinkColumn DataNavigateUrlField="Emailid" DataNavigateUrlFormatString="mailto:{0}"
                                                                        DataTextField="Assigned To" SortExpression="AssignedTo" HeaderText="Assigned To">
                                                                        <HeaderStyle Width="10%"></HeaderStyle>
                                                                    </asp:HyperLinkColumn>
                                                                    <asp:BoundColumn DataField="estimatehours" SortExpression="estimatehours" HeaderText="Est. Hrs">
                                                                        <HeaderStyle Width="7%"></HeaderStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn HeaderText="Edit">
                                                                        <HeaderStyle Font-Names="Arial" Font-Bold="true" HorizontalAlign="Center" ForeColor="Black"
                                                                            Width="5%" BackColor="Gainsboro"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <img id="imageEdit" align="middle" src="../Images/pen.gif" onclick='ModuleEditFunction(<%# Eval("moduleid")%>)'
                                                                                style="cursor: pointer">
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Delete">
                                                                        <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="ImgDel" runat="server" ImageUrl="../Images/cross.gif" CommandName="Delete">
                                                                            </asp:ImageButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Tasks">
                                                                        <HeaderStyle HorizontalAlign="Center" Width="7%"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <img id="imgTasks" align="middle" src="../Images/book2.gif" onclick='ModuleTasksFunction(<%# Eval("moduleid")%>)'
                                                                                style="cursor: pointer">
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="History">
                                                                        <HeaderStyle Width="6%"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <img id="imgDel" src="../Images/project_work_plan_icon3.gif" onclick='ModuleHistory(<%# Eval("Moduleid")%>, <%# Eval("Projectid")%>);'
                                                                                style="cursor: pointer"></ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                                <PagerStyle NextPageText="&lt;img src=../Images/next_bt.gif border=0&gt;" PrevPageText="&lt;img src=../Images/prev_bt.gif border=0&gt;"
                                                                    CssClass="Pagertxt" Mode="NumericPages"></PagerStyle>
                                                            </asp:DataGrid>
                                                            <table cellpadding="0" cellspacing="0" border="0" style="width: 96%">
                                                                <tr>
                                                                    <td class="Errortxt" align="left" width="80%">
                                                                        <asp:Label CssClass="Errortxt" ID="LblException" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td align="right" class="Errortxt" width="20%">
                                                                        <asp:ImageButton ID="btnExport" runat="server" ImageUrl="../Images/exp_excel_ico.gif"
            Style="width: 30px; height: 16px; margin-left: 1260px;" OnClick="btnExport_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <img height="10" src="../Images/spacer.gif" width="10">
                                                        </td>
                                                    </tr>
                                                    <%--<tr class="center-bg" height="10%">
														<td class="Gridtxt">
														</td>
														<td class="Gridtxt">
															&nbsp;
														</td>
														<td class="Gridtxt">
														</td>
													</tr>--%>
                                                    <tr class="center-bg" height="10%">
                                                        <td valign="top" align="left" style="width: 96%">
                                                            <img height="10" src="../Images/spacer.gif" width="10">
                                                        </td>
                                                        <td valign="bottom" align="right">
                                                            <img height="10" src="../Images/right_bottom_corner.gif" width="10">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                            <img height="10" src="../Images/spacer.gif" width="10" alt="" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="80%" valign="top">
                <td valign="top" width="100%">
                    <!-- CENTER -->
                </td>
            </tr>
            <!--FOOTER -->
            <%--<tr>
				<td width="100%" valign="top">
					<table id="tblFooter" cellspacing="0" cellpadding="0" width="100%" border="0">
						<tr>
							<td valign="top" align="left">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td valign="top" align="left">
								&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>--%>
        </tbody>
    </table>
    <script type="text/javascript">
        var popup;
        function ShowPopup(url) {
            popup = window.open(url, "Popup", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=100,height=100,left = 490,top = 262");
            popup.focus();
        }
    </script>
    <script language="javascript" type="text/javascript">
        function EnterVal(evt) {
            var mytarget;
            var keyCode;
            var is_ie;
            if (document.all) {
                keyCode = event.keyCode;
                mytarget = window.event.srcElement.type;
                is_ie = true;
            }
            else if (document.addEventListener) {
                // Firefox

                mytarget = evt.target.type;
                keyCode = evt.keyCode;
                is_ie = false;
            }
            else {
                // Need to add a default or other browser cases here.
                return true; // Just give up and exit for now.
            }
            if (keyCode == 13) {
                evt.returnValue = false;
                evt.cancel = true;
                if (!is_ie) {
                    evt.preventdefault();
                    evt.stopPropagation();
                }
                document.all('cmdGo').click();
            }
        }

        function ModuleAddFunction() {
            var leftPos = (screen.width / 2) - 300;
            var topPos = (screen.height / 2) - 215;
            window.open("Module_edit.aspx?Moduleid=0", "ModuleAdd", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=600, height=430");
        }
        function ModuleEditFunction(id) {
            var leftPos = (screen.width / 2) - 300;
            var topPos = (screen.height / 2) - 215;
            window.open("Module_edit.aspx?Moduleid=" + id, "ModuleEdit", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=600, height=430");
        }

        function ModuleTasksFunction(id) {
            window.location.href = "Tasks.aspx?Moduleid=" + id;
        }
        function PageRefresh() {
            __doPostBack('btnRefresh', '');
        }
        function ModuleHistory(intModuleid, intProjectid) {
            window.location.href = "Project_History.aspx?Moduleid=" + intModuleid + "&Projectid=" + intProjectid;
        }
        function Export() {
            var leftPos = (screen.width / 2) - 400
            var topPos = (screen.height / 2) - 250;
            window.open("ExportExcel.aspx?DataSet=Modules", "ExportExcel", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=yes, scrollbars=yes resizable=yes, copyhistory=yes, width=800, height=500");
        }
    </script>
</asp:Content>
