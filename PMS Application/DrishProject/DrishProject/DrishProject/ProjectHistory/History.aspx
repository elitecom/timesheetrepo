﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="History.aspx.cs" Inherits="DrishProject.ProjectHistory.History" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <h2>
            Project History</h2>
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/details_new.gif"
            OnClick="ImageButton1_Click" />
        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/tabs_bt_02.gif"
            OnClick="ImageButton2_Click" />
        <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/tabs_bt_06.gif"
            OnClick="ImageButton3_Click" />
        <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/tasks.gif" OnClick="ImageButton4_Click" />
        <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/tabs_bt_05.gif"
            OnClick="ImageButton5_Click" />
        <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/tabs_bt_03.gif"
            OnClick="ImageButton7_Click" />
        <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/Images/tabs_bt_10.gif"
            OnClick="ImageButton8_Click" />
        <asp:ImageButton ID="ImageButton11" runat="server" ImageUrl="~/Images/tabs_bt_09.gif"
            OnClick="ImageButton11_Click" />
        <asp:ImageButton ID="ImageButton9" runat="server" ImageUrl="~/Images/history_active.gif"
            OnClick="ImageButton9_Click" />
        <asp:ImageButton ID="ImageButton10" runat="server" ImageUrl="~/Images/tabs_bt_08.gif"
            OnClick="ImageButton10_Click" />
        <asp:ImageButton ID="ImageButton13" runat="server" ImageUrl="~/Images/review_active.gif"
            OnClick="ImageButton13_Click" />
        <asp:ImageButton ID="ImageButton14" runat="server" ImageUrl="~/Images/timesheet_tab_active.gif"
            OnClick="ImageButton14_Click" />
    </div>
    <table id="tblMain" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr height="80%">
            <td width="100%">
                <!-- CENTER -->
                <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                    align="center" border="0">
                    <tr height="100%">
                        <td width="1.5%" style="height: 100%">
                            &nbsp;
                        </td>
                        <td align="left" style="height: 100%">
                            <table height="98%" cellspacing="0" cellpadding="0" width="99%" align="left" border="0">
                                <tbody>
                                    <tr class="center-bg" valign="top">
                                        <td class="Gridtxt" valign="top" height="5%">
                                            <table class="center-bg" valign="middle">
                                                <tbody>
                                                    <tr>
                                                        <td class="Gridtxt" valign="middle" align="left">
                                                            <b>Project&nbsp;Code&nbsp;:&nbsp;&nbsp;</b>
                                                            <asp:Label ID="lblProjectCode" CssClass="Gridtxt" runat="server"></asp:Label>
                                                            <b>&nbsp;&nbsp;&nbsp;&nbsp; Project&nbsp;Name&nbsp;:&nbsp;&nbsp;</b>
                                                            <asp:Label ID="lblProjectName" CssClass="Gridtxt" runat="server"></asp:Label>
                                                            <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Project&nbsp;Start Date&nbsp;:&nbsp;&nbsp;</b>
                                                            <asp:Label ID="lblProjectStartDate" CssClass="Gridtxt" runat="server"></asp:Label>
                                                            <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Project&nbsp;End Date&nbsp;:&nbsp;&nbsp;</b>
                                                            <asp:Label ID="lblProjectEndDate" CssClass="Gridtxt" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                        </td>
                                    </tr>
                                    <tr class="center-bg" valign="top">
                                        <td class="Gridtxt" valign="top" height="5%">
                                            <strong>Select History</strong> &nbsp;&nbsp;<asp:RadioButton ID="rdbProject" runat="server"
                                                Text="Project" AutoPostBack="true" GroupName="History" OnCheckedChanged="rdbProject_CheckedChanged">
                                            </asp:RadioButton>&nbsp;&nbsp;
                                            <asp:RadioButton CssClass="Gridtxt" ID="rdbModule" runat="server" Text="Module" AutoPostBack="true"
                                                GroupName="History" OnCheckedChanged="rdbModule_CheckedChanged"></asp:RadioButton>&nbsp;&nbsp;
                                            <asp:RadioButton CssClass="Gridtxt" ID="rdbTask" runat="server" Text="Task" AutoPostBack="true"
                                                GroupName="History" OnCheckedChanged="rdbTask_CheckedChanged"></asp:RadioButton>&nbsp;
                                            <asp:RadioButton CssClass="Gridtxt" ID="rdbRelease" runat="server" Text="Release"
                                                AutoPostBack="true" GroupName="History" OnCheckedChanged="rdbRelease_CheckedChanged">
                                            </asp:RadioButton>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="center-bg" valign="top">
                                        <td class="Gridtxt" valign="top" height="5%">
                                            <strong>
                                                <asp:Label CssClass="Gridtxt" ID="lblModule" runat="server">Project Modules</asp:Label></strong>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:DropDownList CssClass="Gridtxt" ID="ddlModule" runat="server" AutoPostBack="true"
                                                Width="400px" OnSelectedIndexChanged="ddlModule_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            &nbsp;&nbsp;
                                            <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Module Tasks "></asp:Label>
                                            &nbsp;<asp:DropDownList CssClass="Gridtxt" ID="ddlTasks" runat="server" AutoPostBack="true"
                                                Width="400px" OnSelectedIndexChanged="ddlTasks_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Gridtxt" valign="top">
                                        </td>
                                    </tr>
                                    <tr class="center-bg" valign="top">
                                        <td valign="top" align="left" width="100%" bgcolor="#f7f7f7" height="80%" style="height: 200px">
                                            <!--Modules-->
                                            <asp:DataList ID="dtlDates" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
                                                BorderWidth="1px" Width="100%" ShowFooter="False" BackColor="#F7F7F7" 
                                                OnItemDataBound="dtlDates_ItemDataBound" Height="131px">
                                                <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                <ItemTemplate>
                                                    </td> </tr>
                                                    <tr class="Gridtxt">
                                                        <td width="5px">
                                                            &nbsp;
                                                        </td>
                                                        <td width="3%" class="tdStyle">
                                                            <img runat="server" id="imgExCol" src="../images/plus-sign.gif" title="Expand" style="cursor: pointer">
                                                        </td>
                                                        <td width="97%" class="tdStyle" runat="server" id="FlyRowServer" title="Expand" style="cursor: pointer">
                                                            <asp:TextBox CssClass="Gridtxt" ID="txtdate" runat="server" Visible="False" Text='<%# Eval("ID")%>'>
                                                            </asp:TextBox><asp:Label ID="tdDate" CssClass="Gridtxt" runat="server" Text='<%# Eval("Name")%>'
                                                                ForeColor="#000000" Font-Size="9pt" Font-Bold="true">
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr class="Gridtxt">
                                                        <td class="Gridtxt" width="5px">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt" colspan="2">
                                                            <div id="innerGrid" runat="server" style="display: None">
                                                                <asp:DataGrid ID="grdDetails" OnItemDataBound="grdDetails_ItemDataBound" runat="server"
                                                                    BorderStyle="None" GridLines="Vertical" CellPadding="3" AutoGenerateColumns="False"
                                                                    Width="100%" BackColor="#F7F7F7" BorderColor="White">
                                                                    <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                                    <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                                    <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                                    <HeaderStyle CssClass="SubGridHead"></HeaderStyle>
                                                                    <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="Status" HeaderText="Status Change" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="30%"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Name" HeaderText="Modified By" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="10%"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="ModifiedOn" HeaderText="Modified On" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="10%" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Remarks" HeaderText="Remarks" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="70%"></asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                            </asp:DataList>
                                            <!--Projects-->
                                            <asp:DataGrid ID="GrdProjects" runat="server" Width="100%" BackColor="#F7F7F7" AllowPaging="true"
                                                BorderColor="White" AutoGenerateColumns="False" CellPadding="3" GridLines="Vertical"
                                                BorderStyle="None" OnPageIndexChanged="GrdProjects_PageIndexChanged" PageSize="6">
                                                <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="Status" HeaderText="Status Change">
                                                        <HeaderStyle Width="30%"></HeaderStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Name" HeaderText="Modified By">
                                                        <HeaderStyle Width="15%"></HeaderStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ModifiedOn" HeaderText="Modified On" DataFormatString="{0:dd-MM-yyyy}">
                                                        <HeaderStyle Width="10%"></HeaderStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Remarks" HeaderText="Remarks"></asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle NextPageText="&lt;img src=Images/next_bt.gif border=0&gt;" PrevPageText="&lt;img src=Images/prev_bt.gif border=0&gt;"
                                                    CssClass="Pagertxt" Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid><asp:Label CssClass="Errortxt" ID="LblException" runat="server"></asp:Label>
                                            <td class="center-bg">
                                                <img height="10" src="../images/spacer.gif" width="10">
                                            </td>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!--FOOTER -->
    <%--<tr>
        <td width="100%" valign="top">
        </td>
    </tr>
    </table>--%>
</asp:Content>
