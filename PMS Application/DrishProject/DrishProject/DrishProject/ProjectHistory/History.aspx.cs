﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectBAL;

namespace DrishProject.ProjectHistory
{
	public partial class History : System.Web.UI.Page
	{
		int ProjectID;
		Entities.Project objProject = new Entities.Project();
		Entities.ProjectRelease objRelease = new Entities.ProjectRelease();
		Entities.ProjectModule objModule = new Entities.ProjectModule();
		DataSet dsParent = new DataSet();
		DataView objDataview;
		protected System.Web.UI.HtmlControls.HtmlTableCell tblCount;
		DataSet dsChild = new DataSet();
	
		protected void Page_Load(object sender, EventArgs e)
		{
			objProject = (Entities.Project)(Session["project"]);

			#region "Unused Code"            
			//if (string.IsNullOrEmpty(Request["ProjectID"].ToString()))
			//{
			//    ProjectID = objProject.ProjectID;
			//}
			//else
			//{
			//    Session["ProjectID"] = Request["ProjectID"];
			//    ProjectID = objProject.ProjectID;
			//}
			#endregion

			ProjectID = objProject.ProjectID;
			Entities.Employee objEmployee = new Entities.Employee();
			objEmployee = (Entities.Employee)(Session["emp"]);
			//Code to hide the Release, CheckList, Client and Code Review Tabs for Non Engineering Projects.
			objProject.ProjectID = ProjectID;
			objProject = new ProjectBAL().GetProject(ProjectID);

			lblProjectCode.Text = objProject.ProjectCode;
			lblProjectName.Text = objProject.ProjectName;
			lblProjectStartDate.Text = objProject.ProjectStartDate.ToShortDateString();
			lblProjectEndDate.Text = objProject.ProjectEndDate.ToShortDateString();

			#region "Unused Code"            
			//if (!(objProject.ProjectCategory == "ENGG"))
			//{
			//    System.Web.UI.HtmlControls.HtmlTableCell tdFLY = null;
			//    tdFLY = FindControl("tdRelease");
			//    tdFLY.Visible = false;
			//    tdFLY = FindControl("tdClients");
			//    tdFLY.Visible = false;
			//    tdFLY = FindControl("tdCheckList");
			//    tdFLY.Visible = false;
			//    tdFLY = FindControl("tdCodeReview");
			//    tdFLY.Visible = false;
			//    rdbRelease.Visible = false;
			//}
			#endregion

			try
			{
				if (!Page.IsPostBack)
				{
                    DataSet ds = new DataSet();
                    objProject.ProjectID = ProjectID;
                    ds = new ProjectModuleBAL().GetModuleByProjectId(objProject);
                    ddlModule.DataSource = ds.Tables[0];
                    ddlModule.DataMember = ds.Tables[0].TableName;
                    ddlModule.DataTextField = "ModuleName";
                    ddlModule.DataValueField = "ModuleId";
                    ddlModule.DataBind();
                        
					if (objEmployee.IsAministrator == 0)
					{
						//If the user is not the member of that project then deny his access
						if (!(new EmployeeBAL().IsActiveMember(objEmployee.EmployeeId, ProjectID)))
						{
							Response.Redirect("Default.aspx?Authority=Deny");
						}
						//System.Web.UI.HtmlControls.HtmlTableCell tdFLY = null;
						//tdFLY = FindControl("tdPTimeSheet");
						//tdFLY.Visible = false;
					}
					objDataview = new DataView();
					if ((Request["TaskID"] != null) & (Request["ProjectID"] != null))
					{
						ProjectID = Convert.ToInt32(Request["ProjectID"].ToString());
						int TaskID = 0;
						TaskID = Convert.ToInt32(Request["TaskID"].ToString());

						rdbTask.Checked = true;
						lblModule.Visible = true;
						lblModule.Text = "Select Task";
						ddlModule.Visible = false;
						ddlTasks.Visible = true;
						GrdProjects.Visible = false;
						dtlDates.Visible = true;
						GetTaskNames(TaskID);
						//To bind the dropdown with tasks and select the requested task.

						dsParent = new ProjectModuleTaskBAL().GetSelectedTask(ProjectID, TaskID);
						dsChild = new ProjectModuleTaskBAL().GetSelectedTaskHistory(ProjectID, TaskID);
						objDataview = dsChild.Tables[0].DefaultView;
						dtlDates.DataSource = dsParent.Tables[0].DefaultView;
						dtlDates.DataBind();
						if (dsParent.Tables[0].Rows.Count == 0)
						{
							LblException.Text = "No History Present for this Task.";
						}
						else
						{
							LblException.Text = "";
						}
					}
					else if ((Request["ModuleID"] != null) & (Request["ProjectID"] != null))
					{
						ProjectID = Convert.ToInt32(Request["ProjectID"].ToString());
						int ModuleID = 0;
						ModuleID = Convert.ToInt32(Request["ModuleID"].ToString());

						rdbModule.Checked = true;
						lblModule.Visible = true;
						lblModule.Text = "Select Module";
						ddlModule.Visible = true;
						ddlTasks.Visible = false;
						GrdProjects.Visible = false;
						dtlDates.Visible = true;
						GetModuleNames(ModuleID);
						//To bind the dropdown with modules and select the requested module.

						dsParent = new ProjectModuleBAL().GetSelectedModule(ProjectID, ModuleID);
						dsChild = new ProjectModuleBAL().GetSelectedModuleHistory(ProjectID, ModuleID);
						objDataview = dsChild.Tables[0].DefaultView;
						dtlDates.DataSource = dsParent.Tables[0].DefaultView;
						dtlDates.DataBind();
						if (dsParent.Tables[0].Rows.Count == 0)
						{
							LblException.Text = "No History Present for this Module.";
						}
						else
						{
							LblException.Text = "";
						}
					}
					else
					{
						rdbProject.Checked = true;
						lblModule.Visible = true;
						ddlModule.Visible = true;
						ddlTasks.Visible = true;
						GrdProjects.Visible = true;
						dtlDates.Visible = true;
						ViewState["PageSize"] = 4;
						ViewState["CurrentPage"] = 1;
						GetProjectHistory();
					}                   
				}             
			}
			catch (Exception ex)
			{
				LblException.Text = ex.Message;
			}   
		}

		private void GetTaskNames(int TaskID)
		{
			try 
			{
				DataSet DS = new DataSet();
				DS = new ProjectModuleBAL().GetTaskNamesByTaskID(TaskID);
				DataRow dRow = null;
				dRow = DS.Tables[0].NewRow();
				dRow["ID"] = "0";
				dRow["DESC"] = "--Select--";
				DS.Tables[0].Rows.InsertAt(dRow, 0);
				ddlTasks.DataSource = DS;
				ddlTasks.DataTextField = "DESC";
				ddlTasks.DataValueField = "ID";
				ddlTasks.DataBind();
				ddlTasks.SelectedValue = TaskID.ToString();
			} 
			catch (Exception ex) 
			{
				throw ex;
			}
		}

		private void GetTaskName(int ProjectID)
		{
			try
			{
				DataSet DS = new DataSet();
				DS = new ProjectModuleBAL().GetTaskNames(ProjectID);
				DataRow dRow = null;
				dRow = DS.Tables[0].NewRow();
				dRow["ID"] = "0";
				dRow["DESC"] = "--Select--";
				DS.Tables[0].Rows.InsertAt(dRow, 0);
				ddlTasks.DataSource = DS;
				ddlTasks.DataTextField = "DESC";
				ddlTasks.DataValueField = "ID";
				ddlTasks.DataBind();                
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private void GetModuleNames(int ModuleID)
		{
			DataSet DS = new DataSet();
			DS = new ProjectModuleBAL().GetModuleNames(ProjectID);
			DataRow dRow = null;
			dRow = DS.Tables[0].NewRow();
			dRow["ID"] = "0";
			dRow["DESC"] = "--Select--";
			DS.Tables[0].Rows.InsertAt(dRow, 0);
			ddlModule.DataSource = DS;
			ddlModule.DataTextField = "DESC";
			ddlModule.DataValueField = "ID";
			ddlModule.DataBind();
			ddlModule.SelectedValue = ModuleID.ToString();
		}

		private void GetProjectHistory()
		{
			try
			{
				DataSet DS = new DataSet();
				objProject.ProjectID = ProjectID;
				DS = new ProjectBAL().GetProjectHistory(ProjectID);
				GrdProjects.DataSource = DS.Tables[0];
				GrdProjects.DataMember = DS.Tables[0].TableName;
				GrdProjects.DataBind();
				if (DS.Tables[0].Rows.Count == 0)
				{
					GrdProjects.PagerStyle.Visible = false;
					LblException.Text = "No History Present.";
				}
				else
				{
					GrdProjects.PagerStyle.Visible = true;
					LblException.Text = "";
				}
				if (DS.Tables[0].Rows.Count <= GrdProjects.PageSize)
				{
					GrdProjects.PagerStyle.Visible = false;
				}
				else
				{
					GrdProjects.PagerStyle.Visible = true;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private void GetReleaseHistory()
		{
			try
			{
				DataSet DS = new DataSet();
				objRelease.ProjectId = ProjectID;
				Entities.ProjectRelease release = new Entities.ProjectRelease();
				release.ProjectId = ProjectID;
				DS = new ProjectReleaseBAL().GetReleaseHistory(release);
				GrdProjects.DataSource = DS;
				GrdProjects.DataBind();
				if (DS.Tables[0].Rows.Count == 0)
				{
					GrdProjects.PagerStyle.Visible = false;
					LblException.Text = "No History Present.";
				}
				else
				{
					LblException.Text = "";
					GrdProjects.PagerStyle.Visible = true;
				}
				if (DS.Tables[0].Rows.Count <= GrdProjects.PageSize)
				{
					GrdProjects.PagerStyle.Visible = false;
				}
				else
				{
					GrdProjects.PagerStyle.Visible = true;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private void GetModuleHistory()
		{
			objDataview = new DataView();
			dsParent = new ProjectModuleBAL().GetModuleNames(ProjectID);
			dsChild = new ProjectModuleBAL().GetModuleHistory(ProjectID);
			objDataview = dsChild.Tables[0].DefaultView;
			dtlDates.DataSource = dsParent.Tables[0].DefaultView;
			dtlDates.DataBind();
			if (dsParent.Tables[0].Rows.Count == 0)
			{
				LblException.Text = "No History Present for this Module.";
			}
			else
			{
				LblException.Text = "";
			}
		}

		private void GetTaskHistory()
		{
			objDataview = new DataView();
			dsParent = new ProjectModuleTaskBAL().GetTasks(ProjectID);
			dsChild = new ProjectModuleTaskBAL().GetSelectedTaskHistory(ProjectID);
			objDataview = dsChild.Tables[0].DefaultView;
			dtlDates.DataSource = dsParent.Tables[0].DefaultView;
			dtlDates.DataBind();
			if (dsParent.Tables[0].Rows.Count == 0)
			{
				LblException.Text = "No History Present for this Task.";
			}
			else
			{
				LblException.Text = "";
			}
		}


		protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
		{
			Response.Redirect("~/Projects/ProjectDetail.aspx");
		}

		protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
		{
			Response.Redirect("~/TeamMembers/ProjectTeamMembers.aspx");
		}

		protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
		{
			Response.Redirect("~/ProjectModules/Module.aspx");
		}

		protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
		{
			Response.Redirect("~/Tasks/MyTasks.aspx");
		}

		protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
		{
			Response.Redirect("~/Documents/Documents.aspx");
		}       

		protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
		{
			Response.Redirect("~/Clients/Clients.aspx");
		}

		protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
		{
			Response.Redirect("~/ProjectMeetings/Meeting.aspx");
		}

		protected void ImageButton11_Click(object sender, ImageClickEventArgs e)
		{
			Response.Redirect("~/ProjectNotes/Notes.aspx");
		}

		protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
		{
			Response.Redirect("~/ProjectHistory/History.aspx");
		}

		protected void ImageButton10_Click(object sender, ImageClickEventArgs e)
		{
			Response.Redirect("~/ProjectRelease/Release.aspx");
		}

		protected void ImageButton13_Click(object sender, ImageClickEventArgs e)
		{
			Response.Redirect("~/ProjectReview/Reviews.aspx");
		}

		protected void ImageButton14_Click(object sender, ImageClickEventArgs e)
		{
			Response.Redirect("~/TimeSheets/TimeSheet.aspx");
		}

		protected void rdbProject_CheckedChanged(object sender, EventArgs e)
		{
			if (rdbProject.Checked)
			{
				lblModule.Visible = false;
				ddlModule.Visible = false;
				ddlTasks.Visible = false;
				GrdProjects.Visible = true;
				dtlDates.Visible = false;
				GetProjectHistory();
			}
		}

		protected void rdbModule_CheckedChanged(object sender, EventArgs e)
		{
			if (rdbModule.Checked)
			{
				lblModule.Visible = true;
				lblModule.Text = "Select Module";
				ddlModule.Visible = true;
				ddlTasks.Visible = false;
				GrdProjects.Visible = false;
				dtlDates.Visible = true;
				GetModuleNames(ProjectID);
				GetModuleHistory();
			}
		}

		protected void rdbTask_CheckedChanged(object sender, EventArgs e)
		{
			if (rdbTask.Checked)
			{
				lblModule.Visible = true;
				lblModule.Text = "Select Task";
				ddlModule.Visible = false;
				ddlTasks.Visible = true;
				GrdProjects.Visible = false;
				dtlDates.Visible = true;
				GetTaskNames(ProjectID);
				GetTaskHistory();
			}
		}

		protected void rdbRelease_CheckedChanged(object sender, EventArgs e)
		{
			if (rdbRelease.Checked)
			{
				lblModule.Visible = false;
				ddlModule.Visible = false;
				ddlTasks.Visible = false;
				GrdProjects.Visible = true;
				dtlDates.Visible = false;
				GetReleaseHistory();
			}
		}

		protected void GrdProjects_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
		{
			GrdProjects.CurrentPageIndex = e.NewPageIndex;
			GetProjectHistory();
		}

	   
		public void grdDetails_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			//if (!(e.Item.ItemType == ListItemType.Header))
			//{
			//    RegisterStartupScript("Declare", "<script language='javascript'>var realColor;</script>");
			//    e.Item.Attributes.Add("OnMouseOver", "realColor = this.style.backgroundColor; this.style.backgroundColor = '#cccccc';");
			//    e.Item.Attributes.Add("OnMouseOut", "this.style.backgroundColor = realColor;");
			//}
		}


		protected void dtlDates_ItemDataBound(object sender, DataListItemEventArgs e)
		{
			TextBox BOX = new TextBox();
			string strDate = null;
			BOX = (TextBox)(e.Item.FindControl("txtDate"));
			if (BOX == null)
			{
			}
			else
			{
				strDate = BOX.Text.TrimEnd();
			}
			DataGrid FlyGrid = new DataGrid();
			FlyGrid = (DataGrid)(e.Item.FindControl("grdDetails"));
			if (FlyGrid == null)
			{
			}
			else
			{
				objDataview.RowFilter = "ID='" + strDate + "'";
				if (objDataview.Count > 0)
				{
					FlyGrid.DataSource = objDataview;
					FlyGrid.DataBind();
				}
			}

			//HtmlControls.HtmlImage FlyImage1 = new HtmlControls.HtmlImage();
			//FlyImage1 = e.Item.FindControl("imgExCol");
			//FlyImage1.Attributes.Add("OnClick", "exColMe(this);");

			//HtmlControls.HtmlTableCell FlyRow = new HtmlControls.HtmlTableCell();
			//FlyRow = e.Item.FindControl("FlyRowServer");
			//FlyRow.Attributes.Add("OnClick", "exColMe(document.getElementById('" + FlyImage1.ClientID + "')); if (this.title=='Collapse'){this.title='Expand';}else{this.title='Collapse';}");

		}

		protected void ddlModule_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (ddlModule.SelectedValue == "0")
			{
				rdbModule_CheckedChanged(rdbModule, e);
			}
			else
			{
				objDataview = new DataView();
				dsParent = new ProjectModuleBAL().GetSelectedModule(ProjectID, Convert.ToInt32(ddlModule.SelectedValue.ToString()));
				dsChild = new ProjectModuleBAL().GetSelectedModuleHistory(ProjectID, Convert.ToInt32(ddlModule.SelectedValue.ToString()));
				objDataview = dsChild.Tables[0].DefaultView;
				dtlDates.DataSource = dsParent.Tables[0].DefaultView;
				dtlDates.DataBind();
				if (dsParent.Tables[0].Rows.Count == 0)
				{
					LblException.Text = "No History Present for this Module.";
				}
				else
				{
					LblException.Text = "";
				}
			}
		}

		protected void ddlTasks_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (ddlTasks.SelectedValue == "0")
			{
				rdbTask_CheckedChanged(rdbTask, e);
			}
			else
			{
				objDataview = new DataView();
				dsParent = new ProjectModuleTaskBAL().GetSelectedTask(ProjectID,Convert.ToInt32(ddlTasks.SelectedValue.ToString()));
				dsChild = new ProjectModuleTaskBAL().GetSelectedTaskHistory(ProjectID,Convert.ToInt32(ddlTasks.SelectedValue.ToString()));
				objDataview = dsChild.Tables[0].DefaultView;
				dtlDates.DataSource = dsParent.Tables[0].DefaultView;
				dtlDates.DataBind();
				if (dsParent.Tables[0].Rows.Count == 0)
				{
					LblException.Text = "No History Present for this Task.";
				}
				else
				{
					LblException.Text = "";
				}
			}
		}
	}
}