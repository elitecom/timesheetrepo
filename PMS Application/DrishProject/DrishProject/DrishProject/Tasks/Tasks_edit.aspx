﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tasks_edit.aspx.cs" Inherits="DrishProject.Tasks.Tasks_edit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 196px;
        }
        .style2
        {
            width: 233px;
        }
    </style>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <table style="height: 424px; width: 481px">
            <caption><strong>Edit Task</strong></caption>
            <tr>
                <td class="style1"><asp:Label ID="Label1" runat="server" Text="Project Name"></asp:Label></td>
                <td class="style2"><asp:Label ID="lblProjectName" runat="server" Text="lblProjectName"></asp:Label></td>
            </tr>
            <tr>
                <td class="style1"><asp:Label ID="Label3" runat="server" Text="Module Name"></asp:Label></td>
                <td class="style2"><asp:DropDownList ID="drpModuleName" runat="server">
                </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="style1"><asp:Label ID="Label4" runat="server" Text="Assigned To"></asp:Label></td>
                <td class="style2"><asp:DropDownList ID="drpAssignedTo" runat="server">
                </asp:DropDownList></td>
            </tr>
             <tr>
                <td class="style1"><asp:Label ID="Label5" runat="server" Text="Task Desc"></asp:Label></td>
                <td class="style2"><asp:TextBox ID="txtDesc" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="style1"> <asp:Label ID="Label2" runat="server" Text="Start Date"></asp:Label></td>
                <td class="style2"><asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>               
                <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox>
                <asp:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" TargetControlID="txtStartDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </ContentTemplate>
        </asp:UpdatePanel></td>
            </tr>
            <tr>
                <td class="style1"><asp:Label ID="Label7" runat="server" Text="End Date"></asp:Label></td>
                <td class="style2">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                
                <ContentTemplate>               
                <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
                <asp:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" TargetControlID="txtEndDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </ContentTemplate>    </asp:UpdatePanel></td>
            </tr>
            <tr>
                <td class="style1"><asp:Label ID="Label8" runat="server" Text="Task Status"></asp:Label></td>
                <td class="style2"><asp:DropDownList ID="drpTaskStatus" runat="server">
                </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="style1"><asp:Label ID="Label9" runat="server" Text="Assigned By"></asp:Label></td>
                <td class="style2"><asp:DropDownList ID="drpAssignedBy" runat="server">
                </asp:DropDownList> </td>
            </tr>
            <tr>
                <td class="style1"><asp:Label ID="Label10" runat="server" Text="Estimated Hours"></asp:Label></td>
                <td class="style2"><asp:TextBox ID="txtEstimatedHrs" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="style1"><asp:Label ID="Label11" runat="server" Text="Q.A. Assigned"></asp:Label></td>
                <td class="style2"><asp:DropDownList ID="drpQAAssigned" runat="server">
                </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="style1"><asp:Label ID="Label12" runat="server" Text="QA Estimated Hours"></asp:Label></td>
                <td class="style2"><asp:TextBox ID="txtQAHours" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center"><asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="~/Images/save_bt.gif"  onclick="ImageButton1_Click" /></td>
                <td align="center" class="style2"><asp:ImageButton runat="server" ImageUrl="~/Images/reset_bt.gif" /></td>
            </tr>
        </table>
        
        
       </div>
    </form>
</body>
</html>
