﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectBAL;

namespace DrishProject.Tasks
{
    public partial class Tasks_edit : System.Web.UI.Page
    {
        ProjectModule objModules = new ProjectModule();
        ProjectModuleTasks tasks = new ProjectModuleTasks();
        DataSet ds;
        int Empid;
        int ProjectID;
        Entities.Employee objUser = new Entities.Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            objUser = (Entities.Employee)Session["emp"];
            Project objProject = new Project();
            if (Request["ProjectCode"] == "")
            {
                objProject = (Project)(Session["Project"]);
                ProjectID = objProject.ProjectID;
            }
            else
            {
                ProjectID = Convert.ToInt32(Session["ProjectID"]);
            }
            objProject.ProjectID = ProjectID;
            objProject = new ProjectBAL().GetProject(ProjectID);
            lblProjectName.Text = objProject.ProjectName;

            if (!Page.IsPostBack)
            {
                objModules.ProjectId = objProject.ProjectID;
                ds = new ProjectModuleBAL().GetModules(objModules);

                drpModuleName.DataSource = ds.Tables[0];
                drpModuleName.DataMember = ds.Tables[0].TableName;
                drpModuleName.DataTextField = "ModuleName";
                drpModuleName.DataValueField = "ModuleID";
                drpModuleName.DataBind();
                drpModuleName.Items.Insert(0, "== Select ==");

                ds = new DataSet();
                ds = new ProjectModuleBAL().GetProjectTeamMembers(objProject);
                drpAssignedTo.DataSource = ds.Tables[0];
                drpAssignedTo.DataMember = ds.Tables[0].TableName;
                drpAssignedTo.DataTextField = "Name";
                drpAssignedTo.DataValueField = "EmpID";
                drpAssignedTo.DataBind();
                drpAssignedTo.Items.Insert(0, "== Select ==");

                ds = new DataSet();
                ds = new ProjectModuleTaskBAL().GetStatusList();
                drpTaskStatus.DataSource = ds.Tables[0];
                drpTaskStatus.DataMember = ds.Tables[0].TableName;
                drpTaskStatus.DataTextField = "ModuleStatus";
                drpTaskStatus.DataValueField = "ModuleStatusID";
                drpTaskStatus.DataBind();
                drpTaskStatus.Items.Insert(0, "== Select ==");

                ds = new DataSet();
                ds = new ProjectModuleBAL().GetProjectTeamManagers(objProject);
                drpAssignedBy.DataSource = ds.Tables[0];
                drpAssignedBy.DataMember = ds.Tables[0].TableName;
                drpAssignedBy.DataTextField = "Name";
                drpAssignedBy.DataValueField = "EmpID";
                drpAssignedBy.DataBind();
                drpAssignedBy.Items.Insert(0, "== Select ==");

                ds = new DataSet();
                ds = new ProjectModuleBAL().GetProjectTeamQA(objProject);
                drpQAAssigned.DataSource = ds.Tables[0];
                drpQAAssigned.DataMember = ds.Tables[0].TableName;
                drpQAAssigned.DataTextField = "Name";
                drpQAAssigned.DataValueField = "EmpID";
                drpQAAssigned.DataBind();
                drpQAAssigned.Items.Insert(0, "== Select ==");
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            bool res;
            string action = string.Empty;
            action = Convert.ToString(Request.QueryString["varAction"]);
            tasks.ProjectId = ProjectID;
            tasks.ModuleId = Convert.ToInt32(drpModuleName.SelectedValue.ToString());
            tasks.AssignedTo = Convert.ToInt32(drpAssignedTo.SelectedValue.ToString());
            tasks.TaskDesc = txtDesc.Text;
            tasks.StartDate = Convert.ToDateTime(txtStartDate.Text);
            tasks.EndDate = Convert.ToDateTime(txtEndDate.Text);
            tasks.TaskStatusId = Convert.ToInt32(drpTaskStatus.SelectedValue.ToString());
            tasks.AssignedBy = Convert.ToInt32(drpAssignedBy.SelectedValue.ToString());
            tasks.AssignedOn = Convert.ToDateTime(DateTime.Now);
            tasks.LastModifiedBy = Convert.ToInt32(drpAssignedBy.SelectedValue.ToString());
            tasks.LastModifiedOn = Convert.ToDateTime(DateTime.Now);
            tasks.EstimateHours = Convert.ToDecimal(txtEstimatedHrs.Text);
            tasks.QAAssigned = Convert.ToInt32(drpQAAssigned.SelectedValue.ToString());
            tasks.EstimateQaHours = Convert.ToDecimal(txtQAHours.Text);
            res = new DrishProjectBAL.ProjectModuleTaskBAL().AddProjectModulesTask(tasks);
            
        }
    }
}