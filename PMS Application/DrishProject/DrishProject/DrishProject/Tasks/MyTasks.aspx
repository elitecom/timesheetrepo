﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="MyTasks.aspx.cs" Inherits="DrishProject.Tasks.MyTasks" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <br />
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/details_new.gif"
            OnClick="ImageButton1_Click" Enabled="false" />
        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/tabs_bt_02.gif"
            OnClick="ImageButton2_Click" />
        <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/tabs_bt_06.gif"
            OnClick="ImageButton3_Click" />
        <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/tasks_active.gif"
            OnClick="ImageButton4_Click" />
        <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/tabs_bt_05.gif"
            OnClick="ImageButton5_Click" />
        <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/tabs_bt_03.gif"
            OnClick="ImageButton7_Click" />
        <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/Images/tabs_bt_10.gif"
            OnClick="ImageButton8_Click" />
        <asp:ImageButton ID="ImageButton11" runat="server" ImageUrl="~/Images/tabs_bt_09.gif"
            OnClick="ImageButton11_Click" />
        <asp:ImageButton ID="ImageButton9" runat="server" ImageUrl="~/Images/tabs_bt_07.gif"
            OnClick="ImageButton9_Click" />
        <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/Images/tabs_bt_08.gif"
            OnClick="ImageButton6_Click" />
        <asp:ImageButton ID="ImageButton10" runat="server" ImageUrl="~/Images/review_active.gif"
            OnClick="ImageButton10_Click" />
        <asp:ImageButton ID="ImageButton12" runat="server" ImageUrl="~/Images/timesheet_tab_active.gif"
            OnClick="ImageButton12_Click" />
    </div>
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
        <table style="width: 1264px;">
            <tr>
                <td colspan="8" style="text-align: right">
                    <asp:ImageButton ID="addTasks" style="cursor: POINTER" OnClientClick="TaskAddFunction()" src="../Images/add_new_task_bt.gif" runat="server" />
                    <%--<img id="addTasks" style="cursor: POINTER" onclick="TaskAddFunction()" src="../Images/add_new_task_bt.gif"
                        border="0" />--%>
                </td>
            </tr>
            <tr>
                <td class="SearchTblHead" colspan="8">
                    Task Search
                </td>
            </tr>
            <tr>
                <td class="Gridtxt" align="left" style="width: 167px">
                    <asp:DropDownList ID="ddlEmployees" runat="server" CssClass="Gridtxt" Width="150px"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlEmployees_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td class="Gridtxt" align="left" style="width: 125px">
                    <asp:DropDownList ID="ddlTaskStatus" CssClass="Gridtxt" AutoPostBack="True" Width="100px"
                        runat="server" OnSelectedIndexChanged="ddlTaskStatus_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td class="Gridtxt" align="center" style="width: 102px">
                    <asp:DropDownList ID="ddlHideStatus" CssClass="Gridtxt" AutoPostBack="True" Width="170px"
                        runat="server" OnSelectedIndexChanged="ddlHideStatus_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td class="Labeltxt" align="center" style="width: 109px">
                    Start Date
                </td>
                <td class="Gridtxt" align="center" style="width: 81px">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtStartDate" CssClass="Gridtxt" Width="70px" runat="server" MaxLength="15"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" BehaviorID="txtStartDate_CalendarExtender"
                                TargetControlID="txtStartDate" Format="dd/MM/yyyy" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="Labeltxt" align="center" style="width: 156px">
                    &nbsp;&nbsp;End Date
                </td>
                <td class="Gridtxt" align="center" style="width: 257px">
                    <asp:UpdatePanel ID="updatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEndDate" CssClass="Gridtxt" Width="70px" runat="server" MaxLength="15"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" BehaviorID="txtEndDate_CalendarExtender"
                                TargetControlID="txtEndDate" Format="dd/MM/yyyy" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="Labeltxt" align="left" style="width: 354px">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="imgBtnSearch"
                        runat="server" ImageUrl="~/Images/searc_bt.gif" OnClick="imgBtnSearch_Click">
                    </asp:ImageButton>
                    &nbsp;<asp:DropDownList ID="DropDownList2" runat="server" CssClass="Gridtxt" AutoPostBack="True"
                                        Width="0px" Visible="false">
                                    </asp:DropDownList><asp:Button ID="Button2" runat="server" CssClass="Gridtxt"
                                        Width="0px" Visible="false" OnClick="btnRefresh_Click"></asp:Button>
                    &nbsp;&nbsp;&nbsp; &nbsp;
                    <asp:ImageButton ID="imgBtnDisplay" runat="server" ImageUrl="~/Images/display_all_bt.gif"
                        OnClick="imgBtnDisplay_Click"></asp:ImageButton>
                    &nbsp;&nbsp;
                </td>
            </tr>
        </table>
    </div>
    <div>
    </div>
    <div>
        <asp:DataGrid ID="GrdMyTasks" runat="server" Width="110%" BorderStyle="None" GridLines="Vertical"
            CellPadding="3" AutoGenerateColumns="False" AllowSorting="True" BackColor="#F7F7F7"
            BorderColor="White" AllowPaging="True" CellSpacing="1" OnItemDataBound="GrdMyTasks_ItemDataBound"
            OnPageIndexChanged="GrdMyTasks_PageIndexChanged" 
            OnSortCommand="GrdMyTasks_SortCommand">
            <SelectedItemStyle Font-Bold="True"></SelectedItemStyle>
            <ItemStyle CssClass="Gridtxt"></ItemStyle>
            <HeaderStyle CssClass="GridHead"></HeaderStyle>
            <FooterStyle CssClass="GridFoot"></FooterStyle>
            <Columns>
                <asp:HyperLinkColumn DataNavigateUrlField="ProjectID" DataNavigateUrlFormatString="~\Projects\ProjectDetail.aspx?ProjectID={0}"
                    DataTextField="ProjectCode" SortExpression="ProjectCode" HeaderText="Project Code">
                    <HeaderStyle Width="10%"></HeaderStyle>
                </asp:HyperLinkColumn>
                <asp:BoundColumn DataField="TaskDesc" SortExpression="TaskDesc" HeaderText="Task Description">
                    <HeaderStyle Width="35%"></HeaderStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn SortExpression="TaskDesc" HeaderText="Module - Tasks">
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Width="25%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <ItemTemplate>
                        <a href='#' onclick='TaskDetails(<%# Eval("TaskId")%>, <%# Eval("ModuleId")%>, <%# Eval("ProjectId")%>);'>
                            <%# Eval("TaskDesc")%>
                        </a>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="StartDate" SortExpression="C.StartDate" HeaderText="Start Date"
                    DataFormatString="{0:dd-MM-yyyy}">
                    <HeaderStyle Width="10%"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="EndDate" SortExpression="C.EndDate" HeaderText="End Date"
                    DataFormatString="{0:dd-MM-yyyy}">
                    <HeaderStyle Width="10%"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="EstimateHours" SortExpression="ESTHours" HeaderText="Est. Hours">
                    <HeaderStyle Width="8%"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ModuleStatus" SortExpression="TaskStatus" HeaderText="Task Status">
                    <HeaderStyle Width="10%"></HeaderStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Status">
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Width="5%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <img id="imgDel" src="../Images/right_mark.gif" onclick='TaskStatus(<%# Eval("TaskId")%>, <%# Eval("ProjectId")%>);'
                            style="cursor: POINTER">
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="History">
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Width="10%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <img id="imgDel" src="../Images/project_work_plan_icon3.gif" onclick='TaskHistory(<%# Eval("TaskId")%>, <%# Eval("ProjectId")%>);'
                            style="cursor: POINTER">
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn Visible="False" DataField="MajorIssues" HeaderText="majorIssues">
                </asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="MinorIssues" HeaderText="MinorIssues">
                </asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="Priority" HeaderText="Priority"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="TaskId" HeaderText="TaskId"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="ModuleId" HeaderText="ModuleId"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="ProjectId" HeaderText="ProjectId"></asp:BoundColumn>
            </Columns>
            <PagerStyle NextPageText="&lt;img src=Images/next_bt.gif border=0&gt;" PrevPageText="&lt;img src=Images/prev_bt.gif border=0&gt;"
                CssClass="Pagertxt" Mode="NumericPages"></PagerStyle>
        </asp:DataGrid>
        <br />
        <asp:Label ID="LblException" runat="server" Text="Label"></asp:Label>
    </div>
    <script language="javascript" type="text/javascript">
        function TasksEditFunction(ID) {
            var leftPos = (screen.width / 2) - 225;
            var topPos = (screen.height / 2) - 150;
            window.open("Tasks_edit.aspx?varAction=edit&MemberId=" + ID, "MemberEdit", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no,titlebar=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=450, height=300");
        }

        function TaskAddFunction() {
            var leftPos = (screen.width / 2) - 225;
            var topPos = (screen.height / 2) - 150;
            window.open("Tasks_edit.aspx?varAction=add", "MemberAdd", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=450, height=300");
        }
        function PageRefresh() {
            __doPostBack('btnRefresh', '');
        }
    </script>
</asp:Content>
