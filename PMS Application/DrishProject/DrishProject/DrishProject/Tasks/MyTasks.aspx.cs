﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectBAL;

namespace DrishProject.Tasks
{
    public partial class MyTasks : System.Web.UI.Page
    {
        ProjectModule objModules = new ProjectModule();
        int ProjectID;
        Entities.Employee objUser = new Entities.Employee();
        DataSet ds = new DataSet();

        protected void Page_Load(object sender, EventArgs e)
        {   
            objUser = (Entities.Employee)Session["emp"];
            Project objProject = new Project();
            //objEmployee = (Entities.Employee)Session["emp"];
            if (Request["ProjectCode"] == "")
            {
                //ProjectID = Convert.ToInt32(Session["ProjectID"]);
                objProject = (Project)(Session["Project"]);
                ProjectID = objProject.ProjectID;
            }            

            if (ProjectID > 0)
            {
                objProject.ProjectID = ProjectID;
                objProject = new ProjectBAL().GetProject(ProjectID);
            }
            else
            {
                try
                {
                    int pid = Convert.ToInt32(Session["ProjectID"].ToString());
                    objProject.ProjectID = pid;
                    objProject = new ProjectBAL().GetProject(pid);
                    ProjectID = pid;
                }
                catch (Exception)
                { }
            }
            //lblProjectCode.Text = objProject.ProjectCode;
            //lblProjectName.Text = objProject.ProjectName;
            //lblProjectStartDate.Text = objProject.ProjectStartDate.ToShortDateString();
            //lblProjectEndDate.Text = objProject.ProjectEndDate.ToShortDateString();
            try
            {
                if (!Page.IsPostBack)
                {
                    //if (objUser.IsAministrator == 0)
                    if (new EmployeeBAL().IsAdministartor(objUser) == true)
                    {
                        addTasks.Visible = true;
                        if (User.IsInRole("ViewEmployeeList"))
                        {
                            ddlEmployees.Visible = true;
                        }
                        else
                        {
                            ddlEmployees.Visible = false;
                        }
                    }
                    else
                    {
                        addTasks.Visible = false;
                    }

                    //this.sortCriteria = "C.EndDate";
                    //this.sortDir = "DESC";
                    GetStatusList();
                    GetHideStatusList();
                    GetEmployeeList();
                    if (ProjectID == 0)
                    {
                        GetMyTasksList(objUser);
                    }
                    else
                    {
                        GetMyTasksList(objUser, ProjectID);
                    }

                    ds = new DataSet();
                    objUser.EmployeeId = ((Entities.Employee)Session["emp"]).EmployeeId;
                    ds = new ProjectModuleTaskBAL().GetMyTaskList(objUser);
                    //ImageButton imgHeader = new ImageButton();
                    //imgHeader = (ImageButton)Page.FindControl("UC_Header1").FindControl("imgTasks");
                    //imgHeader.ImageUrl = "../Images/mytask_active.gif";

                    RegisterStartupScript("OnLoadFocus", "<script language = \'javascript\'>document.getElementById(\'txtStartDate\').focus();</script>");

                    if (Request["Member"] == "Inactive")
                    {
                        LblException.Text = "You are not an active member of this project.";
                    }
                }
                txtEndDate.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                //GetNoOfMyTasks();
            }
            catch (Exception ex)
            {
                //LblException.Text = ex.Message;
            }
        }
        private void GetStatusList()
        {
            try
            {
                DataSet DS = new DataSet();
                DS = new ProjectModuleBAL().GetModuleStatus();
                DataRow dRow;
                dRow = DS.Tables[0].NewRow();
                dRow["ModuleStatusID"] = 0;
                dRow["ModuleStatus"] = "All Statuses";
                DS.Tables[0].Rows.InsertAt(dRow, 0);
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        ddlTaskStatus.DataSource = DS;
                        ddlTaskStatus.DataTextField = "ModuleStatus";
                        ddlTaskStatus.DataValueField = "ModuleStatusID";
                        ddlTaskStatus.DataBind();
                        //ddlTaskStatus.SelectedValue = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        //Function to bind dropdownlist ddlHideStatus with Hide Module Status
        private void GetHideStatusList()
        {
            try
            {               
                DataSet DS = new DataSet();
                DataRow dRow;
                DS = new ProjectModuleBAL().GetHideModuleStatus();

                dRow = DS.Tables[0].NewRow();
                dRow["Priority"] = 0;
                dRow["ModuleStatus"] = "Hide None";
                DS.Tables[0].Rows.InsertAt(dRow, 0);
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        ddlHideStatus.DataSource = DS;
                        ddlHideStatus.DataTextField = "ModuleStatus";
                        ddlHideStatus.DataValueField = "Priority";
                        ddlHideStatus.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private void GetEmployeeList()
        {
            ProjectTeam objTeam = new ProjectTeam();
            try
            {
                DataSet DSEmployee = new DataSet();
                if (objUser.IsAministrator == 1)
                {
                    DSEmployee = new EmployeeBAL().GetAllEmployees();
                }
                else
                {
                    objTeam.EmpId = objUser.EmployeeId;
                    DSEmployee = new EmployeeBAL().GetEmployeeListPerTeamLead(objUser);
                }
                if (DSEmployee.Tables.Count > 0)
                {
                    if (DSEmployee.Tables[0].Rows.Count > 0)
                    {
                        ddlEmployees.DataTextField = "Name";
                        ddlEmployees.DataValueField = "EmpId";
                        ddlEmployees.DataSource = DSEmployee;
                        ddlEmployees.DataBind();
                        //ddlEmployees.SelectedValue = objUser._EmpId;
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private DataSet GetMyTasksList(Entities.Employee objUser)
        {
            try
            {
                ProjectModuleTasks objMyTasks = new ProjectModuleTasks();
                DataSet DS = new DataSet();
                if (ddlEmployees.Visible == true)
                {
                    //if (Convert.ToInt32(ddlTaskStatus.SelectedItem.Value) == 0 && ddlHideStatus.SelectedItem.Value == GetHideStatusValue("ShowDefaultMyTasks") && (txtStartDate.Text) == "" && (txtEndDate.Text) == "")
                    if (Convert.ToInt32(ddlTaskStatus.SelectedItem.Value) == 0 && (txtStartDate.Text) == "" && (txtEndDate.Text) == "")
                    {

                        objMyTasks.AssignedTo = Convert.ToInt32(ddlEmployees.SelectedValue);
                        DS = new ProjectModuleTaskBAL().GetMyTaskList(objUser);
                    }
                    else
                    {
                        //DS = GetSearchResult();
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlTaskStatus.SelectedItem.Value) == 0 && (txtStartDate.Text) == "" && (txtEndDate.Text) == "")
                    {
                        Entities.Employee objUsers = new Entities.Employee();
                        objMyTasks.AssignedTo = objUser.EmployeeId;
                        //objMyTasks.SortCriteria = this.sortCriteria;
                        //objMyTasks.SortDir = this.sortDir;
                        //DS = objMyTasks.GetMyTaskList()
                        Entities.Employee temp = new Entities.Employee();
                        temp.EmployeeId = objUser.EmployeeId;
                        DS = new ProjectModuleTaskBAL().GetMyTaskList(temp);
                    }
                    else
                    {
                        //DS = GetSearchResult();
                    }
                }

                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        GrdMyTasks.DataSource = DS;
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        GrdMyTasks.DataSource = dt;
                        GrdMyTasks.DataBind();
                        LblException.Text = "No Tasks Found";
                    }
                }

                //Code to check whether the CurrentPageIndex value is not less than 0
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        if (Math.Ceiling(double.Parse(DS.Tables[0].Rows.Count.ToString()) / GrdMyTasks.PageSize) - 1 < GrdMyTasks.CurrentPageIndex)
                        {
                            GrdMyTasks.CurrentPageIndex = 0;
                        }
                    }
                    else
                    {
                        GrdMyTasks.CurrentPageIndex = 0;
                        LblException.Text = "No Tasks Found";
                    }
                }
                else
                {
                    GrdMyTasks.CurrentPageIndex = 0;
                    LblException.Text = "No Tasks Found";
                }

                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count == 0)
                    {
                        LblException.Text = "No Task found under any Project.";
                        GrdMyTasks.PagerStyle.Visible = false;
                        RegisterStartupScript("strHide", "<script language=\'javascript\'>this.document.all.imgExcel.style.display=\'none\';</script>");
                    }
                    else
                    {
                        GrdMyTasks.DataBind();
                        //GetExcelDataset(DS);
                        RegisterStartupScript("strShow", "<script language=\'javascript\'>this.document.all.imgExcel.style.display=\'block\';</script>");
                        LblException.Text = "";
                        GrdMyTasks.PagerStyle.Visible = true;
                    }
                }
                else
                {
                    LblException.Text = "No Task found under any Project.";
                    GrdMyTasks.PagerStyle.Visible = false;
                    RegisterStartupScript("strHide", "<script language=\'javascript\'>this.document.all.imgExcel.style.display=\'none\';</script>");
                }
                

                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count <= GrdMyTasks.PageSize)
                    {
                        GrdMyTasks.PagerStyle.Visible = false;
                    }
                    else
                    {
                        GrdMyTasks.PagerStyle.Visible = true;
                    }
                }
                else
                {
                    GrdMyTasks.PagerStyle.Visible = false;
                    LblException.Text = "No Tasks Found";
                }
                return null;

            }
            catch (Exception ex)
            {
                //Response.Write("Message : " + ex.Message);
                //Response.Write(ex.StackTrace);
                throw (ex);
            }
        }

        private DataSet GetMyTasksList(Entities.Employee objUser, int ProjectID)
        {
            try
            {
                ProjectModuleTasks objMyTasks = new ProjectModuleTasks();
                DataSet DS = new DataSet();
                if (ddlEmployees.Visible == true)
                {
                    //if (Convert.ToInt32(ddlTaskStatus.SelectedItem.Value) == 0 && ddlHideStatus.SelectedItem.Value == GetHideStatusValue("ShowDefaultMyTasks") && (txtStartDate.Text) == "" && (txtEndDate.Text) == "")
                    if (Convert.ToInt32(ddlTaskStatus.SelectedItem.Value) == 0 && (txtStartDate.Text) == "" && (txtEndDate.Text) == "")
                    {

                        objMyTasks.AssignedTo = Convert.ToInt32(ddlEmployees.SelectedValue);
                        DS = new ProjectModuleTaskBAL().GetMyProjectTaskList(objUser,ProjectID);
                    }
                    else
                    {
                        //DS = GetSearchResult();
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlTaskStatus.SelectedItem.Value) == 0 && (txtStartDate.Text) == "" && (txtEndDate.Text) == "")
                    {
                        Entities.Employee objUsers = new Entities.Employee();
                        objMyTasks.AssignedTo = objUser.EmployeeId;
                        //objMyTasks.SortCriteria = this.sortCriteria;
                        //objMyTasks.SortDir = this.sortDir;
                        //DS = objMyTasks.GetMyTaskList()
                        Entities.Employee temp = new Entities.Employee();
                        temp.EmployeeId = objUser.EmployeeId;
                        DS = new ProjectModuleTaskBAL().GetMyProjectTaskList(temp,ProjectID);
                    }
                    else
                    {
                        //DS = GetSearchResult();
                    }
                }

                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        GrdMyTasks.DataSource = DS;
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        GrdMyTasks.DataSource = dt;
                        GrdMyTasks.DataBind();
                    }
                }

                //Code to check whether the CurrentPageIndex value is not less than 0
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        if (Math.Ceiling(double.Parse(DS.Tables[0].Rows.Count.ToString()) / GrdMyTasks.PageSize) - 1 < GrdMyTasks.CurrentPageIndex)
                        {
                            GrdMyTasks.CurrentPageIndex = 0;
                        }
                    }
                    else
                    {
                        GrdMyTasks.CurrentPageIndex = 0;
                    }
                }
                else
                {
                    GrdMyTasks.CurrentPageIndex = 0;
                }

                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count == 0)
                    {
                        LblException.Text = "No Task found under any Project.";
                        GrdMyTasks.PagerStyle.Visible = false;
                        RegisterStartupScript("strHide", "<script language=\'javascript\'>this.document.all.imgExcel.style.display=\'none\';</script>");
                    }
                    else
                    {
                        GrdMyTasks.DataBind();
                        //GetExcelDataset(DS);
                        RegisterStartupScript("strShow", "<script language=\'javascript\'>this.document.all.imgExcel.style.display=\'block\';</script>");
                        LblException.Text = "";
                        GrdMyTasks.PagerStyle.Visible = true;
                    }
                }
                else
                {
                    LblException.Text = "No Task found under any Project.";
                    GrdMyTasks.PagerStyle.Visible = false;
                    RegisterStartupScript("strHide", "<script language=\'javascript\'>this.document.all.imgExcel.style.display=\'none\';</script>");
                }
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count <= GrdMyTasks.PageSize)
                    {
                        GrdMyTasks.PagerStyle.Visible = false;
                    }
                    else
                    {
                        GrdMyTasks.PagerStyle.Visible = true;
                    }
                }
                else
                {
                    GrdMyTasks.PagerStyle.Visible = false;
                }
                return null;

            }
            catch (Exception ex)
            {
                Response.Write("Message : " + ex.Message);
                Response.Write(ex.StackTrace);
                throw (ex);
            }
        }

        private void GetNoOfMyTasks()
        {
            //ProjectModuleTasks objTasks = new ProjectModuleTasks();
            //Entities.Employee objUser = new Entities.Employee();
            //if (ddlEmployees.Visible == true)
            //{
            //    //objTasks.GetNoOfMyTasks(Convert.ToInt32(ddlEmployees.SelectedValue));
            //    objTasks = new ProjectModuleTaskBAL().GetNoOfMyTasks(objUser);                
            //}
            //else
            //{
            //    objTasks.GetNoOfMyTasks(objUser._EmpId);
            //}
            //lblAssigned.Text = "Assigned-" + objTasks.Assigned;
            //lblClosed.Text = "Closed-" + objTasks.Closed;
            //lblCompleted.Text = "Completed-" + objTasks.Completed;
            //lblMaintenance.Text = "Maintenance-" + objTasks.Maintenance;
            //lblNew.Text = "New-" + objTasks.NEWTAsk;
            //lblPipeline.Text = "In Pipeline-" + objTasks.InPipeline;
            //lblProcess.Text = "In Process-" + objTasks.InProcess;
            //lblQAed.Text = "QAed-" + objTasks.QAed;
            //lblSignOff.Text = "SignOff-" + objTasks.SignOff;
        }

        protected void imgBtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            GrdMyTasks.CurrentPageIndex = 0;
            if ((txtStartDate.Text != "") && (txtEndDate.Text != ""))
            {
                if (System.Convert.ToDateTime(txtStartDate.Text) > System.Convert.ToDateTime(txtEndDate.Text))
                {
                    this.LblException.Text = "Invalid date. Start Date cannot be greater than End Date";
                    return;
                }
            }
            GetMyTasksList(objUser);
        }

        protected void imgBtnDisplay_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ddlEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlTaskStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlHideStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {

        }

        protected void GrdMyTasks_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

        }

        protected void GrdMyTasks_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {

        }

        protected void GrdMyTasks_SortCommand(object source, DataGridSortCommandEventArgs e)
        {

        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Projects/ProjectDetail.aspx");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TeamMembers/ProjectTeamMembers.aspx");
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectModules/Module.aspx");
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Tasks/MyTasks.aspx");
        }

        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Documents/Documents.aspx");
        }

        protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectRelease/Release.aspx");
        }

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Clients/Clients.aspx");
        }

        protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectMeetings/Meetings.aspx");
        }

        protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectHistory/History.aspx");
        }

        protected void ImageButton10_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectReview/Reviews.aspx");
        }

        protected void ImageButton11_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectNotes/Notes.aspx");
        }

        protected void ImageButton12_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TimeSheets/TimeSheet.aspx");
        }
    }
}