﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Folder_Add.aspx.cs" Inherits="DrishProject.Documents.Folder_Add" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table id="MAINtable" height="100%" width="100%">
				<tr class="Gridtxt">
					<td width="100%" height="1%"></td>
				</tr>
				<tr>
					<td width="100%">
						<table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%" align="center"
							border="0">
							<tr>
								<td></td>
							</tr>
							<tr height="100%">
								<td></td>
								<td valign="top" align="left" height="100%">
									<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td><IMG src="../Images/add_folder_tab.gif" border="0"></td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="0" width="100%" border="0">
										<tbody>
											<tr>
												<td valign="top" align="center" height="100%"><table height="100%" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
											</tr>
											<tr class="center-bg">
												<td valign="top" align="left" width="96%"><IMG height="10" src="../Images/spacer.gif" width="10"></td>
												<td valign="top" align="right" width="3%"><IMG height="10" src="../Images/right_top_corner.gif" width="10"></td>
											</tr>
											<tr class="center-bg" valign="top" align="left">
												<td valign="top" height="100%">
													<table cellspacing="0" cellpadding="0" align="center" border="0">
														<tbody>
															<tr valign="middle" height="21">
																<td class="Errortxt" valign="top" width="100%" colSpan="4">
                                                                    <asp:Label ID="Label1" runat="server" CssClass="Errortxt" Text="Label"></asp:Label>
                                                                </td>
															</tr>
															<tr height="5">
																<td colSpan="4" height="5"><IMG height="5" src="../Images/spacer.gif" width="5"></td>
															</tr>
															<tr class="Gridtxt" valign="middle">
																<td width="30%">Folder Path</td>
																<td width="40%"><asp:label id="lblFolderPath" runat="server" CssClass="Gridtxt"></asp:label></td>
																<td width="10%"><IMG src="../Images/spacer.gif" width="5"></td>
															</tr>
															<tr height="10">
																<td colSpan="4" height="5"><IMG height="10" src="../Images/spacer.gif" width="5"></td>
															</tr>
															<tr class="Gridtxt" valign="middle">
																<td width="30%">Document Type</td>
																<td width="40%"><asp:dropdownlist id="ddlDocType" runat="server" Width="203px"></asp:dropdownlist></td>
																<td width="10%"><IMG src="../Images/spacer.gif" width="5"></td>
															</tr>
															<tr height="10">
																<td colSpan="4" height="5"><IMG height="10" src="../Images/spacer.gif" width="5"></td>
															</tr>
															<tr class="Gridtxt" valign="middle">
																<td width="30%">Folder Name</td>
																<td width="60%" colspan="2"><asp:textbox id="txtFolderName" CssClass="Gridtxt" Width="202px" MaxLength="30" Runat="server"></asp:textbox>&nbsp;<A onclick="alert('Following folders cannot be created as all are Windows reserved device names from the early days of DOS: CON, PRN, AUX, CLOCK$, NUL, COM1, COM2, COM3, COM4, COM5, COM6, COM7, COM8, COM9, LPT1, LPT2, LPT3, LPT4, LPT5, LPT6, LPT7, LPT8, and LPT9.');"
																		href="#">Info</A>&nbsp;&nbsp;&nbsp;</td>
															</tr>
															<tr class="Gridtxt">
																<td colSpan="4"><asp:requiredfieldvalidator id="reqFolderName" runat="server" ControlToValidate="txtFolderName" ErrorMessage="Folder Name Required !"></asp:requiredfieldvalidator>&nbsp;&nbsp;&nbsp;<asp:regularexpressionvalidator id="regValidate" runat="server" ControlToValidate="txtFolderName" ErrorMessage="Valid Data Type is Char,Digit,Space,-,_,()"
																		ValidationExpression="([0-9,a-z,A-Z, ,-,_,(,)]*)?"></asp:regularexpressionvalidator>
																</td>
															</tr>
															<tr>
																<td class="Gridtxt" valign="top" align="center" colSpan="4" height="20"></td>
															</tr>
															<tr valign="middle">
																<td class="Gridtxt" valign="top" align="center" colSpan="4">
                                                                    <asp:imagebutton id="imgBtnSave" CssClass="Gridtxt" Runat="server" 
                                                                        ImageUrl="../Images/save_bt.gif" onclick="imgBtnSave_Click"></asp:imagebutton>&nbsp;&nbsp;
																	<IMG style="CURSOR: pointer" onclick="window.close();" src="../Images/cancel_bt.gif"></td>
															</tr>
												</td>
											</tr>
									</table>
								</td>
								<td class="Gridtxt"><IMG height="10" src="../Images/spacer.gif" width="10"></td>
							</tr>
							<tr class="center-bg">
								<td class="Gridtxt" valign="top" align="left"><IMG height="10" src="../Images/spacer.gif" width="10"></td>
								<td class="Gridtxt" valign="bottom" align="right"><IMG height="10" src="../Images/right_bottom_corner.gif" width="10"></td>
							</tr>
							</table>
					</td>
				</tr>
			</table>
			
    </div>
    </form>
</body>
</html>
