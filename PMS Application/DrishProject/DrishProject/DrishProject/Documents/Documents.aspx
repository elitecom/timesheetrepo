﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="Documents.aspx.cs" Inherits="DrishProject.WebForm13" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table id="tblMain" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr height="10%">
            <td width="100%">
                <!-- HEADER -->
                <table id="tblHeader" height="100%" cellspacing="0" cellpadding="0" width="100%"
                    border="0">
                    <tr>
                        <td valign="top" align="left">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" align="top" border="0">
                </table>
            </td>
        </tr>
        <tr height="80%">
            <td valign="top" width="100%">
                <!-- CENTER -->
                <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="1%">
                        </td>
                        <td valign="top" align="left">
                            <table height="98%" cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
                                <tbody>
                                    <tr>
                                        <td colspan="2">
                                            <div>
                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/details_new.gif"
                                                    OnClick="ImageButton1_Click" />
                                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/tabs_bt_02.gif"
                                                    OnClick="ImageButton2_Click" />
                                                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/tabs_bt_06.gif"
                                                    OnClick="ImageButton3_Click" />
                                                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/tasks.gif" OnClick="ImageButton4_Click" />
                                                <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/doc_active.gif"
                                                    OnClick="ImageButton5_Click" />
                                                <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/Images/tabs_bt_08.gif"
                                                    OnClick="ImageButton6_Click" />
                                                <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/tabs_bt_03.gif"
                                                    OnClick="ImageButton7_Click" />
                                                <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/Images/tabs_bt_10.gif"
                                                    OnClick="ImageButton8_Click" />
                                                <asp:ImageButton ID="ImageButton11" runat="server" ImageUrl="~/Images/tabs_bt_09.gif"
                                                    OnClick="ImageButton11_Click" />
                                                <asp:ImageButton ID="ImageButton9" runat="server" ImageUrl="~/Images/tabs_bt_07.gif"
                                                    OnClick="ImageButton9_Click" />
                                                <asp:ImageButton ID="ImageButton10" runat="server" ImageUrl="~/Images/tabs_bt_08.gif"
                                                    OnClick="ImageButton10_Click" />
                                                <asp:ImageButton ID="ImageButton13" runat="server" ImageUrl="~/Images/review_active.gif"
                                                    OnClick="ImageButton13_Click" />
                                                <asp:ImageButton ID="ImageButton14" runat="server" ImageUrl="~/Images/timesheet_tab_active.gif"
                                                    OnClick="ImageButton14_Click" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="center-bg" valign="top" align="left">
                                        <td colspan="2">
                                            <table height="38" border="0">
                                                <tr valign="middle">
                                                    <td class="Gridtxt" valign="middle" align="left" style="width: 387px">
                                                        &nbsp;&nbsp;<strong>Project&nbsp;Code&nbsp;:&nbsp;</strong><asp:Label ID="lblProjectCode"
                                                            CssClass="Gridtxt" runat="server"></asp:Label><asp:Button ID="btnRefresh" runat="server"
                                                                CssClass="Gridtxt" Width="0px" Visible="false"></asp:Button><asp:DropDownList ID="DropDownList1"
                                                                    runat="server" CssClass="Gridtxt" Width="0px" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:HyperLink ID="hlFolderPath" Font-Bold="True" runat="server" ForeColor="#FF8000">Folder Root</asp:HyperLink>
                                                    </td>
                                                    <td class="Gridtxt" valign="middle" align="left" style="width: 273px">
                                                        <strong>Find a Document &nbsp; </strong>
                                                        <asp:TextBox ID="txtDocument" runat="server" CssClass="Gridtxt" MaxLength="50" Height="19px"></asp:TextBox>&nbsp;&nbsp;
                                                    </td>
                                                    <td class="Gridtxt" valign="bottom" align="right" style="width: 215px">
                                                        <img id="imgGenDoc" style="cursor: POINTER" onclick="DocGenerate();" src="../Images/generate_doc_bt.gif"
                                                            visible="false">&nbsp;
                                                    </td>
                                                    <td class="Gridtxt" valign="bottom" align="right" style="width: 156px">
                                                        <img id="imgAddDocs" style="cursor: POINTER" onclick="DocAddFunction();" src="../Images/add_new_doc_bt.gif"
                                                            border="0">&nbsp;
                                                    </td>
                                                    <td class="Gridtxt" width="1%">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="center-bg" valign="top">
                                        <td class="GridBG" width="98%" height="100%">
                                            <asp:DataGrid ID="dgFolders" runat="server" Width="101%" PageSize="12" AllowPaging="True"
                                                AutoGenerateColumns="False" BackColor="#F7F7F7" BorderColor="White" CellPadding="3"
                                                GridLines="Vertical" BorderStyle="None" DataKeyField="KeyField" OnItemCommand="dgFolders_ItemCommand"
                                                OnItemDataBound="dgFolders_ItemDataBound" OnPageIndexChanged="dgFolders_PageIndexChanged"
                                                OnSelectedIndexChanged="dgFolders_SelectedIndexChanged" OnDeleteCommand="dgFolders_DeleteCommand">
                                                <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                <Columns>
                                                    <asp:TemplateColumn>
                                                        <HeaderStyle Width="2%"></HeaderStyle>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="ImgExpand" ToolTip="Expand" AlternateText="Expand" runat="server"
                                                                Width="9px" Height="9px" ImageUrl="../Images/Plus-sign.gif" CommandName="Expand">
                                                            </asp:ImageButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn Visible="False" DataField="FolderPath" HeaderText="FolderPath">
                                                    </asp:BoundColumn>
                                                    <asp:HyperLinkColumn Target="_blank" ItemStyle-Font-Bold="true" DataNavigateUrlField="FolderLink"
                                                        DataNavigateUrlFormatString="{0}" DataTextField="FolderPathDocNo" HeaderText="Folder - (No. of Files)">
                                                        <HeaderStyle Width="50%"></HeaderStyle>
                                                        <ItemStyle Font-Bold="True"></ItemStyle>
                                                    </asp:HyperLinkColumn>
                                                    <asp:TemplateColumn HeaderText="Add Folder">
                                                        <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        <ItemTemplate>
                                                            <img id="imgDel" src="../Images/folder_ico.gif" title='Add Folder - <%# Eval("FolderPath")%>\'
                                                                alt='Add Folder - <%# Eval("FolderPath")%>\' onclick='AddFolder("<%# Eval("repFolderPath")%>");'
                                                                style="cursor: POINTER">
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Synchronize Files">
                                                        <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ImageUrl="../Images/synchronize_ico.gif" runat="server" ID="imgSync"
                                                                CommandName="Synchronize" ToolTip='<%# Eval("FolderPath")%>' AlternateText='<%# Eval("FolderPath")%>'>
                                                            </asp:ImageButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Delete Folder">
                                                        <HeaderStyle HorizontalAlign="Center" ForeColor="Black" Width="15%" BackColor="Gainsboro">
                                                        </HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ImageUrl="../Images/cross.gif" runat="server" ID="imgDelete" CommandName="Delete"
                                                                AlternateText='<%# Eval("FolderPath")%>' ToolTip='<%# Eval("FolderPath")%>'>
                                                            </asp:ImageButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn>
                                                        <ItemTemplate>
                                                            <asp:PlaceHolder ID="Expanded" runat="server" Visible="False"></TD> </TR>
                                                                <tr>
                                                                    <td width="9">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td class="GridBG" colspan="4">
                                                                        <asp:DataGrid ID="dgDocs" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                            BackColor="#F7F7F7" BorderColor="White" DataKeyField="DocId" CellPadding="3"
                                                                            GridLines="Vertical" BorderStyle="None">
                                                                            <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                                            <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                                            <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                                            <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                                            <Columns>
                                                                                <asp:TemplateColumn HeaderText="Document Name">
                                                                                    <ItemTemplate>
                                                                                        <a class="Gridtxt" href='<%# GetPath(Eval("DataItem.docfilename").ToString(),Eval("DataItem.folderpath").ToString()) %>'
                                                                                            title="Open Document" target="_blank">
                                                                                            <asp:Label runat="server" Text='<%# Eval("DataItem.DocName").ToString() %>' ID="lblDocName" /></a>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="DocType" ReadOnly="True" HeaderText="Document Type">
                                                                                    <HeaderStyle Width="15%"></HeaderStyle>
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="DocDesc" ReadOnly="True" HeaderText="Document Description">
                                                                                    <HeaderStyle Width="35%"></HeaderStyle>
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="username" HeaderText="Uploaded By">
                                                                                    <HeaderStyle Width="10%"></HeaderStyle>
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="Uploadedon" HeaderText="Uploaded On">
                                                                                    <HeaderStyle Width="10%"></HeaderStyle>
                                                                                </asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Edit">
                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                    <ItemTemplate>
                                                                                        <img id="imageEdit" align="middle" src="../Images/pen.gif" onclick='DocEditFunction(<%# Eval("Docid")%>)'
                                                                                            style="cursor: POINTER">
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Delete">
                                                                                    <HeaderStyle Font-Names="Arial" Font-Bold="True" HorizontalAlign="Center" ForeColor="Black"
                                                                                        Width="5%" BackColor="Gainsboro"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="imgDel" CommandName="Delete" ImageUrl="../Images/cross.gif"
                                                                                            runat="server"></asp:ImageButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn Visible="False" DataField="FolderPath" HeaderText="folderpath">
                                                                                    <HeaderStyle Width="0%"></HeaderStyle>
                                                                                </asp:BoundColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                            </asp:PlaceHolder>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <PagerStyle NextPageText="&lt;img src=Images/next_bt.gif border=0&gt;" PrevPageText="&lt;img src=Images/prev_bt.gif border=0&gt;"
                                                    CssClass="Pagertxt" Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid><asp:Label ID="LblException" runat="server" CssClass="Errortxt"></asp:Label>
                                            <asp:ImageButton ID="imgBtnSearch" TabIndex="5" runat="server" Width="0%" ImageUrl="../Images/searc_bt.gif"
                                                OnClick="imgBtnSearch_Click"></asp:ImageButton>
                                        </td>
                                        <td>
                                            <img height="10" src="../Images/spacer.gif" width="1" alt=""/>
                                        </td>
                                    </tr>
                                    <tr class="center-bg" height="10%">
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Gridtxt">
                                        <td colspan="2" height="3%">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <img height="10" src="../Images/spacer.gif" width="10">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <!--FOOTER -->
        <tr>
            <td width="100%">
                <table id="tblFooter" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td valign="top" align="left">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
<!--
        function EnterVal(evt) {

            /*debugger;
            if (window.event.keyCode == 13) {
            event.returnValue=false;
            event.cancel = true;
            document.all('imgBtnSearch').click();
            }*/
            var mytarget;
            var keyCode;
            var is_ie;
            if (document.all) {
                keyCode = event.keyCode;
                mytarget = window.event.srcElement.type;
                is_ie = true;
            }
            else if (document.addEventListener) {
                // Firefox

                mytarget = evt.target.type;
                keyCode = evt.keyCode;
                is_ie = false;
            }
            else {
                // Need to add a default or other browser cases here.
                return true; // Just give up and exit for now.
            }

            if (keyCode == 13) {
                evt.returnValue = false;
                evt.cancel = true;
                if (!is_ie) {
                    evt.preventDefault();
                    evt.stopPropagation();
                }

                document.all('imgBtnSearch').click();
            }
        }
        //-->

        function DocAddFunction() {
            var leftPos = (screen.width / 2) - 245;
            var topPos = (screen.height / 2) - 163;
            window.open("Documents_add.aspx", "DocumentAdd", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=490, height=325");
            
        }
        function DocGenerate() {
            var leftPos = (screen.width / 2) - 250;
            var topPos = (screen.height / 2) - 150;
            window.open("GenerateDocument.aspx", "FormAdd", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=500, height=300");
        }
        function DocEditFunction(ID) {
            var leftPos = (screen.width / 2) - 265;
            var topPos = (screen.height / 2) - 190;
            window.open("Documents_Edit.aspx?DocId=" + ID, "DocumentEdit", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=530, height=380");
        }
        function DocDetailFunction(ID) {
            var leftPos = (screen.width / 2) - 240;
            var topPos = (screen.height / 2) - 161;
            window.open("Document_Detail.aspx?DocId=" + ID, "DocumentEdit", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=480, height=322");
        }
        function AddFolder(Path) {
            // Set height and width
            var NewWinHeight = 200;
            var NewWinWidth = 200;
            alert("OK");      
            window.open("Folder_Add.aspx?FolderPath=" + Path, "MemberEdit", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=450, height=230");       
        }
        function PageRefresh() {
            __doPostBack('btnRefresh', '');
        }
    </script>
</asp:Content>
