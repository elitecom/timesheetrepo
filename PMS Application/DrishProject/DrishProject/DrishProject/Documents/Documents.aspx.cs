﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectBAL;

namespace DrishProject
{
    public partial class WebForm13 : System.Web.UI.Page
    {
        int ProjectId;
        Entities.Employee objEmployee;
        Project objProject = new Project();
        Entities.Documents docs = new Entities.Documents();

        protected void Page_Load(object sender, EventArgs e)
        {
            objEmployee = new Entities.Employee();
            objEmployee = (Entities.Employee)Session["emp"];
            if (Request["ProjectID"] == "")
            {
                ProjectId = Convert.ToInt32(Session["ProjectID"]);
            }
            else
            {
                ProjectId = Convert.ToInt32(Session["ProjectID"]);
            }
            objProject.ProjectID = ProjectId;
            objProject = new ProjectBAL().GetProject(ProjectId);

            if (objEmployee.IsAministrator == 0)
            {
                //If the user is not the member of that project then deny his access
                //if (!new ProjectBAL().IsActiveMember(objEmployee.EmployeeId, objProject.ProjectID))
                if (!new ProjectBAL().IsActiveMember(objEmployee.EmployeeId, ProjectId))
                {
                    Response.Redirect("Default.aspx?Authority=Deny");
                }
                #region "Unused Code"
                //RoleAuthenticate();
                //if (User.IsInRole("AddDocuments"))
                //{
                //    RegisterStartupScript("strShowDOCS", "<script language=\'javascript\'>this.document.all.imgAddDocs.style.display=\'block\';</script>");
                //}
                //else
                //{
                //    RegisterStartupScript("strHideDOCS", "<script language=\'javascript\'>this.document.all.imgAddDocs.style.display=\'none\';</script>");
                //}
                //if (User.IsInRole("AddISODocuments"))
                //{
                //    RegisterStartupScript("strShowISODOCS", "<script language=\'javascript\'>this.document.all.imgGenDoc.style.display=\'block\';</script>");
                //}
                //else
                //{
                //    RegisterStartupScript("strHideISODOCS", "<script language=\'javascript\'>this.document.all.imgGenDoc.style.display=\'none\';</script>");
                //}
                #endregion
            }
            if (!Page.IsPostBack)
            {
                Entities.Documents doc = new Entities.Documents();
                doc.ProjectId = ProjectId;
                doc.ProjectCode = objProject.ProjectCode;
                lblProjectCode.Text = doc.ProjectCode;
                BindGrid(doc);
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Projects/ProjectDetail.aspx");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TeamMembers/ProjectTeamMembers.aspx");
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectModules/Module.aspx");
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Tasks/MyTasks.aspx");
        }

        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Documents/Documents.aspx");
        }

        protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectRelease/Release.aspx");
        }

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Clients/Clients.aspx");
        }

        protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectMeetings/Meeting.aspx");
        }

        protected void ImageButton11_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectNotes/Notes.aspx");
        }

        protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectHistory/History.aspx");
        }

        protected void ImageButton10_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectRelease/Release.aspx");
        }

        protected void ImageButton13_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectReview/Reviews.aspx");
        }

        protected void ImageButton14_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TimeSheets/TimeSheet.aspx");
        }

        private void BindGrid(Entities.Documents doc)
        {
            DataSet ds = new DataSet();
            LblException.Text = "";
            try
            {
                doc.FolderName = doc.ProjectCode;
                doc.ProjectId = ProjectId;
                if (txtDocument.Text == "")
                {
                    ds = new DocumentsBAL().GetFolderList(doc.ProjectId);
                }
                else
                {
                    ds = new DocumentsBAL().GetFolderList(txtDocument.Text, ProjectId);
                }
                dgFolders.DataSource = ds;
                dgFolders.DataMember = ds.Tables[0].TableName;
                dgFolders.DataBind();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public string GetPath(string strFilename, string strFolderPath)
        {
            string strDocPath;
            Project objProject = new Project();
            objProject.ProjectID = ProjectId;
            objProject = new ProjectBAL().GetProject(ProjectId);

            strDocPath = System.Configuration.ConfigurationManager.AppSettings["ProjectFolders"].ToString() + "\\" + objProject.ProjectCode + "\\" + strFolderPath.Trim();
            return strDocPath + "\\" + strFilename;
        }

        protected void dgFolders_ItemCommand(object source, DataGridCommandEventArgs e)
        {

        }

        protected void dgFolders_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

        }

        protected void dgFolders_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {

        }

        protected void dgFolders_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void dgFolders_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            int intdocId;
            string strSql = string.Empty;
            string strRetVal = string.Empty;
            string strFolderPath = string.Empty;
            Project objProject = new Project();
            Entities.Documents objDoc = new Entities.Documents();
            intdocId = 0;
            strRetVal = "";

            intdocId = Convert.ToInt32(dgFolders.DataKeys[e.Item.ItemIndex].ToString());

            objProject.ProjectID = ProjectId;
            objProject = new ProjectBAL().GetProject(ProjectId);
            objDoc.DocId = intdocId;
            objDoc.ProjectCode = objProject.ProjectCode;
            strRetVal = new DocumentsBAL().GetDocName(objDoc);

            if (String.IsNullOrEmpty(strRetVal))
            {
                if (Server.MapPath(strRetVal) != "")
                {
                    System.IO.File.Delete(Server.MapPath(strRetVal));
                }
            }

            //objDoc.DocId = intdocId;
            //strRetVal = objDoc.DeleteDoc();
            //strFolderPath = e.Item.Cells(7).Text
            //BindSub(strFolderPath, source)
            //GetdocumentsPerProject()
            //source.EditItemIndex = -1
            //If strRetVal = "" Then
            //    strRetVal = "Deleted Successfully !"
            //End If
            //LblException.Text = strRetVal;
        }

        protected void imgBtnSearch_Click(object sender, ImageClickEventArgs e)
        {

        }
    }
}