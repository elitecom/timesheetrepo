﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using Entities;

namespace DrishProject.Documents
{
    public partial class Folder_Add : System.Web.UI.Page
    {
        string FolderPath = string.Empty;
        DataSet ds = new DataSet();
        Entities.Documents objDocuments = new Entities.Documents();
        string DocumentType = string.Empty;
        Entities.Project objPro = new Entities.Project();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    FolderPath = Request["FolderPath"].ToString();
                    FolderPath = FolderPath.Replace(";", "\\");
                    lblFolderPath.Text = FolderPath;
                    FolderPath = FolderPath.Substring(0, FolderPath.Length - 1);
                    if (!Page.IsPostBack)
                    {
                        ds = new DocumentsBAL().GetDocumentTypes();
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddlDocType.DataSource = ds.Tables[0];
                            ddlDocType.DataMember = ds.Tables[0].TableName;
                            ddlDocType.DataTextField = "DocType";
                            ddlDocType.DataValueField = "DocTypeID";
                            ddlDocType.DataBind();
                            ddlDocType.Items.Insert(0, "== Select ==");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Label1.Text = ex.Message;
                }
            }
            txtFolderName.Attributes.Add("OnKeyPress", " return EnterVal(event);");
        }

        protected void imgBtnSave_Click(object sender, ImageClickEventArgs e)
        {
            string Folderstr = "CON, PRN, AUX, CLOCK$, NUL, COM1, COM2, COM3, COM4, COM5, COM6, COM7, COM8, COM9, LPT1, LPT2, LPT3, LPT4, LPT5, LPT6, LPT7, LPT8, and LPT9";
            string strProjectCode;
            string strFolderPath;
            int x;
            Project objProject = new Project();
            try
            {
                x = Folderstr.IndexOf((txtFolderName.Text).ToUpper());
                if (x > 0)
                {
                    Label1.Text = "Windows is unable to create this folder !";
                }
                if (ddlDocType.SelectedValue.ToString() == "0")
                {
                    Label1.Text = "Please select a Document Type !";
                }
                else
                {
                    DocumentType = ddlDocType.SelectedItem.Text;
                }
                DocumentType = DocumentType + "\\" + txtFolderName.Text.Trim();
                FolderPath = FolderPath + "\\" + txtFolderName.Text.Trim();
                objPro.ProjectID = Convert.ToInt32(Session["ProjectID"].ToString());
                objPro = new ProjectBAL().GetProject(objPro.ProjectID);

                objDocuments.ProjectId = objPro.ProjectID;
                objDocuments.DocType = DocumentType;
                objDocuments.FolderPath = FolderPath;
                objDocuments.ProjectCategory = "Engineering";

                bool res1 = new DocumentsBAL().IfFolderPathExists(objDocuments);
                if (res1 == false)
                {
                    new DocumentsBAL().InsertDoc(objDocuments);
                }
                else
                {
                    Label1.Text = "Folder Exists !";
                }

                try
                {                    
                    objProject.ProjectID = Convert.ToInt32(Session["ProjectID"].ToString());
                    objProject = new ProjectBAL().GetProject(objProject.ProjectID);
                    strProjectCode = objProject.ProjectCode;
                }
                catch (Exception)
                {

                }
                //strFolderPath = objConfig.GetFoldersPath + "\" + Trim(strProjectCode) + "\" + Trim(FolderPath)
                strFolderPath = System.Configuration.ConfigurationManager.AppSettings["ProjectFolders"].ToString() + "\\" + objProject.ProjectCode.Trim() + "\\" + FolderPath.Trim();
                System.IO.Directory.CreateDirectory(Server.MapPath(strFolderPath));
                Label1.Text = "Folder Added Successfully !";
                RegisterStartupScript("strRefresh", "<script language='javascript'>window.opener.PageRefresh();</script>");
            }
            catch (Exception ex)
            {
                Label1.Text = ex.Message;
            }
        }
    }
}