﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Documents_add.aspx.cs"
    Inherits="DrishProject.Documents.Documents_add" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="MAINTABLE" height="100%" width="100%">
            <tr>
                <td width="100%" height="1%">
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                        align="center" border="0">
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr height="100%">
                            <td>
                            </td>
                            <td align="left" height="100%">
                                <table cellspacing="0" cellpadding="0" align="top" border="0">
                                    <tr>
                                        <td>
                                            <img src="../images/addnew_doc_tab.gif" border="0">
                                        </td>
                                    </tr>
                                </table>
                                <table height="40%" cellspacing="0" cellpadding="0" width="100%" border="0" valign="top">
                                    <tbody>
                                        <tr>
                                            <td valign="top" align="center" height="100%">
                                                <table height="100%" cellspacing="0" cellpadding="0" width="100%" align="center"
                                                    border="0">
                                        </tr>
                                        <tr class="center-bg">
                                            <td valign="top" align="left" width="96%">
                                                <img height="10" src="../images/spacer.gif" width="10">
                                            </td>
                                            <td valign="top" align="right" width="3%">
                                                <img height="10" src="../images/right_top_corner.gif" width="10">
                                            </td>
                                        </tr>
                                        <tr class="center-bg" valign="top" align="left">
                                            <td valign="top" height="100%" colspan="2">
                                                <table cellspacing="0" cellpadding="2" align="left" border="0">
                                                    <tbody>
                                                        <tr valign="middle" height="21">
                                                            <td valign="top" class="Errortxt" colspan="4">
                                                                <asp:Label ID="lblException" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td>
                                                            </td>
                                                            <td class="Labeltxt" valign="top">
                                                                Project&nbsp;Code
                                                            </td>
                                                            <td class="Gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt">
                                                                <asp:Label CssClass="Gridtxt" ID="lblProjectCode" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td valign="top" class="mendatoryfld">
                                                                *
                                                            </td>
                                                            <td class="Labeltxt" valign="top">
                                                                Document Name
                                                            </td>
                                                            <td class="Gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt">
                                                                <asp:TextBox CssClass="Gridtxt" ID="txtDocName" runat="server" Width="250px" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td valign="top" class="mendatoryfld">
                                                                *
                                                            </td>
                                                            <td class="Labeltxt" valign="top">
                                                                Document Type
                                                            </td>
                                                            <td class="Gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt">
                                                                <asp:DropDownList CssClass="Gridtxt" ID="ddlDocType" runat="server" Width="250px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td class="Gridtxt">
                                                            </td>
                                                            <td class="Labeltxt" valign="top">
                                                                Document Description
                                                            </td>
                                                            <td class="Gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt">
                                                                <asp:TextBox CssClass="Gridtxt" ID="txtDocDesc" TextMode="MultiLine" Rows="5" runat="server"
                                                                    Width="250px" MaxLength="200"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td valign="top" class="mendatoryfld">
                                                                *
                                                            </td>
                                                            <td class="Labeltxt" valign="top">
                                                                Document File
                                                            </td>
                                                            <td class="Gridtxt" rowspan="2">
                                                                &nbsp;
                                                            </td>
                                                            <td rowspan="2" class="Gridtxt">
                                                                <input class="Gridtxt" id="UploadDocFile" size="28" type="file" runat="server">
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td class="smalltxt">
                                                            </td>
                                                            <td class="smalltxt" valign="top">
                                                                (Less than10 MB)
                                                            </td>
                                                            <td class="smalltxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="smalltxt">
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td class="smalltxt">
                                                            </td>
                                                            <td class="Labeltxt" valign="center">
                                                                Send Notification
                                                            </td>
                                                            <td class="smalltxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="smalltxt" valign="bottom">
                                                                <asp:CheckBox ID="chkMail" runat="server" Checked="True"></asp:CheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="Gridtxt" height="20">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td class="Gridtxt">
                                                            </td>
                                                            <td class="Gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt" valign="top">
                                                                <asp:ImageButton ID="imgBtnSave" OnClick="imgBtnSave_Click" runat="server" 
                                                                    ImageUrl="../Images/save_bt.gif">
                                                                </asp:ImageButton>&nbsp;&nbsp;
                                                                <img style="cursor: POINTER" onclick="window.close();" src="../Images/cancel_bt.gif">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="center-bg">
                                            <td valign="top" align="left">
                                                <img height="10" src="../images/spacer.gif" width="10">
                                                </td>
                                            <td valign="bottom" align="right">
                                                <img height="10" src="../images/right_bottom_corner.gif" width="10">
                                            </td>
                                        </tr>
                                </table>
                            </td>
                            <td>
                                <img height="10" src="../images/spacer.gif" width="2">
                            </td>
                        </tr>
                        <tr class="Gridtxt">
                            <td height="3%">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
