﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.SqlClient;
using Entities;
using System.Data;
using Entities;
using DrishProjectBAL;

namespace DrishProject.Documents
{
    public partial class Documents_add : System.Web.UI.Page
    {
        Project objProject = new Project();
        Entities.Documents document = new Entities.Documents();
        Entities.Employee objUser = new Entities.Employee();
        int intProjectId;
        int intDocTypeId;
        DataSet ds;
        string strProjectCode;
        const Int64 MAXFILESIZE = 20971520;
        bool bln_Save;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                objProject.ProjectID = Convert.ToInt32(Session["ProjectID"].ToString());
                objProject = new DrishProjectBAL.ProjectBAL().GetProject(objProject.ProjectID);

                lblProjectCode.Text = objProject.ProjectCode;

                ds = new DataSet();
                ds = new DrishProjectBAL.DocumentsBAL().GetDocTypes(objProject.ProjectID);
                ddlDocType.DataSource = ds.Tables[0];
                ddlDocType.DataMember = ds.Tables[0].TableName;
                ddlDocType.DataTextField = "DocType";
                ddlDocType.DataValueField = "DocTypeId";
                ddlDocType.DataBind();
                ddlDocType.Items.Insert(0, "-- Select --");
                ddlDocType.SelectedIndex = 0;
            }
        }

        protected void imgBtnSave_Click(object sender, ImageClickEventArgs e)
        {
            string strFileExt = string.Empty;
            string strFileName = string.Empty;
            string strDocFileName = string.Empty;
            string strDocName = string.Empty;
            string strDocDesc = string.Empty;

            strFileExt = System.IO.Path.GetExtension(UploadDocFile.PostedFile.FileName);
            strFileName = System.IO.Path.GetFileNameWithoutExtension(UploadDocFile.PostedFile.FileName);

            objProject.ProjectID = Convert.ToInt32(Session["ProjectID"].ToString());
            objProject = new DrishProjectBAL.ProjectBAL().GetProject(objProject.ProjectID);
            strProjectCode = objProject.ProjectCode;

            if (!String.IsNullOrEmpty(strFileName))
            {
                strDocFileName = strFileName.Trim() + "_" + strProjectCode.Trim() + strFileExt.ToString();
            }
            else
            {
                strDocFileName = "";
                lblException.Text = "Invalid Document !";
            }
            if (UploadDocFile.PostedFile.ContentLength > MAXFILESIZE)
            {
                lblException.Text = "File too large !";
            }
            else
            {
                lblException.Text = "";
            }
            intProjectId = Convert.ToInt32(Session["ProjectID"].ToString());
            if (Convert.ToInt32(ddlDocType.SelectedValue.ToString()) > 0)
            {
                intDocTypeId = Convert.ToInt32(ddlDocType.SelectedValue.ToString());
            }
            if (!String.IsNullOrEmpty(txtDocName.Text))
            {
                strDocName = txtDocName.Text.Trim();
            }
            if (!String.IsNullOrEmpty(txtDocDesc.Text))
            {
                strDocDesc = txtDocDesc.Text.Trim();
            }
            objUser = (Entities.Employee)(Session["emp"]);
            document.UploadedByID = objUser.EmployeeId;
            document.UploadedOn = DateTime.Now;
            document.UpLoadFile = false;
            document.DocTypeId = Convert.ToInt32(ddlDocType.SelectedValue.ToString());
            document.FolderPath = strProjectCode + "\\" + new DrishProjectBAL.DocumentsBAL().GetFolderPath(document);

            if (strDocFileName.IndexOf("'") > 0)
            {
                lblException.Text  = "Invalid File " + strDocFileName + ", remove quotes and wild characters.";                
            }
            if (!String.IsNullOrEmpty(strDocFileName.Trim() ))
            {
                //C:\Users\DR-91\Desktop\Palak_Trainee\FinalProject\FinalProject\DrishProject\DrishProject\Documents\DrishProjects/DTS/Customer Feedback\Acceptance/WordImage_DTS.txt
                string temp = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["DocumentFolders"].ToString()).Replace(@"Documents\DrishProjects",@"Projects\DrishProjects");
                //string tempPath = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["ProjectFolders"].ToString());
                UploadDocFile.PostedFile.SaveAs(temp + "\\" + document.FolderPath + "\\" + strDocFileName); 
            }
        }
    }
}