﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DrishProject.Forum
{
    public partial class Forum : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string  GetPath(string  strFilename)
        {
            string strDocPath = string.Empty;

            strDocPath = System.Configuration.ConfigurationManager.AppSettings["ForumPath"].ToString();
            return strDocPath + "/" + strFilename;

        }
    }
}