﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="reply.aspx.cs" Inherits="DrishProject.Forum.reply" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    			<TABLE id="MAINTABLE" height="100%" width="100%">
				<tr>
					<td width="100%" height="1%"></td>
				</tr>
				<tr>
					<td width="100%">
						<TABLE id="tblCentral" height="100%" cellSpacing="0" cellPadding="0" width="100%" align="center"
							border="0">
							<tr class="Gridtxt">
								<td></td>
							</tr>
							<TR height="100%">
								<td class="Gridtxt"></td>
								<TD vAlign="top" align="left" height="100%">
									<table cellSpacing="0" cellPadding="0" align="top" border="0">
										<tr>
											<TD><IMG id="imgTop" src="../Images/reply_tab.gif" border="0"></TD>
										</tr>
									</table>
									<table height="98%" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TBODY>
											<TR class="center-bg">
												<TD vAlign="top" align="left" width="96%"><IMG height="10" src="../Images/spacer.gif" width="10"></TD>
												<TD vAlign="top" align="right" width="3%"><IMG height="10" src="../Images/right_top_corner.gif" width="10"></TD>
											</TR>
											<TR class="center-bg" vAlign="top" align="left">
												<TD vAlign="top" height="100%">
													<TABLE cellSpacing="0" cellPadding="4" align="left" border="0">
														<TBODY>
															<tr vAlign="middle" height="21">
																<td class="Errortxt" vAlign="top" colSpan="4">&nbsp;<asp:label id="LblException" runat="server" CssClass="Errortxt"></asp:label></td>
															</tr>
															<tr vAlign="middle">
																<td class="mendatoryfld" vAlign="top"></td>
																<td class="Labeltxt" vAlign="top">Topic Summary</td>
																<td class="Gridtxt">&nbsp;</td>
																<td class="Gridtxt"><asp:Label id="lblSummary" runat="server" Width="300"></asp:Label><asp:label id="lblTopicId" runat="server" Width="0px" Visible="false"></asp:label></td>
															</tr>
															<tr vAlign="middle">
																<td class="mendatoryfld" vAlign="top"></td>
																<td class="Labeltxt" vAlign="top">Description</td>
																<td class="Gridtxt">&nbsp;</td>
																<td class="Gridtxt"><asp:Label id="lblDescription" runat="server" Width="300"></asp:Label></td>
															</tr>
															<tr vAlign="middle">
																<td class="mendatoryfld" vAlign="top"></td>
																<td class="Labeltxt" vAlign="top">Created By</td>
																<td class="Gridtxt">&nbsp;</td>
																<td class="Gridtxt"><asp:Label id="LblCreatedBy" runat="server" Width="300"></asp:Label></td>
															</tr>
															<tr vAlign="middle">
																<td class="mendatoryfld" vAlign="top"></td>
																<td class="Labeltxt" vAlign="top">Created On</td>
																<td class="Gridtxt">&nbsp;</td>
																<td class="Gridtxt"><asp:Label id="lblCreatedOn" runat="server" Width="300"></asp:Label></td>
															</tr>
															<tr vAlign="middle">
																<td class="mendatoryfld" vAlign="top"></td>
																<td class="Labeltxt" vAlign="top">Reply</td>
																<td class="Gridtxt">&nbsp;</td>
																<td class="Gridtxt"><asp:TextBox id="txtReply" runat="server" MaxLength="4000" TextMode="MultiLine" Rows="4" Width="300"></asp:TextBox></td>
															</tr>
															<tr vAlign="top">
																<td class="mendatoryfld" vAlign="top"></td>
																<td class="Labeltxt" vAlign="top">Attachment</td>
																<td rowspan=2 class="Gridtxt">&nbsp;</td>
																<td rowspan=2 class="Gridtxt"><input class="Gridtxt" id="UploadDocFile" type="file" size="35" name="UploadDocFile" runat="server"></td>
															</tr>
															<tr vAlign="middle">
																<td class="smalltxt"></td>
																<td class="smalltxt" vAlign="top">(Less than10 MB)</td>
															</tr>
															<tr>
																<td class="Gridtxt" colSpan="4" height="20">&nbsp;</td>
															</tr>
															<tr vAlign="middle">
																<td class="Gridtxt"></td>
																<td class="Gridtxt">&nbsp;</td>
																<td class="Gridtxt">&nbsp;</td>
																<td class="Gridtxt" vAlign="top" colSpan="3"><asp:imagebutton id="imgBtnSave" 
                                                                        Runat="server" ImageUrl="../Images/save_bt.gif" onclick="imgBtnSave_Click" 
                                                                        style="height: 19px"></asp:imagebutton>&nbsp;&nbsp;<IMG style="CURSOR:pointer" onclick="window.close();" src="../Images/cancel_bt.gif">
																</td>
															</tr>
														</TBODY>
													</TABLE>
												</TD>
												<TD><IMG height="10" src="../Images/spacer.gif" width="10"></TD>
											</TR>
											<TR class="center-bg" height="20">
												<TD vAlign="top" align="left"><IMG height="10" src="../Images/spacer.gif" width="10"></TD>
												<TD vAlign="bottom" align="right"><IMG height="10" src="../Images/right_bottom_corner.gif" width="10"></TD>
											</TR>
											<tr class="Gridtxt">
												<td colSpan="2" height="3%">&nbsp;</td>
											</tr>
										</TBODY>
									</table>
								</TD>
							</TR>
						</TABLE>
					</td>
				</tr>
			</TABLE>
    </div>
    </form>
</body>
</html>
