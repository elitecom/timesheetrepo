﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Entities;
using DrishProjectBAL;

namespace DrishProject.Forum
{
    public partial class reply : System.Web.UI.Page
    {
        int intTopicId;
        const int MAXFILESIZE = 20971520;
        Entities.Employee objUser = new Entities.Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            Forum objForum = new Forum();
            int intTopicId = 0;
            
            if (!Page.IsPostBack) 
            {	
                intTopicId = Convert.ToInt32(Request.QueryString["Topicid"]);
                lblTopicId.Text = intTopicId.ToString();
                if (intTopicId > 0) 
                {
                    GetTopicInfo(intTopicId);
                }
                SetFocus(txtReply);
            }
        }

        private void GetTopicInfo(int intTopicId)
        {
            Entities.Forum objForum = new Entities.Forum();
            intTopicId = Convert.ToInt32(lblTopicId.Text);
            if (intTopicId > 0)
            {
                objForum.TopicId = intTopicId;
                objForum = new ForumBAL().GetTopics(intTopicId);
                lblSummary.Text = objForum.TopicSummary;
                lblDescription.Text = objForum.TopicDesc;
                LblCreatedBy.Text = objForum.NameCreatedBy;
                lblCreatedOn.Text = objForum.CreatedOn.ToShortDateString();
            }

        }

        private void SetFocus(Control ctrl)
        {
            // Define the JavaScript function for the specified control.
            string FocusScript = null;
            FocusScript = "<script language='javascript'>" + "document.getElementById('" + ctrl.ClientID + "').focus();</script>";

            //Add the JavaScript code to the page.
            Page.RegisterStartupScript("FocusScript", FocusScript);
        }

        protected void imgBtnSave_Click(object sender, ImageClickEventArgs e)
        {
            string strReply = "";
            int intTopicId = 0;
            int intReplyBy = 0;
            System.DateTime dtReplyOn = Convert.ToDateTime("1/1/1900");
            Forum objForum = new Forum();
            string strRetVal = "";
            string strFileExt = "";
            string strFileName = "";
            string strDocFileName = "";
            string strOldPath = "";
            int intId = 0;

            intTopicId = Convert.ToInt32(lblTopicId.Text);
            if ((new DrishProjectBAL.ForumBAL().bln_ToSelectTopic(intTopicId) == false))
            {
                LblException.Text = "Topic already deleted. Reply cannot be saved ! ";
            }
            else
            {
                if (string.IsNullOrEmpty(txtReply.Text.Trim()))
                {
                    LblException.Text = "Reply required !";
                    SetFocus(txtReply);
                    return;
                }
                else
                {
                    LblException.Text = "";
                }

                if (txtReply.Text.Trim().Length > 4000)
                {
                    LblException.Text = "Reply can not exceed it's allowed max length (4000 chars) !";
                    SetFocus(txtReply);
                    return;
                }
                else
                {
                    LblException.Text = "";
                }

                intId = new ForumBAL().GetNextReplyId();

                if (!string.IsNullOrEmpty(UploadDocFile.PostedFile.FileName.Trim()))
                {
                    strFileExt = System.IO.Path.GetExtension(UploadDocFile.PostedFile.FileName);
                    strFileName = System.IO.Path.GetFileNameWithoutExtension(UploadDocFile.PostedFile.FileName);
                    if (!string.IsNullOrEmpty(strFileName.Trim()) & !string.IsNullOrEmpty(strFileExt.Trim()))
                    {
                        strDocFileName = strFileName.Trim() + "_" + "R" + Convert.ToString(intId) + strFileExt.Trim();
                    }
                    else
                    {
                        strDocFileName = "";
                        LblException.Text = "Invalid Document !";
                        return;
                    }
                    if (UploadDocFile.PostedFile.ContentLength > MAXFILESIZE)
                    {
                        LblException.Text = "File too large !";
                        return;
                    }
                    else
                    {
                        LblException.Text = "";
                    }
                }

                objUser = (Entities.Employee)(Session["emp"]);
                intReplyBy = objUser.EmployeeId;
                dtReplyOn = System.DateTime.Now;
                //insert reply
                Entities.Forum _with1 = new Entities.Forum();
                _with1.TopicId = intTopicId;
                _with1.ReplyDesc = txtReply.Text.Trim();
                _with1.ReplyOn = dtReplyOn;
                _with1.ReplyBy = intReplyBy;
                _with1.ReplyAttachment = strDocFileName;
                strRetVal = new DrishProjectBAL.ForumBAL().InsertReply(_with1);

                if (!string.IsNullOrEmpty(strRetVal.Trim()))
                {
                    if (!string.IsNullOrEmpty(strDocFileName.Trim()))
                    {
                        UploadDocFile.PostedFile.SaveAs(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["GetForumDocPath"].ToString()) + "/" + strDocFileName);
                    }
                    strRetVal = "Reply added Successfully";                    
                }

                LblException.Text = strRetVal;
                RegisterStartupScript("strRefresh", "<script language='javascript'>window.opener.PageRefresh();</script>");
            }
        }
    }
}