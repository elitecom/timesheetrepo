﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="Forum.aspx.cs" Inherits="DrishProject.Forum.Forum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        var againClick = 0;
        function OpenReplyWindow(ID) {
            var leftPos = (screen.width / 2) - 262;
            var topPos = (screen.height / 2) - 200;
            window.open("reply.aspx?TopicId=" + ID, "ReplyAdd", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=yes, width=525, height=400");
        }
        function OpenRepliesWindow(ID) {
            var leftPos = (screen.width / 2) - 350;
            var topPos = (screen.height / 2) - 250;
            window.open("replies.aspx?TopicId=" + ID, "Replies", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=yes, width=700, height=500");
        }
        function TopicAddFunction() {
            var leftPos = (screen.width / 2) - 262;
            var topPos = (screen.height / 2) - 150;
            window.open("AddTopic.aspx?TopicId=0", "TopicAdd", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=525, height=300");
        }
        function TopicEditFunction(ID) {
            var leftPos = (screen.width / 2) - 262;
            var topPos = (screen.height / 2) - 155;
            window.open("AddTopic.aspx?TopicId=" + ID, "TopicEdit", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=525, height=310");
        }
        function PageRefresh() {
            __doPostBack('btnRefresh', '');
        }
    </script>
    <table id="tblMain" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr height="10%">
            <td width="100%">
                <!-- HEADER -->
                <table id="tblHeader" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td valign="top" align="left">
                            <!-- Header     --->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="80%">
            <td width="100%">
                <!-- CENTER -->
                <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="1%">
                        </td>
                        <td align="left">
                            <table cellspacing="0" cellpadding="0" align="top" border="0">
                                <tr>
                                    <td>
                                        <img src="../Images/discussion_forum_active.gif" border="0">
                                    </td>
                                </tr>
                            </table>
                            <table height="96%" cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tbody valign="top">
                                    <tr class="center-bg">
                                        <td valign="top" align="right" width="100%" colspan="2">
                                            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" Width="0px">
                                            </asp:DropDownList>
                                            &nbsp;&nbsp;<asp:Button ID="btnRefresh" runat="server" Width="0px" Visible="False">
                                            </asp:Button>&nbsp;&nbsp;<img style="cursor: POINTER" onclick="TopicAddFunction();"
                                                src="../Images/add_topic_bt.gif" border="0">&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr height="100%" class="center-bg" valign="top" align="left">
                                        <td height="100%" valign="top" width="98%">
                                            <table height="100%" cellspacing="0" cellpadding="0" width="100%" align="center"
                                                border="0">
                                                <tbody>
                                                    <tr class="center-bg" valign="top" align="left">
                                                        <td class="GridBG" valign="top" height="100%">
                                                            <asp:DataGrid ID="dgTopics" runat="server" Width="100%" AllowPaging="true" AutoGenerateColumns="False"
                                                                BackColor="#F7F7F7" BorderColor="White" DataKeyField="Topicid" CellPadding="3"
                                                                GridLines="Vertical" BorderStyle="None">
                                                                <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                                <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                                <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                                <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                                <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="Topic">
                                                                        <HeaderStyle Width="50%"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label1" runat="server" CssClass="Labeltxt" Text="Summary :" />
                                                                            <a href="#" title="Post Reply" onclick='OpenReplyWindow(<%#Eval("TopicId")%>)'>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("TopicSummery") %>'>
                                                                                </asp:Label></a><br />
                                                                            <asp:Label ID="Label3" runat="server" CssClass="Labeltxt" Text="Description :" />
                                                                            <asp:Label runat="server" Text='<%# Eval( "TopicDesc") %>' ID="lblTopicDesc">
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Attachment">
                                                                        <HeaderStyle Width="12%"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <a class="Gridtxt" href='<%# GetPath(Eval( "Attachment").ToString()) %>' title="View Attachment"
                                                                                target="_blank">
                                                                                <asp:Label runat="server" Text='<%# Eval( "Attachment") %>' ID="lblDocName" /></a>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Replies">
                                                                        <HeaderStyle Width="8%"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <a href='replies.aspx?TopicId=<%#Eval( "TopicId")%>' title="View Replies">
                                                                                <asp:Label ID="Label4" runat="server" Text='<%# Eval( "NoReplies") %>'>
                                                                                </asp:Label>&nbsp;&nbsp;<img border="0" src="../images/view_ico.gif"></a>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Created By">
                                                                        <HeaderStyle Width="10%"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label5" runat="server" Text='<%# Eval( "NameCreatedBy") %>'>
                                                                            </asp:Label><br>
                                                                            <asp:Label runat="server" Text='<%# Eval( "DateCreated") %>' ID="Label3">
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Last Reply">
                                                                        <HeaderStyle Width="10%"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label6" runat="server" Text='<%# Eval( "NameLastreplyBy") %>'>
                                                                            </asp:Label><br>
                                                                            <asp:Label runat="server" Text='<%# Eval( "DateLastreply") %>' ID="Label1">
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Edit">
                                                                        <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="ImgEdit" ImageUrl="~/Images/pen.gif" CommandName="Edit" CommandArgument='<%# Eval( "CreatedBy") %>'
                                                                                runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Delete">
                                                                        <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="ImgDel" runat="server" ImageUrl="~/images/cross.gif" CommandName="Delete">
                                                                            </asp:ImageButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                                <PagerStyle NextPageText="&lt;img src=~/Images/next_bt.gif border=0&gt;" PrevPageText="&lt;img src=~/Images/prev_bt.gif border=0&gt;"
                                                                    CssClass="Pagertxt" Mode="NumericPages"></PagerStyle>
                                                            </asp:DataGrid>
                                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td class="Errortxt" align="left" width="100%">
                                                                        <asp:Label ID="LblException" runat="server" CssClass="Errortxt"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <img src="../images/spacer.gif">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                            <img height="10" src="../images/spacer.gif" width="1">
                                        </td>
                                    </tr>
                                    <tr class="center-bg">
                                        <td colspan="2" height="7" rowspan="1">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr class="center-bg" height="10%">
                                        <td valign="top" align="left" height="15">
                                            <img height="10" src="../images/spacer.gif" width="10">
                                        </td>
                                        <td valign="bottom" align="right" height="15">
                                            <img height="10" src="../images/right_bottom_corner.gif" width="10">
                                        </td>
                                    </tr>
                                    <tr class="Gridtxt">
                                        <td colspan="2">
                                            <img height="5" src="../images/spacer.gif" width="10">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <img height="10" src="../images/spacer.gif" width="10">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <!--FOOTER -->
        <tr>
            <td width="100%">
                <table id="tblFooter" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td valign="top" align="left">
                            <!--FOOTER -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
