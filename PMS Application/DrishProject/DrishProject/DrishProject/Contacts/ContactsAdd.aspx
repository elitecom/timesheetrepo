﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="Contacts.aspx.cs" Inherits="DrishProject.WebForm14" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <TABLE height="100%" id="MAINTABLE" width="100%">
				<tr>
					<td width="100%" valign="top">
						<TABLE id="tblCentral" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<tr class="Gridtxt">
								<td colspan="3" class="Gridtxt"></td>
							</tr>
							<TR>
								<TD valign="top" align="left">
									<table height="100%" cellSpacing="0" cellPadding="0" align="top" border="0">
										<tr>
											<TD><IMG src="../Images/client_contacts_tab.gif" border="0"></TD>
										</tr>
									</table>
									<table "height=100%"  cellSpacing="0" cellPadding="0" width="100%" border="0" valign="top">
										<TBODY>
											<TR class="center-bg">
												<TD vAlign="top" align="left" width="96%" style="HEIGHT: 1px"><IMG height="10" src="../Images/spacer.gif" width="10"></TD>
												<TD vAlign="top" align="right" width="3%" style="HEIGHT: 1px"><IMG height="10" src="../Images/right_top_corner.gif" width="10"></TD>
											</TR>
											<TR class="center-bg" vAlign="top" align="left">
												<TD vAlign="top">
													<TABLE height="98%" cellSpacing="0" cellPadding="2" align="left" border="0">
														<TBODY>
															<tr height="20px" vAlign="middle">
																<td height="20px" class="Errortxt" vAlign="top" colSpan="5"><asp:label id="LblException" runat="server" CssClass="Errortxt"></asp:label></td>
															</tr>
															<tr class="Gridtxt" vAlign="middle">
																<td valign=top class="Mendatoryfld">*</td>
																<td class="Labeltxt" vAlign="top">Client Name</td>
																<td class="Gridtxt">&nbsp;
																</td>
																<td class="Gridtxt"><asp:dropdownlist CssClass="Gridtxt" id="ddlClient" 
                                                                        runat="server" AutoPostBack="True" Width="141px"></asp:dropdownlist></td>
																<td class="Gridtxt"><asp:label id="lblName" runat="server" CssClass="Errortxt"></asp:label></td>
															</tr>
															<tr class="Gridtxt" vAlign="middle">
																<td class="Gridtxt" vAlign="top" colspan="5"></td>
															</tr>
															<tr class="Gridtxt" vAlign="middle">
																<td valign=top class="Mendatoryfld">*</td>
																<td class="Labeltxt" style="HEIGHT: 27px" vAlign="top">Contact</td>
																<td class="Gridtxt">&nbsp;</td>
																<td class="Gridtxt"><asp:dropdownlist CssClass="Gridtxt" id="ddlContact" runat="server" Width="141px">
																		<asp:ListItem Value="0" Selected="True">--Select--</asp:ListItem>
																	</asp:dropdownlist></td>
																<td class="Errortxt"><asp:label id="LblContact" runat="server" CssClass="Errortxt"></asp:label></td>
															</tr>
															<tr class="Gridtxt">
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr class="Gridtxt" vAlign="middle">
																<td class="Gridtxt">&nbsp;</td>
																<td class="Gridtxt">&nbsp;</td>
																<td class="Gridtxt" vAlign="top" colspan="3">
                                                                    <asp:imagebutton id="imgBtnSave" 
                                                                        Runat="server" ImageUrl="~/Images/save_bt.gif" onclick="imgBtnSave_Click"></asp:imagebutton>&nbsp;<IMG 
                                                                        style="CURSOR: POINTER" onclick="window.close();" src="../Images/cancel_bt.gif">&nbsp;<asp:imagebutton 
                                                                        id="imgReset" Runat="server" ImageUrl="~/Images/reset_bt.gif" 
                                                                        onclick="imgReset_Click" style="height: 19px"></asp:imagebutton></td>
															</tr>
															<tr class="Gridtxt">
																<td colspan="5">&nbsp;</td>
															</tr>
														</TBODY>
													</TABLE>
												</TD>
												<td></td>
											</TR>
											<TR class="center-bg">
												<TD vAlign="top" align="left"><IMG height="10" src="../Images/spacer.gif" width="10"></TD>
												<TD vAlign="bottom" align="right"><IMG height="10" src="../Images/right_bottom_corner.gif" width="10"></TD>
											</TR>
										</TBODY>
									</table>
								</TD>
							</TR>
							<tr class="Gridtxt">
								<td colspan="3" height="1%">&nbsp;</td>
							</tr>
						</TABLE>
					</td>
				</tr>
			</TABLE>

            <script language="javascript">
                function EnterVal(event) {
                    if (window.event.keyCode == 13) {
                        event.returnValue = false;
                        event.cancel = true;
                        document.getElementById('imgBtnSave').click();
                    }
                }
		</script>
</asp:Content>
