﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TeamMembers_Add.aspx.cs" Inherits="DrishProject.AddProjectTeamMembers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
    <script language="javascript" type="text/javascript">
        function EnterVal(event) {
            if (window.event.keyCode == 13) {
                event.returnValue = false;
                event.cancel = true;
                document.getElementById('ImageButton1').click();
            }
        }
		</script>
</head>
<body>
	<form id="form1" runat="server">
	<div>
	<b>Add Project Team Members</b>

	</div>
	<div>
			<br />
		<asp:Label ID="Label1" runat="server" Text="Member Name"></asp:Label>
		<asp:DropDownList ID="drpMemberName" runat="server"  style="margin-left:50px;">
		</asp:DropDownList>
        <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
	</div><br />
	 <div>
		<asp:Label ID="Label2" runat="server" Text="Member Role"></asp:Label>
		<asp:DropDownList ID="drpMemberRole" runat="server"  style="margin-left:60px;">
		</asp:DropDownList>
         <asp:Label ID="LblRole" runat="server" Text=""></asp:Label>
	</div><br />
	 <div>
		<asp:Label ID="Label3" runat="server" Text="Member Working"></asp:Label>
		<asp:DropDownList ID="drpMemberWorking" runat="server"  style="margin-left:30px;">
			<asp:ListItem Value="0" Selected="True">== Select ==</asp:ListItem>
			<asp:ListItem Value="F">Full Time</asp:ListItem>
			<asp:ListItem Value="P">Part Time</asp:ListItem>
		</asp:DropDownList>
	     <asp:Label ID="LblTime" runat="server" Text=""></asp:Label>
	</div><br />
	 <div>
		<asp:Label ID="Label4" runat="server" Text="Member Availability"></asp:Label>
		<asp:DropDownList ID="drpMemberAvialability" runat="server" style="margin-left:20px;">			
			<asp:ListItem Selected="True">== Select ==</asp:ListItem>
			<asp:ListItem Value="Y">Active</asp:ListItem>
			<asp:ListItem Value="N">InActive</asp:ListItem>
		</asp:DropDownList>
	</div><br />
	 <div>
		<asp:Label ID="Label5" runat="server" Text="Reporting Manager"></asp:Label>
		<asp:DropDownList ID="drpReportingManager" runat="server"  style="margin-left:20px;">
            <asp:ListItem Selected="True">== Select ==</asp:ListItem>
		</asp:DropDownList>
	     <asp:Label ID="LblReportTo" runat="server" Text=""></asp:Label>
	</div>
	<div>
		<br />
		<asp:ImageButton ID="ImageButton1" runat="server" 
			ImageUrl="~/Images/add_btn.gif" style="margin-left:25px;" 
			onclick="ImageButton1_Click" />
		<asp:ImageButton ID="ImageButton2" runat="server" 
			ImageUrl="~/Images/reset_bt.gif" style="margin-left:55px;" 
			onclick="ImageButton2_Click" />
	    <br />
        <br />
        <asp:Label ID="LblException" runat="server"></asp:Label>
	</div>
    <div>
        <asp:HiddenField id="hdMode" runat="server" />
        <asp:HiddenField id="hdTeamMemberID" runat="server" />
    </div>
	</form>
</body>
</html>
