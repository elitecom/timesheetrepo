﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TeamMembers_Edit.aspx.cs" Inherits="DrishProject.EditProjectTeamMembers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <b>AddProjectTeamMembers</b>
    </div>
    <div>
        <asp:Label ID="Label1" runat="server" Text="Member Name"></asp:Label>
        <asp:DropDownList ID="DropDownList1" runat="server">
        </asp:DropDownList>
    </div><br />
     <div>
        <asp:Label ID="Label2" runat="server" Text="Member Role"></asp:Label>
        <asp:DropDownList ID="DropDownList2" runat="server">
        </asp:DropDownList>
    </div><br />
     <div>
        <asp:Label ID="Label3" runat="server" Text="Member Working"></asp:Label>
        <asp:DropDownList ID="DropDownList3" runat="server">
        </asp:DropDownList>
    </div><br />
     <div>
        <asp:Label ID="Label4" runat="server" Text="Member Availability"></asp:Label>
        <asp:DropDownList ID="DropDownList4" runat="server">
        </asp:DropDownList>
    </div><br />
     <div>
        <asp:Label ID="Label5" runat="server" Text="Reporting Manager"></asp:Label>
        <asp:DropDownList ID="DropDownList5" runat="server">
        </asp:DropDownList>
    </div>
    </form>
</body>
</html>
