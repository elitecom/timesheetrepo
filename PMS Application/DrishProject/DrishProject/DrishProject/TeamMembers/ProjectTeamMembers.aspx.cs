﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using Entities;
using System.IO;
using System.Web.UI.HtmlControls;

namespace DrishProject
{
    public partial class WebForm10 : System.Web.UI.Page
    {
        int ProjectID;
        int MemberID;
        int EmpID;
        Entities.Employee objEmployee = new Entities.Employee();
        Project objProject = new Project();

        protected void Page_Load(object sender, EventArgs e)
        {
            Project objProject = new Project();
            objEmployee = (Entities.Employee)Session["emp"];
            if (Request["ProjectCode"] == "")
            {
                //ProjectID = Convert.ToInt32(Session["ProjectID"]);
                objProject = (Project)(Session["Project"]);
                ProjectID = objProject.ProjectID;
            }
            else
            {
                ProjectID = Convert.ToInt32(Session["ProjectID"]);
            }

            objProject.ProjectID = ProjectID;

            objProject = new ProjectBAL().GetProject(ProjectID);

            //Code to hide the Release, CheckList, Client and Code Review Tabs for Non Engineering Projects.
            if (objProject.ProjectCategory != "ENGG")
            {
                //System.Web.UI.HtmlControls.HtmlTableCell tdFLY;
                //tdFLY = (HtmlTableCell)FindControl("tdRelease");
                //tdFLY.Visible = false;
                //tdFLY = (HtmlTableCell)FindControl("tdClients");
                //tdFLY.Visible = false;
                //tdFLY = (HtmlTableCell)FindControl("tdCheckList");
                //tdFLY.Visible = false;
                //tdFLY = (HtmlTableCell)FindControl("tdCodeReview");
                //tdFLY.Visible = false;
            }
            lblProjectCode.Text = objProject.ProjectCode;
            lblProjectName.Text = objProject.ProjectName;
            lblProjectStartDate.Text = objProject.ProjectStartDate.ToShortDateString();
            lblProjectEndDate.Text = objProject.ProjectEndDate.ToShortDateString();
            Entities.Employee ObjUser = new Entities.Employee();
            ObjUser = (Entities.Employee)Session["emp"];
            ObjUser.EmployeeId = new EmployeeBAL().GetEmployeeId(ObjUser);
            ObjUser = new EmployeeBAL().EmployeeDetailForMail(ObjUser);
            ObjUser.EmployeeId = new EmployeeBAL().GetEmployeeId(ObjUser);
            if (ObjUser.IsAministrator == 0)
            {
                //If the user is not the member of that project then deny his access
                if (!new DrishProjectBAL.ProjectBAL().IsActiveMember(ObjUser.EmployeeId, ProjectID))
                {
                    Response.Redirect("Default.aspx?Authority=Deny");
                }
                //objData.RoleAuthenticate();
                //if (User.IsInRole("AddTeamMember"))
                //{
                //    RegisterStartupScript("strShow", "<script language=\'javascript\'>this.document.all.addMember.style.display=\'block\';</script>");
                //}
                //else
                //{
                //    RegisterStartupScript("strHide", "<script language=\'javascript\'>this.document.all.addMember.style.display=\'none\';</script>");
                //}
                //System.Web.UI.HtmlControls.HtmlTableCell tdFLY;
                //tdFLY = (HtmlTableCell)FindControl("tdPTimeSheet");
                //tdFLY.Visible = false;
            }
            if (!Page.IsPostBack)
            {
                //this.SortCriteria = "EmpCode, Availability";
                //this.SortDir = "ASC";
                try
                {
                    GetEmployeeListPerProject(ProjectID);

                    //ImageButton img = new ImageButton();
                    //img = (ImageButton)(Page.FindControl("UC_Header1").FindControl("imgProjects"));
                    //img.ImageUrl = "..\\Images\\Project_active.gif";
                }
                catch (Exception ex)
                {
                    //LblException.Text = ex.Message;
                }
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            GetEmployeeListPerProject(ProjectID);
        }

        protected void GrdProjects_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            ImageButton imgbtn;
            //int intEmpId=0;
            imgbtn = (ImageButton)e.Item.FindControl("imageDel");
            if (imgbtn != null)
            {
                imgbtn.Attributes.Add("onclick", "return confirm(\'Are you sure you want to delete?\');");
            }
            if (e.Item.ItemType != ListItemType.Header)
            {
                RegisterStartupScript("Declare", "<script language='javascript'>var realColor;</script>");
                e.Item.Attributes.Add("OnMouseOver", "realColor = this.style.backgroundColor; this.style.backgroundColor = '#cccccc';");
                e.Item.Attributes.Add("OnMouseOut", "this.style.backgroundColor = realColor;");
            }
        }

        protected void GrdProjects_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            GrdProjects.CurrentPageIndex = e.NewPageIndex;
            GetEmployeeListPerProject(ProjectID);
        }

        protected void GrdProjects_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            //if (this.SortCriteria == e.SortExpression)
            //{
            //    if (this.SortDir == "asc")
            //    {
            //        this.SortDir = "desc";
            //    }
            //    else
            //    {
            //        this.SortDir = "asc";
            //    }
            //}

            //this.SortCriteria = e.SortExpression;
            GetEmployeeListPerProject(ProjectID);
        }
        public void GetEmployeeListPerProject(int ProjectID)
        {

            ProjectTeam objTeam = new ProjectTeam();
            Entities.Employee ObjUser = new Entities.Employee();
            try
            {
                DataSet DS = new DataSet();
                objTeam.ProjectId = ProjectID;
                //DS = objTeam.GetEmployeeListPerProject(this.SortCriteria, this.SortDir);
                DS = new ProjectTeamBAL().GetEmployeeListPerProject(ProjectID);
                GrdProjects.DataSource = DS;
                //Code to check whether the CurrentPageIndex value is not less than 0
                if (DS.Tables[0].Rows.Count > 0)
                {
                    if ((Math.Ceiling(double.Parse(DS.Tables[0].Rows.Count.ToString()) / GrdProjects.PageSize) - 1) < GrdProjects.CurrentPageIndex)
                    {
                        GrdProjects.CurrentPageIndex = 0;
                    }
                }
                else
                {
                    GrdProjects.CurrentPageIndex = 0;
                }
                GrdProjects.DataBind();
                ObjUser = (Entities.Employee)(Session["emp"]);
                //if (ObjUser.IsAministrator == 0)                
                if (new EmployeeBAL().IsAdministartor(ObjUser) == true)
                {
                    GrdProjects.Columns[5].Visible = true;
                    GrdProjects.Columns[6].Visible = true;
                    //if (User.IsInRole("EditTeamMember"))
                    //{
                    //    GrdProjects.Columns[5].Visible = true;
                    //}
                    //else
                    //{
                    //    GrdProjects.Columns[5].Visible = false;
                    //}
                    //if (User.IsInRole("DeleteTeamMember"))
                    //{
                    //    GrdProjects.Columns[6].Visible = true;
                    //}
                    //else
                    //{
                    //    GrdProjects.Columns[6].Visible = false;
                    //}
                }
                else
                {
                    GrdProjects.Columns[5].Visible = false;
                    GrdProjects.Columns[6].Visible = false;
                }
                if (DS.Tables[0].Rows.Count == 0)
                {
                    GrdProjects.PagerStyle.Visible = false;
                    LblException.Text = "No Employee Present.";
                }
                else
                {
                    GrdProjects.PagerStyle.Visible = true;
                    LblException.Text = "";
                }
                if (DS.Tables[0].Rows.Count <= GrdProjects.PageSize)
                {
                    GrdProjects.PagerStyle.Visible = false;
                }
                else
                {
                    GrdProjects.PagerStyle.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        protected void GrdProjects_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                string strRetVal;
                int intMemberId;
                ProjectTeam objTeam = new ProjectTeam();

                intMemberId = Convert.ToInt32(GrdProjects.DataKeys[e.Item.ItemIndex]);
                objTeam.MemberId = intMemberId;
                EmpID = new ProjectTeamBAL().GetSelectedEmpID(objTeam);
                objTeam.MemberId = intMemberId;
                objTeam.EmpId = EmpID;
                objTeam.ProjectId = ProjectID;
                strRetVal = new ProjectTeamBAL().DeleteTeamMember(objTeam).ToString();
                Entities.Employee objEmployee = new Entities.Employee();
                if (strRetVal == "")
                {
                    StreamReader objReader;
                    //File objFile;
                    string FullPath;
                    string strBodyContents;
                    FullPath = Server.MapPath("./Mails/RemoveTeamMember.txt");
                    objReader = File.OpenText(FullPath);
                    //objFile.OpenText(FullPath);
                    strBodyContents = objReader.ReadToEnd();
                    objReader.Close();
                    //string strBody;
                    //string strSubject;
                    //string strFrom;
                    //string strTo;
                    objEmployee.EmployeeId = EmpID;

                    objEmployee = new EmployeeBAL().EmployeeDetailForMail(objEmployee);


                    //strTo = objEmployee._Fname + " " + objEmployee._Lname + "<" + objEmployee._Email + ">";
                    //strFrom = ObjUser.FirstName + " " + ObjUser.LastName + "<" + ObjUser.EmailID + ">";
                    //strSubject = "Team Member Deallocated from " + objProject.ProjectName;
                    //strBody = strBodyContents.Replace("<<TeamMember>>", objEmployee._Fname + " " + objEmployee._Lname);
                    //strBody = strBody.Replace("<<ProjectName>>", objProject.ProjectName);
                    //strBody = strBody.Replace("<<TeamLead>>", ObjUser.FirstName + " " + ObjUser.LastName);
                    //strBody = strBody.Replace("<<Email>>", "<A HREF=mailto:" + ObjUser.EmailId + ">" + ObjUser.EmailId + "</A>");

                    //if (strTo != "")
                    //{
                    //    if (SendDynamicMail(strFrom, strTo, "", strSubject, "", strBody, "HTML") == false)
                    //    {
                    //        LblException.Text = "Member updated! Mail not sent to " + strTo + " due to invalid Email Address";
                    //    }
                    //    else
                    //    {
                    //        strRetVal = "Deleted Successfully!";
                    //  }
                    //}
                }
                GetEmployeeListPerProject(ProjectID);
                GrdProjects.EditItemIndex = -1;
                LblException.Text = "<b>Team Member Deleted Successfully</b>";
            }
            catch (Exception ex)
            {
                LblException.Text = ex.Message;
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {

            Response.Redirect("~/Projects/ProjectDetail.aspx");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TeamMembers/ProjectTeamMembers.aspx");
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectModules/Module.aspx");
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Tasks/MyTasks.aspx");
        }

        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Documents/Documents.aspx");
        }

        protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectRelease/Release.aspx");
        }

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Clients/Clients.aspx");
        }

        protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectMeetings/Meeting.aspx");
        }

        protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectHistory/History.aspx");
        }

        protected void ImageButton10_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectReview/Reviews.aspx");
        }

        protected void ImageButton11_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectNotes/Notes.aspx");
        }
    }
}