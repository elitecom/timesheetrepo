﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectBAL;
using System.IO;

namespace DrishProject
{
    public partial class AddProjectTeamMembers : System.Web.UI.Page
    {
        int ProjectID;
        int empId;
        DataSet ds;
        string action = string.Empty;
        Project project = new Project();
        Entities.Employee employee = new Entities.Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            ProjectID = Convert.ToInt32(Session["ProjectID"]);
            action = Request.QueryString["varAction"].ToString();
            hdMode.Value = action;
            if (!Page.IsPostBack)
            {
                // Get All Employee List
                ds = new DataSet();
                ds = new EmployeeBAL().GetAllEmployees();
                drpMemberName.DataSource = ds.Tables[0];
                drpMemberName.DataMember = ds.Tables[0].TableName;
                drpMemberName.DataTextField = "Name";
                drpMemberName.DataValueField = "EmpId";
                drpMemberName.DataBind();
                drpMemberName.Items.Insert(0, "== Select ==");
                
                // Get All ReportingManagers List
                ds = new DataSet();
                ds = new EmployeeBAL().GetAllReportingManagers();
                drpReportingManager.DataSource = ds.Tables[0];
                drpReportingManager.DataMember = ds.Tables[0].TableName;
                drpReportingManager.DataTextField = "Name";
                drpReportingManager.DataValueField = "EmpId";
                drpReportingManager.DataBind();
                drpReportingManager.Items.Insert(0, "== Select ==");

                // Member Role
                ds = new DataSet();
                ds = new EmployeeBAL().GetMemberRolesList();
                drpMemberRole.DataSource = ds.Tables[0];
                drpMemberRole.DataMember = ds.Tables[0].TableName;
                drpMemberRole.DataTextField = "MemberRole";
                drpMemberRole.DataValueField = "MemberRoleID";
                drpMemberRole.DataBind();
                drpMemberRole.Items.Insert(0, "== Select ==");

                if (action == "add")
                {
                    ImageButton1.ImageUrl = "~/Images/add_btn.gif";
                }
                else
                {
                    empId = Convert.ToInt32(Request.QueryString["MemberId"].ToString());
                    // Get details of the Team Member
                    ds = new DataSet();
                    ds = new ProjectTeamBAL().GetTeamMemberDetails(ProjectID, empId);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        drpMemberName.SelectedValue = Convert.ToInt32(ds.Tables[0].Rows[0]["EmpID"].ToString()).ToString();
                        drpMemberRole.SelectedValue = Convert.ToInt32(ds.Tables[0].Rows[0]["MemberRoleID"].ToString()).ToString();
                        drpMemberWorking.SelectedValue = ds.Tables[0].Rows[0]["MemberWorking"].ToString();
                        drpMemberAvialability.SelectedValue = ds.Tables[0].Rows[0]["Availability"].ToString();
                        drpReportingManager.SelectedValue = Convert.ToInt32(ds.Tables[0].Rows[0]["ReportTo"].ToString()).ToString();
                    }
                    ImageButton1.ImageUrl = "~/Images/update_bt.gif";

                }
                drpMemberName.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                drpMemberRole.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                drpMemberWorking.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                drpReportingManager.Attributes.Add("OnKeyPress", " return EnterVal(event);");
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            ProjectTeam objTeam = new ProjectTeam();
            string str = "";
            string str1 = "";

            str = Convert.ToString(drpMemberName.SelectedItem);
            str1 = Convert.ToString(drpReportingManager.SelectedItem);

            try
            {
                if (drpMemberName.SelectedValue == "0")
                {
                    lblName.Text = "Name required";
                    SetFocus(drpMemberName);
                    return;
                }
                else
                {
                    lblName.Text = "";
                }

                if (drpMemberRole.SelectedValue == "0")
                {
                    LblRole.Text = "Role required";
                    SetFocus(drpMemberRole);
                    return;
                }
                else
                {
                    LblRole.Text = "";
                }
                if (drpMemberWorking.SelectedValue == "0")
                {
                    LblTime.Text = "Work type required";
                    SetFocus(drpMemberWorking);
                    return;
                }
                else
                {
                    LblTime.Text = "";
                }
                if (drpReportingManager.SelectedValue == "0")
                {
                    LblReportTo.Text = "Report to  required";
                    SetFocus(drpReportingManager);
                    return;
                }
                else
                {
                    LblReportTo.Text = "";
                }
                if (str == str1)
                {
                    LblRole.Text = "Member cannot report to himself";
                    //SetFocus(ddlReport);
                    return;
                }

                int intMemberNameID = 0;
                int intMemberRoleID = 0;
                string strMemberWorkID = "";
                string strAvailability = "";
                int intReportTo = 0;
                intMemberNameID = Convert.ToInt32(drpMemberName.SelectedValue);
                intMemberRoleID = Convert.ToInt32(drpMemberRole.SelectedValue);
                strMemberWorkID = drpMemberWorking.SelectedValue;
                strAvailability = drpMemberAvialability.SelectedValue;
                intReportTo = Convert.ToInt32(drpReportingManager.SelectedValue);

                //string strBody;
                //string strSubject;
                //string strFrom;
                //string strTo;
                //string strUserName;
                StreamReader objReader;
                // File objFile;
                string FullPath;
                string strBodyContents;

                project.ProjectID = ProjectID;
                project = new ProjectBAL().GetProject(ProjectID);
                //  System.Data.SqlClient.SqlDataReader DSEmail;
                employee.EmployeeId = Convert.ToInt32(drpMemberName.SelectedValue.ToString());
                employee = new EmployeeBAL().EmployeeDetailForMail(employee);
                if (drpMemberAvialability.SelectedValue == "Y")
                {
                    //FullPath = Server.MapPath("./Mails/AssignTeamMember.txt");
                    //objReader = File.OpenText(FullPath);
                    //strBodyContents = objReader.ReadToEnd();
                    //objReader.Close();

                    // strTo = objEmployee.EmailId
                    //strTo = ddlMemberName.SelectedItem.Text + "<" + objEmployee.EmailId + ">";
                    //strFrom = Strings.Trim(objUser.FirstName) + " " + Strings.Trim(objUser.LastName) + "<" + Strings.Trim(objUser.EmailID) + ">";
                    //strSubject = "Team Member Assigned for " + objProjectClass.ProjectName;
                    //strBody = strBodyContents.Replace("<<TeamMember>>", ddlMemberName.SelectedItem.Text);
                    //strBody = strBody.Replace("<<ProjectName>>", objProjectClass.ProjectName);
                    //strBody = strBody.Replace("<<Availability>>", ddlAvailable.SelectedItem.Text);
                    //strBody = strBody.Replace("<<MemberRole>>", ddlMemberRole.SelectedItem.Text);
                    //strBody = strBody.Replace("<<PartFull>>", ddlFullPart.SelectedItem.Text);
                    //strBody = strBody.Replace("<<TeamLead>>", objUser.FirstName + " " + objUser.LastName);
                    //strBody = strBody.Replace("<<Email>>", "<A HREF=mailto:" + Strings.Trim(objUser.EmailId) + ">" + Strings.Trim(objUser.EmailId) + "</A>");
                    //strSubject = "Member Assigned for " + objProjectClass.ProjectName + "";
                    //strFrom = objUser.EmailId;
                }

                if (drpMemberAvialability.SelectedValue == "N")
                {
                    //FullPath = Server.MapPath("./Mails/RemoveTeamMember.txt");
                    //objReader = File.OpenText(FullPath);
                    //strBodyContents = objReader.ReadToEnd();
                    //objReader.Close();

                    //strTo = Strings.Trim(ddlMemberName.SelectedItem.Text) + "<" + Strings.Trim(objEmployee.EmailId) + ">";
                    //strFrom = Strings.Trim(objUser.FirstName) + " " + Strings.Trim(objUser.LastName) + "<" + Strings.Trim(objUser.EmailID) + ">";
                    //strSubject = "Team Member Deallocated from " + objProjectClass.ProjectName;
                    //strBody = strBodyContents.Replace("<<TeamMember>>", ddlMemberName.SelectedItem.Text);
                    //strBody = strBody.Replace("<<ProjectName>>", objProjectClass.ProjectName);
                    //strBody = strBody.Replace("<<TeamLead>>", objUser.FirstName + " " + objUser.LastName);
                    //strBody = strBody.Replace("<<Email>>", "<A HREF=mailto:" + Strings.Trim(objUser.EmailId) + ">" + Strings.Trim(objUser.EmailId) + "</A>");
                }

                objTeam.ProjectId = ProjectID;
                objTeam.MemberId = employee.EmployeeId;
                objTeam.MemberNameId = employee.EmployeeId;
                objTeam.EmpId = intMemberNameID;
                objTeam.MemberRoleId = intMemberRoleID;
                objTeam.MemberWorking = drpMemberWorking.SelectedValue.ToString();
                objTeam.Availability = strAvailability;
                objTeam.ReportTo = intReportTo;
                //objTeam.ModifiedBy = employee.EmployeeId;
                objTeam.ModifiedBy = ((Entities.Employee)(Session["emp"])).EmployeeId;
                objTeam.ModifiedOn = DateTime.Today;
                if (hdMode.Value.ToLower() == "edit")
                {
                    //MemberID = Convert.ToInt32(txtMemberID.Text);
                    //objTeam.MemberId = Convert.ToInt32(drpMemberName.SelectedValue.ToString());
                    //objTeam.MemberNameId = Convert.ToInt32(drpMemberName.SelectedValue.ToString());
                    //objTeam.MemberAvailability = drpMemberAvialability.SelectedValue.ToString();
                    new ProjectTeamBAL().UpdateTeamMember(objTeam);
                    LblException.Text = "Member Updated Successfully";
                }
                else
                {
                    objTeam.EmpId = Convert.ToInt32(drpMemberName.SelectedValue);
                    objTeam.ProjectId = ProjectID;
                    if (new ProjectTeamBAL().MemberExist(objTeam))                    
                    {
                        lblName.Text = "Member exists";
                        SetFocus(drpMemberName);
                        return;
                    }
                    else
                    {
                        // @MemberNameID
                        new ProjectTeamBAL().InsertTeamMember(objTeam);
                        hdMode.Value = "edit";
                        LblException.Text = "Member Added Successfully";
                    }
                }
                //if (strTo != "")
                //{
                //    if (SendDynamicMail(strFrom, strTo, "", strSubject, "", strBody, "HTML") == false)
                //    {
                //        LblException.Text = "Member updated! Mail not sent to " + strTo + " due to invalid Email Address";
                //    }
                //}
            }
            catch (Exception ex)
            {
                Response.Write("Message : " + ex.Message + "\n" + ex.StackTrace);
                LblException.Text = ex.Message;
            }
            RegisterStartupScript("strRefresh", "<script language=\'javascript\'>window.opener.PageRefresh();</script>");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("TeamMembers_Add.aspx");
        }

        private void SetFocus(Control ctrl)
        {
            // Define the JavaScript function for the specified control.
            string FocusScript;
            FocusScript = "<script language=\'javascript\'>" + ("document.getElementById(\'" + ctrl.ClientID) + "\').focus();</script>";
            //Add the JavaScript code to the page.
            Page.RegisterStartupScript("FocusScript", FocusScript);
        }
    }
}