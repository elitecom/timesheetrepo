﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="ProjectTeamMembers.aspx.cs" Inherits="DrishProject.WebForm10" %>

<%@ Register src="../UserControls/ProjectMenu.ascx" tagname="ProjectMenu" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div>
        <asp:ImageButton ID="ImageButton1" runat="server" 
            ImageUrl="~/Images/details_new.gif" onclick="ImageButton1_Click" />
        <asp:ImageButton ID="ImageButton2" runat="server" 
            ImageUrl="~/Images/teammem_active.gif" onclick="ImageButton2_Click" />
        <asp:ImageButton ID="ImageButton3" runat="server" 
            ImageUrl="~/Images/tabs_bt_06.gif" onclick="ImageButton3_Click" />
        <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/tasks.gif" 
            onclick="ImageButton4_Click" />
        <asp:ImageButton ID="ImageButton5" runat="server" 
            ImageUrl="~/Images/tabs_bt_05.gif" onclick="ImageButton5_Click" />
        <asp:ImageButton ID="ImageButton6" runat="server" 
            ImageUrl="~/Images/tabs_bt_08.gif" onclick="ImageButton6_Click" />
        <asp:ImageButton ID="ImageButton7" runat="server" 
            ImageUrl="~/Images/tabs_bt_03.gif" onclick="ImageButton7_Click" />
        <asp:ImageButton ID="ImageButton8" runat="server" 
            ImageUrl="~/Images/tabs_bt_10.gif" onclick="ImageButton8_Click" />
        <asp:ImageButton ID="ImageButton9" runat="server" 
            ImageUrl="~/Images/tabs_bt_07.gif" onclick="ImageButton9_Click" />
        <asp:ImageButton ID="ImageButton10" runat="server" 
            ImageUrl="~/Images/review_active.gif" onclick="ImageButton10_Click" />
        <asp:ImageButton ID="ImageButton11" runat="server" 
            ImageUrl="~/Images/tabs_bt_09.gif" onclick="ImageButton11_Click" />
        <%--<uc1:ProjectMenu ID="ProjectMenu1" runat="server" />--%>
    </div>
    <div>
        <table class="center-bg" valign="middle">
            <tbody>
                <tr>
                    <td class="Gridtxt" valign="middle" align="left" style="width: 1091px">
                        <b>Project&nbsp;Code&nbsp;:&nbsp;&nbsp;</b>
                        <asp:Label ID="lblProjectCode" CssClass="Gridtxt" runat="server"></asp:Label>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp; Project&nbsp;Name&nbsp;:&nbsp;&nbsp;</b>
                        <asp:Label ID="lblProjectName" CssClass="Gridtxt" runat="server"></asp:Label>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Project&nbsp;Start Date&nbsp;:&nbsp;&nbsp;</b>
                        <asp:Label ID="lblProjectStartDate" CssClass="Gridtxt" runat="server"></asp:Label>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Project&nbsp;End Date&nbsp;:&nbsp;&nbsp;</b>
                        <asp:Label ID="lblProjectEndDate" CssClass="Gridtxt" runat="server"></asp:Label>
                         <asp:Button ID="btnRefresh" runat="server" Visible="False" Width="0px" OnClick="btnRefresh_Click"></asp:Button>
                        <%-- <asp:DropDownList ID="DropDownList1" runat="server" Width="0px" AutoPostBack="True">
                         </asp:DropDownList>--%>
                    </td>
                    <td style="width:100px;"></td>
                    <td class="Gridtxt" valign="middle" align="right" style="width:100px;">
                        &nbsp;<img id="addMember" style="cursor: POINTER" onclick="MemberAddFunction()" src="../Images/add_mem_bt.gif" border="0" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div>
    </div>
    <div>
        <%--<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="EmpID" HeaderText="EmpID" />
                <asp:BoundField DataField="FirstName" HeaderText="EmpName" />
                <asp:BoundField DataField="MemberRole" HeaderText="Member Role" />
                <asp:BoundField DataField="MemberWorking" HeaderText="Member Working" />
                <asp:BoundField DataField="Availability" HeaderText="Availability" />
                <asp:BoundField DataField="EmailId" HeaderText="EmailId" />
            </Columns>
        </asp:GridView>--%>
        <asp:DataGrid ID="GrdProjects" runat="server" Width="115%" AllowPaging="True" BorderColor="White"
            BackColor="#F7F7F7" AutoGenerateColumns="False" CellPadding="3" GridLines="Vertical"
            BorderStyle="None" DataKeyField="MemberID" AllowSorting="True" OnItemDataBound="GrdProjects_ItemDataBound"
            OnPageIndexChanged="GrdProjects_PageIndexChanged" 
            OnSortCommand="GrdProjects_SortCommand" 
            ondeletecommand="GrdProjects_DeleteCommand">
            <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
            <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
            <ItemStyle CssClass="Gridtxt"></ItemStyle>
            <HeaderStyle CssClass="GridHead"></HeaderStyle>
            <FooterStyle CssClass="GridFoot"></FooterStyle>
            <Columns>
                <asp:BoundColumn DataField="MemberName" SortExpression="MemberName" HeaderText="Member Name">
                </asp:BoundColumn>
                <asp:HyperLinkColumn DataNavigateUrlField="EmailID" DataNavigateUrlFormatString="mailto:{0}"
                    DataTextField="EmailID" SortExpression="EmailID" HeaderText="Email"></asp:HyperLinkColumn>
                <asp:BoundColumn DataField="MemberRole" SortExpression="MemberRole" HeaderText="Member Role">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="MemberWorking" SortExpression="MemberWorking" HeaderText="Part/Full Time">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Availability" SortExpression="Availability" HeaderText="Availability">
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Edit">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <img id="imgDel" src="../Images/pen.gif" alt="Edit" onclick='MemberEditFunction(<%# Eval("MemberID")%>);'
                            style="cursor: pointer" title="Edit">
                        <%--<img ID="imgDel" src="Images/pen.gif" alt="Edit" OnClick='MemberEditFunction(<%# container.dataitem("MemberID")%>);' style="CURSOR:pointer" title="Edit">--%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Delete">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="imageDel" runat="server" ImageUrl="~/Images/cross.gif" CommandName="Delete"
                            CausesValidation="false" ToolTip="Delete"></asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
            <PagerStyle NextPageText="&lt;img src=Images/next_bt.gif border=0&gt;" PrevPageText="&lt;img src=Images/prev_bt.gif border=0&gt;"
                CssClass="Pagertxt" Mode="NumericPages"></PagerStyle>
        </asp:DataGrid>

        <br />
        <asp:Label ID="LblException" runat="server" Text="Label"></asp:Label>
    </div>
    <script language="javascript" type="text/javascript">
        function MemberEditFunction(ID) {
            var leftPos = (screen.width / 2) - 225;
            var topPos = (screen.height / 2) - 150;
            window.open("TeamMembers_Add.aspx?varAction=edit&MemberId=" + ID, "MemberEdit", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no,titlebar=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=450, height=300");
        }

        function MemberAddFunction() {
            var leftPos = (screen.width / 2) - 225;
            var topPos = (screen.height / 2) - 150;
            window.open("TeamMembers_Add.aspx?varAction=add", "MemberAdd", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=450, height=300");
        }
        function PageRefresh() {
            __doPostBack('btnRefresh', '');
        }
    </script>
</asp:Content>
