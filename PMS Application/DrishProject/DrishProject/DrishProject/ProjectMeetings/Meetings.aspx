﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="Meetings.aspx.cs" Inherits="DrishProject.ProjectMeetings.Meetings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table id="tblMain" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tbody>
            <tr height="10%">
                <td width="100%">
                    <!-- HEADER -->
                    <table id="tblHeader" height="50%" cellspacing="0" cellpadding="0" width="100%"
                        border="0">
                        <tr>
                            <td valign="top" align="left">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="80%">
                <td valign="top" align="left">
                    <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                        align="center" border="0">
                        <tr>
                            <%-- <tr>
                                        <td width="1%"></td>
                                      </tr>--%>
                            <td width="1%">
                            </td>
                            <td width="98%" height="100%" align="left" valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" align="top" border="0">
                                                <tr>
                                                    <td>
                                                        <div>
                                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/details_new.gif"
                                                                OnClick="ImageButton1_Click" />
                                                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/tabs_bt_02.gif"
                                                                OnClick="ImageButton2_Click" />
                                                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/tabs_bt_06.gif"
                                                                OnClick="ImageButton3_Click" />
                                                            <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/tasks.gif" OnClick="ImageButton4_Click" />
                                                            <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/tabs_bt_05.gif"
                                                                OnClick="ImageButton5_Click" />                                                            
                                                            <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/tabs_bt_03.gif"
                                                                OnClick="ImageButton7_Click" />
                                                            <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/Images/meeting_active.gif"
                                                                OnClick="ImageButton8_Click" />
                                                            <asp:ImageButton ID="ImageButton11" runat="server" ImageUrl="~/Images/tabs_bt_09.gif"
                                                                OnClick="ImageButton11_Click" />
                                                            <asp:ImageButton ID="ImageButton9" runat="server" ImageUrl="~/Images/tabs_bt_07.gif"
                                                                OnClick="ImageButton9_Click" />
                                                            <asp:ImageButton ID="ImageButton10" runat="server" ImageUrl="~/Images/tabs_bt_08.gif"
                                                                OnClick="ImageButton10_Click" />
                                                            <asp:ImageButton ID="ImageButton13" runat="server" ImageUrl="~/Images/review_active.gif"
                                                                OnClick="ImageButton13_Click" />
                                                            <asp:ImageButton ID="ImageButton14" runat="server" ImageUrl="~/Images/timesheet_tab_active.gif"
                                                                OnClick="ImageButton14_Click" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--<td width="1%"></td>--%>
                                        <td>
                                            <table height="400px" cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
                                                <tbody>
                                                   <%-- <tr class="center-bg">
                                                        <td valign="top" align="left" width="1%" style="height: 6px">
                                                            <img src="../images/spacer.gif" width="10">
                                                        </td>
                                                        <td valign="top" align="left" width="96%" style="height: 6px">
                                                            <img src="../images/spacer.gif" width="10">
                                                        </td>
                                                        <td valign="top" align="right" width="3%" style="height: 6px">
                                                            <img src="../images/right_top_corner.gif" width="10">
                                                        </td>
                                                    </tr>--%>
                                                    <tr valign="top">
                                                        <td align="right" colspan="2">
                                                            <table  width="100%" border="0">
                                                                <tr>
                                                                    <td class="Gridtxt" align="left" style="width: 71%">
                                                                        &nbsp;&nbsp; <strong>Project&nbsp;Code&nbsp;:&nbsp;</strong>
                                                                        <asp:Label ID="lblProjectCode" runat="server"></asp:Label>
                                                                        &nbsp;&nbsp; <strong>Start Date&nbsp;:&nbsp;</strong>
                                                                        <asp:Label CssClass="Gridtxt" ID="lblStartdate" runat="server"></asp:Label>
                                                                        &nbsp;&nbsp; <strong>End Date&nbsp;:&nbsp;</strong>
                                                                        <asp:Label CssClass="Gridtxt" ID="lblEndDate" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td class="Gridtxt" align="left" width="49.5%">
                                                                        <asp:DropDownList CssClass="Gridtxt" ID="DropDownList1" runat="server" AutoPostBack="True"
                                                                            Width="0px" Height="16px">
                                                                        </asp:DropDownList>
                                                                        <asp:Button ID="btnRefresh" runat="server" Width="0px" Visible="false" OnClick="btnRefresh_Click"
                                                                            Style="height: 26px"></asp:Button>
                                                                    </td>
                                                                    <td class="Gridtxt">
                                                                        <img id="imgAddMeeting" style="cursor: pointer" onclick="MeetingAddFunction();" src="../Images/meeting_bt.gif"
                                                                            border="0">
                                                                    </td>
                                                                    <td class="Gridtxt" width=".5%">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr class="center-bg" valign="top">
                                                        <td valign="top" align="left" width="98%" class="GridBG" height="100%">
                                                            <asp:DataGrid ID="dgMeetings" runat="server" Width="100%" AllowPaging="True" AutoGenerateColumns="False"
                                                                BackColor="#F7F7F7" BorderColor="White" DataKeyField="meetingid" CellPadding="3"
                                                                GridLines="Vertical" BorderStyle="None" AllowSorting="True" OnDeleteCommand="dgMeetings_DeleteCommand"
                                                                OnItemDataBound="dgMeetings_ItemDataBound" OnPageIndexChanged="dgMeetings_PageIndexChanged"
                                                                OnSortCommand="dgMeetings_SortCommand">
                                                                <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                                <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                                <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                                <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                                <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="MeetingDate" SortExpression="DateTime" ReadOnly="True"
                                                                        HeaderText="Date">
                                                                        <HeaderStyle Width="8%"></HeaderStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="subject" SortExpression="subject" ReadOnly="True" HeaderText="Subject">
                                                                        <HeaderStyle Width="30%"></HeaderStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="strMeetingStatus" SortExpression="strMeetingStatus" HeaderText="Status">
                                                                        <HeaderStyle Width="8%"></HeaderStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="UserName" SortExpression="UserName" ReadOnly="True" HeaderText="Hosted By">
                                                                        <HeaderStyle Width="10%"></HeaderStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="MeetingTime" SortExpression="MeetingTime" HeaderText="Time">
                                                                        <HeaderStyle Width="7%"></HeaderStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="duration" SortExpression="duration" HeaderText="Duration">
                                                                        <HeaderStyle Width="7%"></HeaderStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn HeaderText="Edit">
                                                                        <HeaderStyle HorizontalAlign="Center" Width="4%"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <%--<img id="imgEdit" align="middle" title="Edit Meeting" alt="Edit Meeting" src="../images/pen.gif" onclick='MeetingEditFunction(<%# Eval("Meetingid")%>)' style="CURSOR: pointer">--%>
                                                                            <img id="img1" align="middle" title="Edit Meeting" alt="Edit Meeting" src="../Images/pen.gif"
                                                                                onclick='' style="cursor: pointer">
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Recap">
                                                                        <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <%#GetUrl(Eval("MeetingStatus").ToString(), Eval("Meetingid").ToString())%> </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Delete">
                                                                        <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="btnDel" ToolTip="Delete Meeting" AlternateText="Delete Meeting"
                                                                                ImageUrl="~/Images/cross.gif" CommandName="Delete" runat="server"></asp:ImageButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                                <PagerStyle NextPageText="&lt;img src=Images/next_bt.gif border=0&gt;" PrevPageText="&lt;img src=../Images/prev_bt.gif border=0&gt;"
                                                                    CssClass="Pagertxt" Mode="NumericPages"></PagerStyle>
                                                            </asp:DataGrid>
                                                            <asp:Label ID="LblException" runat="server" CssClass="Errortxt"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <img height="10" src="../Images/spacer.gif" width="10">
                                                        </td>
                                                    </tr>
                                                    <tr class="center-bg">
                                                        <td class="Gridtxt" style="height: 53px">
                                                        </td>
                                                        <td class="Gridtxt" style="height: 53px">
                                                        </td>
                                                    </tr>
                                                    <tr class="center-bg" height="2%">
                                                        <td valign="top" align="left">
                                                            <img height="10" src="../Images/spacer.gif" width="10">
                                                        </td>
                                                        <td valign="bottom" align="right">
                                                            <img height="10" src="../Images/right_bottom_corner.gif" width="10">
                                                        </td>
                                                    </tr>
                                                    <tr class="Gridtxt">
                                                        <td colspan="2" height="3%">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="1%">
                                <img height="10" src="../images/spacer.gif" width="10">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--FOOTER -->
            <tr>
                <td width="100%">
                    <table id="tblFooter" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tbody>
                            <tr>
                                <td valign="top" align="left">
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <script language="javascript" type="text/javascript">
        function MeetingAddFunction() 
        {
            var leftPos = (screen.width / 2) - 250;
            var topPos = (screen.height / 2) - 250;
            window.open("Meeting_Add.aspx?MeetingId=0", "MeetingAdd", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=500, height=500");
        }
        function MeetingDetailFunction(ID) 
        {
            window.location = 'Project_Meeting.aspx?MeetingId=' + ID;
        }

        function MeetingEditFunction(ID) 
        {
            var leftPos = (screen.width / 2) - 250;
            var topPos = (screen.height / 2) - 250;
            window.open("Meeting_Add.aspx?MeetingId=" + ID, "MeetingEdit", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=500, height=500");
        }

        function PageRefresh() 
        {
            __doPostBack('btnRefresh', '');
        }
    </script>
</asp:Content>
