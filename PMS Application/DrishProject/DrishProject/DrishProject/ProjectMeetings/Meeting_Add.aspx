﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Meeting_Add.aspx.cs" Inherits="DrishProject.ProjectMeetings.Meeting_Add" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <script language="javascript">
        function EnterVal(event) {
            if (window.event.keyCode == 13) {
                event.returnValue = false;
                event.cancel = true
                document.getElementById('imgBtnSave').click();
            }
        }
        function ChangeCursor(field) {
            field.style.cursor = 'Wait';
            //document.style.cursor = 'Wait';
        }
        function CheckDuration() {
            var IdDura = document.getElementById('txtduration');
            var str = IdDura.value;

            for (var i = 0; i < str.length; i++) {
                var dot = str.indexOf(".", i);
                var dot1 = str.indexOf(":", i);

                var ch = str.substring(i, i + 1);
                if (((ch > "a" || "z" > ch) && (ch > "A" || "Z" > ch)) && (ch != ' ') && (ch < "0" || "9" < ch) && (ch = ".") && (ch = ":")) {
                    return false;
                }
            }
            return true;
        }
        function check() {
            var IdLblSave = document.getElementById('LblException');
            var IdtrainingName = document.getElementById('txttraining');
            var test2;
            test2 = CheckDuration();

            if (test2 == false) {
                IdLblSave.innerHTML = "The duration field accepts only numbers";
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="FrmEditMeeting" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table id="MAINtable" height="100%" width="100%">
        <tr class="Gridtxt">
            <td width="100%" height="1%">
            </td>
        </tr>
        <tr>
            <td width="100%">
                <table id="tblCentral" height="100%" cellspacing="0" cellpadding="0" width="100%"
                    align="center" border="0">
                    <tr height="100%">
                        <td>
                        </td>
                        <td align="left" height="100%">
                            <table cellspacing="0" cellpadding="0" width="98%" align="top" border="0">
                                <tr>
                                    <td>
                                        <img id="imgTop" src="../Images/add_meeting_tab.gif" border="0">
                                    </td>
                                </tr>
                            </table>
                            <table height="98%" cellspacing="0" cellpadding="0" width="100%" border="0" valign="top">
                                <tbody>
                                    <tr class="center-bg">
                                        <td valign="top" align="left" width="96%">
                                            <img height="10" src="../Images/spacer.gif" width="10">
                                        </td>
                                        <td valign="top" align="right" width="3%">
                                            <img height="10" src="../Images/right_top_corner.gif" width="10">
                                        </td>
                                    </tr>
                                    <tr class="center-bg" valign="top" align="left">
                                        <td valign="top" colspan="2" height="100%">
                                            <table cellspacing="0" cellpadding="2" align="left" border="0">
                                                <tbody>
                                                    <tr valign="middle" height="21">
                                                        <td class="Errortxt" valign="top" colspan="4">
                                                            <asp:Label ID="LblException" runat="server" CssClass="Errortxt"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td class="Gridtxt">
                                                        </td>
                                                        <td class="Labeltxt" valign="top">
                                                            Project&nbsp;Code
                                                        </td>
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt">
                                                            <asp:Label ID="lblProjectCode" runat="server"></asp:Label><asp:Label ID="lblMeetingId"
                                                                runat="server" Visible="false" Enabled="False" Width="0px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td class="Mendatoryfld" valign="top">
                                                            *
                                                        </td>
                                                        <td class="Labeltxt" valign="top">
                                                            Meeting&nbsp;Status
                                                        </td>
                                                        <td class="gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="gridtxt">
                                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="Gridtxt" Width="200" AutoPostBack="true">
                                                                <asp:ListItem Selected="true">Planned</asp:ListItem>
                                                                <asp:ListItem>Executed</asp:ListItem>
                                                                <asp:ListItem>Cancelled</asp:ListItem>
                                                                <asp:ListItem></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtMeetingStatus" runat="server" Width="52px" Visible="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="Mendatoryfld" valign="top">
                                                            *
                                                        </td>
                                                        <td class="Labeltxt" valign="top">
                                                            Subject
                                                        </td>
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt">
                                                            <asp:TextBox ID="txtSubject" runat="server" Width="290px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="Mendatoryfld" valign="top">
                                                            *
                                                        </td>
                                                        <td class="Labeltxt" valign="top">
                                                            Hosted By
                                                        </td>
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlEmp" runat="server" CssClass="Gridtxt" Width="226px" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td class="Mendatoryfld" valign="top">
                                                            *
                                                        </td>
                                                        <td class="Labeltxt" valign="top">
                                                            Date
                                                        </td>
                                                        <td class="Gridtxt">
                                                            &nbsp;
                                                        </td>
                                                        <td class="Gridtxt">
                                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:TextBox ID="txtdate" runat="server" Width="141"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="txtdate_CalendarExtender" runat="server" BehaviorID="txtdate_CalendarExtender"
                                                                        TargetControlID="txtdate" Format="dd/MM/yyyy"></cc1:CalendarExtender>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            
                                                          <%--  <asp:Image ID="ImgDate" title="Calendar" Style="cursor: POINTER" runat="server" ImageUrl="~/Images/selectall.gif">
                                                            </asp:Image>--%>
                                                        </td>
                                                        <tr valign="top">
                                                            <td class="Mendatoryfld" valign="top">
                                                                *
                                                            </td>
                                                            <td class="Labeltxt" valign="top">
                                                                Venue
                                                            </td>
                                                            <td class="Gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlVenue" runat="server" CssClass="Gridtxt" Width="126px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr valign="middle">
                                                            <td class="Mendatoryfld" valign="top">
                                                                *
                                                            </td>
                                                            <td class="Labeltxt" valign="top">
                                                                Time
                                                            </td>
                                                            <td class="Gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt">
                                                                <asp:DropDownList ID="ddlHr" runat="server" Width="50">
                                                                </asp:DropDownList>
                                                                :<asp:DropDownList ID="ddlMin" runat="server" Width="50">
                                                                </asp:DropDownList>
                                                                &nbsp;<asp:DropDownList ID="ddlAMPM" runat="server" Width="50">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td class="Mendatoryfld" valign="top">
                                                            </td>
                                                            <td class="Labeltxt" valign="top">
                                                                Duration
                                                            </td>
                                                            <td class="Gridtxt">
                                                                &nbsp;
                                                            </td>
                                                            <td class="Gridtxt">
                                                                <asp:TextBox ID="txtduration" runat="server" MaxLength="3" CssClass="Gridtxt" Width="83px"></asp:TextBox>&nbsp;
                                                                <asp:DropDownList ID="ddldur" runat="server" Width="71px">
                                                                    <asp:ListItem>Mins</asp:ListItem>
                                                                    <asp:ListItem>Hrs</asp:ListItem>
                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                    <asp:ListItem>Weeks</asp:ListItem>
                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RegularExpressionValidator ID="RegExpTime" ControlToValidate="txtduration" ValidationExpression="(^[0-9\s]{1,20}$)"
                                                                    runat="server" ErrorMessage="Invalid entry!"></asp:RegularExpressionValidator>
                                                            </td>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="Mendatoryfld" valign="top">
                                        </td>
                                        <td class="Labeltxt" style="height: 20px" valign="top">
                                            Media
                                        </td>
                                        <td class="Gridtxt">
                                            &nbsp;
                                        </td>
                                        <td class="Gridtxt">
                                            <asp:DropDownList ID="ddlMedia" runat="server" CssClass="Gridtxt" Width="200">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Mendatoryfld" style="height: 3px" valign="top">
                                            *
                                        </td>
                                        <td class="Gridtxt" valign="top" align="left">
                                            <strong>Attendees<br>
                                            </strong>(Multi Select)
                                        </td>
                                        <td class="Gridtxt">
                                        </td>
                                        <td class="Gridtxt" valign="top">
                                            <asp:ListBox ID="lstAttendies" runat="server" Width="180px" DataValueField="EmpId"
                                                DataTextField="UserName" SelectionMode="Multiple" Height="132px"></asp:ListBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Mendatoryfld" valign="top" height="30">
                                        </td>
                                        <td class="Gridtxt" height="30">
                                        </td>
                                        <td class="Gridtxt" height="30">
                                        </td>
                                        <td class="Gridtxt" valign="top" height="30">
                                        </td>
                                    </tr>
                                    <tr valign="middle">
                                        <td class="Mendatoryfld" valign="top">
                                        </td>
                                        <td class="Gridtxt">
                                            &nbsp;
                                        </td>
                                        <td class="Gridtxt">
                                            &nbsp;
                                        </td>
                                        <td class="Gridtxt" valign="top">
                                            <asp:ImageButton ID="imgBtnSave" runat="server" ImageUrl="~/Images/save_bt.gif" OnClick="imgBtnSave_Click">
                                            </asp:ImageButton>&nbsp;&nbsp;
                                            <img style="cursor: POINTER" onclick="window.close();" src="../Images/canc_bt.gif">&nbsp;&nbsp;
                                            <asp:ImageButton ID="imgAddNew" runat="server" ImageUrl="~/Images/add_btn.gif" OnClick="imgAddNew_Click">
                                            </asp:ImageButton>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="center-bg" height="10">
                        <td valign="top" align="left" height="10">
                            <img height="10" src="../Images/spacer.gif" width="10">
                        </td>
                        <td valign="bottom" align="right" height="10">
                            <img height="10" src="../Images/right_bottom_corner.gif" width="10" />
                        </td>
                    </tr>
                    <tr class="Gridtxt">
                        <td colspan="2" height="3%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <img height="10" src="../Images/spacer.gif" width="1" />
            </td>
        </tr>
        </tr>
    </table>
    </form>
    <script language="javascript" type="text/javascript">
        var dtStart = document.getElementById('txtdate').value;
        new DateInput("txtdate", "ImgDate", null);
        document.getElementById('txtdate').value = dtStart;
    </script>
</body>
</html>
