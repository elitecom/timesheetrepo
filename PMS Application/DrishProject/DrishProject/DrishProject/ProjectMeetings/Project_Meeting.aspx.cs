﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using Entities;
using System.IO;

namespace DrishProject.ProjectMeetings
{
    public partial class Project_Meeting : System.Web.UI.Page
    {
        int intMeetingId;
        int ProjectId;
        Entities.Project objProject = new Entities.Project();
        Entities.Employee objUser = new Entities.Employee();
        Entities.Meeting objMeeting = new Entities.Meeting();
        bool res;

        protected void Page_Load(object sender, EventArgs e)
        {
            objUser = (Entities.Employee)(Session["emp"]);
			intMeetingId = Convert.ToInt32(Request.QueryString["MeetingId"].ToString());
			ProjectId = Convert.ToInt32(Session["ProjectId"].ToString());
			objProject.ProjectID = ProjectId;
            objProject = new ProjectBAL().GetProject(ProjectId);
			lblProjectCode.Text = objProject.ProjectCode;
            //if (IfAccessible(intMeetingId, ProjectId, "Meeting") == false) {
            //    Response.Redirect("Default.aspx?Authority=Deny");
            //}


			if (!Page.IsPostBack) 
            {
				GetMeetingDetails();
				if ((!string.IsNullOrEmpty(hlFile.Text))) 
                {
					Session["FileName"] = hlFile.Text;
					Session["NavigateUrl"] = hlFile.NavigateUrl;
				}
				DataSet DS = new DataSet();
                DS = new EmployeeBAL().GetListEmailAllEmployees();
				lstEmp.DataSource = DS.Tables[0].DefaultView;
				lstEmp.DataTextField = DS.Tables[0].Columns[0].ToString();
				lstEmp.DataBind();
                objUser.ProjectId = ProjectId;
                DS = new EmployeeBAL().GetEmailListPerProject(ProjectId);
				lstTeam.DataSource = DS.Tables[0].DefaultView;
				lstTeam.DataTextField = DS.Tables[0].Columns[0].ToString();
				lstTeam.DataBind();
			}
			//For inserting/retrieving the value of javasript textbox in web control textbox
			imgSave.Attributes.Add("OnClick", "ReturnText();");
			imgSend.Attributes.Add("OnClick", "ReturnText();");
            if (new EmployeeBAL().IsClient(objUser) == true) 
            {
				HideClientFeatures();
			}
        }

        private void GetMeetingDetails()
        {
            if (intMeetingId > 0)
            {
                objMeeting.MeetingId = intMeetingId;
                objMeeting = new DrishProjectBAL.MeetingBAL().GetMeeting(objMeeting);
                //lblStatus.Text =  GetStatus(objMeeting.MeetingStatusId);
                lblDate.Text = objMeeting.MeetingDate;
                lblTime.Text = objMeeting.MeetingTime;
                lblDuration.Text = objMeeting.Duration;
                lblHosted.Text = objMeeting.CalledByName;
                lblMedia.Text = objMeeting.Media;
                if (!string.IsNullOrEmpty(objMeeting.To))
                {
                    txtTO.Text = objMeeting.To;
                }
                txtCC.Text = objMeeting.Cc;
                txtSubject.Text = objMeeting.Subject;
                txtMessage.Text = objMeeting.Recap;
                if (!string.IsNullOrEmpty(objMeeting.Attachment))
                {
                    lblAttacment.Visible = true;
                    hlFile.Visible = true;
                    imgRemove.Visible = true;
                    lblAttacment.Text = "Attachment:";
                    hlFile.Text = objMeeting.Attachment;
                    hlFile.NavigateUrl = Request.ApplicationPath + "/" + System.Configuration.ConfigurationManager.AppSettings["GetAttachmentPath"].ToString() + "/" + objMeeting.Attachment;
                    hlFile.Target = "blank";
                }
                else
                {
                    lblAttacment.Visible = false;
                    hlFile.Visible = false;
                    imgRemove.Visible = false;
                }
            }
        }

        private void HideClientFeatures()
        {
            System.Web.UI.HtmlControls.HtmlImage FlyTo = null;
            FlyTo = (System.Web.UI.HtmlControls.HtmlImage)this.FindControl("To");
            System.Web.UI.HtmlControls.HtmlImage FlyCc = null;
            FlyCc = (System.Web.UI.HtmlControls.HtmlImage)this.FindControl("Cc");
            imgSave.Visible = false;
            imgSend.Visible = false;
            imgRemove.Visible = false;
            FlyTo.Visible = false;
            FlyCc.Visible = false;
        }

        protected void imgSave_Click(object sender, ImageClickEventArgs e)
        {
            string strSubject = "";
            string strEmpCode = "";
            string strRetVal = "";
            string strMessage = "";
            int intEmpId = 0;

            objMeeting.MeetingId = intMeetingId;
            objMeeting = new MeetingBAL().GetMeeting(objMeeting);
            intEmpId = objMeeting.CalledBy;

            if (txtSubject.Text.Length > 500)
            {
                LblException.Text = "Subject can not be more than 500 characters !";
                SetFocus(txtSubject);
                res = false;
                return;
            }
            else
            {
                LblException.Text = "";
            }

            if (!string.IsNullOrEmpty((txtSubject.Text)))
            {
                strSubject = txtSubject.Text.Trim();
            }

            if (!string.IsNullOrEmpty((txtMessage.Text)))
            {
                if (txtMessage.Text.Length > 4000)
                {
                    LblException.Text = "Recap can not be more than 4000 characters !";
                    SetFocus(txtMessage);
                    res = false;
                    return;
                }
                else
                {
                    LblException.Text = "";
                }
                strMessage = txtMessage.Text.Trim();
            }

            if (txtTO.Text.Length > 4000)
            {
                LblException.Text = "To many recipients in To Field. Not be more than 4000 characters !";
                SetFocus(txtTO);
                res = false;
                return;
            }

            if (txtCC.Text.Length > 4000)
            {
                LblException.Text = "To many recipients in Cc Field. Not be more than 4000 characters !";
                SetFocus(txtCC);
                res = false;
                return;
            }

            string strFileName = "";
            if (!string.IsNullOrEmpty(AttachFile.PostedFile.FileName))
            {
                strFileName = System.IO.Path.GetFileName(AttachFile.PostedFile.FileName);
                if (System.IO.File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["GetAttachmentPath"].ToString() + "/" + strFileName)))
                {
                    strFileName = System.IO.Path.GetFileNameWithoutExtension(AttachFile.PostedFile.FileName);
                    strFileName = strFileName + "_" + intMeetingId + System.IO.Path.GetExtension(AttachFile.PostedFile.FileName);
                    AttachFile.PostedFile.SaveAs(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["GetAttachmentPath"].ToString() + "/" + strFileName));
                }
                else
                {
                    AttachFile.PostedFile.SaveAs(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["GetAttachmentPath"].ToString() + "/" + strFileName));
                }
            }

            try
            {
                var _with1 = objMeeting;
                _with1.MeetingId = intMeetingId;
                _with1.Recap = strMessage;
                _with1.MeetingDate = objMeeting.MeetingDate;
                _with1.MeetingHr = objMeeting.MeetingHr;
                _with1.MeetingMin = objMeeting.MeetingMin;
                _with1.MeetingAmPm = objMeeting.MeetingAmPm;
                _with1.Duration = objMeeting.Duration;
                _with1.MediaId = objMeeting.MediaId;
                _with1.Subject = strSubject;
                _with1.CalledBy = intEmpId;
                _with1.MeetingStatusId = objMeeting.MeetingStatusId;
                _with1.To = txtTO.Text;
                _with1.Cc = txtCC.Text;
                _with1.Attachment = strFileName;
                if (new MeetingBAL().UpdateMeeting(_with1) == true)
                {
                    strRetVal = "Recap saved successfully.";
                    res = true;
                }
                else
                {
                    strRetVal = "Meeting already recorded, Could not be modified.";
                    res = false;
                }
                LblException.Text = strRetVal;
                if (!string.IsNullOrEmpty(strFileName))
                {
                    lblAttacment.Visible = true;
                    hlFile.Visible = true;
                    imgRemove.Visible = true;
                    lblAttacment.Text = "Attachment:";
                    hlFile.Text = strFileName;
                    Session["FileName"] = strFileName;
                    hlFile.NavigateUrl = Request.ApplicationPath + "/" + System.Configuration.ConfigurationManager.AppSettings["GetAttachmentPath"].ToString() + "/" + objMeeting.Attachment;
                    Session["NavigateUrl"] = Request.ApplicationPath + "/" + System.Configuration.ConfigurationManager.AppSettings["GetAttachmentPath"].ToString() + "/" + objMeeting.Attachment;
                    hlFile.Target = "blank";
                }
                else
                {
                    lblAttacment.Visible = false;
                    hlFile.Visible = false;
                    imgRemove.Visible = false;
                }

                if (!string.IsNullOrEmpty(Session["FileName"].ToString()))
                {
                    lblAttacment.Visible = true;
                    hlFile.Visible = true;
                    imgRemove.Visible = true;
                    lblAttacment.Text = "Attachment:";
                    hlFile.Text = Session["FileName"].ToString();
                    hlFile.NavigateUrl = Session["NavigateUrl"].ToString();
                    hlFile.Target = "blank";
                }
            }
            catch (Exception ex)
            {
                LblException.Text = ex.Message;
            }
        }

        protected void imgSend_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtTO.Text))
                {
                    LblException.Text = "You must specify some recipients for the message.";
                    return;
                }
                if (string.IsNullOrEmpty(txtSubject.Text))
                {
                    LblException.Text = "Please specify the subject for the message.";
                    return;
                }
                if (string.IsNullOrEmpty(txtMessage.Text))
                {
                    LblException.Text = "Please specify the message for the mail.";
                    return;
                }
                //Save the meeting and then send.
                imgSave_Click(sender, e);
                if (res == true)
                {
                    bool res1;
                    res1 = SendMailtoMembers(intMeetingId, Convert.ToDateTime(objMeeting.MeetingDate), objMeeting.MeetingTime, txtMessage.Text);
                    if (res1 == true)
                    {
                        LblException.Text = "Mail Sent Successfully";
                    }
                }
                else
                {
                    LblException.Text = LblException.Text;
                }
                LblException.Text = "Recap saved and sent successfully.";
            }
            catch (Exception ex)
            {
                LblException.Text = ex.Message;
            }
        }

        private void SetFocus(Control ctrl)
        {
            // Define the JavaScript function for the specified control.
            string FocusScript = null;
            FocusScript = "<script language='javascript'>" + "document.getElementById('" + ctrl.ClientID + "').focus();</script>";
            //Add the JavaScript code to the page.
            Page.RegisterStartupScript("FocusScript", FocusScript);
        }

        private bool SendMailtoMembers(int pMeetingId, System.DateTime pMeetingDate, string pTime, string pMessage)
        {            
            try
            {
                DataSet dbDt = null;
                string strFrom = null;
                string strEmailId = null;
                dynamic i = null;
                string strLine1R = null;
                string strLine2R = null;
                string strSubjectR = null;

                Entities.Meeting objMeeting = new Entities.Meeting();
                objMeeting.MeetingId = pMeetingId;
                objMeeting = new MeetingBAL().GetMeeting(objMeeting);
                string strFileName = objMeeting.Attachment;
                bool bSendModMail = false;
                bool bSendRecapMail = false;
                ListItem lstItem = default(ListItem);

                objUser = (Entities.Employee)(Session["emp"]);
                strFrom = objUser.FirstName + " " + objUser.LastName;
                strEmailId = objUser.EmailId;

                strLine1R = "This is the recap of the meeting regarding " + txtSubject.Text + "<BR>" + "Date : " + Convert.ToString(pMeetingDate.ToShortDateString()) + "<BR>" + "Time : " + pTime;
                strLine2R = txtMessage.Text;
                strSubjectR = txtSubject.Text;
                bSendRecapMail = true;

                //SendMail(txtTO.Text, txtCC.Text, strFrom, strEmailId, strLine1R, strLine2R, strSubjectR, strFileName);
                return true;
            }
            catch (Exception ex)
            {
                LblException.Text = ex.Message;
                return false;
            }
        }

        private void SendMail(string ToMembers, string CcMembers, string pstrFrom, string pstrFromEmailId, string strLine1, string strLine2, string strsubject, string strAttachment)
        {
            StreamReader objReader = null;
            string FullPath = null;
            string strBodyContents = null;
            string strBody = null;
            string strFrom = null;
            string strTo = null;
            DataSet DSEmail = null;
            string str = null;
            string strEmpName = null;
            string strProjectName = null;
            string strEmailId = null;
            System.Data.SqlClient.SqlDataReader dbdr = null;
            Entities.Clients objClient = new Entities.Clients();
            Entities.Employee objEmployees = new Entities.Employee();

            strProjectName = lblProjectCode.Text;

            FullPath = Server.MapPath("./Mails/MeetingRevised.txt");
            objReader = File.OpenText(FullPath);
            strBodyContents = objReader.ReadToEnd();
            objReader.Close();


            strFrom = pstrFrom + "<" + pstrFromEmailId + ">";
            strTo = ToMembers;
            string strCc = CcMembers;
            strBody = strBodyContents.Replace("<<FirstLine>>", strLine1);
            strBody = strBody.Replace("<<SecondLine>>", strLine2);
            strBody = strBody.Replace("<<Informer>>", pstrFrom);
            strBody = strBody.Replace("<<InformerEmail>>", "<A HREF=mailto:" + pstrFromEmailId.Trim() + ">" + pstrFromEmailId.Trim() + "</A>");

            try
            {
                if (!string.IsNullOrEmpty(strAttachment))
                {
                    strAttachment = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["GetAttachmentPath"].ToString() + "/" + strAttachment);
                }
                if (!string.IsNullOrEmpty(strTo))
                {
                    //if (SendDynamicMail(strFrom, strTo, strCc, strsubject, strAttachment, strBody, "HTML") == false)
                    //{
                    //    LblException.Text = "Recap recorded! Mail not sent due to invalid Email Address";
                    //}
                    //else
                    //{
                    //    LblException.Text = "Recap sent successfully.";
                    //}
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void imgRemove_Click(object sender, ImageClickEventArgs e)
        {
            bool res;
            objMeeting.MeetingId = intMeetingId;
            res = new MeetingBAL().UpdateMeetingAttachment(objMeeting);
            string strFileName = null;
            strFileName = hlFile.Text;
            File.Delete(Server.MapPath("./Mails/Attachments/" + strFileName));
            lblAttacment.Visible = false;
            hlFile.Visible = false;
            imgRemove.Visible = false;
            Session["FileName"] = "";
            Session["NavigateUrl"] = "";
        }
    }
}