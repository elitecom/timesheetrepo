﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Entities;
using DrishProjectBAL;
using System.Data;
using System.Data.SqlClient;

namespace DrishProject.ProjectMeetings
{
    public partial class Meeting_Add : System.Web.UI.Page
    {
        int ProjectId;
        Entities.Employee objEmployee = new Entities.Employee();
        int MeetingId;
        DataSet ds = new DataSet();

        protected void Page_Load(object sender, EventArgs e)
        {
            string  ctr = string.Empty;;
            ctr = Request.QueryString["MeetingId"].ToString();
            if (!String.IsNullOrEmpty(ctr))
            {
                MeetingId = Convert.ToInt32(ctr);
            }
            Project objProject = new Project();
            ProjectId = Convert.ToInt32(Session["ProjectId"]);
            objProject.ProjectID = ProjectId;
            objProject = new ProjectBAL().GetProject(ProjectId);
            
            Entities.Employee objUser = new Entities.Employee();
            objEmployee = (Entities.Employee)Session["emp"];

            lblProjectCode.Text = objProject.ProjectCode;

            try
            {
                if (!Page.IsPostBack)
                {
                    Meeting ms = new Meeting();
                    DataSet ds = new DataSet();
                    //ds = new MeetingBAL().GetMeetingId(ProjectId);
                    //if (ds.Tables[0].Rows.Count > 0)
                    //{
                       // MeetingId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                     Session["MeetindId"] = MeetingId;
                    //}
                    //Session["MeetingId"] = Request["MeetingId"];
                    //MeetingId = Convert.ToInt32(Session["MeetingId"]);
                    //intMeetingId =Convert.ToInt32(Request.QueryString["MeetingId"]);
                    lblMeetingId.Text = MeetingId.ToString();
                    GetEmpList();
                    getVenue();
                    GetStatusList();
                    getMedia();
                    MakeTimeList();
                    GetTopImage();
                    if (MeetingId > 0)
                    {
                        imgAddNew.Visible = false;
                        GetMeetingInfo(MeetingId);
                        //if (IfAccessible(MeetingId, ProjectId, "Meeting") == false)
                        //{
                        //    Response.Redirect("Default2.aspx?Authority=Deny");
                        //}
                    }
                    txtSubject.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    ddlEmp.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    txtdate.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    ddlHr.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    ddlMin.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    ddlAMPM.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    txtduration.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    ddldur.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    ddlMedia.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                    lstAttendies.Attributes.Add("OnKeyPress", " return EnterVal(event);");
                }
                imgBtnSave.Attributes.Add("OnClick", "return ChangeCursor(this);");
                imgBtnSave.Attributes.Add("OnClick", "return check();");
            }
            catch (Exception ex)
            {
                Response.Write("Message : " + ex.Message);
                Response.Write(ex.StackTrace);
                throw (ex);
            }
        }

        protected void imgBtnSave_Click(object sender, ImageClickEventArgs e)
        {
            ListItem lstItem;
            string strSubject = "";
            int intTimehr = 0;
            int intTimeMin = 0;
            string strTimeAmPm = "";
            string strEmpCode = "";
            string strRetVal = "";
            DateTime dtDate = System.DateTime.Now;
            int intVenueId = 0;
            string strDuration = "";
            string strTime = "";
            int intMediaId = 0;
            int intEmpId = 0;
            int intStatusId = 0;
            Meeting objMeeting = new Meeting();
            int i;

            MeetingId = Convert.ToInt32(lblMeetingId.Text);

            if (Convert.ToInt32(ddlStatus.SelectedItem.Value) == 0)
            {
                LblException.Text = "Status of the meeting required !";
                SetFocus(ddlStatus);
                return;
            }
            else
            {
                intStatusId = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                LblException.Text = "";
            }

            if (Convert.ToInt32(ddlVenue.SelectedItem.Value) == 0)
            {
                LblException.Text = "Venue of the Meeting required !";
                SetFocus(ddlVenue);
                return;
            }
            else
            {
                intVenueId = Convert.ToInt32(ddlVenue.SelectedItem.Value);
                LblException.Text = "";
            }

            if (txtSubject.Text == "")
            {
                LblException.Text = "Subject of the meeting required !";
                SetFocus(txtSubject);
                return;
            }
            else
            {
                if (txtSubject.Text.Length > 500)
                {
                    LblException.Text = "Subject can not be more than 500 characters !";
                    SetFocus(txtSubject);
                    return;
                }
                LblException.Text = "";
            }
            if ((txtSubject.Text) != "")
            {
                strSubject = txtSubject.Text.TrimStart();
            }
            if (Convert.ToInt32(ddlEmp.SelectedItem.Value) == 0)
            {
                LblException.Text = "Host Name Required !";
                SetFocus(ddlEmp);
                return;
            }
            else
            {
                LblException.Text = "";
            }
            if (txtdate.Text == "")
            {
                LblException.Text = "Date required !";
                SetFocus(txtdate);
                return;
            }
            else
            {
                LblException.Text = "";
            }
            if (txtdate.Text != "")
            {
                dtDate = Convert.ToDateTime(txtdate.Text.TrimStart());
            }
            if (ddlHr.SelectedItem.Value == null || ddlMin.SelectedItem.Value == null || ddlAMPM.SelectedItem.Value == null)
            {
                LblException.Text = "Time required !";
                SetFocus(ddlHr);
                return;
            }
            else
            {
                LblException.Text = "";
            }

            if (Convert.ToInt32(ddlHr.SelectedValue) > 0)
            {
                intTimehr = System.Convert.ToInt32(ddlHr.SelectedItem.Text);
            }

            if (Convert.ToInt32(ddlMin.SelectedValue) > 0)
            {
                intTimeMin = System.Convert.ToInt32(ddlMin.SelectedItem.Text);
            }

            if (Convert.ToInt32(ddlAMPM.SelectedValue) > 0)
            {
                strTimeAmPm = ddlAMPM.SelectedItem.Text;
            }

            if ((txtduration.Text) != "")
            {
                strDuration = txtduration.Text.TrimStart() + " " + (ddldur.SelectedItem.ToString());
            }
            if (Convert.ToInt32(ddlMedia.SelectedValue) > 0)
            {
                intMediaId = Convert.ToInt32(ddlMedia.SelectedItem.Value);
            }
            int count = 0;
            for (i = 0; i <= lstAttendies.Items.Count - 1; i++)
            {
                if (lstAttendies.Items[i].Selected == true)
                {
                    count++;
                }
            }
            if (count == 0)
            {
                LblException.Text = "Select Attendee";
                return;
            }
            else
            {
                LblException.Text = "";
            }
            try
            {
                if (MeetingId == 0) //add new meeting
                {
                    intEmpId = Convert.ToInt32(ddlEmp.SelectedValue);
                    objMeeting.MeetingId = MeetingId;
                    objMeeting.ProjectId = ProjectId;
                    objMeeting.MeetingDate = Convert.ToDateTime(dtDate).ToLongDateString();
                    objMeeting.MeetingHr = Convert.ToString(intTimehr);
                    objMeeting.MeetingMin = Convert.ToString(intTimeMin);
                    objMeeting.MeetingAmPm = strTimeAmPm;
                    objMeeting.Duration = strDuration;
                    objMeeting.MediaId = intMediaId;
                    objMeeting.Subject = strSubject;
                    objMeeting.CalledBy = intEmpId;
                    objMeeting.Recap = "";
                    objMeeting.MeetingStatusId = Convert.ToInt32(ddlStatus.SelectedValue.ToString());
                    objMeeting.To = lstAttendies.SelectedValue.ToString();
                    objMeeting.Cc = lstAttendies.SelectedValue.ToString();
                    objMeeting.Attachment = "";
                    
                    new DrishProjectBAL.MeetingBAL().InsertMeeting(objMeeting).ToString();
                    //strRetVal = new DrishProjectBAL.MeetingBAL().GetMaxMeetingID();
                    //if (strRetVal != "")
                    //{
                    //    lblMeetingId.Text = strRetVal;
                    //    MeetingId = System.Convert.ToInt32(lblMeetingId.Text);
                    //}
                    ////Insert Attendies of the meeting
                    //objMeeting.MeetingId = MeetingId;
                    //new DrishProjectBAL.MeetingBAL().DeleteMembers(objMeeting);
                    ////objMeeting.DeleteMembers();
                    //if (lstAttendies.Items.Count > 0)
                    //{
                    //    foreach (ListItem tempLoopVar_lstItem in lstAttendies.Items)
                    //    {
                    //        lstItem = tempLoopVar_lstItem;
                    //        if (lstItem.Selected == true)
                    //        {
                    //            objMeeting.MeetingId = MeetingId;
                    //            objMeeting.MemberId = Convert.ToInt32(lstItem.Value);
                    //            new DrishProjectBAL.MeetingBAL().InsertMembers(objMeeting);
                    //        }
                    //    }
                    //}
                    //if (Convert.ToInt32(ddlStatus.SelectedValue) == 1 || Convert.ToInt32(ddlStatus.SelectedValue) == 3)
                    //{
                    //    //SendMail(intMeetingId, dtDate, ddlHr.SelectedItem.Text.ToString(), ddlMin.SelectedItem.Text.ToString(), strTimeAmPm, ddlVenue.SelectedItem.Text.ToString(), ddlStatus.SelectedValue);
                    //}
                    //if (Convert.ToInt32(ddlStatus.SelectedValue) == 2)
                    //{
                    //    LblException.Text = "Meeting added sucessfully, please send the recap positively";
                    //}

                    GetTopImage();
                }
                else //Edit the meeting
                {
                    intEmpId = Convert.ToInt32(ddlEmp.SelectedValue);
                    objMeeting.MeetingId = MeetingId;
                    objMeeting.MeetingDate = Convert.ToDateTime(dtDate).ToShortDateString();
                    objMeeting.MeetingHr = Convert.ToString(intTimehr);
                    objMeeting.MeetingMin = Convert.ToString(intTimeMin);
                    objMeeting.MeetingAmPm = strTimeAmPm;
                    objMeeting.Duration = strDuration;
                    objMeeting.MediaId = intMediaId;
                    objMeeting.Subject = strSubject;
                    objMeeting.CalledBy = intEmpId;
                    objMeeting.MeetingStatusId = intStatusId;
                    //strRetVal = objMeeting.UpdateMeeting().ToString();
                    new DrishProjectBAL.MeetingBAL().UpdateMeeting(objMeeting);

                    LblException.Text = "Meeting updated successfully";
                    if (Convert.ToInt32(ddlStatus.SelectedValue) == 2)
                    {
                        LblException.Text = "Meeting updated successfully, please send the recap positively";
                    }
                    //Insert Attendies of the meeting
                    objMeeting.MeetingId = MeetingId;
                    new DrishProjectBAL.MeetingBAL().DeleteMembers(objMeeting);
                    //objMeeting.DeleteMembers();
                    if (lstAttendies.Items.Count > 0)
                    {
                        foreach (ListItem tempLoopVar_lstItem in lstAttendies.Items)
                        {
                            lstItem = tempLoopVar_lstItem;
                            if (lstItem.Selected == true)
                            {
                                objMeeting.MeetingId = MeetingId;
                                objMeeting.MemberId = Convert.ToInt32(lstItem.Value);
                                //objMeeting.InsertMembers();
                                new DrishProjectBAL.MeetingBAL().InsertMembers(objMeeting);
                            }
                        }
                    }
                    //if ((Convert.ToInt32(ddlStatus.SelectedValue) == 1 && this.txtMeetingStatus.Text != ddlStatus.SelectedValue) || (ddlStatus.SelectedValue == 3 && this.txtMeetingStatus.Text != ddlStatus.SelectedValue))
                    //{
                    //    //SendMail(intMeetingId, dtDate, ddlHr.SelectedItem.Text.ToString(), ddlMin.SelectedItem.Text.ToString(), strTimeAmPm, ddlVenue.SelectedItem.Text.ToString(), ddlStatus.SelectedValue);
                    //}
                    this.txtMeetingStatus.Text = ddlStatus.SelectedValue;
                }
                GetTopImage();
                RegisterStartupScript("strRefresh", "<script language=\'javascript\'>window.opener.PageRefresh();</script>");
            }
            catch (Exception ex)
            {
                LblException.Text = ex.Message;
            }
        }

        protected void imgAddNew_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Meeting_Add.aspx?MeetingId = '0'");
        }



        private void GetEmpList()
        {
            DataSet dS2 = new DataSet();                
            //Get Employee list for Hosted By
            try
            {
                dS2 = new EmployeeBAL().GetEmployeeList();
                //DataRow dRow;
                //dRow = DS2.Tables[0].NewRow();
                //dRow["empid"] = 0;
                //dRow["username"] = "--Select--";
                //DS2.Tables[0].Rows.InsertAt(dRow, 0);
                ddlEmp.DataSource = dS2;
                ddlEmp.DataTextField = "username";
                ddlEmp.DataValueField = "empid";
                ddlEmp.DataBind();
                //ddlEmp.SelectedValue = 0;
                //Get Employee list for Attendies
                dS2 = null;
                dS2 = new EmployeeBAL().GetEmployeeList();
                lstAttendies.DataSource = dS2;
                lstAttendies.DataValueField = dS2.Tables[0].Columns[0].ToString();
                lstAttendies.DataTextField = dS2.Tables[0].Columns[5].ToString();
                lstAttendies.DataBind();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private void GetStatusList()
        {
            DataSet ds = new DataSet();
            ds = new DrishProjectBAL.MeetingBAL().GetMeetingStatus();
            ddlStatus.DataSource = ds.Tables[0];
            ddlStatus.DataTextField = "Status";
            ddlStatus.DataValueField = "StatusID";
            ddlStatus.DataBind();
            ddlStatus.Items.Insert(0, "== Select ==");            
        }

        public void getVenue()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = new DataSet();

                ds = new MeetingBAL().GetMeetingVenue();
                ddlVenue.DataSource = ds;
                ddlVenue.DataTextField = "venue";
                ddlVenue.DataValueField = "VenueId";
                ddlVenue.DataBind();
                ddlVenue.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }	

        private void getMedia()
        {
            Meeting objMeeting = new Meeting();
            try
            {
                DataSet DS = new DataSet();
                DS = new DrishProjectBAL.MeetingBAL().GetMediaList();
                DataRow dRow;
                dRow = DS.Tables[0].NewRow();
                dRow["mediaid"] = 0;
                dRow["media"] = "--Select--";
                DS.Tables[0].Rows.InsertAt(dRow, 0);
                ddlMedia.DataSource = DS;
                ddlMedia.DataTextField = "media";
                ddlMedia.DataValueField = "mediaid";
                ddlMedia.DataBind();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private void MakeTimeList()
        {
            ListItem lItem;
            int i;

            lItem = new ListItem();
            lItem.Text = "-hh-";
            lItem.Value = "0";
            ddlHr.Items.Add(lItem);
            for (i = 0; i <= 12; i++)
            {
                lItem = new ListItem();
                lItem.Text = i < 10 ? ("0" + i.ToString().Trim()) : (i.ToString());
                lItem.Value = (i + 1).ToString();
                ddlHr.Items.Add(lItem);
            }

            lItem = new ListItem();
            lItem.Text = "-mm-";
            lItem.Value = "0";

            ddlMin.Items.Add(lItem);
            for (i = 0; i <= 59; i++)
            {
                lItem = new ListItem();
                lItem.Text = i < 10 ? ("0" + i.ToString().Trim()) : (i.ToString());
                lItem.Value = (i + 1).ToString();
                ddlMin.Items.Add(lItem);
            }

            lItem = new ListItem();
            lItem.Text = "---";
            lItem.Value = "0";
            ddlAMPM.Items.Add(lItem);

            lItem = new ListItem();
            lItem.Text = "AM";
            lItem.Value = "1";
            ddlAMPM.Items.Add(lItem);

            lItem = new ListItem();
            lItem.Text = "PM";
            lItem.Value = "2";
            ddlAMPM.Items.Add(lItem);

        }

        private void GetMeetingInfo(int pintMeetingId)
        {
            Entities.Meeting  objMeeting = new Entities.Meeting();

            int i;
            string holdstr;

            if (pintMeetingId > 0)
            {
                objMeeting.MeetingId = pintMeetingId;
                //objMeeting.GetMeeting();
                objMeeting = new MeetingBAL().GetMeeting(objMeeting);

                ddlStatus.SelectedValue = (objMeeting.MeetingStatusId).ToString();
                this.txtMeetingStatus.Text = (objMeeting.MeetingStatusId).ToString();
                txtdate.Text = objMeeting.MeetingDate;
                ddlEmp.SelectedValue = (objMeeting.CalledBy).ToString();
                ViewState["OldMeetingDate"] = objMeeting.MeetingDate;
                txtSubject.Text = objMeeting.Subject;
                ViewState["OldMeetingHr"] = objMeeting.MeetingHr;
                for (i = 0; i <= ddlHr.Items.Count - 1; i++)
                {
                    ddlHr.SelectedIndex = i;
                    if (ddlHr.SelectedItem.Text == objMeeting.MeetingHr)
                    {
                        break;
                    }
                    else
                    {
                        ddlHr.SelectedIndex = 0;
                    }
                }
                ViewState["OldMeetingMin"] = objMeeting.MeetingMin;
                holdstr = objMeeting.MeetingMin;
                for (i = 0; i <= ddlMin.Items.Count - 1; i++)
                {
                    ddlMin.SelectedIndex = i;
                    if (ddlMin.SelectedItem.Text == objMeeting.MeetingMin)
                    {
                        break;
                    }
                    else
                    {
                        ddlMin.SelectedIndex = 0;
                    }
                }
                ViewState["OldMeetingAmPm"] = objMeeting.MeetingAmPm;
                for (i = 0; i <= ddlAMPM.Items.Count - 1; i++)
                {
                    ddlAMPM.SelectedIndex = i;
                    if (ddlAMPM.SelectedItem.Text == objMeeting.MeetingAmPm)
                    {
                        break;
                    }
                    else
                    {
                        ddlAMPM.SelectedIndex = 0;
                    }
                }

                ddlVenue.SelectedValue = (objMeeting.VenueId).ToString();
                string str;
                str = objMeeting.Duration;

                txtduration.Text = str;
                // txtDuration.Text = objMeeting.Duration

                ddlEmp.SelectedValue = (objMeeting.CalledBy).ToString();
                ddlMedia.SelectedValue = (objMeeting.MediaId).ToString();
                //Select attendies of the meeting.
                DataSet DS1 = new DataSet();
                objMeeting.MeetingId = MeetingId;
                DS1 = new MeetingBAL().GetMemberListByMeetingID(MeetingId);
                ListBox lstFly = new ListBox();
                lstFly.DataSource = DS1;
                lstFly.DataBind();


                if (DS1.Tables[0].Rows.Count > 0)
                {
                    if (lstAttendies.Items.Count > 0)
                    {
                        if (lstFly.Items.Count > 0)
                        {
                            foreach (DataRow lstItem2 in DS1.Tables[0].Rows)
                            {
                                foreach (ListItem lstItem3 in lstAttendies.Items)
                                {
                                    string tt = Convert.ToString(lstItem2[0]);
                                    string tt2 = Convert.ToString(lstItem3.Value.ToString());

                                    //lstItem3.Value.ToString(),Convert.ToString(lstItem2[0].ToString())))
                                    if (String.Equals(tt2,tt))                                        
                                    {
                                        lstItem3.Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            SetFocus(ddlStatus);
        }

        private void GetTopImage()
        {
            MeetingId = System.Convert.ToInt32(lblMeetingId.Text);
            if (MeetingId > 0)
            {
                RegisterStartupScript("strShow", "<script language=\'javascript\'>this.document.all.imgTop.src=\'images/edit_meeting_tab.gif\';</script>");
            }
            else
            {
                RegisterStartupScript("strShow", "<script language=\'javascript\'>this.document.all.imgTop.src=\'Images/add_meeting_tab.gif\';</script>");
            }
        }

    }
}