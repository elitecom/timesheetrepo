﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectBAL;
using System.Web.UI.HtmlControls;

namespace DrishProject.ProjectMeetings
{
    public partial class Meetings : System.Web.UI.Page
    {
        int ProjectId;
        int MeetingId;
        bool bln_Delete;
        const int DelColIndex = 8;
        const int EditColIndex = 6;
        Project objProject = new Project();
        Meeting meeting = new Meeting();

        protected void Page_Load(object sender, EventArgs e)
        {

            bln_Delete = false;
            Entities.Employee objUser = new Entities.Employee();
            objUser = (Entities.Employee)Session["emp"];

            if (objUser.IsAministrator == 0)
            {
                //System.Web.UI.HtmlControls.HtmlTableCell tdFLY;
                //tdFLY = (HtmlTableCell)FindControl("tdPTimeSheet");
                //tdFLY.Visible = false;
                /*dsc.RoleAuthenticate();
                if (User.IsInRole("AddMeetings"))
                {
                    RegisterStartupScript("strShow", "<script language=\'javascript\'>this.document.all.imgAddMeeting.style.display=\'block\';</script>");
                }
                else
                {
                    RegisterStartupScript("strHide", "<script language=\'javascript\'>this.document.all.imgAddMeeting.style.display=\'none\';</script>");
                }*/
            }

            ProjectId = Convert.ToInt32(Session["ProjectID"]);
            Meeting ms = new Meeting();
            //Code to hide the Release, CheckList, Client and Code Review Tabs for Non Engineering Projects.


            DataSet ds = new DataSet();
            ds = new DrishProjectBAL.MeetingBAL().GetMeetingId(ProjectId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                MeetingId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                Session["MeetindId"] = MeetingId;
            }

            objProject.ProjectID = ProjectId;
            objProject = new DrishProjectBAL.ProjectBAL().GetProject(ProjectId);

            if (objProject.ProjectCategory != "ENGG")
            {
                //System.Web.UI.HtmlControls.HtmlTableCell tdFLY;
                //tdFLY = (HtmlTableCell)FindControl("tdRelease");
                //tdFLY.Visible = false;
                //tdFLY = (HtmlTableCell)FindControl("tdClients");
                //tdFLY.Visible = false;
                //tdFLY = (HtmlTableCell)FindControl("tdCheckList");
                //tdFLY.Visible = false;
                //tdFLY = (HtmlTableCell)FindControl("tdCodeReview");
                //tdFLY.Visible = false;
            }
            lblProjectCode.Text = objProject.ProjectCode;
            lblStartdate.Text = objProject.ProjectStartDate.ToShortDateString();
            lblEndDate.Text = objProject.ProjectEndDate.ToShortDateString();
            LblException.Text = "";
            if (!Page.IsPostBack)
            {
                //this.SortCriteria = "DateTime";
                //this.sortdir = "DESC";
                try
                {
                    meeting.ProjectId = ProjectId;
                    ds = new DrishProjectBAL.MeetingBAL().GetMeetingsPerProject(meeting);
                    GetMeetingsPerProject();

                    //To make the header image of the project active
                    //ImageButton imgHeader = new ImageButton();
                    //imgHeader =(ImageButton) Page.FindControl("UC_Header").FindControl("imgProjects");
                    //imgHeader.ImageUrl = "..\\Images\\Project_Active.gif";
                }
                catch (Exception ex)
                {
                    LblException.Text = ex.Message;
                }
            }
        }

        public string GetUrl(string intMeetingStatus, string intMeetingID)
        {
            string strRetVal;

            if (intMeetingStatus.Trim() == "2")
            {
                strRetVal = "<img id=\'imageDetail\' align=\'middle\' alt=\'Compose Recap\' src=\'../images/recap_active_ico.gif\' STYLE=\'CURSOR:Pointer\' onclick=\'MeetingDetailFunction(" + Convert.ToInt32(intMeetingID) + ")\'>";
            }
            else
            {
                strRetVal = "<img id=\'imageDetail\' align=\'middle\' src=\'../images/recap_unactive_ico.gif\'>";
            }
            return strRetVal;
        }

        private void GetMeetingsPerProject()
        {
            Meeting objMeeting = new Meeting();

            Entities.Employee objUser = new Entities.Employee();
            if (objUser.IsAministrator == 0)
            {
                if (User.IsInRole("EditMeetings"))
                {
                    dgMeetings.Columns[EditColIndex].Visible = true;
                }
                else
                {
                    dgMeetings.Columns[EditColIndex].Visible = false;
                }
            }
            else
            {
                dgMeetings.Columns[EditColIndex].Visible = true;
            }

            if (objUser.IsAministrator == 0)
            {
                if (User.IsInRole("DeleteMeetings"))
                {
                    dgMeetings.Columns[DelColIndex].Visible = true;
                }
                else
                {
                    dgMeetings.Columns[DelColIndex].Visible = false;
                }
            }
            else
            {
                dgMeetings.Columns[DelColIndex].Visible = true;
            }
            try
            {
                DataSet ds = new DataSet();
                objMeeting.ProjectId = ProjectId;
                //objMeeting.SortCriteria = this.SortCriteria;
                //objMeeting.SortDir = this.sortdir;
                //ds = objMeeting.GetMeetingsPerProject(ProjectId);
                ds = new DrishProjectBAL.MeetingBAL().GetMeetingsPerProject(objMeeting);
                dgMeetings.DataSource = ds;
                //Code to check whether the CurrentPageIndex value is not less than 0
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (Math.Ceiling(Double.Parse(ds.Tables[0].Rows.Count.ToString()) / dgMeetings.PageSize) - 1 < dgMeetings.PageSize)
                    {
                        dgMeetings.CurrentPageIndex = 0;
                    }
                }
                else
                {
                    dgMeetings.CurrentPageIndex = 0;
                }
                dgMeetings.DataBind();
                if (ds.Tables[0].Rows.Count == 0)
                {
                    dgMeetings.PagerStyle.Visible = false;
                    LblException.Text = "No Meetings Present.";
                }
                else
                {
                    dgMeetings.PagerStyle.Visible = true;
                    LblException.Text = "";
                }
                if (ds.Tables[0].Rows.Count <= dgMeetings.PageSize)
                {
                    dgMeetings.PagerStyle.Visible = false;
                }
                else
                {
                    dgMeetings.PagerStyle.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }       

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Projects/ProjectDetail.aspx");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TeamMembers/ProjectTeamMembers.aspx");
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectModules/Module.aspx");
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Tasks/MyTasks.aspx");
        }

        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Documents/Documents.aspx");
        }

        

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Clients/Clients.aspx");
        }

        protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectMeetings/Meetings.aspx");
        }

        protected void ImageButton11_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectNotes/Notes.aspx");
        }

        protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectHistory/History.aspx");
        }

        protected void ImageButton10_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectRelease/Release.aspx");
        }

        protected void ImageButton13_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectReview/Reviews.aspx");
        }

        protected void ImageButton14_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TimeSheets/TimeSheet.aspx");
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            GetMeetingsPerProject();
        }

        protected void dgMeetings_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            bool res;
            if (bln_Delete == false)
            {
                bln_Delete = true;
                int intMeetingid;                
                string strRetVal;
                Meeting objMeeting = new Meeting();
                intMeetingid = 0;
                strRetVal = "";
                intMeetingid = Convert.ToInt32(dgMeetings.DataKeys[e.Item.ItemIndex]);
                objMeeting.MeetingId = intMeetingid;
                res = new MeetingBAL().DeleteMeeting(objMeeting);
                GetMeetingsPerProject();
                dgMeetings.EditItemIndex = -1;
                if (res == true)
                {
                    strRetVal = "Deleted Successfully !";
                }               
                LblException.Text = strRetVal;
            }
        }

        protected void dgMeetings_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgMeetings.CurrentPageIndex = e.NewPageIndex;
            GetMeetingsPerProject();
        }

        protected void dgMeetings_SortCommand(object source, DataGridSortCommandEventArgs e)
        {           
            GetMeetingsPerProject();
        }

        protected void dgMeetings_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            ImageButton imgbtn;
            imgbtn = (ImageButton)e.Item.FindControl("btnDel");
            if (imgbtn != null)
            {
                imgbtn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");
            }

            if (e.Item.ItemType != ListItemType.Header)
            {
                RegisterStartupScript("Declare", "<script language=\'javascript\'>var realColor;</script>");
                e.Item.Attributes.Add("OnMouseOver", "realColor = this.style.backgroundColor; this.style.backgroundColor = \'#cccccc\';");
                e.Item.Attributes.Add("OnMouseOut", "this.style.backgroundColor = realColor;");
            }
        }
    }
}