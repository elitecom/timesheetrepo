﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Project_Meeting.aspx.cs" Inherits="DrishProject.ProjectMeetings.Project_Meeting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />    
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table class="Gridtxt" id="tblMain" height="50%" cellSpacing="0" cellPadding="0" width="50%"
				border="0">
				<tr height="10%">
					<td width="100%">
						&nbsp;</td>
				</tr>
				<tr height="1%">
					<td class="Gridtxt"></td>
				</tr>
				<tr class="Gridtxt" height="80%">
					<td class="Gridtxt" vAlign="top" width="100%">
						<!-- CENTER -->
						<TABLE id="tblCentral" height="100%" cellSpacing="0" cellPadding="0" align="center"
							border="0">
							<TR align="left">
								<td width="1%"></td>
								<TD vAlign="top" align="left" height="100%">
									<table cellSpacing="0" cellPadding="0" border="0">
										<tr>
											<TD><A href="Meetings.aspx"><IMG src="../images/tabs_bt_10.gif" border="0"></A></TD>
											<TD><IMG src="../images/recap_bt.gif" border="0"></TD>
										</tr>
									</table>
									<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0" 
                                        style="height: 88%">
										<TBODY align="center">
											<TR class="center-bg">
												<TD vAlign="top" align="left" width="96%"><IMG height="10" src="../images/spacer.gif" width="10"></TD>
												<TD vAlign="top" align="right" width="3%"><IMG height="10" src="../images/right_top_corner.gif" width="10"></TD>
											</TR>
											<TR class="center-bg" vAlign="top" align="center">
												<TD vAlign="top" align="center" colSpan="2">
													<table width="100%" align="center" border="0">
														<TR class="center-bg" vAlign="top" align="left">
															<td vAlign="top" align="left" colSpan="3"><asp:label id="LblException" runat="server" CssClass="Errortxt"></asp:label></td>
														</TR>
														<tr class="center-bg" vAlign="top">
															<td align="left"><strong>Project&nbsp;Code:&nbsp;</strong></td>
															<td align="left"><asp:label id="lblProjectCode" CssClass="Gridtxt" Runat="server"></asp:label></td>
															<td width="6%">&nbsp;</td>
															<td align="left"><strong>Meeting Status:&nbsp;</strong></td>
															<td align="left"><asp:label id="lblStatus" CssClass="Gridtxt" Runat="server"></asp:label></td>
															<td>&nbsp;</td>
															<td align="left"><strong>Hosted By:&nbsp;</strong></td>
															<td align="left"><asp:label id="lblHosted" CssClass="Gridtxt" Runat="server"></asp:label></td>
														</tr>
														<tr class="center-bg" vAlign="top">
															<td align="left"><strong>Date:&nbsp;</strong></td>
															<td align="left"><asp:label id="lblDate" CssClass="Gridtxt" Runat="server"></asp:label></td>
															<td width="6%">&nbsp;</td>
															<td align="left"><strong>Time:&nbsp;</strong></td>
															<td align="left"><asp:label id="lblTime" CssClass="Gridtxt" Runat="server"></asp:label></td>
															<td>&nbsp;</td>
															<td align="left"><strong>Duration:&nbsp;</strong></td>
															<td align="left"><asp:label id="lblDuration" CssClass="Gridtxt" Runat="server"></asp:label></td>
															<td width="6%">&nbsp;</td>
															<td align="left"><strong>Media:&nbsp;</strong><asp:label id="lblMedia" CssClass="Gridtxt" Runat="server"></asp:label></td>
														</tr>
													</table>
												</TD>
												<TD><IMG src="../images/spacer.gif"></TD>
											</TR>
											<tr class="center-bg" height="10">
												<td vAlign="top" align="center" colSpan="2"><IMG height="10" src="../images/spacer.gif"></td>
											</tr>
											<TR class="center-bg" vAlign="top" align="center">
												<TD vAlign="top" align="center" colSpan="1" rowSpan="1">
													<table id="Meetings" width="100%" border="0">
														<tr class="center-bg">
															<td width="5%"><strong>To:</strong>
															</td>
															<td width="90%"><asp:textbox id="txtTO" Runat="server" width="99%" TextMode="MultiLine"></asp:textbox>
																<DIV id="DIVEmail" style="DISPLAY: none; Z-INDEX: 1500; WIDTH: 346px; POSITION: absolute; HEIGHT: 136px">
																	<TABLE class="DIVtablestyle" height="100%" cellSpacing="0" cellPadding="0" width="100%"
																		bgColor="#ffffff" border="0">
																		<TR>
																			<TD vAlign="top" height="100%">
																				<TABLE class="DIVtablestyle" height="100%" cellSpacing="1" cellPadding="0" width="100%"
																					border="1">
																					<TR>
																						<TD class="txtDiv" style="HEIGHT: 20px" align="left" height="20"><B>Email 
																								Addresses(double click)</B></TD>
																						<TD title="Close" style="CURSOR: pointer" onclick="HideDiv();" align="center"><STRONG style="FONT-SIZE: 12px; COLOR: #ff9966">X</STRONG></TD>
																					</TR>
																					<TR>
																						<TD align="center" width="100%" colSpan="2"><INPUT id="optTeam" onclick="GetTeamList(this);" type="radio" CHECKED name="grpEmail">Team 
																							Members&nbsp; <INPUT id="optEmp" onclick="GetEmpList(this);" type="radio" name="grpEmail">All 
																							Employees
																						</TD>
																					</TR>
																					<TR>
																						<TD style="HEIGHT: 143px" align="center" colSpan="2"><asp:listbox id="lstTeam" ondblclick="addEmailAddress(this);" runat="server" Width="100%" Height="150px"></asp:listbox><asp:listbox id="lstEmp" ondblclick="addEmailAddress(this);" runat="server" Width="100%" Height="150px"></asp:listbox></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																	</TABLE>
																</DIV>
															</td>
															<td width="5%"><IMG id="To" runat="server" style="CURSOR: pointer" onclick="ShowDiv(this);" title="Email List" alt="Email List"
																	src="../images/releasefeatures_icon.gif"></td>
														</tr>
														<tr class="center-bg">
															<td width="5%"><strong>Cc:</strong>
															</td>
															<td width="90%"><asp:textbox id="txtCC" Runat="server" width="99%" TextMode="MultiLine"></asp:textbox></td>
															<td width="5%"><IMG id="Cc" runat="server" style="CURSOR: pointer" onclick="ShowDiv(this);" title="Email List" alt="Email List"
																	src="../images/releasefeatures_icon.gif"></td>
														</tr>
														<tr class="center-bg">
															<td width="5%"><strong>Subject:</strong>
															</td>
															<td width="90%"><asp:textbox id="txtSubject" Runat="server" width="99%"></asp:textbox></td>
															<td width="5%"></td>
														</tr>
														<tr class="center-bg">
															<td width="5%">
																<asp:Label id="lblAttacment" runat="server" Font-Bold="True">Attachment: </asp:Label>
															</td>
															<td width="90%">
																<asp:HyperLink id="hlFile" runat="server"></asp:HyperLink>&nbsp;&nbsp;&nbsp;
																<asp:ImageButton id="imgRemove" runat="server" ImageUrl="../Images/cross.gif" 
                                                                    ToolTip="Remove Attachment" AlternateText="Remove Attachment" 
                                                                    onclick="imgRemove_Click"></asp:ImageButton></td>
															<td width="5%"></td>
														</tr>
														<tr class="center-bg">
															<td width="5%"><strong>Attach:</strong>
															</td>
															<td width="90%">
																<input id="AttachFile" type="file" size="99" name="AttachFile" runat="server"></td>
															<td width="5%"></td>
														</tr>
														<tr class="center-bg">
															<td width="5%" colSpan="2">
																<script>
																    var obj1 = new ACEditor("obj1")
																    obj1.width = "100%"
																    obj1.height = 300
																    obj1.useStyle = false
																    obj1.useAsset = false
																    obj1.useImage = false
																    obj1.usePageProperties = false
																    obj1.RUN() 
																</script>
																<asp:textbox id="txtMessage" runat="server" Width="0" Height="0"></asp:textbox></td>
															<td width="90%"></td>
															<td width="5%"></td>
														</tr>
													</table>
												</TD>
												<TD height="100"><IMG height="10" src="../images/spacer.gif" width="1"></TD>
											</TR>
											<tr class="center-bg" height="10%">
												<td align="center"><asp:imagebutton id="imgSave" ToolTip="Save" 
                                                        AlternateText="Save" runat="server" ImageUrl="../Images/save_bt.gif" 
                                                        onclick="imgSave_Click"></asp:imagebutton>&nbsp;&nbsp;
													<asp:imagebutton  ToolTip="Save and Send" AlternateText="Save and Send" 
                                                        id="imgSend" runat="server" ImageUrl="../Images/send_bt.gif" 
                                                        onclick="imgSend_Click"></asp:imagebutton>&nbsp;&nbsp;
													<IMG alt="Cancel" title="Cancel" src="../images/canc_bt.gif" onclick="javascript : window.location.href = 'Meetings.aspx';"
														style="CURSOR:pointer"></td>
												<td></td>
											</tr>
											<TR class="center-bg">
												<TD vAlign="top" align="left"><IMG height="10" src="../images/spacer.gif" width="10"></TD>
												<TD vAlign="bottom" align="right"><IMG height="10" src="../images/right_bottom_corner.gif" width="10"></TD>
											</TR>
											<tr class="Gridtxt">
												<td colSpan="2" height="3%">&nbsp;</td>
											</tr>
										</TBODY>
									</table>
								</TD>
								<TD><IMG height="10" src="../images/spacer.gif" width="10"></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				
			</TABLE>
		<script language="javascript">
<!--
		    var htmlElement = document.getElementById('txtMessage');
		    obj1.putContent(htmlElement.value);
//-->
		</script>
    </div>
    </form>
</body>
</html>
