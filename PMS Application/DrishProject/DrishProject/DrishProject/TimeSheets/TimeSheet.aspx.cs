﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectBAL;

namespace DrishProject.TimeSheets
{
    public partial class TimeSheet : System.Web.UI.Page
    {
        Entities.Employee objUser = new Entities.Employee();
        const int ExpandColIndex = 6;
        int ProjectId;

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.MaintainScrollPositionOnPostBack = true;
            objUser = (Entities.Employee)Session["emp"];
            if (objUser.IsAministrator == 0)
            {
                //EmployeeAuthenticate;
                if (User.IsInRole("ProjectWorkPlan"))
                {
                    RegisterStartupScript("strShowWorkPlan", "<script language=\'javascript\'>this.document.all.imgWorkPlan.style.display=\'block\';</script>");
                }
                else
                {
                    RegisterStartupScript("strHideWorkPlan", "<script language=\'javascript\'>this.document.all.imgWorkPlan.style.display=\'none\';</script>");
                }
                if (User.IsInRole("AddResourceHours"))
                {
                    RegisterStartupScript("strAddRes", "<script language=\'javascript\'>this.document.all.imgAddRes.style.display=\'block\';</script>");
                }
                else
                {
                    RegisterStartupScript("strAddRes", "<script language=\'javascript\'>this.document.all.imgAddRes.style.display=\'none\';</script>");

                }
                RegisterStartupScript("strShowResDetail", "<script language=\'javascript\'>this.document.all.imgResAllDetail.style.display=\'none\';</script>");
            }
            else
            {
                RegisterStartupScript("strShowResDetail", "<script language=\'javascript\'>this.document.all.imgResAllDetail.style.display=\'block\';</script>");
            }

            if (!Page.IsPostBack)
            {
                try
                {
                    BindDropDownYear();
                    GetTimesheets();
                    //To make the Timesheet button Active
                    //ImageButton img = new ImageButton();
                    //img = (ImageButton)(Page.FindControl("UC_Header1").FindControl("imgTimesheets"));
                    //img.ImageUrl = "..\\Images\\timesheet_active.gif";
                }
                catch (Exception ex)
                {
                    LblException.Text = ex.Message;
                }
            }
            RegisterStartupScript("OnLoadFocus", "<script language = \'javascript\'>document.getElementById(\'txtProjectName\').focus();</script>");
            txtProjectName.Attributes.Add("OnKeyPress", " return EnterVal(event);");
        }

        private void BindDropDownYear() //Function to bind the dropdown with current year.
        {
            try
            {
                int iYear;
                iYear = DateTime.Today.Year;
                int iCounter;
                for (iCounter = 0; iCounter <= 5; iCounter++)
                {
                    ddlYear.Items.Add((iYear - iCounter).ToString());
                }
            }
            catch (Exception ex)
            {
                LblException.Text = ex.Message;
            }
        }

        private void GetTimesheets()
        {
            try
            {
                DataSet DS = new DataSet();
                if (objUser.IsAministrator == 1)
                {
                    //DS = objTimesheet.GetTimesheetV2();
                    DS = new DrishProjectBAL.DsrBAL().GetTimesheetV2();
                }
                else
                {
                    DS = new DrishProjectBAL.DsrBAL().GetTimesheetV2(objUser.EmployeeId);
                }
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        GrdTimesheet.DataSource = DS;
                        GrdTimesheet.DataBind();
                    }
                    else
                    {
                        DataTable dsEmpty = new DataTable();
                        GrdTimesheet.DataSource = dsEmpty;
                        GrdTimesheet.DataBind();
                        GrdTimesheet.CurrentPageIndex = 0;
                        GrdTimesheet.PagerStyle.Visible = false;
                        LblException.Text = "No Timesheet availiable.";
                    }
                }
                else
                {
                    DataTable dsEmpty = new DataTable();
                    GrdTimesheet.DataSource = dsEmpty;
                    GrdTimesheet.DataBind();
                    GrdTimesheet.CurrentPageIndex = 0;
                    GrdTimesheet.PagerStyle.Visible = false;
                    LblException.Text = "No Timesheet availiable.";
                }
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count == 0)
                    {
                        DataTable dsEmpty = new DataTable();
                        GrdTimesheet.DataSource = dsEmpty;
                        GrdTimesheet.DataBind();
                        GrdTimesheet.CurrentPageIndex = 0;
                        GrdTimesheet.PagerStyle.Visible = false;
                        LblException.Text = "No Timesheet availiable.";
                    }
                    else
                    {
                        GrdTimesheet.PagerStyle.Visible = true;
                        LblException.Text = "";
                    }
                }
                else
                {
                    DataTable dsEmpty = new DataTable();
                    GrdTimesheet.DataSource = dsEmpty;
                    GrdTimesheet.DataBind();
                    GrdTimesheet.CurrentPageIndex = 0;
                    GrdTimesheet.PagerStyle.Visible = false;
                    LblException.Text = "No Timesheet availiable.";
                }
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count <= GrdTimesheet.PageSize)
                    {
                        GrdTimesheet.PagerStyle.Visible = false;
                    }
                    else
                    {
                        GrdTimesheet.PagerStyle.Visible = true;
                    }
                }
                else
                {
                    DataTable dsEmpty = new DataTable();
                    GrdTimesheet.DataSource = dsEmpty;
                    GrdTimesheet.DataBind();
                    GrdTimesheet.CurrentPageIndex = 0;
                    GrdTimesheet.PagerStyle.Visible = false;
                    LblException.Text = "No Timesheet availiable.";
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        protected void imgBtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            GrdTimesheet.CurrentPageIndex = 0;
            GetSearchResults();  
        }

        private void GetSearchResults()
        {
            try
            {
                DataSet DS = new DataSet();
                if (objUser.IsAministrator == 1)
                {
                    DS = new DsrBAL().GetSearchedTimesheetsV2(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(ddlMonth.SelectedValue), txtProjectName.Text);
                }
                else
                {
                    DS = new DsrBAL().GetSearchedTimesheetV2(objUser.EmployeeId, Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(ddlMonth.SelectedValue), txtProjectName.Text);
                }
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        GrdTimesheet.DataSource = DS;
                        GrdTimesheet.DataBind();
                    }
                    else
                    {
                        DataTable dsEmpty = new DataTable();
                        GrdTimesheet.DataSource = dsEmpty;
                        GrdTimesheet.DataBind();
                        GrdTimesheet.CurrentPageIndex = 0;
                        GrdTimesheet.PagerStyle.Visible = false;
                        LblException.Text = "No Timesheet availiable.";
                    }
                }
                else
                {
                    DataTable dsEmpty = new DataTable();
                    GrdTimesheet.DataSource = dsEmpty;
                    GrdTimesheet.DataBind();
                    GrdTimesheet.CurrentPageIndex = 0;
                    GrdTimesheet.PagerStyle.Visible = false;
                    LblException.Text = "No Timesheet availiable.";
                }
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count == 0)
                    {
                        DataTable dsEmpty = new DataTable();
                        GrdTimesheet.DataSource = dsEmpty;
                        GrdTimesheet.DataBind();
                        GrdTimesheet.CurrentPageIndex = 0;
                        GrdTimesheet.PagerStyle.Visible = false;
                        LblException.Text = "No Timesheet availiable.";
                    }
                    else
                    {
                        GrdTimesheet.PagerStyle.Visible = true;
                        LblException.Text = "";
                    }
                }
                else
                {
                    DataTable dsEmpty = new DataTable();
                    GrdTimesheet.DataSource = dsEmpty;
                    GrdTimesheet.DataBind();
                    GrdTimesheet.CurrentPageIndex = 0;
                    GrdTimesheet.PagerStyle.Visible = false;
                    LblException.Text = "No Timesheet availiable.";
                }
                if (DS.Tables.Count > 0)
                {
                    if (DS.Tables[0].Rows.Count <= GrdTimesheet.PageSize)
                    {
                        GrdTimesheet.PagerStyle.Visible = false;
                    }
                    else
                    {
                        GrdTimesheet.PagerStyle.Visible = true;
                    }
                }
                else
                {
                    DataTable dsEmpty = new DataTable();
                    GrdTimesheet.DataSource = dsEmpty;
                    GrdTimesheet.DataBind();
                    GrdTimesheet.CurrentPageIndex = 0;
                    GrdTimesheet.PagerStyle.Visible = false;
                    LblException.Text = "No Timesheet availiable.";
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Unclosed quotation mark") || ex.Message.Contains("Incorrect syntax"))
                {
                    GrdTimesheet.PagerStyle.Visible = false;
                    GrdTimesheet.Visible = false;
                    LblException.Text = "No Timesheet availiable.";
                }
                else
                {
                    throw (ex);
                }
            }
        }

        protected void imgBtnDisplay_Click(object sender, ImageClickEventArgs e)
        {
            GrdTimesheet.CurrentPageIndex = 0;
            ddlYear.SelectedValue = "0";
            ddlMonth.SelectedValue = "0";
            txtProjectName.Text = "";
            GetTimesheets();
        }

        protected void GrdTimesheet_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Expand")
            {
                ImageButton img;
                img = (ImageButton)e.Item.Cells[0].FindControl("ImgExpand");
                if (img.ImageUrl == "../Images/Plus-sign.gif")
                {
                    img.ImageUrl = "../Images/Minus-sign.gif";
                    img.AlternateText = "Collapse";
                }
                else
                {
                    img.ImageUrl = "../Images/Plus-sign.gif";
                    img.AlternateText = "Expand";
                }
                PlaceHolder exp;
                exp = (PlaceHolder)e.Item.Cells[ExpandColIndex].FindControl("Expanded");
                exp.Visible = !exp.Visible;
            }
        }

        protected void GrdTimesheet_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            DataGrid SubDG;
            DataSet DS = new DataSet();

            SubDG = (DataGrid)e.Item.FindControl("GrdSubTimesheet");
            if (SubDG != null)
            {
                string strKeyField;
                int intProjectID;
                int intMonth;
                int intYear;
                try
                {
                    strKeyField = GrdTimesheet.DataKeys[e.Item.ItemIndex].ToString();
                    intProjectID = strKeyField[0];
                    intMonth = strKeyField[1];
                    intYear = strKeyField[2];
                    DS = new DsrBAL().GetTimesheetEmployeeSummaryV2(intProjectID, intMonth, intYear);
                }
                catch (Exception)
                {

                }
                try
                {
                    if (DS.Tables.Count > 0)
                    {
                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            SubDG.DataSource = DS;
                            SubDG.DataBind();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
            }
            if (e.Item.ItemType != ListItemType.Header)
            {
                RegisterStartupScript("Declare", "<script language=\'javascript\'>var realColor;</script>");
                e.Item.Attributes.Add("OnMouseOver", "realColor = this.style.backgroundColor; this.style.backgroundColor = \'#cccccc\';");
                e.Item.Attributes.Add("OnMouseOut", "this.style.backgroundColor = realColor;");
            }
        }

        protected void GrdTimesheet_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            GrdTimesheet.CurrentPageIndex = e.NewPageIndex;
            if (Convert.ToInt32(ddlMonth.SelectedValue) == 0 && Convert.ToInt32(ddlYear.SelectedValue) == 0 && txtProjectName.Text == "")
            {
                GetTimesheets();
            }
            else
            {
                GetSearchResults();
            }
        }
    }
}