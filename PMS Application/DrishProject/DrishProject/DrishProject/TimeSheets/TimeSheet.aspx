﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="TimeSheet.aspx.cs" Inherits="DrishProject.TimeSheets.TimeSheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table id="tblMain" cellspacing="0" cellpadding="0" width="100%" border="0" style="height: 1%">        
        <tr height="80%">
            <td width="100%" style="height: 68%">
                <!-- CENTER -->
                <table id="tblCentral" cellspacing="0" cellpadding="0" align="center" border="0"
                    style="width: 101%; height: 63%">
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr height="100%">
                        <td style="height: 88%">
                        </td>
                        <td align="left" style="height: 88%">
                            <table cellspacing="0" cellpadding="0" width="98%" align="top" border="0" style="height: 72%">
                                <tbody>
                                    <tr>
                                        <td valign="top" align="left" style="height: 66%">
                                            <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0" style="height: 88%">
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table cellspacing="0" cellpadding="0" align="top" border="0">
                                                <tr>
                                                    <td>
                                                        <a href="../DSR/MyDSRNew.aspx">
                                                            <img src="../Images/DSR_bt.gif" border="0"></a>
                                                    </td>
                                                    <td>
                                                        <a href="../DSR/DSR.aspx">
                                                            <img src="../Images/dsr_history_tab.gif" border="0"></a>
                                                    </td>
                                                    <td>                                                        
                                                        <img src="../Images/Project_Time_Active.gif" border="0">
                                                    </td>
                                                    <td>
                                                        <a href="Project_WorkPlan.aspx">
                                                            <img id="imgWorkPlan" src="../Images/Project_work_plan.gif" border="0"></a>
                                                    </td>
                                                    <td height="21px">
                                                        <a href="AddResourceHours.aspx">
                                                            <img alt="" id="imgAddRes" src="../Images/add_res_hrs_tab.gif" border="0" /></a>
                                                    </td>
                                                    <td>
                                                        <a href="ResourceAllocation_Detail.aspx">
                                                            <img alt="" id="imgResAllDetail" src="../Images/res_alloc_details_tab.gif" border="0" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="center-bg">
                                        <td valign="top" align="left" width="96%" style="height: 28px">
                                            <img height="10" src="../Images/spacer.gif" width="10">
                                        </td>
                                        <td valign="top" align="right" width="3%" style="height: 28px">
                                            <img height="10" src="../Images/right_top_corner.gif" width="10">
                                        </td>
                                    </tr>
                                    <tr class="center-bg" valign="top" align="right">
                                        <td align="right" colspan="2" style="height: 29px">
                                            &nbsp;&nbsp;&nbsp;
                                            <img style="cursor: pointer" onclick="ShowTimesheetSummary()" src="../Images/project_summary.gif"
                                                border="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr class="center-bg" height="2%">
                                        <td style="height: 2%">
                                            <table class="SearchtableStyle" cellspacing="0" cellpadding="0" width="100%" align="center">
                                                <tr valign="bottom">
                                                    <td class="SearchTblHead" colspan="11">
                                                        &nbsp;Timesheets Search
                                                    </td>
                                                </tr>
                                                <tr class="Gridtxt">
                                                    <td colspan="11" height="10">
                                                    </td>
                                                </tr>
                                                <tr valign="middle">
                                                    <td class="Labeltxt" align="right" height="15">
                                                        Month
                                                    </td>
                                                    <td class="Gridtxt" align="left" colspan="1" height="15" rowspan="1">
                                                        &nbsp;
                                                        <asp:DropDownList ID="ddlMonth" runat="server" Width="128px" CssClass="Gridtxt">
                                                            <asp:ListItem Value="0" Selected="true">--Select--</asp:ListItem>
                                                            <asp:ListItem Value="1">January</asp:ListItem>
                                                            <asp:ListItem Value="2">February</asp:ListItem>
                                                            <asp:ListItem Value="3">March</asp:ListItem>
                                                            <asp:ListItem Value="4">April</asp:ListItem>
                                                            <asp:ListItem Value="5">May</asp:ListItem>
                                                            <asp:ListItem Value="6">June</asp:ListItem>
                                                            <asp:ListItem Value="7">July</asp:ListItem>
                                                            <asp:ListItem Value="8">August</asp:ListItem>
                                                            <asp:ListItem Value="9">September</asp:ListItem>
                                                            <asp:ListItem Value="10">October</asp:ListItem>
                                                            <asp:ListItem Value="11">November</asp:ListItem>
                                                            <asp:ListItem Value="12">December</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="Labeltxt" align="right" colspan="1" height="15" rowspan="1">
                                                        Year
                                                    </td>
                                                    <td class="Gridtxt" align="left" colspan="1" height="15" rowspan="1">
                                                        &nbsp;
                                                        <asp:DropDownList ID="ddlYear" runat="server" Width="128px" CssClass="Gridtxt">
                                                            <asp:ListItem Value="0" Selected="true">--Select--</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="Labeltxt" align="right" height="15">
                                                        Project Code
                                                    </td>
                                                    <td class="Gridtxt" align="left" height="15">
                                                        &nbsp;<asp:TextBox ID="txtProjectName" runat="server" Width="136px" CssClass="Gridtxt"
                                                            MaxLength="15"></asp:TextBox>
                                                    </td>
                                                    <td class="Gridtxt" align="center">
                                                        <asp:ImageButton ID="imgBtnSearch" runat="server" ImageUrl="../Images/searc_bt.gif"
                                                            OnClick="imgBtnSearch_Click"></asp:ImageButton>&nbsp;<asp:ImageButton ID="imgBtnDisplay"
                                                                runat="server" ImageUrl="../Images/display_all_bt.gif" OnClick="imgBtnDisplay_Click">
                                                            </asp:ImageButton>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Labeltxt" align="right" colspan="11">
                                                        <asp:DropDownList ID="DropDownList1" runat="server" Width="0px" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        <asp:Button ID="btnRefresh" runat="server" Width="0px" Visible="false"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="Gridtxt" style="height: 2%">
                                        </td>
                                    </tr>
                                    <tr class="center-bg" valign="top" align="center">
                                        <td class="GridBG" valign="top" align="left" width="100%" style="height: 300px">
                                            <asp:DataGrid ID="GrdTimesheet" runat="server" Width="100%" AllowPaging="true" BorderColor="White"
                                                BackColor="#F7F7F7" AutoGenerateColumns="False" DataKeyField="KeyField" CellPadding="3"
                                                GridLines="Vertical" BorderStyle="None" OnItemDataBound="GrdTimesheet_ItemDataBound"
                                                OnItemCommand="GrdTimesheet_ItemCommand" OnPageIndexChanged="GrdTimesheet_PageIndexChanged">
                                                <SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
                                                <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                <Columns>
                                                    <asp:TemplateColumn>
                                                        <HeaderStyle Width="2%"></HeaderStyle>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgExpand" AlternateText="Expand" runat="server" Width="9px"
                                                                Height="9px" ImageUrl="../Images/Plus-sign.gif" CommandName="Expand"></asp:ImageButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="MonthDisplay" HeaderText="Time Period">
                                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ProjectCode" HeaderText="Project Code">
                                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="TotalHrs" HeaderText="Hrs Spent">
                                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Detailed TimeSheets">
                                                        <HeaderStyle Font-Bold="true" HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        <ItemTemplate>
                                                            <img id="imgView" title='TimeSheet - <%#Eval("MonthDisplay")%>(<%#Eval("ProjectCode")%>)'
                                                                alt='TimeSheet - <%#Eval("MonthDisplay")%>(<%#Eval("ProjectCode")%>)' src="../Images/Summary_img.gif"
                                                                onclick='ViewEmpReport(<%# Eval("Month")%>,<%# Eval("Year")%>,<%# Eval("ProjectID")%>);'
                                                                style="cursor: pointer">
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Generate Projects TimeSheets">
                                                        <HeaderStyle Font-Bold="true" HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        <ItemTemplate>
                                                            <img id="imgExport" title='Export TimeSheet - <%#Eval("MonthDisplay")%>(<%#Eval("ProjectCode")%>)'
                                                                alt='Export TimeSheet - <%#Eval("MonthDisplay")%>(<%#Eval("ProjectCode")%>)'
                                                                src="../Images/exp_excel_ico.gif" onclick='ExportTimeSheet(<%# Eval("Month")%>,<%# Eval("Year")%>,<%# Eval("ProjectID")%>);'
                                                                style="cursor: pointer">
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn>
                                                        <ItemTemplate>
                                                            <asp:PlaceHolder ID="Expanded" runat="server" Visible="False"></td> </tr>
                                                                <tr>
                                                                    <td width="9px">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td colspan="6" width="100%">
                                                                        <asp:DataGrid ID="GrdSubTimesheet" runat="server" Width="100%" BackColor="#F7F7F7"
                                                                            BorderWidth="1px" BorderColor="White" CellPadding="2" GridLines="None" ForeColor="Black"
                                                                            AutoGenerateColumns="False">
                                                                            <ItemStyle CssClass="Gridtxt"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridHead"></HeaderStyle>
                                                                            <FooterStyle CssClass="GridFoot"></FooterStyle>
                                                                            <PagerStyle CssClass="Pagertxt" NextPageText="<img src=Images/next_bt.gif border=0>"
                                                                                PrevPageText="<img src=Images/prev_bt.gif border=0>" HorizontalAlign="Center"
                                                                                VerticalAlign="Middle"></PagerStyle>
                                                                            <Columns>
                                                                                <asp:BoundColumn DataField="NAME" HeaderText="Team Members">
                                                                                    <HeaderStyle Width="20%"></HeaderStyle>
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="TotalHrs" HeaderText="HrsSpent">
                                                                                    <HeaderStyle Width="20%"></HeaderStyle>
                                                                                </asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Generate Employee TimeSheets">
                                                                                    <HeaderStyle HorizontalAlign="left" Width="60%"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                                                    <ItemTemplate>
                                                                                        <img id="imgExportEmp" src="../Images/exp_excel_ico.gif" title='Export TimeSheet - <%#Eval("NAME")%>(<%#Eval("MonthDisplay")%>)'
                                                                                            alt='Export TimeSheet - <%#Eval("NAME")%>(<%#Eval("MonthDisplay")%>)' onclick='ExportTimeSheetEmp(<%# Eval("ProjectID")%>,<%# Eval("EmpID")%>,<%# Eval("tYear")%>,<%# Eval("tMonth")%>);'
                                                                                            style="cursor: pointer">
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                            </asp:PlaceHolder>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn Visible="False" DataField="KeyField" HeaderText="KeyField"></asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle NextPageText="&lt;img src=Images/next_bt.gif border=0&gt;" PrevPageText="&lt;img src=Images/prev_bt.gif border=0&gt;"
                                                    CssClass="Pagertxt" Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid><asp:Label ID="LblException" runat="server" CssClass="Errortxt"></asp:Label>
                                        </td>
                                        <td style="height: 300px">
                                            <img src="../Images/spacer.gif">
                                        </td>
                                    </tr>
                                    <tr class="center-bg">
                                        <td valign="top" align="left" style="height: 43px">
                                            <img height="10" src="../Images/spacer.gif" width="10">
                                        </td>
                                        <td valign="bottom" align="right" style="height: 43px">
                                            <img height="10" src="../Images/right_bottom_corner.gif" width="10">
                                        </td>
                                    </tr>
                                    </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>    


    <script language="javascript" type="text/javascript">
        function EnterVal(event) {
        	var keyCode = event.which ? event.which : event.keyCode;
            if (keyCode=="13") {
        			__doPostBack('imgBtnSearch','');
        			return false;
        			}
        			}

        function SummaryReport(tMonth, tYear, PID) {
            var leftPos = (screen.width / 2) - 200;
            var topPos = (screen.height / 2) - 200;
            window.open("TimeSheet_EmployeeSummary.aspx?Month=" + tMonth + "&Year=" + tYear + "&PID=" + PID, "EmployeeSummaryReport", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=400, height=400");
        }
        function ViewEmpReport(tMonth, tYear, PID) {
            var leftPos = (screen.width / 2) - 450;
            var topPos = (screen.height / 2) - 300;
            window.open("View_EmployeeReport.aspx?Month=" + tMonth + "&Year=" + tYear + "&PID=" + PID, "EmployeeReport", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=yes, width=900, height=600");
        }
        function ShowTimesheetSummary() {
            var leftPos = (screen.width / 2) - 200;
            var topPos = (screen.height / 2) - 200;
            window.open("Timesheet_Summary.aspx", "TimesheetSummary", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=400, height=400");
        }
        function ExportTimeSheet(tMonth, tYear, PID) {
            var leftPos = (screen.width / 2) - 400
            var topPos = (screen.height / 2) - 250;
            window.open("ExportTimesheetM.aspx?Month=" + tMonth + "&Year=" + tYear + "&PID=" + PID, "ExportExcel", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=yes, scrollbars=yes resizable=yes, copyhistory=yes, width=800, height=500");
        }
        function ExportTimeSheetEmp(PID, EmpID, tYear, tMonth) {
            var leftPos = (screen.width / 2) - 400
            var topPos = (screen.height / 2) - 250;
            window.open("ExportTimeSheetW.aspx?Month=" + tMonth + "&Year=" + tYear + "&PID=" + PID + "&EmpID=" + EmpID, "ExportExcel", "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=yes, scrollbars=yes resizable=yes, copyhistory=yes, width=800, height=500");
        }
	</script>
</asp:Content>
