﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using Entities;

namespace DrishProject.TimeSheets
{
    public partial class Timesheet_Summary : System.Web.UI.Page
    {
        Entities.Employee objUser = new Entities.Employee();
        protected void Page_Load(object sender, EventArgs e)
        {
            objUser = (Entities.Employee)(Session["emp"]);
            if (!Page.IsPostBack)
            {
                try
                {
                    GetTimesheetSummary();
                }
                catch (Exception ex)
                {
                    LblException.Text = ex.Message;

                }
            }
        }

        private void GetTimesheetSummary()
        {
            try
            {
                DataSet DS = new DataSet();
                if (objUser.IsAministrator==1)
                {
                    DS = new DsrBAL().GetTimesheetSummaryV2();
                }
                else
                {
                    DS = new DsrBAL().GetTimesheetSummaryV2(objUser.EmployeeId);
                }
                GrdDSR.DataSource = DS;
                GrdDSR.DataBind();
                if (DS.Tables[0].Rows.Count == 0)
                {
                    GrdDSR.PagerStyle.Visible = false;
                    LblException.Text = "No Summary Present for any Timesheet.";
                }
                else
                {
                    GrdDSR.PagerStyle.Visible = true;
                    LblException.Text = "";
                }
                if (DS.Tables[0].Rows.Count <= GrdDSR.PageSize)
                {
                    GrdDSR.PagerStyle.Visible = false;
                }
                else
                {
                    GrdDSR.PagerStyle.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void GrdDSR_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            GrdDSR.CurrentPageIndex = e.NewPageIndex;
            GetTimesheetSummary();
        }

        protected void GrdDSR_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (!(e.Item.ItemType == ListItemType.Header))
            {
                RegisterStartupScript("Declare", "<script language='javascript'>var realColor;</script>");
                e.Item.Attributes.Add("OnMouseOver", "realColor = this.style.backgroundColor; this.style.backgroundColor = '#cccccc';");
                e.Item.Attributes.Add("OnMouseOut", "this.style.backgroundColor = realColor;");
            }
        }
    }
}