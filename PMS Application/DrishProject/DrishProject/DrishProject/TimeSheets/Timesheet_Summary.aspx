﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Timesheet_Summary.aspx.cs" Inherits="DrishProject.TimeSheets.Timesheet_Summary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="http://localhost:61832/Styles.css" rel="stylesheet" 
        type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <TABLE id="tblMain" height="98%" cellSpacing="0" cellPadding="0" width="100%" border="0">			
				<tr height="1%">
					<td></td>
				</tr>
				<tr height="80%">
					<td width="100%"><!-- CENTER -->
						<TABLE id="tblCentral" height="100%" cellSpacing="0" cellPadding="0" width="100%" align="center"
							border="0">
							<tr>
								<td></td>
							</tr>
							<TR height="100%">
								<td width="2%"></td>
								<TD align="left" height="100%">
									<table height="100%" cellSpacing="0" cellPadding="0" width="98%" align="top" border="0">
										<TBODY>
											<tr>
												<td vAlign="top" align="left" height="100%">
													<table cellSpacing="0" cellPadding="0" align="top" border="0">
														<tr>
															<TD><IMG src="../Images/project_summaries_tab.gif" border="0"></TD>
														</tr>
													</table>
													<TABLE height="98%" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
											</tr>
											<TBODY>
												<TR class="center-bg">
													<TD vAlign="top" align="left" width="96%" height="21"><IMG height="5" src="../Images/spacer.gif" width="10"></TD>
													<TD vAlign="top" align="right" width="3%" height="21"><IMG height="5" src="../Images/right_top_corner.gif" width="10"></TD>
												</TR>
												<tr class="center-bg" vAlign="top">
													<td height="10"><strong>Total Hours per Project</strong></td>
													<td height="20">&nbsp;<IMG src="../Images/spacer.gif"></td>
												</tr>
												<TR class="center-bg" vAlign="top">
													<TD vAlign="top" align="left" width="100%" class="GridBG" height="80%">
														<asp:datagrid id="GrdDSR" runat="server" BorderStyle="None" 
                                                            GridLines="Vertical" CellPadding="3"
															AutoGenerateColumns="False" Width="100%" BackColor="#F7F7F7" BorderColor="White" AllowPaging="True" 
                                                            onitemdatabound="GrdDSR_ItemDataBound" 
                                                            onpageindexchanged="GrdDSR_PageIndexChanged">
															<SelectedItemStyle CssClass="Labeltxt"></SelectedItemStyle>
															<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
															<ItemStyle CssClass="Gridtxt"></ItemStyle>
															<HeaderStyle CssClass="GridHead"></HeaderStyle>
															<FooterStyle CssClass="GridFoot"></FooterStyle>
															<Columns>
																<asp:BoundColumn DataField="ProjectCode" HeaderText="Project Code">
																	<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
																	<ItemStyle HorizontalAlign="Left"></ItemStyle>
																</asp:BoundColumn>
																<asp:BoundColumn DataField="TotalHrs" HeaderText="Total Hrs">
																	<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																</asp:BoundColumn>
															</Columns>
															<PagerStyle NextPageText="&lt;img src=Images/next_bt.gif border=0&gt;" PrevPageText="&lt;img src=Images/prev_bt.gif border=0&gt;"
																CssClass="Pagertxt" Mode="NumericPages"></PagerStyle>
														</asp:datagrid><asp:label CssClass="Errortxt" id="LblException" runat="server"></asp:label>
													</TD>
													<TD><IMG height="10" src="../Images/spacer.gif" width="10"></TD>
												</TR>
												<TR class="center-bg">
													<TD vAlign="top" align="left"><IMG height="10" src="../Images/spacer.gif" width="10"></TD>
													<TD vAlign="bottom" align="right"><IMG height="10" src="../Images/right_bottom_corner.gif" width="10"></TD>
												</TR>
											</TBODY>
									</table>
								</TD>
							</TR>
						</TABLE>
					</td>
				</tr>
		</TABLE>
		</TD></TR> 
		<!--FOOTER --> </TBODY></TABLE></TABLE>
    </div>
    </form>
</body>
</html>
