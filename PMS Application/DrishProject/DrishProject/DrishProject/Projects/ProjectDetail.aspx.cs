﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Entities;
using DrishProjectBAL;
using System.Data;
using System.Data.SqlClient;

namespace DrishProject
{
    public partial class WebForm8 : System.Web.UI.Page
    {
        int ProjectID;
        protected void Page_Load(object sender, EventArgs e)
        {     
            Project pp = new Project();
            if (Request["ProjectID"] == "")
            {
                ProjectID = Convert.ToInt32(Session["ProjectID"]);
                pp.ProjectID = Convert.ToInt32(Session["ProjectID"]);
                Session["ProjectID"] = ProjectID;
            }
            else
            {
                Entities.Project objProject = new Entities.Project();
                //objProject = (Entities.Project)(Session["project"]);
                try
                {
                    objProject.ProjectID = Convert.ToInt32(Request.QueryString["ProjectID"].ToString());

                    Session["ProjectID"] = Request["ProjectID"];
                }
                catch (Exception)
                { }
                ProjectID = Convert.ToInt32(Session["ProjectID"]);
                pp.ProjectID = Convert.ToInt32(Session["ProjectID"]);
            }

            #region "Unused Code"            
            //Code to hide the Release, CheckList, Client and Code Review Tabs for Non Engineering Projects.
            //if (pp.ProjectCategory != "ENGG")
            //{
            //    HtmlTableCell tdFLY;
            //    //System.Web.UI.HtmlControls.HtmlTableCell tdFLY;
            //    tdFLY = (HtmlTableCell)FindControl("tdRelease");
            //    tdFLY.Visible = false;
            //    tdFLY = (HtmlTableCell)FindControl("tdClients");
            //    tdFLY.Visible = false;
            //    tdFLY = (HtmlTableCell)FindControl("tdCheckList");
            //    tdFLY.Visible = false;
            //    tdFLY = (HtmlTableCell)FindControl("tdCodeReview");
            //    tdFLY.Visible = false;

            //}
            #endregion

            if (ProjectID > 0)
            {
                pp.ProjectID = ProjectID;
                pp = new ProjectBAL().ProjectDetails(pp);
                if (!Page.IsPostBack)
                {
                    pcode.Text = pp.ProjectCode;
                    pname.Text = pp.ProjectName;
                    ptype.Text = pp.ProjectType;
                    pmailid.Text = pp.ProjectEmail;
                    pstatus.Text = pp.ProjectStatus;
                    psdate.Text = pp.ProjectStartDate.ToShortDateString();
                    pedate.Text = pp.ProjectEndDate.ToShortDateString();
                    Session["Project"] = pp;
                }
            }
            else
            {
                Response.Redirect("~/Projects/ProjectSearch.aspx");
            }
        }

        protected void ImageButton12_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Projects/EditProject.aspx?ProjectCode=" + pcode.Text);
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TeamMembers/ProjectTeamMembers.aspx");
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectModules/Module.aspx");
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Tasks/MyTasks.aspx");
        }

        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Documents/Documents.aspx");
        }

        //protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
        //{
        //    Response.Redirect("~/ProjectRelease/Release.aspx");
        //}

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Clients/Clients.aspx");
        }

        protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectMeetings/Meetings.aspx");
        }

        protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectHistory/History.aspx");
        }

        protected void ImageButton10_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectRelease/Release.aspx");
        }

        protected void ImageButton11_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectNotes/Notes.aspx");
        }

        protected void ImageButton13_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ProjectReview/Reviews.aspx");
        }

        protected void ImageButton14_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TimeSheets/TimeSheet.aspx");
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Projects/ProjectDetail.aspx");
        }
    }
}