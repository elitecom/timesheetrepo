﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="EditProject.aspx.cs" Inherits="DrishProject.WebForm6" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <br />
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/tabs_bt_01.gif" />
        <asp:ImageButton ID="ImageButton2" runat="server" 
            ImageUrl="~/Images/tabs_bt_02.gif" onclick="ImageButton2_Click" />
        <asp:ImageButton ID="ImageButton3" runat="server" 
            ImageUrl="~/Images/tabs_bt_06.gif" onclick="ImageButton3_Click" />
        <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/tasks.gif" 
            onclick="ImageButton4_Click" />
        <asp:ImageButton ID="ImageButton5" runat="server" 
            ImageUrl="~/Images/tabs_bt_05.gif" onclick="ImageButton5_Click" />
        <asp:ImageButton ID="ImageButton7" runat="server" 
            ImageUrl="~/Images/tabs_bt_03.gif" onclick="ImageButton7_Click" />
        <asp:ImageButton ID="ImageButton8" runat="server" 
            ImageUrl="~/Images/tabs_bt_10.gif" onclick="ImageButton8_Click" />
        <asp:ImageButton ID="ImageButton11" runat="server" 
            ImageUrl="~/Images/tabs_bt_09.gif" onclick="ImageButton11_Click" />
        <asp:ImageButton ID="ImageButton9" runat="server" 
            ImageUrl="~/Images/tabs_bt_07.gif" onclick="ImageButton9_Click" />
        <asp:ImageButton ID="ImageButton10" runat="server" 
            ImageUrl="~/Images/tabs_bt_08.gif" onclick="ImageButton10_Click" />
        <asp:ImageButton ID="ImageButton13" runat="server" 
            ImageUrl="~/Images/review_active.gif" onclick="ImageButton13_Click"/>
     <asp:ImageButton ID="ImageButton14" runat="server" 
            ImageUrl="~/Images/timesheet_tab_active.gif" onclick="ImageButton14_Click" />
    </div>
    <div>
        <br />
    </div>
    <div>
        <table style="height: 400px">
            <caption>
                <h2><asp:Label ID="Label1" runat="server" Text="Edit Project"></asp:Label></h2>
            </caption>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label14" runat="server" Text="Project ID"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label2" runat="server" Text="Project Name"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label3" runat="server" Text="Project Code"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label4" runat="server" Text="Project Category"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:DropDownList ID="DropDownList1" runat="server" Width="132px" 
                        AutoPostBack="True">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label5" runat="server" Text="Project Type"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:DropDownList ID="DropDownList2" runat="server" Width="132px" 
                        AutoPostBack="True">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label6" runat="server" Text="Project Level"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:DropDownList ID="DropDownList3" runat="server" Width="132px" 
                        AutoPostBack="True">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label7" runat="server" Text="Project Mail Id"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:TextBox ID="TextBox3" runat="server" Width="220px" AutoPostBack="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label8" runat="server" Text="Bug Tracking Setup"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:CheckBox ID="CheckBox1" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label9" runat="server" Text="Project Status"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:DropDownList ID="DropDownList4" runat="server" Width="132px">
                    </asp:DropDownList>
                </td>
            </tr>
            <%--<tr>
                <td style="width: 239px">
                    <asp:Label ID="Label10" runat="server" Text="Project Phase"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:DropDownList ID="DropDownList5" runat="server" Width="132px">
                    </asp:DropDownList>
                </td>
            </tr>--%>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label16" runat="server" Text="Reason For Changing Status"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:TextBox ID="txtStatusReason" runat="server" Height="16px" 
                        TextMode="MultiLine" Width="259px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label11" runat="server" Text="Project Start Date"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="TextBox4" runat="server" ></asp:TextBox>
                            <asp:CalendarExtender ID="TextBox4_CalendarExtender" runat="server" TargetControlID="TextBox4" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label12" runat="server" Text="Project End Date"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="TextBox5" runat="server" ></asp:TextBox>
                            <asp:CalendarExtender ID="TextBox5_CalendarExtender" runat="server" TargetControlID="TextBox5" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label17" runat="server" Text="Reason for Changing End Date"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:TextBox ID="txtEndDateReason" runat="server" Height="16px" 
                        TextMode="MultiLine" Width="259px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 239px">
                    <asp:Label ID="Label13" runat="server" Text="Project Plan"></asp:Label>
                </td>
                <td style="width: 269px">
                    <asp:CheckBox ID="CheckBox2" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" style="width: 239px">
                    <asp:ImageButton ID="Button1" runat="server" ImageUrl="~/Images/save_bt.gif" 
                        onclick="Button1_Click" />
                </td>
                <td align="center" style="width: 269px">
                    <asp:ImageButton ID="Button2" runat="server" ImageUrl="~/Images/reset_bt.gif" 
                        Text="Reset" onclick="Button2_Click" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="Label15" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    &nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
