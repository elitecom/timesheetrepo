﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using Entities;
using System.IO;

namespace DrishProject
{
    public partial class WebForm5 : System.Web.UI.Page
    {
        DataSet ds;
        Project prop = new Project();
        ProjectBAL bal = new ProjectBAL();
        ProjectTeamBAL team = new ProjectTeamBAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ds = new DataSet();
                ds = new DrishProjectBAL.ProjectBAL().GetProjectCategories();
                DropDownList1.DataSource = ds.Tables[0];
                DropDownList1.DataMember = ds.Tables[0].TableName;
                DropDownList1.DataTextField = "ProjectCategory";
                DropDownList1.DataValueField = "CatgeoryID";
                DropDownList1.DataBind();

                ds = new DataSet();
                ds = new DrishProjectBAL.ProjectBAL().GetProjectTypes();
                DropDownList2.DataSource = ds.Tables[0];
                DropDownList2.DataMember = ds.Tables[0].TableName;
                DropDownList2.DataTextField = "ProjectType";
                DropDownList2.DataValueField = "Proj_TypeID";
                DropDownList2.DataBind();

                ds = new DataSet();
                ds = new DrishProjectBAL.ProjectBAL().GetProjectLevels();
                DropDownList3.DataSource = ds.Tables[0];
                DropDownList3.DataMember = ds.Tables[0].TableName;
                DropDownList3.DataTextField = "LevelName";
                DropDownList3.DataValueField = "LevelId";
                DropDownList3.DataBind();

                ds = new DataSet();
                ds = new DrishProjectBAL.ProjectBAL().GetProjectStatus();
                DropDownList4.DataSource = ds.Tables[0];
                DropDownList4.DataMember = ds.Tables[0].TableName;
                DropDownList4.DataTextField = "ProjectStatus";
                DropDownList4.DataValueField = "Proj_StatusID";
                DropDownList4.DataBind();
            }
        }      

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("ProjectSearch.aspx");
        }

        protected void Button1_Click(object sender, ImageClickEventArgs e)
        {
            bool res, res1;
            string folderName = string.Empty;
            try
            {
                prop.ProjectName = TextBox1.Text;
                prop.ProjectCode = TextBox2.Text;
                prop.ProjectCategory = DropDownList1.SelectedValue;
                prop.ProjectType = (DropDownList2.SelectedValue);
                prop.ProjectLevel = (DropDownList3.SelectedValue);
                prop.ProjectStatus = (DropDownList4.SelectedValue);
                prop.ProjectStartDate = Convert.ToDateTime(TextBox4.Text);
                prop.ProjectEndDate = Convert.ToDateTime(TextBox5.Text);
                prop.ProjectEmail = Convert.ToString(TextBox3.Text);
                res = bal.AddNewProject(prop);
                if (res == true)
                {
                    DataSet ds = new DataSet();
                    ds = new ProjectBAL().GetProjectFolders();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            try
                            {
                                folderName = System.Configuration.ConfigurationManager.AppSettings["ProjectFolders"].ToString() + "\\" + prop.ProjectCode + "\\" + ds.Tables[0].Rows[i]["FolderName"].ToString();
                                System.IO.Directory.CreateDirectory(Server.MapPath(folderName));
                            }
                            catch (Exception ex)
                            {
                                Response.Write(ex.Message + "\n" + ex.StackTrace);
                            }
                        }
                        Entities.Documents docs = new Entities.Documents();
                        docs.ProjectId = new DrishProjectBAL.ProjectBAL().GetProjectIDByCode(prop);
                        docs.ProjectCategory = prop.ProjectCategory;
                        res1 = new ProjectBAL().AddProjectDocumentTypes(docs);
                        Response.Write("<script language='JavaScript'>alert('Project is Created with Folder');</script>");
                    }
                }
                // Add the Person in Team also
                /*
                 * @MemberNameID
                 * @ProjectID
                 * @MemberRoleID
                 * @MemberWorkID
                 * @Availability
                 * @ModifiedBy
                 * @ModifiedOn
                 * @MemberID
                 * @ReportTo
                 */
                ProjectTeam pteam = new ProjectTeam();
                pteam.MemberNameId = ((Entities.Employee)(Session["emp"])).EmployeeId;
                //pteam.ProjectId = prop.ProjectID;
                pteam.ProjectId = new DrishProjectBAL.ProjectBAL().GetProjectIDByCode(prop);
                pteam.EmpId = ((Entities.Employee)(Session["emp"])).EmployeeId;
                //pteam.MemberRoleId = ((Entities.Employee)(Session["emp"])).RoleId;
                pteam.MemberRoleId = 1;  // by default as Project Manager
                pteam.MemberWorking = "F";
                pteam.Availability = "F";
                pteam.ModifiedBy = ((Entities.Employee)(Session["emp"])).EmployeeId;
                pteam.ModifiedOn = DateTime.Now;
                pteam.MemberId = 0;
                pteam.ReportTo = ((Entities.Employee)(Session["emp"])).EmployeeId;
                bool ans;

                ans = team.InsertTeamMember(pteam);
                if (ans == true)
                {
                    Response.Write("<script language='JavaScript'>alert('Project Team Member is Added Successfully');</script>");
                }
            }
            catch (Exception ee)
            {
                Response.Write("Message : " + ee.Message + "\n" + ee.StackTrace);
            }
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("ProjectNew.aspx");
        }
    }
}