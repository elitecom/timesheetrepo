﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using DrishProjectBAL;
using System.Data;
using System.Data.SqlClient;
namespace DrishProject
{
    public partial class WebForm7 : System.Web.UI.Page
    {
        DataSet ds;
        Project prop = new Project();
        Entities.Employee emp = new Entities.Employee();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    ds = new DataSet();
                    ds = new DrishProjectBAL.ProjectBAL().GetProjectTypes();
                    DropDownList1.DataSource = ds.Tables[0];
                    DropDownList1.DataMember = ds.Tables[0].TableName;
                    DropDownList1.DataTextField = "ProjectType";
                    DropDownList1.DataValueField = "Proj_TypeID";
                    DropDownList1.DataBind();
                    DropDownList1.Items.Insert(0, " == Select ==");
                    ds = new DataSet();
                    ds = new DrishProjectBAL.ProjectBAL().GetProjectStatus();
                    DropDownList2.DataSource = ds.Tables[0];
                    DropDownList2.DataMember = ds.Tables[0].TableName;
                    DropDownList2.DataTextField = "ProjectStatus";
                    DropDownList2.DataValueField = "Proj_StatusID";

                    DropDownList2.DataBind();
                    DropDownList2.Items.Insert(0, " == Select ==");

                    emp = (Entities.Employee)(Session["emp"]);

                    ds = new DrishProjectBAL.ProjectBAL().GetProjectDetailsByEmpId(emp.EmployeeId);
                    GridView1.DataSource = ds;
                    GridView1.DataBind();

                    //Session["ProjectID"] = Convert.ToInt32(ds.Tables[0].Rows[0]["ProjectID"].ToString());
                }
                catch (Exception )
                {
                    //Response.Write("Message");
                }
            }
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            /*int Proj_TypeID;
            int Proj_StatusID;
            string ProjectName = string.Empty;
            if (DropDownList1.SelectedIndex >0)
            {
                Proj_TypeID = Convert.ToInt32(DropDownList1.SelectedValue.ToString());
            }
            if (DropDownList2.SelectedIndex > 0)
            {
                Proj_StatusID = Convert.ToInt32(DropDownList2.SelectedValue.ToString());
            }
            if (TextBox1.Text.Length > 0)
            {
                ProjectName = TextBox1.Text;
            }
            ds = new DrishProjectBAL.ProjectBAL().projectdata( ProjectName,Proj_TypeID,Proj_StatusID);
            GridView1.DataSource = ds;
            GridView1.DataBind();*/
            Project project = new Project();
            if (DropDownList1.SelectedIndex > 0)
            {
                project.ProjectTypeID = Convert.ToInt32(DropDownList1.SelectedValue.ToString());
                //project.ProjectType = DropDownList1.SelectedItem.Text.ToString();
            }
            if (DropDownList2.SelectedIndex > 0)
            {
                project.ProjectStatusID = Convert.ToInt32(DropDownList2.SelectedValue.ToString());
                //project.ProjectStatus = DropDownList2.SelectedItem.Text.ToString();
            }
            if (!String.IsNullOrEmpty(TextBox1.Text))
            {
                project.ProjectName = TextBox1.Text.Trim();
            }
            emp = (Entities.Employee)(Session["emp"]);
            ds = new DrishProjectBAL.ProjectBAL().SearchProject(project,emp.EmployeeId);
            GridView1.DataSource = ds;
            GridView1.DataBind();
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("ProjectNew.aspx");
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            ds = new DrishProjectBAL.ProjectBAL().GetProjectDetailsByEmpId(((Entities.Employee)(Session["emp"])).EmployeeId);
            GridView1.DataSource = ds;
            GridView1.DataBind();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string projectCode = string.Empty;
            string startDate, endDate;
            string projectStatus = string.Empty;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                projectCode = e.Row.Cells[1].Text.ToString();
                startDate = Convert.ToDateTime(e.Row.Cells[4].Text.ToString()).ToShortDateString();
                endDate = Convert.ToDateTime(e.Row.Cells[5].Text.ToString()).ToShortDateString();

                projectStatus = new DrishProjectBAL.ProjectBAL().GetProjectStatus(projectCode);
                //if (Convert.ToDateTime(endDate) < DateTime.Now)
                //{
                    string str = "";
                    if (projectStatus.ToLower() == "new")
                    {
                        str = System.Configuration.ConfigurationManager.AppSettings["NewColor"].ToString();                        
                    }
                    else if (projectStatus.ToLower() == "assigned")
                    {
                        str = System.Configuration.ConfigurationManager.AppSettings["NewColor"].ToString();                        
                    }
                    else if (projectStatus.ToLower() == "overdue")
                    {
                        str = System.Configuration.ConfigurationManager.AppSettings["NewColor"].ToString();                        
                    }
                    else if (projectStatus.ToLower() == "maintenance")
                    {
                        str = System.Configuration.ConfigurationManager.AppSettings["NewColor"].ToString();                        
                    }
                    else if (projectStatus.ToLower() == "completed")
                    {
                        str = System.Configuration.ConfigurationManager.AppSettings["NewColor"].ToString();                        
                    }
                //}
                //else
                //{
                    
                //}
                //e.Row.Cells[1].Text = "<i>" + e.Row.Cells[1].Text + "</i>";

            }
        }

        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            
        }

        protected void btnExport_Click(object sender, ImageClickEventArgs e)
        {
            Entities.Project proj = new Entities.Project();
            DataSet ds = new DataSet();
            ds = new DrishProjectBAL.ProjectBAL().GetProjectDetailsByEmpId(((Entities.Employee)(Session["emp"])).EmployeeId);
            DataSetToExcel.Convert(ds, Response);
        }              
    }
}