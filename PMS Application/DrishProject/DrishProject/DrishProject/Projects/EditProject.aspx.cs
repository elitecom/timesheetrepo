﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DrishProjectBAL;
using Entities;
using System.IO;

namespace DrishProject
{
    public partial class WebForm6 : System.Web.UI.Page
    {
        Project oldDetails = new Project();
        Project project;
        string pcode = string.Empty;
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {
            pcode = Convert.ToString(Request.QueryString["ProjectCode"].ToString());

            if (!Page.IsPostBack)
            {
                ds = new DataSet();
                ds = new DrishProjectBAL.ProjectBAL().GetProjectCategories();
                DropDownList1.DataSource = ds.Tables[0];
                DropDownList1.DataMember = ds.Tables[0].TableName;
                DropDownList1.DataTextField = "ProjectCategory";
                DropDownList1.DataValueField = "CatgeoryID";
                DropDownList1.DataBind();

                ds = new DataSet();
                ds = new DrishProjectBAL.ProjectBAL().GetProjectTypes();
                DropDownList2.DataSource = ds.Tables[0];
                DropDownList2.DataMember = ds.Tables[0].TableName;
                DropDownList2.DataTextField = "ProjectType";
                DropDownList2.DataValueField = "Proj_TypeID";
                DropDownList2.DataBind();

                ds = new DataSet();
                ds = new DrishProjectBAL.ProjectBAL().GetProjectLevels();
                DropDownList3.DataSource = ds.Tables[0];
                DropDownList3.DataMember = ds.Tables[0].TableName;
                DropDownList3.DataTextField = "LevelName";
                DropDownList3.DataValueField = "LevelId";
                DropDownList3.DataBind();

                ds = new DataSet();
                ds = new DrishProjectBAL.ProjectBAL().GetProjectStatus();
                DropDownList4.DataSource = ds.Tables[0];
                DropDownList4.DataMember = ds.Tables[0].TableName;
                DropDownList4.DataTextField = "ProjectStatus";
                DropDownList4.DataValueField = "Proj_StatusID";
                DropDownList4.DataBind();

                project = new Project();
                project.ProjectCode = pcode;
                project.ProjectID = new ProjectBAL().GetProjectIDByCode(project);
                project = new ProjectBAL().ProjectDetails(project);
                TextBox6.Text = project.ProjectID.ToString();
                TextBox1.Text = project.ProjectName;
                TextBox2.Text = project.ProjectCode;
                TextBox3.Text = project.ProjectEmail;
                CheckBox1.Checked = (project.MantisSetup == 1 ? true : false);
                DropDownList1.Text = project.ProjectCategory;
                DropDownList2.Text = project.ProjectType;
                DropDownList3.Text = project.ProjectLevel;
                DropDownList4.Text = project.ProjectStatus;
                TextBox4.Text = project.ProjectStartDate.ToShortDateString();
                TextBox5.Text = project.ProjectEndDate.ToShortDateString();
                CheckBox2.Checked = (project.ProjectPlan == 1 ? true : false);

                oldDetails = project;   // Get old Project Details
            }
        }

        protected void Button1_Click(object sender, ImageClickEventArgs e)
        {
            bool res;
            string folderName = string.Empty;
            try
            {
                project = new Project();
                project.ProjectID = Convert.ToInt32(TextBox6.Text);
                project.ProjectName = TextBox1.Text;
                project.ProjectCode = TextBox2.Text;
                project.ProjectCategory = DropDownList1.SelectedValue;
                project.ProjectType = (DropDownList2.SelectedValue);
                project.ProjectLevel = (DropDownList3.SelectedValue);
                project.ProjectStatus = (DropDownList4.SelectedValue);
                project.ProjectStartDate = Convert.ToDateTime(TextBox4.Text);
                project.ProjectEndDate = Convert.ToDateTime(TextBox5.Text);
                project.ProjectEmail = Convert.ToString(TextBox3.Text);
                res = new DrishProjectBAL.ProjectBAL().UpdateProject(project);                             
               
                bool ans;                        
                if (res == true)
                {
                    if (project.ProjectEndDate != oldDetails.ProjectEndDate)
                    {
                        ans = new ProjectBAL().AddHistoryForDateChange(project.ProjectID, "P", Convert.ToDateTime(oldDetails.ProjectEndDate), Convert.ToDateTime(project.ProjectEndDate), txtEndDateReason.Text, ((Entities.Employee)(Session["emp"])).EmployeeId, DateTime.Now);
                    }
                    if (project.ProjectStatus != oldDetails.ProjectStatus)
                    {
                        ans = new ProjectBAL().AddHistoryForStatusChange(project.ProjectID, "P", oldDetails.ProjectStatus, project.ProjectStatus, txtEndDateReason.Text, ((Entities.Employee)(Session["emp"])).EmployeeId, DateTime.Now);
                    }  
                    Label15.Text="Project Details Updated Successfully";
                }
                else
                {
                    Label15.Text="Project Details Not Updated Successfully";
                }
            }
            catch (Exception ee)
            {
                Label15.Text = ee.Message;
            }
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~\\TeamMembers\\ProjectTeamMembers.aspx");
        }

        protected void Button2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~\\Projects\\ProjectSearch.aspx");
        }

        protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton11_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton10_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton13_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton14_Click(object sender, ImageClickEventArgs e)
        {

        }
    }
}
