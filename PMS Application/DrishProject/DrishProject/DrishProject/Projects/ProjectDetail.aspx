﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="ProjectDetail.aspx.cs" Inherits="DrishProject.WebForm8" %>
<%@ Register src="~/UserControls/ProjectMenu.ascx" tagname="ProjectMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<h2> Project Details</h2>--%>
  <%--  <div>

        <uc1:ProjectMenu ID="ProjectMenu1" runat="server" />
</div>--%>
<div>
 <br />
 <asp:ImageButton ID="ImageButton1" runat="server" 
        ImageUrl="~/Images/tabs_bt_01.gif" onclick="ImageButton1_Click" />
    <asp:ImageButton ID="ImageButton2" runat="server" 
        ImageUrl="~/Images/tabs_bt_02.gif" onclick="ImageButton2_Click" />
    <asp:ImageButton ID="ImageButton3" runat="server" 
        ImageUrl="~/Images/tabs_bt_06.gif" onclick="ImageButton3_Click" />
    <asp:ImageButton ID="ImageButton4" runat="server" 
        ImageUrl="~/Images/tasks.gif" onclick="ImageButton4_Click" />
    <asp:ImageButton ID="ImageButton5" runat="server" 
        ImageUrl="~/Images/tabs_bt_05.gif" onclick="ImageButton5_Click" />
    <%--<asp:ImageButton ID="ImageButton6" runat="server" 
        ImageUrl="~/Images/tabs_bt_08.gif" onclick="ImageButton6_Click" />--%>
    <asp:ImageButton ID="ImageButton7" runat="server" 
        ImageUrl="~/Images/tabs_bt_03.gif" onclick="ImageButton7_Click" />
    <asp:ImageButton ID="ImageButton8" runat="server" 
        ImageUrl="~/Images/tabs_bt_10.gif" onclick="ImageButton8_Click" />
    <asp:ImageButton ID="ImageButton11" runat="server" 
        ImageUrl="~/Images/tabs_bt_09.gif" onclick="ImageButton11_Click" />
    <asp:ImageButton ID="ImageButton9" runat="server" 
        ImageUrl="~/Images/tabs_bt_07.gif" onclick="ImageButton9_Click" />
    <asp:ImageButton ID="ImageButton10" runat="server" 
        ImageUrl="~/Images/tabs_bt_08.gif" onclick="ImageButton10_Click" />
         <asp:ImageButton ID="ImageButton13" runat="server" 
        ImageUrl="~/Images/review_active.gif" onclick="ImageButton13_Click"/>
     <asp:ImageButton ID="ImageButton14" runat="server" 
        ImageUrl="~/Images/timesheet_tab_active.gif" onclick="ImageButton14_Click" />
    </div>
<div>
    <table width="70%" style="height: 333px">
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td style="width: 191px"><asp:Label ID="Label1" runat="server" Text="Project Code" 
                    Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="pcode" runat="server" Text="Label"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 191px"><asp:Label ID="Label2" runat="server" Text="Project Name" 
                    Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="pname" runat="server" Text="Label"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 191px"><asp:Label ID="Label3" runat="server" Text="Project Type" 
                    Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="ptype" runat="server" Text="Label"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 191px"><asp:Label ID="Label4" runat="server" 
                    Text="Project Mail Id" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="pmailid" runat="server" Text="Label"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 191px"><asp:Label ID="Label5" runat="server" 
                    Text="Project Status" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="pstatus" runat="server" Text="Label"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 191px"><asp:Label ID="Label6" runat="server" 
                    Text="Project Start Date" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="psdate" runat="server" Text="Label"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 191px"><asp:Label ID="Label7" runat="server" 
                    Text="Project End Date" Font-Bold="True"></asp:Label></td>
            <td><asp:Label ID="pedate" runat="server" Text="Label"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="2"> <asp:ImageButton ID="ImageButton12" runat="server" 
        ImageUrl="~/Images/edit_project_bt.gif" style="height: 19px; margin-left:20px;" 
        onclick="ImageButton12_Click" />&nbsp;&nbsp;
            <a href="" style="float: right;" onclick="OpenWindow(this.href, 'MyWindow')"><img src="../Images/project_summary.gif" /></a>
                <%--<asp:ImageButton ID="ImageButton15" runat="server" 
                    ImageUrl="~/Images/project_summary.gif" />--%>
            </td>
            
        </tr>
    </table>
</div>

 <script language="javascript" type="text/javascript">
     function OpenWindow(url, WndName) {
         var leftPos = (screen.width / 2) - 200;
         var topPos = (screen.height / 2) - 100;
         window.open("../ProjectSummary/ProjectSummary.aspx", WndName, "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no,directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=600, height=550");
     }
     function PageRefresh() {
         __doPostBack('btnRefresh', '');
     }
    </script>
</asp:Content>
