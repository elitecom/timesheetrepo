﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="ProjectNew.aspx.cs" Inherits="DrishProject.WebForm5" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <div>
        <b>
            <asp:Label ID="Label1" runat="server" Text="Add New Project"></asp:Label>
        <br />
        </b>       
    </div>
    <div>
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/search_bt.gif"
            OnClick="ImageButton1_Click" />
        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/add_active.gif" />
        <br />
    </div>
    <div>
        <asp:Label ID="Label2" runat="server" Text="Project Name"></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server" Style="margin-left: 70px;"></asp:TextBox>
        
    </div>
    <div>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Project Code"></asp:Label>
        <asp:TextBox ID="TextBox2" runat="server" Style="margin-left: 73px;"></asp:TextBox>
    </div>
    <div>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Project Category"></asp:Label>
        <asp:DropDownList ID="DropDownList1" runat="server" Style="margin-left: 50px;" Height="17px"
            Width="161px">
        </asp:DropDownList>
    </div>
    <div>
        <br />
        <asp:Label ID="Label5" runat="server" Text="Project Type"></asp:Label>
        <asp:DropDownList ID="DropDownList2" runat="server" Style="margin-left: 75px;" Height="18px"
            Width="160px">
        </asp:DropDownList>
    </div>
    <div>
        <br />
        <asp:Label ID="Label6" runat="server" Text="Project Level"></asp:Label>
        <asp:DropDownList ID="DropDownList3" runat="server" Style="margin-left: 72px;" Height="20px"
            Width="159px">
        </asp:DropDownList>
    </div>
    <div>
        <br />
        <asp:Label ID="Label7" runat="server" Text="Project Mail Id"></asp:Label>
        <asp:TextBox ID="TextBox3" runat="server" Style="margin-left: 60px;"></asp:TextBox>
    </div>
    <div>
        <br />
        <asp:Label ID="Label8" runat="server" Text="Bug Tracking Setup"></asp:Label>
        <asp:CheckBox ID="CheckBox1" runat="server" Style="margin-left: 30px;" />
    </div>
    <div>
        <br />
        <asp:Label ID="Label9" runat="server" Text="Project Status"></asp:Label>
        <asp:DropDownList ID="DropDownList4" runat="server" Style="margin-left: 72px;" Height="16px"
            Width="160px">
        </asp:DropDownList>
    </div>
    <div>
        <br />
         <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
        <asp:Label ID="Label11" runat="server" Text="Project Start Date"></asp:Label>
        <asp:TextBox ID="TextBox4" runat="server" Style="margin-left: 45px;"></asp:TextBox>
        <asp:CalendarExtender ID="TextBox4_CalendarExtender" runat="server" 
            BehaviorID="TextBox4_CalendarExtender" TargetControlID="TextBox4" Format="dd/MM/yyyy" />
            </ContentTemplate>
            </asp:UpdatePanel>
    </div>
    <div>
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
        <asp:Label ID="Label12" runat="server" Text="Project End Date"></asp:Label>
        <asp:TextBox ID="TextBox5" runat="server" Style="margin-left: 49px;"></asp:TextBox>
        <asp:CalendarExtender ID="TextBox5_CalendarExtender" runat="server" 
            BehaviorID="TextBox5_CalendarExtender" TargetControlID="TextBox5" Format="dd/MM/yyyy" />
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
        <br />
        <asp:Label ID="Label13" runat="server" Text="Project Plan"></asp:Label>
        <asp:CheckBox ID="CheckBox2" runat="server" Style="margin-left: 75px;" />
    </div>
    <div style="margin-top: 20px;">
        <asp:ImageButton ID="Button1" runat="server" ImageUrl="~/Images/save_bt.gif" Style="margin-left: 150px;" OnClick="Button1_Click" />
        <asp:ImageButton ID="ImageButton3" runat="server" 
            ImageUrl="~/Images/reset_bt.gif" Style="margin-left: 30px;" 
            onclick="ImageButton3_Click" />       
    </div>
    
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>
    

    <script language="javascript" type="text/javascript">
        $(function () {
            $("._datepicker").datepicker({ dateFormat: 'dd/mm/yy' });
        });

    </script>
</asp:Content>
