﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="ProjectSearch.aspx.cs" Inherits="DrishProject.WebForm7" %>

<%@ Register Src="../UserControls/UC_ColorPane.ascx" TagName="UC_ColorPane" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <br />
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/search_active.gif" />
        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/add_bt.gif"
            OnClick="ImageButton2_Click" />
    </div>
    <div style="background-color: #Orange">
        <asp:Label ID="lblProjectType" runat="server" Text="Project Type"></asp:Label>
        <asp:DropDownList ID="DropDownList1" runat="server" Style="margin-left: 20px;">
        </asp:DropDownList>
        <asp:Label ID="Label1" runat="server" Text="Project Status" Style="margin-left: 70px;"></asp:Label>
        <asp:DropDownList ID="DropDownList2" runat="server" Style="margin-left: 20px;" Height="18px"
            Width="160px">
        </asp:DropDownList>
        <asp:Label ID="Label2" runat="server" Text="Project Name" Style="margin-left: 80px;"></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server" Style="margin-left: 20px;"></asp:TextBox>
        <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/searc_bt.gif"
            Style="margin-left: 50px; vertical-align: bottom;" OnClick="ImageButton3_Click" />
        <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/display_all_bt.gif"
            Style="margin-left: 10px; vertical-align: bottom;" OnClick="ImageButton4_Click" />
    </div>
    <div style="height: 500px;">
        <br />
        <asp:GridView ID="GridView1" AutoGenerateColumns="False" DataKeyNames="ProjectCode"
            runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px"
            CellPadding="3" Width="1326px" CellSpacing="2" OnDataBound="GridView1_DataBound"
            OnRowDataBound="GridView1_RowDataBound">
            <Columns>
                <asp:HyperLinkField HeaderText="Project Code" DataNavigateUrlFields="ProjectID" DataNavigateUrlFormatString="ProjectDetail.aspx?ProjectID={0}"
                    DataTextField="ProjectCode">
                    <ItemStyle Width="90px" />
                </asp:HyperLinkField>
                <asp:BoundField HeaderText="Project Name" DataField="Projectname" />
                <asp:BoundField HeaderText="Project Type" DataField="ProjectType" />
                <asp:BoundField HeaderText="Status" DataField="ProjectStatus" />
                <asp:BoundField HeaderText="Start Date" DataField="Proj_StartDate" DataFormatString="{0:dd-MM-yyyy}">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField HeaderText="End Date" DataField="Proj_EndDate" DataFormatString="{0:dd-MM-yyyy}">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Team">
                    <HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <img id="imgDetails" title='<%# Eval("ProjectCode")%> - Team' alt='<%# Eval("ProjectCode")%> - Team'
                            src="../Images/member_icon.gif" style="cursor: pointer" onclick='ProjectTeamMember(<%# Eval("ProjectID")%>);'>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Module">
                    <HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <img id="imgDetails" title='<%# Eval("ProjectCode")%> - Team' alt='<%# Eval("ProjectCode")%> - Team'
                            src="../Images/module_icon.gif" style="cursor: pointer" onclick='ProjectModuleFunc(<%# Eval("ProjectID")%>);'>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tasks">
                    <HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <img id="imgDetails" title='<%# Eval("ProjectCode")%> - Team' alt='<%# Eval("ProjectCode")%> - Team'
                            src="../Images/book2.gif" style="cursor: pointer" onclick='ProjectTasks(<%# Eval("ProjectID")%>);'>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Documents">
                    <HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <img id="imgDetails" title='<%# Eval("ProjectCode")%> - Team' alt='<%# Eval("ProjectCode")%> - Team'
                            src="../Images/summary_img.gif" style="cursor: pointer" onclick='ProjectDocuments(<%# Eval("ProjectID")%>);'>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Release">
                    <HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <img id="imgDetails" title='<%# Eval("ProjectCode")%> - Team' alt='<%# Eval("ProjectCode")%> - Team'
                            src="../Images/member_icon.gif" style="cursor: pointer" onclick='ProjectReleases(<%# Eval("ProjectID")%>);'>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="History">
                    <HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <img id="imgDetails" title='<%# Eval("ProjectCode")%> - Team' alt='<%# Eval("ProjectCode")%> - Team'
                            src="../Images/project_work_plan_icon3.gif" style="cursor: pointer" onclick='ProjectHistory(<%# Eval("ProjectID")%>);'>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#FFF1D4" />
            <SortedAscendingHeaderStyle BackColor="#B95C30" />
            <SortedDescendingCellStyle BackColor="#F1E5CE" />
            <SortedDescendingHeaderStyle BackColor="#93451F" />
        </asp:GridView>
        <%-- <div style="text-align:center; margin-left:50px;vertical-align:baseline;">
        <uc1:UC_ColorPane ID="UC_ColorPane1" runat="server" />
        </div>--%>
        <asp:ImageButton ID="btnExport" runat="server" ImageUrl="../Images/exp_excel_ico.gif"
            Style="width: 30px; height: 16px; margin-left: 1260px;" OnClick="btnExport_Click" />
        <br />
    </div>
    <script language="javascript" type="text/javascript">
        function ProjectTeamMember(ID) {
            window.location.href = "/TeamMembers/ProjectTeamMembers.aspx?ProjectID=" + ID;
        }
        function ProjectModuleFunc(ID) {
            window.location.href = "/ProjectModules/Module.aspx?ProjectID=" + ID;
        }
        function ProjectTasks(ID) {
            window.location.href = "/Tasks/Tasks.aspx?ProjectID=" + ID;
        }
        function ProjectDocuments(ID) {
            window.location.href = "/Documents/Documents.aspx?ProjectID=" + ID;
        }
        function ProjectReleases(ID) {
            window.location.href = "Releases.aspx?ProjectID=" + ID;
        }
        function ProjectHistory(ID) {
            window.location.href = "Project_History.aspx?ProjectID=" + ID;
        }
        function ProjectEditFunc(ID) {
            var leftPos = (screen.width / 2) - 450;
            var topPos = (screen.height / 2) - 250;
            var strFeature = "toolbar=no, left=" + leftPos + ", top=" + topPos + ", location=no, addressbar=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=900, height=500"
            window.open("Projectedit.aspx?ProjectId=" + ID, "ProjectEdit", strFeature);
        }
        function PageRefresh() {
            __doPostBack('btnRefresh', '');
        }
    </script>
</asp:Content>
