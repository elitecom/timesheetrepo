﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using DrishProjectBAL;
using Entities;

namespace DrishProject
{
    public partial class Login : System.Web.UI.Page
    {
        Entities.Employee emp = new Entities.Employee();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["Signout"] == "Yes")
                    {
                        Session.Abandon();
                        FormsAuthentication.SignOut();
                        LblMessage.Text = "Successfully Logout!";
                    }
                    if (Request.QueryString["Authority"] == "Deny")
                    {
                        Session.Abandon();
                        FormsAuthentication.SignOut();
                        LblMessage.Text = "Access Denied!";
                    }
                    SetCookie();
                    lblVersion.Text = System.Configuration.ConfigurationManager.AppSettings["Version"].ToString();
                    hlAdmin.NavigateUrl = "mailto:" + System.Configuration.ConfigurationManager.AppSettings["AdminHRMail"].ToString() + "?subject=New Account on PMSTool";
                }
            }
            catch (Exception ee)
            {
                LblMessage.Text = ee.Message;
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            bool res;
            emp = new Entities.Employee();
            try
            {
                emp.UserName = TextBox1.Text;
                emp.Password = TextBox2.Text;
                res = new EmployeeBAL().AuthenticateUser(emp);
                if (res == true)
                {
                    emp = new EmployeeBAL().GetProfileDetails(emp);
                    FormsAuthentication.Authenticate(emp.UserName, emp.Password);
                    FormsAuthentication.RedirectFromLoginPage(emp.UserName, false);
                    Session["emp"] = emp;
                    if (chkSaveLogin.Checked == true)
                    {
                        HttpCookie objUserCookie = new HttpCookie("PMS_USERCookie", emp.UserName);
                        objUserCookie.Expires = DateTime.MaxValue;
                        Response.Cookies.Add(objUserCookie);
                        HttpCookie objPASSCookie = new HttpCookie("PMS_PASScookie", emp.Password);
                        objPASSCookie.Expires = DateTime.MaxValue;
                        Response.Cookies.Add(objPASSCookie);
                    }
                    else
                    {
                        if (Request.Cookies["PMS_USERCookie"] != null)
                        {
                            HttpCookie objUserCookie = new HttpCookie("PMS_USERCookie", emp.UserName);
                            objUserCookie.Expires = Convert.ToDateTime("1/1/1900");
                            Response.Cookies.Add(objUserCookie);
                        }
                    }
                    if (Request["ReturnUrl"] == null) 
                    {
                        DataSet ds = new DataSet();
                        ds = new EmployeeBAL().GetEmployeeRoles(emp);
                        if (emp.IsAministrator == 1 || emp.IsReportingManager == 1)
                        {
                            Response.Redirect("~\\Projects\\ProjectSearch.aspx", false);
                        }
                        else
                        {
                            Response.Redirect("~\\Tasks\\MyTasks.aspx", false);
                        }
                    }
                }
                else
                {
                    LblMessage.Text = "<b>Login Failed Try Again!</b>";
                }
            }
            catch (Exception ee)
            {
                Response.Write("Message : " + ee.Message + "\n" + ee.StackTrace);
            }
        }

        private void SetCookie()
        {
            try
            {
                string strUser2;
                string strPass;
                if (Request.Cookies["PMS_USERCookie"].Value.ToString() != null)
                {
                    strUser2 = Request.Cookies["PMS_USERCookie"].Value;
                    strPass = Request.Cookies["PMS_PASScookie"].Value;
                    if (strUser2 != "")
                    {
                        TextBox1.Text = strUser2;
                        string strScript;
                        strScript = "<script language = javascript> var elem = document.getElementById('TextBox2');elem.value = '" + strPass + "';</script>";
                        RegisterStartupScript("Startup", strScript);
                        TextBox2.Text = strPass;
                        chkSaveLogin.Checked = true;
                    }
                }
            }
            catch (Exception)
            {               
            }
        }
    }
}