﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectDAL
{
    public class HistoryDAL
    {
        public DataSet GetSelectedTaskHistory(int ProjectID, int TaskID)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            string strSql = string.Empty;
            try
            {
                strSql = "";
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
