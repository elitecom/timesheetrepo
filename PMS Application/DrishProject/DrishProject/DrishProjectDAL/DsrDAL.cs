﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectDAL
{
    public class DsrDAL
    {
        public bool SaveSentDSRContents(DSR dsr)
        {
            bool res = false;
            SqlCommand cmd;
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string strSql = string.Empty;
            DataSet ds;
            strSql = "select tid,projectid,moduleid,activity,HrsSpent,WorkDone from SCT_tblTimesheets where tid in (select tid from Sct_tblTimeSheets where EmpID=" + dsr.EmpID + " and Tday=" + dsr.tDate.Day + " and tMonth=" + dsr.tDate.Month + " and tYear=" + dsr.tDate.Year + ")";
            adpt = new SqlDataAdapter(strSql, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = ds.Tables[0].Rows[i];
                    DSR temp = new DSR();
                    temp.tID = Convert.ToInt32(dr["tid"].ToString());
                    temp.ProjectID = Convert.ToInt32(dr["projectId"].ToString());
                    temp.ModuleID = Convert.ToInt32(dr["moduleId"].ToString());
                    temp.ActivityID = Convert.ToInt32(dr["Activity"].ToString());
                    temp.HrsSpent = Convert.ToDecimal(dr["HrsSpent"].ToString());
                    temp.WorkDone = dr["WorkDone"].ToString();
                    cmd = new SqlCommand();
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "SCT_spInsertSentDSR";
                        cmd.Connection = cn;
                        cmd.Parameters.AddWithValue("@tid", temp.tID);
                        cmd.Parameters.AddWithValue("@projectId", temp.ProjectID);
                        cmd.Parameters.AddWithValue("@moduleId", temp.ModuleID);
                        cmd.Parameters.AddWithValue("@activityId", temp.ActivityID);
                        cmd.Parameters.AddWithValue("@hrsSpent", temp.HrsSpent);
                        cmd.Parameters.AddWithValue("@workDone", temp.WorkDone);
                        cn.Open();
                        cmd.ExecuteNonQuery();
                        res = true;
                    }
                    catch (Exception)
                    {
                        res = false;
                    }
                    finally
                    {
                        if (cn.State == ConnectionState.Open)
                        {
                            cn.Close();
                        }
                        cmd.Dispose();
                    }
                }
            }
            return res;
        }

        public bool SaveDSRContents(DSR dsr)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spInsertDailyDSR";
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue("@vMonth", dsr.tMonth);
                cmd.Parameters.AddWithValue("@vDay", dsr.tDay);
                cmd.Parameters.AddWithValue("@vYear", dsr.tYear);
                cmd.Parameters.AddWithValue("@vdate", dsr.tDate);
                cmd.Parameters.AddWithValue("@vEmpID", dsr.EmpID);
                cmd.Parameters.AddWithValue("@vProjectID", dsr.ProjectID);
                cmd.Parameters.AddWithValue("@vModuleID", dsr.ModuleID);
                cmd.Parameters.AddWithValue("@vHrsSpent", dsr.HrsSpent);
                cmd.Parameters.AddWithValue("@vWorkDone", dsr.WorkDone);
                cmd.Parameters.AddWithValue("@vDSRSent", 0);
                cmd.Parameters.AddWithValue("@vActivity", dsr.ActivityID);
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
            return true;
        }

        public DataSet GetDSRContentsForDate(DSR dsr)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string strSql = string.Empty;
            int dd, mm, yy;
            DataSet ds;
            try
            {
                dd = dsr.tDate.Day;
                mm = dsr.tDate.Month;
                yy = dsr.tDate.Year;

                dsr.tDay = dd;
                dsr.tMonth = mm;
                dsr.tYear = yy;

                strSql = "SELECT SCT_tblTimesheets.tID,SCT_tblTimesheets.EmpID, SCT_tblProjects.ProjectCode, SCT_tblProjectModules.ModuleName, SCT_lkpActivity.ActivityName, SCT_tblTimesheets.HrsSpent, SCT_tblTimesheets.WorkDone, SCT_tblTimesheets.tDay, SCT_tblTimesheets.tMonth, SCT_tblTimesheets.tYear, ";
                strSql += " SCT_tblTimesheets.tDate FROM SCT_tblTimesheets INNER JOIN SCT_tblProjects ON SCT_tblTimesheets.ProjectID = SCT_tblProjects.ProjectID INNER JOIN SCT_tblProjectModules ON SCT_tblTimesheets.ModuleID = SCT_tblProjectModules.ModuleID INNER JOIN ";
                strSql += " SCT_lkpActivity ON SCT_tblTimesheets.Activity = SCT_lkpActivity.ActivityID where EmpID=" + dsr.EmpID + " and tDay=" + dsr.tDay + " and tMonth=" + dsr.tMonth + " and tYear=" + dsr.tYear;
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool SendFinalDSR(DSR dsr)
        {
            return true;
        }
        public DataSet GetMonthSummaryV2(int EmpID, int tMonth, int tYear)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet getPreviousDates()
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public string GetPreviousTime(int EmpId, int ProjectId, DateTime FromDate)
        {
            return "";
        }
        public decimal GetPreviousTotal(int EmpId, DateTime FromDate)
        {
            return 0;
        }
        public string GetProjectCode(int projectid)
        {
            return "";
        }
        public Project GetProjectInfo(int ProjectID, DateTime FromDate)
        {
            Project Proj = new Project();
            return Proj;
        }
        public string getProjectStage(int ProjectID)
        {
            return "";
        }
        public DataSet GetProjectSummary(int EmpID, DateTime FromDate, DateTime ToDate)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetProjectSummaryV2(int EmpID)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetProjectSummaryV2(int EmpID, DateTime FromDate, DateTime ToDate)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DSR GetProjTimeSheet(int ProjectId)
        {
            DSR dsr = new DSR();
            return dsr;
        }
        public DSR GetProjTimeSheetV2(int ProjectId)
        {
            DSR dsr = new DSR();
            return dsr;
        }
        public int getQANum(int ProjectID, DateTime FromDate, DateTime Todate)
        {
            return 0;
        }
        public DataSet getReleaseCodes(int ProjectID, DateTime FromDate, DateTime Todate)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetResourceHours(int ProjectID, int EmpId, DateTime FromDate, DateTime Todate)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public int GetSameProject(int projectid, int moduleid, int activityid, int EmpID, DateTime dDate)
        {
            return 0;
        }
        public DataSet GetSearchedTimesheets(int tYear, int tMonth, string ProjectCode)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetSearchedTimesheets(int EmpID, int tYear, int tMonth, string ProjectCode)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetSearchedTimesheetsV2(int tYear, int tMonth, string ProjectCode)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetSearchedTimesheetsV2(int EmpID, int tYear, int tMonth, string ProjectCode)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetSearchedTimesheetV2(int tYear, int tMonth, string ProjectCode)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetSearchedTimesheetV2(int EmpID, int tYear, int tMonth, string ProjectCode)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public string getTeamLead(int ProjectID)
        {
            return "";
        }
        public DataSet GetTimesheetEmployeeSummary(int ProjectID, int tMonth, int tYear)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetTimesheetEmployeeSummaryV2(int ProjectID, int tMonth, int tYear)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetTimesheets()
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetTimesheets(int EmpID)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetTimesheetSummary()
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetTimesheetSummary(int EmpID)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetTimesheetSummaryV2()
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetTimesheetSummaryV2(int EmpID)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string strSql = string.Empty;
            strSql += "SELECT SCT_tblProjects.ProjectName, sum(SCT_tblTimesheets.HrsSpent) as TotalHours FROM SCT_tblProjects INNER JOIN ";
            strSql += " SCT_tblTimesheets ON SCT_tblProjects.ProjectID = SCT_tblTimesheets.ProjectID where SCT_tblTimesheets.EmpID= " + EmpID;
            strSql += " group by SCT_tblProjects.ProjectName";
            adpt = new SqlDataAdapter(strSql, cn);
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }
        public DataSet GetTimesheetV2()
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet GetTimesheetV2(int EmpID)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string strSql = string.Empty;
            strSql += "SELECT SCT_tblProjects.ProjectName, sum(SCT_tblTimesheets.HrsSpent) as TotalHours FROM SCT_tblProjects INNER JOIN ";
            strSql += " SCT_tblTimesheets ON SCT_tblProjects.ProjectID = SCT_tblTimesheets.ProjectID where SCT_tblTimesheets.EmpID= " + EmpID;
            strSql += " group by SCT_tblProjects.ProjectName";
            adpt = new SqlDataAdapter(strSql, cn);
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }
        public decimal GetTimeSpent(int pintProjectId, int pintModuleId, int pintEmpId)
        {
            return 0;
        }
        public DataSet GetTodayDSR(int EmpID, DateTime dDate)
        {
            int dd, mm, yy;
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string qstr = string.Empty;//initialization is compulsory

            DataSet ds;
            try
            {
                dd = dDate.Day;
                mm = dDate.Month;
                yy = dDate.Year;
                qstr += "SELECT   SCT_tblProjects.ProjectID,SCT_tblProjects.ProjectCode, SCT_tblProjects.ProjectName, SCT_tblProjectModules.ModuleName, SCT_tblTimesheets.Activity, SCT_lkpActivity.ActivityName, SCT_tblTimesheets.HrsSpent, ";
                qstr += " SCT_tblTimesheets.WorkDone, SCT_tblTimesheets.tDay, SCT_tblTimesheets.tMonth, SCT_tblTimesheets.tYear, ";
                qstr += " SCT_tblTimesheets.EmpID, SCT_tblTimesheets.tID FROM SCT_tblProjectModules INNER JOIN SCT_lkpActivity INNER JOIN ";
                qstr += " SCT_tblTimesheets ON SCT_lkpActivity.ActivityID = SCT_tblTimesheets.Activity INNER JOIN ";
                qstr += " SCT_tblProjects ON SCT_tblTimesheets.ProjectID = SCT_tblProjects.ProjectID ON ";
                qstr += " SCT_tblProjectModules.ProjectID = SCT_tblProjects.ProjectID AND SCT_tblProjectModules.ModuleID = SCT_tblTimesheets.ModuleID ";
                qstr += " where Empid=" + EmpID + " and tday=" + dd + " and tmonth=" + mm + " and tyear=" + yy;
                adpt = new SqlDataAdapter(qstr, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cn.Dispose();
            }
        }

        public DataSet GetTodayDSRHours(int EmpID, DateTime dDate)
        {
            int dd, mm, yy;
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string qstr = string.Empty; //initialization is compulsory

            DataSet ds;
            try
            {
                dd = dDate.Day;
                mm = dDate.Month;
                yy = dDate.Year;

                qstr += "SELECT sum(SCT_tblTimesheets.HrsSpent) as TotalHours FROM SCT_lkpActivity INNER JOIN SCT_tblTimesheets ON SCT_lkpActivity.ActivityID = SCT_tblTimesheets.Activity INNER JOIN SCT_tblTimesheetV2 ON SCT_tblTimesheets.tID = SCT_tblTimesheetV2.tID INNER JOIN ";
                qstr += " SCT_tblDSRHistory ON SCT_tblTimesheetV2.tID = SCT_tblDSRHistory.tID AND SCT_tblTimesheets.ProjectID = SCT_tblDSRHistory.ProjectID INNER JOIN SCT_tblProjects ON SCT_tblDSRHistory.ProjectID = SCT_tblProjects.ProjectID INNER JOIN ";
                qstr += " SCT_tblProjectModules ON SCT_tblDSRHistory.ModuleID = SCT_tblProjectModules.ModuleID AND  SCT_tblProjects.ProjectID = SCT_tblProjectModules.ProjectID where SCT_tblTimesheetV2.tDay=" + dd + " and SCT_tblTimesheetV2.tMonth=" + mm + " and SCT_tblTimesheetV2.tYear=" + yy + " and SCT_tblTimesheetV2.EmpID=" + EmpID;
                adpt = new SqlDataAdapter(qstr, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cn.Dispose();
            }
        }

        public decimal getTotalHours(int EmpId, DateTime FromDate, DateTime Todate)
        {
            return 0;
        }

        public string GetTotalProjectHrs(int ProjectID, int tMonth, int tYear)
        {
            return "";
        }

        public DataSet GetValuesToEdit(int DSRHistID)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            string strSql = string.Empty;
            strSql += "SELECT SCT_tblProjects.ProjectID,SCT_tblProjects.ProjectName, SCT_tblProjectModules.ModuleName, SCT_tblTimesheets.Activity, SCT_lkpActivity.ActivityName, SCT_tblTimesheets.HrsSpent,";
            strSql += " SCT_tblTimesheets.WorkDone, SCT_tblTimesheets.tDay, SCT_tblTimesheets.tMonth, SCT_tblTimesheets.tYear, SCT_tblTimesheets.EmpID, SCT_tblTimesheets.tID,SCT_tblTimesheets.ModuleID FROM SCT_tblProjectModules INNER JOIN SCT_lkpActivity INNER JOIN ";
            strSql += " SCT_tblTimesheets ON SCT_lkpActivity.ActivityID = SCT_tblTimesheets.Activity INNER JOIN SCT_tblProjects ON SCT_tblTimesheets.ProjectID = SCT_tblProjects.ProjectID ON ";
            strSql += " SCT_tblProjectModules.ProjectID = SCT_tblProjects.ProjectID AND SCT_tblProjectModules.ModuleID = SCT_tblTimesheets.ModuleID where tid=" + DSRHistID;
            adpt = new SqlDataAdapter(strSql, cn);
            adpt.Fill(ds);
            return ds;
        }

        public bool InsertMonth(int EmpID)
        {
            return true;
        }

        public bool InsertMonthV2(int EmpID)
        {
            return true;
        }

        public bool saveDSR(int EmpID, int ProjectID, int ModuleID, double HrsSpent, string workdone, DateTime dDate, DateTime dtModifiedOn)
        {
            return true;
        }

        public string saveDSRV2(int EmpID, int ProjectID, int ModuleID, int ActivityID, double HrsSpent, string workdone, DateTime dDate, DateTime dtModifiedOn)
        {
            return "";
        }

        public bool ToCheckIfDSRSentV2(int EmpID, DateTime dDate)
        {
            int dd, mm, yy;
            string strSql = string.Empty;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            try
            {
                dd = dDate.Day;
                mm = dDate.Month;
                yy = dDate.Year;
                strSql = "select count(*) from Sct_tblTimeSheets where empid=" + EmpID + " and tday=" + dd + " and tMonth=" + mm + " and tYear=" + yy + " and IfDSrSent=0";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.CommandText = strSql;
                cn.Open();
                int res = Convert.ToInt32(cmd.ExecuteScalar());
                if (res > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public int update(DateTime dDate, int empID, int DSRSent)
        {
            return 0;
        }

        public int UpdateDSRGrid(int ProjectID, int ModuleID, int ActivityID, double HrsSpent, string workdone, DateTime dtModifiedOn, int DSRHistID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new DataConn().GetConn();
            int ctr = -1;
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.CommandText = "SCT_spUpdateTodaysTimesheet";
                cn.Open();
                cmd.Parameters.AddWithValue("@ProjectID", ProjectID);
                cmd.Parameters.AddWithValue("@ModuleID", ModuleID);
                cmd.Parameters.AddWithValue("@ActivityID", ActivityID);
                cmd.Parameters.AddWithValue("@HrsSpent", HrsSpent);
                cmd.Parameters.AddWithValue("@workdone", workdone);
                cmd.Parameters.AddWithValue("@tID", DSRHistID);
                ctr = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {

            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
            return ctr;
        }

        public bool UpdateIfDSRSent(int EmpID, DateTime tDate)
        {
            return true;
        }

        public int UpdateIFSendDSR(DateTime dDate, int empID)
        {
            return 0;
        }

        public bool UpdateTimeAllocated(int ProjectID, int EmpId, DateTime FromDate, string Time)
        {
            return true;
        }

        public bool checkIfPresentNew(int EmpID, DateTime date)
        {
            SqlDataAdapter adpt;
            int dd, mm, yy;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds;
            string strSQL = string.Empty;
            try
            {
                dd = date.Day;
                mm = date.Month;
                yy = date.Year;
                strSQL = "select * from SCT_tblTimesheets where Empid=" + EmpID + "  and tMonth=" + mm + " and tDay =" + dd + " and tYear =" + yy;
                adpt = new SqlDataAdapter(strSQL, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool checkIfPresent(int EmployeeId, DateTime date)
        {
            SqlDataAdapter adpt;
            int dd, mm, yy;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds;
            string strSQL = string.Empty;
            try
            {
                dd = date.Day;
                mm = date.Month;
                yy = date.Year;
                strSQL = "select * from SCT_tblTimesheets where Empid=" + EmployeeId + "  and tMonth=" + mm + " and tDay =" + dd + " and tYear =" + yy;
                adpt = new SqlDataAdapter(strSQL, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool checkIfDSRSentNew(int EmpID, DateTime date)
        {
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds;
            string strSQL = string.Empty;
            try
            {
                strSQL = "select * from SCT_tblDSRHistory where tid in (select tid from Sct_tblTimeSheets where EmpID=" + EmpID + " and Tday=" + date.Day + " and tMonth=" + date.Month + " and tYear=" + date.Year + ")";
                adpt = new SqlDataAdapter(strSQL, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Get Old DSR Values
        public DataSet GetOldDSR(int EmpID, DateTime date)
        {
            int dd, mm, yy;
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds;
            string strSQL = string.Empty;
            try
            {
                dd = date.Day;
                mm = date.Month;
                yy = date.Year;

                strSQL += "SELECT SCT_tblProjects.ProjectCode, SCT_tblProjectModules.ModuleName, SCT_lkpActivity.ActivityName, SCT_tblTimesheets.HrsSpent,";
                strSQL += " SCT_tblTimesheets.WorkDone, SCT_tblTimesheets.tDay, SCT_tblTimesheets.tMonth, SCT_tblTimesheets.tYear, ";
                strSQL += " SCT_tblTimesheets.EmpID, SCT_tblTimesheets.tID FROM SCT_tblProjectModules INNER JOIN SCT_lkpActivity INNER JOIN ";
                strSQL += " SCT_tblTimesheets ON SCT_lkpActivity.ActivityID = SCT_tblTimesheets.Activity INNER JOIN ";
                strSQL += " SCT_tblProjects ON SCT_tblTimesheets.ProjectID = SCT_tblProjects.ProjectID ON SCT_tblProjectModules.ProjectID = SCT_tblProjects.ProjectID AND SCT_tblProjectModules.ModuleID = SCT_tblTimesheets.ModuleID ";
                strSQL += " where Empid=" + EmpID + " and tday=" + dd + " and tmonth=" + mm + " and tyear=" + yy;
                adpt = new SqlDataAdapter(strSQL, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet GetActivities()
        {
            SqlDataAdapter adpt;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            adpt = new SqlDataAdapter("select * from SCT_lkpActivity", cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

        public DataSet getDSNewModule(int projectID)
        {
            SqlDataAdapter adpt;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            adpt = new SqlDataAdapter("select * from SCT_tblProjectModules where ProjectID=" + projectID, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

        public int DeleteDSRNew(int dsrId)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.CommandText = "SCT_spDeleteTodaysTimesheetEntry";
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cn.Open();
                cmd.Parameters.AddWithValue("@tId", dsrId);
                cmd.ExecuteNonQuery();
                return 2;
            }
            catch (Exception)
            {
                return 0;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public DataSet GetCompleteDSR(int UserID, DateTime ClndMonth)
        {
            return null;
        }

        public DataSet GetModulesToEdit(string ProjName)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt = new SqlDataAdapter("select * from SCT_tblProjectModules where projectID = (select projectID from Sct_tblProjects where projectname='" + ProjName + "')", cn);
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

        public DataSet getDSRV2(int EmpID, DateTime date)
        {
            int dd, mm, yy;
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds;
            string strSQL = string.Empty;
            try
            {
                dd = date.Day;
                mm = date.Month;
                yy = date.Year;


                strSQL += "SELECT SCT_tblProjects.ProjectID,SCT_tblProjects.ProjectCode, SCT_tblProjectModules.ModuleName, SCT_lkpActivity.ActivityName, SCT_tblTimesheets.HrsSpent,";
                strSQL += " SCT_tblTimesheets.WorkDone, SCT_tblTimesheets.tDay, SCT_tblTimesheets.tMonth, SCT_tblTimesheets.tYear, ";
                strSQL += " SCT_tblTimesheets.EmpID, SCT_tblTimesheets.tID FROM SCT_tblProjectModules INNER JOIN SCT_lkpActivity INNER JOIN ";
                strSQL += " SCT_tblTimesheets ON SCT_lkpActivity.ActivityID = SCT_tblTimesheets.Activity INNER JOIN ";
                strSQL += " SCT_tblProjects ON SCT_tblTimesheets.ProjectID = SCT_tblProjects.ProjectID ON SCT_tblProjectModules.ProjectID = SCT_tblProjects.ProjectID AND SCT_tblProjectModules.ModuleID = SCT_tblTimesheets.ModuleID ";
                strSQL += " where Empid=" + EmpID + " and tday=" + dd + " and tmonth=" + mm + " and tyear=" + yy;
                adpt = new SqlDataAdapter(strSQL, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet getDSRNewModule(int iProjectID)
        {
            return null;
        }

        public DataSet GetDSRMonths(int empId)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            string strSql = string.Empty;
            try
            {
                strSql = "select * from SCT_lkpMonths";
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet GetDSRModule(int employeeId, DateTime selectedDate, int projectid)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            int dd, mm, yy;
            string strSql = string.Empty;
            try
            {
                dd = selectedDate.Day;
                mm = selectedDate.Month;
                yy = selectedDate.Year;
                strSql = "select * from SCT_tblTimesheets where EmpID=" + employeeId + " and ProjectID=" + projectid + " and tday=" + dd + " and tMonth=" + mm + " and tYear=" + yy + " order by tID";
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool DeleteDSR(int employeeId, int projectid, int moduleid, DateTime selectedDate)
        {
            SqlCommand cmd;
            SqlConnection cn = new DataConn().GetConn();
            int dd, mm, yy;
            string strSql = string.Empty;
            try
            {
                dd = selectedDate.Day;
                mm = selectedDate.Month;
                yy = selectedDate.Year;

                cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.CommandText = "delete from SCT_tblDSRHistory where tid in (select * from SCT_tblTimesheets where EmpID=@employeeId and ProjectID=@projectid  and tday=@dd and tMonth=@mm and tYear=@yy)";
                cmd.Parameters.AddWithValue("@employeeId", employeeId);
                cmd.Parameters.AddWithValue("@projectid", projectid);
                cmd.Parameters.AddWithValue("@dd", dd);
                cmd.Parameters.AddWithValue("@mm", mm);
                cmd.Parameters.AddWithValue("@yy", yy);
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
                cmd.Dispose();

                cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.CommandText = " delete from SCT_tblTimesheetV2 where tid in (select * from SCT_tblTimesheets where EmpID=@employeeId and ProjectID=@projectid  and tday=@dd and tMonth=@mm and tYear=@yy)";
                cmd.Parameters.AddWithValue("@employeeId", employeeId);
                cmd.Parameters.AddWithValue("@projectid", projectid);
                cmd.Parameters.AddWithValue("@dd", dd);
                cmd.Parameters.AddWithValue("@mm", mm);
                cmd.Parameters.AddWithValue("@yy", yy);
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
                cmd.Dispose();

                cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.CommandText = "delete from SCT_tblTimesheets where tid in (select * from SCT_tblTimesheets where EmpID=@employeeId and ProjectID=@projectid  and tday=@dd and tMonth=@mm and tYear=@yy)";
                cmd.Parameters.AddWithValue("@employeeId", employeeId);
                cmd.Parameters.AddWithValue("@projectid", projectid);
                cmd.Parameters.AddWithValue("@dd", dd);
                cmd.Parameters.AddWithValue("@mm", mm);
                cmd.Parameters.AddWithValue("@yy", yy);
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
                cmd.Dispose();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public DataSet getDSRModuleOld(int employeeId, DateTime selectedDate, int projectid)
        {
            DataSet ds = new DataSet();
            return ds;
        }

        public DataSet getDSRModule(int employeeId, DateTime selectedDate, int projectid)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet getDSR(int employeeId, DateTime selectedDate)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public DataSet getDSROldAvailable(int employeeId, DateTime selectedDate)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        // public bool UpdateIfDSRSent(int employeeId, DateTime time)
        //{
        //    return true;
        //}
    }
}
