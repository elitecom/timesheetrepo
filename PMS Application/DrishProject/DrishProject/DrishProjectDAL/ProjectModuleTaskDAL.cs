﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectDAL
{
    public class ProjectModuleTaskDAL
    {
        public DataSet GetStatusList()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string strSql = string.Empty;
            DataSet ds;
            try
            {
                strSql = "SELECT * FROM SCT_lkpmodulestatus ORDER BY Priority";
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds, "SCT_lkpmodulestatus");
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet GetHideStatusList()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string strSql = string.Empty;
            DataSet ds;
            try
            {
                strSql = "SELECT 'Hide ' + ModuleStatus + ' and Above' AS [ModuleStatus], Priority FROM SCT_lkpmodulestatus ORDER BY priority";
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds, "SCT_lkpmodulestatus");
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet GetAllEmployeeList()
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT EmpID,FirstName+LastName as [NAME] FROM SCT_tblEMPLOYEES WHERE Status = 'A' ORDER BY FirstName,LastName ", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetEmployeeListPerTeamLead(Employee emp)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT  DISTINCT A.EmpID, A.FirstName+A.LastName as [NAME] FROM SCT_tblEmployees A JOIN SCT_tblTeams B ON A.EmpID = B.EmpID WHERE A.Status = 'A' AND (B.ProjectID IN (SELECT ProjectID FROM SCT_tblTeams WHERE (EmpID = '" + emp.EmployeeId + "') AND (MemberRoleID = 2 OR MemberRoleID = 9)))", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetMyTasksList(Employee emp)
        {
            string strSql = string.Empty;
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                /*strSql += "SELECT SCT_tblProjectModuleTasks.TaskId, SCT_tblProjectModuleTasks.ModuleId, SCT_tblProjectModuleTasks.ProjectId, ";
                strSql += " SCT_tblProjectModuleTasks.TaskDesc, SCT_tblProjectModuleTasks.StartDate, SCT_tblProjectModuleTasks.EndDate, ";
                strSql += " SCT_tblProjectModuleTasks.EstimateHours, SCT_lkpModuleStatus.ModuleStatus, SCT_tblProjectModuleTasks.MajorIssues, ";
                strSql += " SCT_tblProjectModuleTasks.MinorIssues, SCT_lkpModuleStatus.Priority, SCT_tblEmployees.EmpID FROM SCT_tblProjectModuleTasks INNER JOIN ";
                strSql += " SCT_lkpModuleStatus ON SCT_tblProjectModuleTasks.TaskStatusId = SCT_lkpModuleStatus.ModuleStatusId INNER JOIN ";
                strSql += " SCT_tblEmployees ON SCT_tblProjectModuleTasks.AssignedTo = SCT_tblEmployees.EmpID where EmpID=" + emp.EmployeeId;*/

                strSql += "SELECT SCT_tblProjectModuleTasks.TaskId, SCT_tblProjectModuleTasks.ModuleId, SCT_tblProjectModuleTasks.ProjectId, ";
                strSql += " SCT_tblProjectModuleTasks.TaskDesc, SCT_tblProjectModuleTasks.StartDate, SCT_tblProjectModuleTasks.EndDate, ";
                strSql += " SCT_tblProjectModuleTasks.EstimateHours, SCT_lkpModuleStatus.ModuleStatus, SCT_tblProjectModuleTasks.MajorIssues, ";
                strSql += " SCT_tblProjectModuleTasks.MinorIssues, SCT_lkpModuleStatus.Priority, SCT_tblEmployees.EmpID, SCT_tblProjects.ProjectCode ";
                strSql += " FROM SCT_tblProjectModuleTasks INNER JOIN SCT_lkpModuleStatus ON SCT_tblProjectModuleTasks.TaskStatusId = SCT_lkpModuleStatus.ModuleStatusId INNER JOIN ";
                strSql += " SCT_tblEmployees ON SCT_tblProjectModuleTasks.AssignedTo = SCT_tblEmployees.EmpID INNER JOIN ";
                strSql += " SCT_tblProjects ON SCT_tblProjectModuleTasks.ProjectId = SCT_tblProjects.ProjectID where EmpID=" + emp.EmployeeId;
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetMyProjectTaskList(Employee emp,int ProjectID)
        {
            string strSql = string.Empty;
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                /*strSql += "SELECT SCT_tblProjectModuleTasks.TaskId, SCT_tblProjectModuleTasks.ModuleId, SCT_tblProjectModuleTasks.ProjectId, ";
                strSql += " SCT_tblProjectModuleTasks.TaskDesc, SCT_tblProjectModuleTasks.StartDate, SCT_tblProjectModuleTasks.EndDate, ";
                strSql += " SCT_tblProjectModuleTasks.EstimateHours, SCT_lkpModuleStatus.ModuleStatus, SCT_tblProjectModuleTasks.MajorIssues, ";
                strSql += " SCT_tblProjectModuleTasks.MinorIssues, SCT_lkpModuleStatus.Priority, SCT_tblEmployees.EmpID FROM SCT_tblProjectModuleTasks INNER JOIN ";
                strSql += " SCT_lkpModuleStatus ON SCT_tblProjectModuleTasks.TaskStatusId = SCT_lkpModuleStatus.ModuleStatusId INNER JOIN ";
                strSql += " SCT_tblEmployees ON SCT_tblProjectModuleTasks.AssignedTo = SCT_tblEmployees.EmpID where EmpID=" + emp.EmployeeId;*/

                strSql += "SELECT SCT_tblProjectModuleTasks.TaskId, SCT_tblProjectModuleTasks.ModuleId, SCT_tblProjectModuleTasks.ProjectId, ";
                strSql += " SCT_tblProjectModuleTasks.TaskDesc, SCT_tblProjectModuleTasks.StartDate, SCT_tblProjectModuleTasks.EndDate, ";
                strSql += " SCT_tblProjectModuleTasks.EstimateHours, SCT_lkpModuleStatus.ModuleStatus, SCT_tblProjectModuleTasks.MajorIssues, ";
                strSql += " SCT_tblProjectModuleTasks.MinorIssues, SCT_lkpModuleStatus.Priority, SCT_tblEmployees.EmpID, SCT_tblProjects.ProjectCode ";
                strSql += " FROM SCT_tblProjectModuleTasks INNER JOIN SCT_lkpModuleStatus ON SCT_tblProjectModuleTasks.TaskStatusId = SCT_lkpModuleStatus.ModuleStatusId INNER JOIN ";
                strSql += " SCT_tblEmployees ON SCT_tblProjectModuleTasks.AssignedTo = SCT_tblEmployees.EmpID INNER JOIN ";
                strSql += " SCT_tblProjects ON SCT_tblProjectModuleTasks.ProjectId = SCT_tblProjects.ProjectID where EmpID=" + emp.EmployeeId;
                strSql += " and SCT_tblProjectModuleTasks.ProjectId =" + ProjectID;
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetSearchMyTasks(ProjectModuleTasks tasks)
        {
            string strSql = string.Empty;
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                strSql = "SELECT SCT_tblProjectModuleTasks.ProjectId, SCT_tblProjects.ProjectName, SCT_tblProjects.ProjectCode, SCT_tblProjectModuleTasks.TaskDesc,SCT_tblTeams.EmpID, SCT_tblProjectModuleTasks.StartDate, SCT_tblProjectModuleTasks.EndDate, ";
                strSql += " SCT_lkpModuleStatus.ModuleStatus FROM SCT_tblProjectModuleTasks INNER JOIN ";
                strSql += " SCT_tblProjects ON SCT_tblProjectModuleTasks.ProjectId = SCT_tblProjects.ProjectID INNER JOIN ";
                strSql += " SCT_tblTeams ON SCT_tblProjectModuleTasks.ProjectId = SCT_tblTeams.ProjectID INNER JOIN ";
                strSql += " SCT_lkpModuleStatus ON SCT_tblProjectModuleTasks.TaskStatusId = SCT_lkpModuleStatus.ModuleStatusId where EmpID=" + tasks.AssignedTo;
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public ProjectModuleTasks GetNoOfMyTasks(Employee emp)
        {
            string strSql = string.Empty;
            SqlDataAdapter adpt;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            ProjectModuleTasks tasks = new ProjectModuleTasks();
            try
            {
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    tasks.InProcess = "0";
                    tasks.Maintenance = "0";
                    tasks.QAed = "0";
                }
            }
            catch (Exception)
            {
            }
            return tasks;
        }

        public bool AddProjectModulesTask(ProjectModuleTasks task)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd;
            cmd = new SqlCommand();
            try
            {               
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.CommandText = "SCT_spInsertTask";
                cn.Open();
                cmd.Parameters.AddWithValue("@ProjectId", task.ProjectId);
                cmd.Parameters.AddWithValue("@ModuleId", task.ModuleId);
                cmd.Parameters.AddWithValue("@AssignedTo", task.AssignedTo);
                cmd.Parameters.AddWithValue("@TaskDesc", task.TaskDesc);
                cmd.Parameters.AddWithValue("@StartDate", task.StartDate);
                cmd.Parameters.AddWithValue("@EndDate", task.EndDate);
                cmd.Parameters.AddWithValue("@StatusId", task.TaskStatusId);
                cmd.Parameters.AddWithValue("@AssignedBy", task.AssignedBy);
                cmd.Parameters.AddWithValue("@AssignedOn", task.AssignedOn);
                cmd.Parameters.AddWithValue("@LastModifiedBy", task.AssignedBy);
                cmd.Parameters.AddWithValue("@LastModifiedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@EstimateHours", task.EstimateHours);
                cmd.Parameters.AddWithValue("@intQaId", task.QAAssigned);
                cmd.Parameters.AddWithValue("@EstimateQaHours", task.EstimateQaHours);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public DataSet GetTaskActivity()
        {
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds = new DataSet();
            adpt = new SqlDataAdapter("select * from SCT_lkpActivity", cn);
            adpt.Fill(ds, "SCT_lkpActivity");
            return ds;
        }

        public decimal GetModuleHoursForEmp(int AssignedTo, int ModuleId)
        {
            decimal temp = 0;
            string strQry = string.Empty;
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds = new DataSet();
            strQry = " select EstimateHours from SCT_tblProjectModuleTasks where ModuleId=" + ModuleId + " and AssignedTo=" + AssignedTo;
            adpt = new SqlDataAdapter(strQry, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                temp = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
            }
            else
            {
                temp = 0;
            }
            return temp;
        }

        public DataSet GetSelectedTask(int ProjectID, int TaskID)
        {            
            string strQry = string.Empty;
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds = new DataSet();
            strQry = "select * from SCT_tblProjectModuleTasks where ProjectId=" + ProjectID + " and TaskId=" + TaskID;
            adpt = new SqlDataAdapter(strQry, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

        public DataSet GetSelectedTaskHistory(int ProjectID, int TaskID)
        {
            string strQry = string.Empty;
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds = new DataSet();
            strQry = "select * from SCT_tblProjectModuleTasks where ProjectId=" + ProjectID + " and TaskId=" + TaskID;
            adpt = new SqlDataAdapter(strQry, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

         public DataSet GetTaskNamesByTaskID(int TaskID)
        {
            string strQry = string.Empty;
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds = new DataSet();
            strQry = "select * from SCT_tblProjectModuleTasks where TaskId=" + TaskID;
            adpt = new SqlDataAdapter(strQry, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

        public DataSet GetTasks(int ProjectID)
        {
            string strQry = string.Empty;
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds = new DataSet();
            strQry = "select * from SCT_tblProjectModuleTasks where ProjectId=" + ProjectID;
            adpt = new SqlDataAdapter(strQry, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

        public DataSet GetSelectedTaskHistory(int ProjectID)
        {
            string strQry = string.Empty;
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds = new DataSet();
            strQry = "select * from SCT_tblProjectModuleTasks where ProjectId=" + ProjectID;
            adpt = new SqlDataAdapter(strQry, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }
    }
}
