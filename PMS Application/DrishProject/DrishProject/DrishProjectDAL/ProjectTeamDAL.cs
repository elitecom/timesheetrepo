﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectDAL
{
    public class ProjectTeamDAL
    {
        public DataSet GetDevListPerProject(ProjectTeam projectTeam)
        {
            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT A.empid, A.MemberID, B.FirstName+' '+B.LastName AS MemberName,B.EmailID,C.MemberRole,A.MemberWorking FROM SCT_tblTeams A LEFT JOIN SCT_tblEmployees B ON A.EmpID = B.EmpID LEFT JOIN SCT_lkpMemberRole C ON A.MemberRoleID = C.MemberRoleID WHERE B.STATUS = 'A' AND A.Availability = 'Y' AND A.ProjectID = '" + projectTeam.ProjectId + "' and isqa <> 1", cn);
                DTSet = new DataSet();
                adpt.Fill(DTSet);
                return DTSet;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }
        public DataSet GetEmployeeListPerProject(ProjectTeam projectTeam,string strSortCriteria, string strSortDir)
        {
            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "";
            if (strSortCriteria != "")
            {
                strSql += "SELECT A.empid,B.EmpCode, A.MemberID, B.FirstNAme+' '+B.LastName AS MemberName,B.EmailID,C.MemberRole, ";
                strSql += " 'MemberWorking' = Case A.MemberWorking WHEN 'P' THEN 'Part Time' ELSE 'Full Time' END, 'Availability' = Case A.Availability WHEN 'Y' THEN 'Active' ELSE 'InActive' END FROM SCT_tblTeams A LEFT JOIN SCT_tblEmployees B ON A.EmpID = B.EmpID ";
                strSql += " LEFT JOIN SCT_lkpMemberRole C ON A.MemberRoleID = C.MemberRoleID WHERE B.Status = 'A' AND A.ProjectID = '";
                strSql += projectTeam.ProjectId;
                strSql += "' order by ";
                strSql += strSortCriteria;
                strSql += " ";
                strSql += strSortDir;
            }
            else
            {
                strSql += "SELECT A.empid,B.EmpCode, A.MemberID, B.FirstNAme+' '+B.LastName AS MemberName,B.EmailID,C.MemberRole, ";
                strSql += " 'MemberWorking' = Case A.MemberWorking WHEN 'P' THEN 'Part Time' ELSE 'Full Time' END, 'Availability' = Case A.Availability WHEN 'Y' THEN 'Active' ELSE 'InActive' END FROM SCT_tblTeams A LEFT JOIN SCT_tblEmployees B ON A.EmpID = B.EmpID ";
                strSql += " LEFT JOIN SCT_lkpMemberRole C ON A.MemberRoleID = C.MemberRoleID WHERE B.Status = 'A' AND A.ProjectID = '";
                strSql += projectTeam.ProjectId;
                strSql += "' order by membername";
            }
            adpt = new SqlDataAdapter(strSql, new DataConn().GetConn());
            DTSet = new DataSet();
            adpt.Fill(DTSet);            
            return DTSet;
        }
        public DataSet GetAllEmployeeList()
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT EmpID,FirstName+LastName as [NAME] FROM SCT_tblEMPLOYEES WHERE Status = 'A' ORDER BY FirstName,LastName ", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }
        public DataSet GetEmployeeListPerTeamLead(int EmpId)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT  DISTINCT A.EmpID, A.FirstName+A.LastName as [NAME] FROM SCT_tblEmployees A JOIN SCT_tblTeams B ON A.EmpID = B.EmpID WHERE A.Status = 'A' AND (B.ProjectID IN (SELECT ProjectID FROM SCT_tblTeams WHERE (EmpID = '" + EmpId + "') AND (MemberRoleID = 2 OR MemberRoleID = 9)))", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }
       
        public DataSet GetMemberRolesList()
        {

            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT MemberRoleID,MemberRole FROM SCT_lkpMemberRole order by MemberRole", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetSelectedMemberID(ProjectTeam projectTeam)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT A.EmpID,A.MemberRoleID,A.MemberWorking, A.Availability, B.FirstName+' '+B.LastName AS MemberName FROM SCT_tblTEAMS A JOIN SCT_tblEmployees B ON A.EmpID = B.EmpID WHERE A.MemberID = " + projectTeam.MemberId, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }
        
        public bool MemberExist(ProjectTeam member)
        {
            DataSet DTS = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strsql = "";
            strsql += "SELECT COUNT(*) AS [Exists] FROM SCT_tblTEAMS WHERE EmpID = ";
            strsql += member.EmpId;
            strsql += " AND ProjectID = ";
            strsql += member.ProjectId;
            adpt = new SqlDataAdapter(strsql, cn);
            adpt.Fill(DTS);
            try
            {
                if (System.Convert.ToInt32(DTS.Tables[0].Rows[0]["Exists"]) == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }
        
        public bool UpdateTeamMember(ProjectTeam ptr)
        {            
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            bool ans = true;
            try
            {
                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.CommandText = "SCT_spUpdateTeamMember";
                cmd.CommandType = CommandType.Text;
                //cmd.CommandText = "UPDATE SCT_tblTeams SET EmpID = @MemberNameID,MemberRoleID =@MemberRoleID,MemberWorking = @MemberWorkID,	Availability = @Availability, ModifiedBy = @ModifiedBy, ModifiedOn = @ModifiedOn,ReportTo =  @ReportTo WHERE MemberID = @MemberID";
                cmd.CommandText = "UPDATE SCT_tblTeams SET EmpID = " + ptr.EmpId + ",MemberRoleID = " + ptr.MemberRoleId + ",MemberWorking = '" + ptr.MemberWorking + "',Availability = '" + ptr.Availability + "', ModifiedBy = " + ptr.ModifiedBy + ",ModifiedOn = @modifiedOn,ReportTo =  " + ptr.ReportTo + " WHERE MemberID = " + ptr.EmpId + " and projectID=" + ptr.ProjectId;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@modifiedOn", DateTime.Now);
                cn.Open();
                
                //cmd.Parameters.AddWithValue("@MemberNameID", ptr.EmpId);
                //cmd.Parameters.AddWithValue("@MemberRoleID", ptr.MemberRoleId);
                //cmd.Parameters.AddWithValue("@MemberWorkID", ptr.MemberWorking);
                //cmd.Parameters.AddWithValue("@Availability", ptr.Availability);
                //cmd.Parameters.AddWithValue("@ModifiedBy", ptr.ModifiedBy);
                //cmd.Parameters.AddWithValue("@ModifiedOn", ptr.ModifiedOn);
                //cmd.Parameters.AddWithValue("@MemberID", ptr.EmpId);
                //cmd.Parameters.AddWithValue("@ReportTo", ptr.ReportTo);
                //pp = new SqlParameter();
                //pp.ParameterName = "@ID";
                //pp.DbType = DbType.Int32;
                //pp.Direction = ParameterDirection.Output;

                //cmd.Parameters.AddWithValue("@ID", 0);
                int temp;
                temp = cmd.ExecuteNonQuery();

                //res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                //if (res != -1)
                if (temp > 0)
                {
                    return true;
                }
                else
                {
                    string strRetVal;
                    strRetVal = "Team Member already Exist, Could not be added"; ;
                    ans = false;
                }
                return ans;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }

        }
       
        public bool InsertTeamMember(ProjectTeam ptr)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();           
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spInsertTeamMember";
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue("@MemberNameID", ptr.EmpId);
                cmd.Parameters.AddWithValue("@ProjectID", ptr.ProjectId);
                cmd.Parameters.AddWithValue("@MemberRoleID", ptr.MemberRoleId);
                cmd.Parameters.AddWithValue("@MemberWorkID", ptr.MemberWorking);
                cmd.Parameters.AddWithValue("@Availability", ptr.Availability);
                cmd.Parameters.AddWithValue("@ModifiedBy", ptr.ModifiedBy);
                cmd.Parameters.AddWithValue("@ModifiedOn", Convert.ToDateTime(ptr.ModifiedOn));
                cmd.Parameters.AddWithValue("@MemberID", ptr.MemberNameId);
                cmd.Parameters.AddWithValue("@ReportTo", ptr.ReportTo);

                //pp = new SqlParameter();
                //pp.ParameterName = "@ID";
                //pp.DbType = DbType.Int32;
                //pp.Direction = ParameterDirection.Output;

                //cmd.Parameters.AddWithValue("@ID", 0);
                //int temp;
                cmd.ExecuteNonQuery();
                return true;
                
            }
            catch (Exception ee)
            {
                return false;                
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }

        }
       
        public bool DeleteTeamMember(ProjectTeam ptr)
        {            
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            try
            {
                if (TeamMemberRefExists(ptr.EmpId, ptr.ProjectId) == true)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SCT_spDeleteTeamMember";
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.Parameters.AddWithValue("@pEmpId", ptr.EmpId);
                    cmd.Parameters.AddWithValue("@pProjectId", ptr.ProjectId);
                    //pp = new SqlParameter();
                    //pp.ParameterName = "@ID";
                    //pp.DbType = DbType.Int32;
                    //pp.Direction = ParameterDirection.Output;

                    //cmd.Parameters.AddWithValue("@ID", 0);
                    int temp;
                    temp = cmd.ExecuteNonQuery();

                    //res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                    //if (res != -1)
                    if (temp > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    string strRetVal = "";
                    strRetVal = "Reference Exists in Database, Can not be deleted !";
                    return false;
                }
                //return strRetVal;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }


        }
      
        public int GetSelectedEmpID(ProjectTeam projectTeam)
        {
            int intEmpID = 0;
            SqlDataReader rdr;
            try
            {
                SqlConnection cn = new DataConn().GetConn();
                SqlCommand cmd = new SqlCommand("SELECT EmpID FROM SCT_tblTEAMS WHERE MemberID = " + projectTeam.MemberId, cn);
                cn.Open();
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        intEmpID = Convert.ToInt32(rdr.GetInt32(0));
                    }
                }
                return intEmpID;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }
        
        public bool TeamMemberRefExists(int pEmpId, int Projectid)
        {
            string strSql = "";
            string strRetVal;
            strSql += "SELECT SUM(ct) FROM (SELECT COUNT(*) ct FROM SCT_tblProjectModules A WHERE A.AssignedTo = '";
            strSql += pEmpId.ToString();
            strSql += "' AND A.ProjectID = ";
            strSql += Projectid.ToString();
            strSql += " UNION select count(*) ct from SCT_tblProjectModuleTasks B where (B.Assignedto = ";
            strSql += pEmpId.ToString();
            strSql += " OR B.QAAssigned = ";
            strSql += pEmpId.ToString();
            strSql += ") AND B.ProjectID = ";
            strSql += Projectid.ToString();
            strSql += ") AS Table1";
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strSql, cn);
            cn.Open();
            strRetVal = cmd.ExecuteScalar().ToString();
            if (strRetVal == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool IsActiveMember(int EmpId, int Projectid)
        {
            string strsql = "";
            strsql += "SELECT Availability FROM SCT_tblTEAMS WHERE MemberID = '" + EmpId + "' AND ProjectId = " + Projectid;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strsql, cn);
            cn.Open();
            strsql = Convert.ToString(cmd.ExecuteScalar().ToString());
            if (strsql == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    
        public DataSet GetEmployeeListPerProject(int ProjectId)
        {
            string strSQL = "";
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                strSQL ="SELECT A.empid,B.EmpCode, A.MemberID, B.FirstNAme+' '+B.LastName AS MemberName,B.EmailID,C.MemberRole, ";
                strSQL += "'MemberWorking' = Case A.MemberWorking WHEN 'P' THEN 'Part Time' ELSE 'Full Time' END, 'Availability' = Case A.Availability WHEN 'Y' THEN 'Active' ELSE 'InActive' END FROM SCT_tblTeams A LEFT JOIN SCT_tblEmployees B ON A.EmpID = B.EmpID ";
                strSQL += " LEFT JOIN SCT_lkpMemberRole C ON A.MemberRoleID = C.MemberRoleID WHERE B.Status = 'A' AND A.ProjectID = " + ProjectId;
                //adpt = new SqlDataAdapter("SELECT EmpID as MemberID,FirstName+' '+LastName as [MemberName] FROM SCT_tblEMPLOYEES WHERE Status = 'A' ORDER BY FirstName,LastName ", cn);
                adpt = new SqlDataAdapter(strSQL, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetTeamMemberDetails(int ProjectId,int empId)
        {
            string strSQL = string.Empty;
            SqlDataAdapter adpt;
            DataSet ds;
            strSQL = "Select * from SCT_tblTeams where ProjectID=" + ProjectId + " and MemberId=" + empId;
            adpt = new SqlDataAdapter(strSQL, new DataConn().GetConn());
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }
    }
}
