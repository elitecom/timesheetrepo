﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Entities;

namespace DrishProjectDAL
{
    public class ProjectDAL
    {
        DataSet ds;
        public DataSet GetProjectCategories()
        {
            SqlConnection cn = new DataConn().GetConn();
            ds = new DataSet();
            SqlDataAdapter adpt = new SqlDataAdapter("SELECT * FROM SCT_lkpProjectCategory", cn);
            adpt.Fill(ds);
            return ds;
        }
        public DataSet GetProjectTypes()
        {
            SqlConnection cn = new DataConn().GetConn();
            ds = new DataSet();
            SqlDataAdapter adpt = new SqlDataAdapter("select * from Sct_lkpProjectType", cn);
            adpt.Fill(ds);
            return ds;
        }
        public DataSet GetProjectLevels()
        {
            SqlConnection cn = new DataConn().GetConn();
            ds = new DataSet();
            SqlDataAdapter adpt = new SqlDataAdapter("select * from Sct_lkpProjectLevel", cn);
            adpt.Fill(ds);
            return ds;
        }
        public DataSet GetProjectStatus()
        {
            SqlConnection cn = new DataConn().GetConn();
            ds = new DataSet();
            SqlDataAdapter adpt = new SqlDataAdapter("select * from Sct_lkpProjectStatus", cn);
            adpt.Fill(ds);
            return ds;

        }
        public DataSet GetProjectPhase()
        {
            SqlConnection cn = new DataConn().GetConn();
            ds = new DataSet();
            SqlDataAdapter adpt = new SqlDataAdapter("select * from Sct_lkpProjectPhase", cn);
            adpt.Fill(ds);
            return ds;
        }
        public DataSet SearchProject(string ProjectName, int Proj_TypeID, int Proj_StatusID)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string strSql = "";
            string strSearch = "";
            string strtxt;
            try
            {
                strSql = "select distinct SCT_tblProjects.ProjectCode,Sct_tblProjects.ProjectName,Sct_tblProjects.Proj_StartDate,SCT_tblProjects.Proj_EndDate,SCT_lkpProjectType.ProjectType,SCT_lkpProjectStatus.ProjectStatus from SCT_lkpProjectType Inner join SCT_tblProjects on SCT_lkpProjectType.Proj_TypeID=SCT_tblProjects.Proj_TypeID inner join SCT_lkpProjectStatus on SCT_tblProjects.Proj_StatusID=SCT_lkpProjectStatus.Proj_StatusID";
                if (ProjectName != "")
                {
                    if (ProjectName.ToString() == "")
                    {
                        strSearch += " WHERE ((SCT_tblProjects.ProjectName LIKE '%";
                        strSearch += ProjectName;
                        strSearch += "%'))";
                    }
                    else
                    {
                        strSearch += " AND ((SCT_tblProjects.ProjectName LIKE '%";
                        strSearch += ProjectName;
                        strSearch += "%'))";
                    }
                }

                if (Proj_TypeID == -1)
                {
                    strSearch += " WHERE Sct_tblProjects.Proj_TypeID =" + Proj_TypeID;
                }
                else
                {
                    strSearch += " AND  SCT_tblProjects.Proj_TypeID =" + Proj_TypeID;
                }
                if (Proj_StatusID == -1)
                {
                    strSearch += " WHERE Sct_tblProjects.Proj_StatusID =" + Proj_StatusID;
                }
                else
                {
                    strSearch += " AND  SCT_tblProjects.Proj_StatusID =" + Proj_StatusID;
                }
                strSql += strSearch.ToString();
                strSql += " ORDER BY ProjectCode";

                adpt = new SqlDataAdapter(strSql, cn);
                DataSet ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet GetProjectDetailsByEmpId(int empid)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            string strSql = string.Empty;
            strSql = "SELECT distinct SCT_tblProjects.ProjectID, SCT_tblProjects.ProjectCode, SCT_tblProjects.ProjectName, SCT_tblProjects.Proj_StartDate, SCT_tblProjects.Proj_EndDate, SCT_tblProjects.Proj_Email, SCT_tblTeams.EmpID, SCT_tblTeams.ReportTo, ";
            strSql += " SCT_lkpProjectType.ProjectType, SCT_tblEmployees.FirstName, SCT_tblEmployees.LastName, SCT_lkpProjectStatus.ProjectStatus ";
            strSql += " FROM SCT_tblProjects INNER JOIN SCT_tblTeams ON SCT_tblProjects.ProjectID = SCT_tblTeams.ProjectID INNER JOIN SCT_lkpProjectType ON SCT_tblProjects.Proj_TypeID = SCT_lkpProjectType.Proj_TypeID INNER JOIN SCT_tblEmployees ON SCT_tblTeams.EmpID = SCT_tblEmployees.EmpID INNER JOIN SCT_lkpProjectStatus ON SCT_tblProjects.Proj_StatusID = SCT_lkpProjectStatus.Proj_StatusID WHERE SCT_tblTeams.EmpID =  " + empid;
            //adpt = new SqlDataAdapter("Select * from SCT_tblProjects inner join SCT_lkpProjectType on  SCT_lkpProjectType.Proj_TypeID=SCT_tblProjects.Proj_TypeID inner join Sct_lkpProjectStatus on SCT_lkpProjectStatus.Proj_StatusID=SCT_tblProjects.Proj_StatusID where Sct_tblProjects.EmpId=" + empid, cn);
            adpt = new SqlDataAdapter(strSql, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

        public DataSet GetProjectDetails()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;

            adpt = new SqlDataAdapter("Select * from SCT_tblProjects inner join SCT_lkpProjectType on  SCT_lkpProjectType.Proj_TypeID=SCT_tblProjects. Proj_TypeID inner join Sct_lkpProjectStatus on SCT_lkpProjectStatus.Proj_StatusID=SCT_tblProjects.Proj_StatusID ", cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

        public Project ProjectDetails(Project project)
        {
            Project prop = new Project();
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string qry = string.Empty;
            DataSet ds = new DataSet();
            try
            {
                qry = "select SCT_tblProjects.ProjectCode,Sct_tblProjects.ProjectName,SCT_lkpProjectType.ProjectType,SCT_tblProjects.Proj_Email,SCT_lkpProjectStatus.ProjectStatus,Sct_tblProjects.Proj_StartDate,SCT_tblProjects.Proj_EndDate,SCT_tblProjects.ProjectID from SCT_lkpProjectType Inner join SCT_tblProjects on SCT_lkpProjectType.Proj_TypeID=SCT_tblProjects.Proj_TypeID inner join SCT_lkpProjectStatus on SCT_tblProjects.Proj_StatusID=SCT_lkpProjectStatus.Proj_StatusID where SCt_tblProjects.ProjectID=" + project.ProjectID;
                adpt = new SqlDataAdapter(qry, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    prop.ProjectCode = dr[0].ToString();
                    prop.ProjectName = dr[1].ToString();
                    prop.ProjectType = dr[2].ToString();
                    prop.ProjectEmail = dr[3].ToString();
                    prop.ProjectStatus = dr[4].ToString();
                    prop.ProjectStartDate = Convert.ToDateTime(dr[5].ToString());
                    prop.ProjectEndDate = Convert.ToDateTime(dr[6].ToString());
                    prop.ProjectID = Convert.ToInt32(dr[7].ToString());
                }
                return prop;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int GetProjectIDByCode(Project prop)
        {
            SqlDataAdapter adpt;
            DataSet ds;
            int projectId = -1;
            SqlConnection cn = new DataConn().GetConn();
            adpt = new SqlDataAdapter("select projectId from Sct_tblprojects where projectcode='" + prop.ProjectCode + "'", cn);
            ds = new DataSet();
            adpt.Fill(ds, "Sct_tblProjects");
            if (ds.Tables[0].Rows.Count > 0)
            {
                projectId = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
            }
            return projectId;
        }

        public bool AddNewProject(Project project)
        {
            // Call Stored procedure to add project
            bool res;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.CommandText = "SCT_spInsertProject";
                cmd.Parameters.AddWithValue("@ProjectName", project.ProjectName);
                cmd.Parameters.AddWithValue("@ProjectCode", project.ProjectCode);
                cmd.Parameters.AddWithValue("@ProjectCategory", project.ProjectCategory);
                cmd.Parameters.AddWithValue("@ProjectType", project.ProjectType);
                cmd.Parameters.AddWithValue("@ProjectStatus", project.ProjectStatus);
                cmd.Parameters.AddWithValue("@ProjectStartDate ", project.ProjectStartDate);
                cmd.Parameters.AddWithValue("@ProjectEndDate", project.ProjectEndDate);
                cmd.Parameters.AddWithValue("@ProjectLevel ", project.ProjectLevel);
                cmd.Parameters.AddWithValue("@ProjectPlan", project.ProjectPlan);
                cmd.Parameters.AddWithValue("@MantisSetup", project.MantisSetup);
                cmd.Parameters.AddWithValue("@ProjectEmail", project.ProjectEmail);
                cn.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                res = false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
            return res;
            // create dataset and get value from Sct_lkpProjectFolders
            // create folders in project
        }

        public bool UpdateProject(Project project)
        {
            // Call Stored procedure to add project
            bool res;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.CommandText = "SCT_spUpdateProject";
                cmd.Parameters.AddWithValue("@ProjectId", project.ProjectID);
                cmd.Parameters.AddWithValue("@ProjectName", project.ProjectName);
                cmd.Parameters.AddWithValue("@ProjectCode", project.ProjectCode);
                //cmd.Parameters.AddWithValue("@ProjectCategory", project.ProjectCategory);
                cmd.Parameters.AddWithValue("@ProjectType", project.ProjectType);
                cmd.Parameters.AddWithValue("@ProjectStatus", project.ProjectStatus);
                cmd.Parameters.AddWithValue("@ProjectStartDate ", project.ProjectStartDate);
                cmd.Parameters.AddWithValue("@ProjectEndDate", project.ProjectEndDate);
                cmd.Parameters.AddWithValue("@ProjectLevel ", project.ProjectLevel);
                cmd.Parameters.AddWithValue("@ProjectPlan", project.ProjectPlan);
                cmd.Parameters.AddWithValue("@MantisSetup", project.MantisSetup);
                cmd.Parameters.AddWithValue("@ProjectEmail", project.ProjectEmail);
                cn.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                res = false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
            return res;
        }

        public DataSet GetProjectFolders()
        {
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds = new DataSet();
            SqlDataAdapter adpt = new SqlDataAdapter("select * from Sct_lkpProjectFolders where ProjectType='ENGG'", cn);
            adpt.Fill(ds);
            return ds;
        }

        public DataSet SearchProject(Project prj)
        {
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string prjName = prj.ProjectName;
            int prjStatusID = prj.ProjectStatusID;
            int prjTypeID = prj.ProjectTypeID;
            string strSql = string.Empty;

            try
            {
                //strSql = "select distinct SCT_tblProjects.ProjectCode,Sct_tblProjects.ProjectName,Sct_tblProjects.Proj_StartDate,SCT_tblProjects.Proj_EndDate,SCT_lkpProjectType.ProjectType,SCT_lkpProjectStatus.ProjectStatus,Sct_tblProjects.ProjectID from SCT_lkpProjectType Inner join SCT_tblProjects on SCT_lkpProjectType.Proj_TypeID=SCT_tblProjects.Proj_TypeID inner join SCT_lkpProjectStatus on SCT_tblProjects.Proj_StatusID=SCT_lkpProjectStatus.Proj_StatusID WHERE  ";

                #region "Unused Code"

                //if (prjName.ToString() == "")
                //{
                //    strSearch += " WHERE ((SCT_tblProjects.ProjectName LIKE '%";
                //    strSearch += prjName;
                //    strSearch += "%'))";

                //}
                //else
                //{
                //    strSearch += " AND ((SCT_tblProjects.ProjectName LIKE '%";
                //    strSearch += prjName;
                //    strSearch += "%'))";
                //}
                //if (prjStatus.ToString() == "")
                //{
                //    strSearch += " WHERE ((SCT_lkpProjectStatus LIKE '%";
                //    strSearch += prjStatus;
                //    strSearch += "%'))";
                //}
                //else
                //{
                //    strSearch += " AND((SCT_lkpProjectStatus LIKE '%";
                //    strSearch += prjStatus;
                //    strSearch += "%'))";
                //}
                //if (prjType.ToString() == "")
                //{
                //    strSearch += " WHERE ((SCT_lkpProjectType LIKE '%";
                //    strSearch += prjType;
                //    strSearch += "%'))";
                //}
                //else
                //{
                //    strSearch += " AND((SCT_lkpProjectType LIKE '%";
                //    strSearch += prjType;
                //    strSearch += "%'))";
                //}
                //strSql += strSearch.ToString();
                //strSql += " ORDER BY ProjectCode";
                #endregion
                if (prjTypeID > 0)
                {
                    strSql = "select distinct SCT_tblProjects.ProjectCode,Sct_tblProjects.ProjectName,Sct_tblProjects.Proj_StartDate,SCT_tblProjects.Proj_EndDate,SCT_lkpProjectType.ProjectType,SCT_lkpProjectStatus.ProjectStatus,Sct_tblProjects.ProjectID from SCT_lkpProjectType Inner join SCT_tblProjects on SCT_lkpProjectType.Proj_TypeID=SCT_tblProjects.Proj_TypeID inner join SCT_lkpProjectStatus on SCT_tblProjects.Proj_StatusID=SCT_lkpProjectStatus.Proj_StatusID WHERE  ";
                    strSql += "Sct_tblProjects.Proj_TypeID = " + prjTypeID + " and SCT_tblProjects.ProjectID in (select ProjectID from SCt_tblTeams where empid=5) ";
                }
                else
                {
                    strSql = "";
                }
                if (prjStatusID > 0)
                {
                    if (!string.IsNullOrEmpty(strSql))
                    {
                        strSql += " union ";
                    }
                    else
                    {
                        strSql = "";
                    }
                    strSql = "select distinct SCT_tblProjects.ProjectCode,Sct_tblProjects.ProjectName,Sct_tblProjects.Proj_StartDate,SCT_tblProjects.Proj_EndDate,SCT_lkpProjectType.ProjectType,SCT_lkpProjectStatus.ProjectStatus,Sct_tblProjects.ProjectID from SCT_lkpProjectType Inner join SCT_tblProjects on SCT_lkpProjectType.Proj_TypeID=SCT_tblProjects.Proj_TypeID inner join SCT_lkpProjectStatus on SCT_tblProjects.Proj_StatusID=SCT_lkpProjectStatus.Proj_StatusID WHERE  ";
                    strSql += " SCT_tblProjects.Proj_StatusID=" + prjStatusID;
                }
                if (!String.IsNullOrEmpty(prjName))
                {
                    if (!string.IsNullOrEmpty(strSql))
                    {
                        strSql += " union ";
                    }
                    else
                    {
                        strSql = "";
                    }
                    strSql += "select distinct SCT_tblProjects.ProjectCode,Sct_tblProjects.ProjectName,Sct_tblProjects.Proj_StartDate,SCT_tblProjects.Proj_EndDate,SCT_lkpProjectType.ProjectType,SCT_lkpProjectStatus.ProjectStatus,Sct_tblProjects.ProjectID from SCT_lkpProjectType Inner join SCT_tblProjects on SCT_lkpProjectType.Proj_TypeID=SCT_tblProjects.Proj_TypeID inner join SCT_lkpProjectStatus on SCT_tblProjects.Proj_StatusID=SCT_lkpProjectStatus.Proj_StatusID WHERE  ";
                    strSql += " Sct_tblProjects.ProjectName like '%" + prjName + "%' or SCT_tblProjects.ProjectCode like '%" + prjName + "%'";
                }
                adpt = new SqlDataAdapter(strSql, cn);
                DataSet ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet SearchProject(Project prj, int empID)
        {
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string prjName = prj.ProjectName;
            int prjStatusID = prj.ProjectStatusID;
            int prjTypeID = prj.ProjectTypeID;
            string strSql = string.Empty;

            try
            {
                //strSql = "select distinct SCT_tblProjects.ProjectCode,Sct_tblProjects.ProjectName,Sct_tblProjects.Proj_StartDate,SCT_tblProjects.Proj_EndDate,SCT_lkpProjectType.ProjectType,SCT_lkpProjectStatus.ProjectStatus,Sct_tblProjects.ProjectID from SCT_lkpProjectType Inner join SCT_tblProjects on SCT_lkpProjectType.Proj_TypeID=SCT_tblProjects.Proj_TypeID inner join SCT_lkpProjectStatus on SCT_tblProjects.Proj_StatusID=SCT_lkpProjectStatus.Proj_StatusID WHERE  ";        
                if (prjTypeID > 0)
                {
                    strSql = "select distinct SCT_tblProjects.ProjectCode,Sct_tblProjects.ProjectName,Sct_tblProjects.Proj_StartDate,SCT_tblProjects.Proj_EndDate,SCT_lkpProjectType.ProjectType,SCT_lkpProjectStatus.ProjectStatus,Sct_tblProjects.ProjectID from SCT_lkpProjectType Inner join SCT_tblProjects on SCT_lkpProjectType.Proj_TypeID=SCT_tblProjects.Proj_TypeID inner join SCT_lkpProjectStatus on SCT_tblProjects.Proj_StatusID=SCT_lkpProjectStatus.Proj_StatusID WHERE  ";
                    strSql += "Sct_tblProjects.Proj_TypeID = " + prjTypeID + " and SCT_tblProjects.ProjectID in (select ProjectID from SCt_tblTeams where empid=" + empID + ") ";
                }
                else
                {
                    strSql = "";
                }
                if (prjStatusID > 0)
                {
                    if (!string.IsNullOrEmpty(strSql))
                    {
                        strSql += " union ";
                    }
                    else
                    {
                        strSql = "";
                    }
                    strSql = "select distinct SCT_tblProjects.ProjectCode,Sct_tblProjects.ProjectName,Sct_tblProjects.Proj_StartDate,SCT_tblProjects.Proj_EndDate,SCT_lkpProjectType.ProjectType,SCT_lkpProjectStatus.ProjectStatus,Sct_tblProjects.ProjectID from SCT_lkpProjectType Inner join SCT_tblProjects on SCT_lkpProjectType.Proj_TypeID=SCT_tblProjects.Proj_TypeID inner join SCT_lkpProjectStatus on SCT_tblProjects.Proj_StatusID=SCT_lkpProjectStatus.Proj_StatusID WHERE  ";
                    strSql += " SCT_tblProjects.Proj_StatusID=" + prjStatusID + " and SCT_tblProjects.ProjectID in (select ProjectID from SCt_tblTeams where empid=" + empID + ") "; ;
                }
                if (!String.IsNullOrEmpty(prjName))
                {
                    if (!string.IsNullOrEmpty(strSql))
                    {
                        strSql += " union ";
                    }
                    else
                    {
                        strSql = "";
                    }
                    //strSql += "select distinct SCT_tblProjects.ProjectCode,Sct_tblProjects.ProjectName,Sct_tblProjects.Proj_StartDate,SCT_tblProjects.Proj_EndDate,SCT_lkpProjectType.ProjectType,SCT_lkpProjectStatus.ProjectStatus,Sct_tblProjects.ProjectID from SCT_lkpProjectType Inner join SCT_tblProjects on SCT_lkpProjectType.Proj_TypeID=SCT_tblProjects.Proj_TypeID inner join SCT_lkpProjectStatus on SCT_tblProjects.Proj_StatusID=SCT_lkpProjectStatus.Proj_StatusID WHERE  ";
                    //strSql += " Sct_tblProjects.ProjectName like '%" + prjName + "%' or SCT_tblProjects.ProjectCode like '%" + prjName + "%'";
                    strSql += "select distinct SCT_tblProjects.ProjectID,SCT_tblProjects.ProjectCode,Sct_tblProjects.ProjectName,Sct_tblProjects.Proj_StartDate, SCT_tblProjects.Proj_EndDate, SCT_lkpProjectType.ProjectType,SCT_lkpProjectStatus.ProjectStatus,Sct_tblProjects.ProjectID from SCT_lkpProjectType Inner join SCT_tblProjects on SCT_lkpProjectType.Proj_TypeID=SCT_tblProjects.Proj_TypeID inner join SCT_lkpProjectStatus on SCT_tblProjects.Proj_StatusID=SCT_lkpProjectStatus.Proj_StatusID where SCT_tblProjects.ProjectID in (select ProjectID from Sct_tblTeams ";
                    strSql += " where ProjectID in (select projectID from Sct_tblprojects where Sct_tblProjects.ProjectName like '%" + prjName + "%' or SCT_tblProjects.ProjectCode like '%" + prjName + "%'))";
                }
                adpt = new SqlDataAdapter(strSql, cn);
                DataSet ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet GetTeamData()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;

            adpt = new SqlDataAdapter("Select  SCt_tblProjects.ProjectCode,SCT_tblTeams.EmpID,SCT_tblTeams.MemberWorking,SCT_tblTeams.Availability,SCT_lkpMemberRole.MemberRole,SCT_tblEmployees.FirstName,SCT_tblEmployees.EmailId from SCT_lkpMemberRole inner join SCT_tblTeams on  SCT_lkpMemberRole.MemberRoleID=SCT_tblTeams. MemberRoleID inner join Sct_tblEmployees on SCT_tblTeams.EmpId=SCT_tblEmployees.EmpId  inner join SCT_tblProjects on SCT_tblTeams.ProjectID=SCT_tblProjects.ProjectID", cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

        public Project GetProject(int projectId)
        {
            Project PP = new Project();
            string strSql = "SELECT A.PROJECTID, LTRIM(RTRIM(A.ProjectCategory)) AS ProjectCategory, A.PROJECTNAME, LTRIM(RTRIM(A.ProjectCode)) AS ProjectCode, A.PROJ_STARTDATE as PROJ_STARTDATE, A.PROJ_STARTDATE as PROJ_STARTDATEF, A.PROJ_ENDDATE AS PROJ_ENDDATEF, A.PROJ_ENDDATE AS PROJ_ENDDATE, A.PROJECTLEVELID, A.PROJECTPLAN,A.MANTISSETUP,A.Proj_StatusID,B.PROJECTTYPE,C.PROJECTSTATUS,D.LevelName, A.Proj_TypeId, A.Proj_Email ,E.ProjectPhase FROM SCT_tblPROJECTS A left JOIN SCT_lkpPROJECTTYPE B ON A.PROJ_TYPEID = B.PROJ_TYPEID left JOIN SCT_lkpPROJECTSTATUS C ON A.PROJ_STATUSID = C.PROJ_STATUSID left join SCT_lkpProjectLevel D on A.ProjectLevelId = D.LevelId left join SCT_lkpProjectPhase E on A.ProjectId = E.ProjectPhaseId WHERE ProjectID =" + projectId;
            SqlConnection con = new DataConn().GetConn();
            SqlDataAdapter adpt = new SqlDataAdapter(strSql,con);
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];

                if (dr["ProjectID"].ToString() != null)
                {
                    PP.ProjectID = Convert.ToInt32(dr["ProjectID"].ToString());
                }
                else
                {
                    PP.ProjectID = 0;
                }
                if (dr["ProjectCode"].ToString() != null)
                {
                    PP.ProjectCode = dr["ProjectCode"].ToString();
                }
                else
                {
                    PP.ProjectCode = "";
                }

                if (dr["ProjectName"].ToString() != null)
                {
                    PP.ProjectName = dr["ProjectName"].ToString();
                }
                else
                {
                    PP.ProjectName = "";
                }

                if (dr["ProjectCategory"].ToString() != null)
                {
                    PP.ProjectCategory = dr["ProjectCategory"].ToString();
                }
                else
                {
                    PP.ProjectCategory = "";
                }
                if (Convert.ToInt32(dr["Proj_TypeId"]).ToString() != null)
                {
                    PP.ProjectTypeID = Convert.ToInt32(dr["Proj_TypeId"].ToString());
                }
                else
                {
                    PP.ProjectTypeID = 0;
                }

                if (dr["ProjectStatus"].ToString() != null)
                {
                    PP.ProjectStatus = dr["ProjectStatus"].ToString();
                }
                else
                {
                    PP.ProjectStatus = "";
                }
                if (Convert.ToInt32(dr["Proj_StatusId"]).ToString() != null)
                {
                    PP.ProjectStatusID = Convert.ToInt32(dr["Proj_StatusId"].ToString());
                }
                else
                {
                    PP.ProjectStatusID = 0;
                }

                if (dr["ProjectPhase"].ToString() != null)
                {
                    PP.ProjectPhase = dr["ProjectPhase"].ToString();
                }
                else
                {
                    PP.ProjectPhase = "";
                }

                if (Convert.ToInt32(dr["PROJECTLEVELID"]).ToString() != null)
                {
                    PP.ProjectLevelID = Convert.ToInt32(dr["PROJECTLEVELID"].ToString());
                }
                else
                {
                    PP.ProjectLevelID = 0;
                }
                if (dr["LEVELName"].ToString() != null)
                {
                    PP.ProjectLevel = dr["LEVELName"].ToString();
                }
                else
                {
                    PP.ProjectLevel = "";
                }
                if (dr["Proj_startDate"].ToString() != null)
                {
                    PP.ProjectStartDate = Convert.ToDateTime(dr["Proj_startDate"].ToString());
                }
                else
                {
                    PP.ProjectStartDate = DateTime.Now;
                }
                if (dr["Proj_EndDate"].ToString() != null)
                {
                    PP.ProjectEndDate = Convert.ToDateTime(dr["Proj_EndDate"].ToString());
                }
                else
                {
                    PP.ProjectEndDate = DateTime.Now;
                }
                if (dr["Proj_startDateF"].ToString() != null)
                {
                    PP.ProjectStartDateF = Convert.ToDateTime(dr["Proj_startDateF"].ToString());
                }
                else
                {
                    PP.ProjectStartDateF = DateTime.Now;
                }
                if (dr["Proj_EndDateF"].ToString() != null)
                {
                    PP.ProjectEndDateF = Convert.ToDateTime(dr["Proj_EndDateF"].ToString());
                }
                else
                {
                    PP.ProjectEndDateF = DateTime.Now;
                }

                if (dr["Proj_Email"].ToString() != null)
                {
                    PP.ProjectEmailId = dr["Proj_Email"].ToString();
                }
                else
                {
                    PP.ProjectEmailId = "";
                }
            }
            return PP;
            #region "Unused Code"
           
            //try
            //{
            //    //strSql += "SELECT A.PROJECTID, LTRIM(RTRIM(A.ProjectCategory)) AS ProjectCategory, ";
            //    //strSql += "A.PROJECTNAME, LTRIM(RTRIM(A.ProjectCode)) AS ProjectCode, ";
            //    //strSql += "convert(char,nullif(A.PROJ_STARTDATE,'1/1/1900'),101) as PROJ_STARTDATE, ";
            //    //strSql += "convert(char,nullif(A.PROJ_STARTDATE,'1/1/1900'),106) as PROJ_STARTDATEF, ";
            //    //strSql += "convert(char,nullif(A.PROJ_ENDDATE,'1/1/1900'),106) AS PROJ_ENDDATEF, ";
            //    //strSql += "convert(char,nullif(A.PROJ_ENDDATE,'1/1/1900'),101) AS PROJ_ENDDATE, A.PROJECTLEVELID, ";
            //    //strSql += "A.PROJECTPLAN,A.MANTISSETUP,A.Proj_StatusID,B.PROJECTTYPE,C.PROJECTSTATUS,D.LevelName, ";
            //    //strSql += "A.Proj_TypeId, A.Proj_Email ,E.ProjectPhase FROM SCT_tblPROJECTS A left JOIN SCT_lkpPROJECTTYPE B ON ";
            //    //strSql += "A.PROJ_TYPEID = B.PROJ_TYPEID left JOIN SCT_lkpPROJECTSTATUS C ON ";
            //    //strSql += "A.PROJ_STATUSID = C.PROJ_STATUSID left join SCT_lkpProjectLevel D on ";
            //    //strSql += "A.ProjectLevelId = D.LevelId left join SCT_lkpProjectPhase E on A.ProjectId = E.ProjectPhaseId WHERE ProjectID =" + projectId;
            //    strSql += "SELECT A.PROJECTID, LTRIM(RTRIM(A.ProjectCategory)) AS ProjectCategory, ";
            //    strSql += "A.PROJECTNAME, LTRIM(RTRIM(A.ProjectCode)) AS ProjectCode, ";
            //    strSql += "convert(char,nullif(A.PROJ_STARTDATE,'1/1/1900'),101) as PROJ_STARTDATE, ";
            //    strSql += "convert(char,nullif(A.PROJ_STARTDATE,'1/1/1900'),106) as PROJ_STARTDATEF, ";
            //    strSql += "convert(char,nullif(A.PROJ_ENDDATE,'1/1/1900'),106) AS PROJ_ENDDATEF, ";
            //    strSql += "convert(char,nullif(A.PROJ_ENDDATE,'1/1/1900'),106) AS PROJ_ENDDATE, A.PROJECTLEVELID, ";
            //    strSql += "A.PROJECTPLAN,A.MANTISSETUP,A.Proj_StatusID,B.PROJECTTYPE,C.PROJECTSTATUS,D.LevelName, ";
            //    strSql += "A.Proj_TypeId, A.Proj_Email ,E.ProjectPhase FROM SCT_tblPROJECTS A left JOIN SCT_lkpPROJECTTYPE B ON ";
            //    strSql += "A.PROJ_TYPEID = B.PROJ_TYPEID left JOIN SCT_lkpPROJECTSTATUS C ON ";
            //    strSql += "A.PROJ_STATUSID = C.PROJ_STATUSID left join SCT_lkpProjectLevel D on ";
            //    strSql += "A.ProjectLevelId = D.LevelId left join SCT_lkpProjectPhase E on A.ProjectId = E.ProjectPhaseId WHERE ProjectID =" + projectId;

            //    SqlDataReader rdr;
            //    SqlCommand cmd;
            //    Project PP = new Project();

            //    cmd = new SqlCommand(strSql, con);
            //    con.Open();
            //    rdr = cmd.ExecuteReader();
            //    if (rdr.HasRows)
            //    {
            //        while (rdr.Read())
            //        {
            //            PP.ProjectID = Convert.ToInt32(rdr["ProjectID"].ToString());
            //            if (rdr["ProjectCode"].ToString() != null)
            //            {
            //                PP.ProjectCode = rdr["ProjectCode"].ToString();
            //            }
            //            else
            //            {
            //                PP.ProjectCode = "";
            //            }

            //            if (rdr["ProjectName"].ToString() != null)
            //            {
            //                PP.ProjectName = rdr["ProjectName"].ToString();
            //            }
            //            else
            //            {
            //                PP.ProjectName = "";
            //            }

            //            if (rdr["ProjectCategory"].ToString() != null)
            //            {
            //                PP.ProjectCategory = rdr["ProjectCategory"].ToString();
            //            }
            //            else
            //            {
            //                PP.ProjectCategory = "";
            //            }
            //            if (Convert.ToInt32(rdr["Proj_TypeId"]).ToString() != null)
            //            {
            //                PP.ProjectTypeID = Convert.ToInt32(rdr["Proj_TypeId"].ToString());
            //            }
            //            else
            //            {
            //                PP.ProjectTypeID = 0;
            //            }

            //            if (rdr["ProjectStatus"].ToString() != null)
            //            {
            //                PP.ProjectStatus = rdr["ProjectStatus"].ToString();
            //            }
            //            else
            //            {
            //                PP.ProjectStatus = "";
            //            }
            //            if (Convert.ToInt32(rdr["Proj_StatusId"]).ToString() != null)
            //            {
            //                PP.ProjectStatusID = Convert.ToInt32(rdr["Proj_StatusId"].ToString());
            //            }
            //            else
            //            {
            //                PP.ProjectStatusID = 0;
            //            }

            //            if (rdr["ProjectPhase"].ToString() != null)
            //            {
            //                PP.ProjectPhase = rdr["ProjectPhase"].ToString();
            //            }
            //            else
            //            {
            //                PP.ProjectPhase = "";
            //            }

            //            if (Convert.ToInt32(rdr["PROJECTLEVELID"]).ToString() != null)
            //            {
            //                PP.ProjectLevelID = Convert.ToInt32(rdr["PROJECTLEVELID"].ToString());
            //            }
            //            else
            //            {
            //                PP.ProjectLevelID = 0;
            //            }
            //            if (rdr["LEVELName"].ToString() != null)
            //            {
            //                PP.ProjectLevel = rdr["LEVELName"].ToString();
            //            }
            //            else
            //            {
            //                PP.ProjectLevel = "";
            //            }
            //            if (rdr["Proj_startDate"].ToString() != null)
            //            {
            //                PP.ProjectStartDate = Convert.ToDateTime(rdr["Proj_startDate"].ToString());
            //            }
            //            else
            //            {
            //                PP.ProjectStartDate = DateTime.Now;
            //            }
            //            if (rdr["Proj_EndDate"].ToString() != null)
            //            {
            //                PP.ProjectEndDate = Convert.ToDateTime(rdr["Proj_EndDate"].ToString());
            //            }
            //            else
            //            {
            //                PP.ProjectEndDate = DateTime.Now;
            //            }
            //            if (rdr["Proj_startDateF"].ToString() != null)
            //            {
            //                PP.ProjectStartDateF = Convert.ToDateTime(rdr["Proj_startDateF"].ToString());
            //            }
            //            else
            //            {
            //                PP.ProjectStartDateF = DateTime.Now;
            //            }
            //            if (rdr["Proj_EndDateF"].ToString() != null)
            //            {
            //                PP.ProjectEndDateF = Convert.ToDateTime(rdr["Proj_EndDateF"].ToString());
            //            }
            //            else
            //            {
            //                PP.ProjectEndDateF = DateTime.Now;
            //            }

            //            if (rdr["Proj_Email"].ToString() != null)
            //            {
            //                PP.ProjectEmailId = rdr["Proj_Email"].ToString();
            //            }
            //            else
            //            {
            //                PP.ProjectEmailId = "";
            //            }
            //        }
            //    }
            //    return PP;
            //}
            //catch (Exception ee)
            //{
            //    throw new Exception(ee.Message + "\n" + ee.StackTrace);
            //}
            //finally
            //{
            //    if (con.State == ConnectionState.Open)
            //    {
            //        con.Close();
            //    }
            //}
            #endregion
        }

        public bool IsActiveMember(int EmployeeId, int ProjectID)
        {
            string strsql;
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds = new DataSet();
            try
            {
                adpt = new SqlDataAdapter("SELECT Availability FROM SCT_tblTEAMS WHERE EmpID = " + EmployeeId + " and ProjectId = " + ProjectID, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                strsql = Convert.ToString(ds.Tables[0].Rows[0]["Availability"].ToString());
                if (strsql == "N" || strsql == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ee)
            {
                //throw new ArgumentException(ee.Message);
                return false;
            }
        }

        public string GetProjectStatus(string projectCode)
        {
            string status = string.Empty;
            string strSql = string.Empty;
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            strSql = "select projectStatus from SCT_tblProjects inner join SCT_lkpProjectStatus on SCT_tblProjects.Proj_StatusID = SCT_lkpProjectStatus.Proj_StatusID where Sct_tblprojects.ProjectCode='" + projectCode + "'";
            adpt = new SqlDataAdapter(strSql, cn);
            adpt.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                status = Convert.ToString(ds.Tables[0].Rows[0][0].ToString());
            }
            else
            {
                status = "";
            }
            return status;
        }

        public bool AddProjectDocumentTypes(Documents prop)
        {
            int projectid = -1;
            string projectCat = string.Empty;
            string strSql = string.Empty;
            Documents temp;
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds = new DataSet();

            projectid = prop.ProjectId;
            projectCat = prop.ProjectCategory.ToUpper();
            if (projectCat.Equals("1"))
            {
                strSql = "select * from  SCT_lkpDocumentType where projectid is null and ProjectCategory='ENGG'";
            }
            else if (projectCat.Equals("2"))
            {
                strSql = "select * from  SCT_lkpDocumentType where projectid is null and ProjectCategory='IS'";
            }
            adpt = new SqlDataAdapter(strSql, cn);
            ds = new DataSet();
            adpt.Fill(ds,"SCT_lkpDocumentType");
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i=0;i<ds.Tables[0].Rows.Count ;i++)
                {
                    temp = new Documents();
                    temp.ProjectId = prop.ProjectId;
                    temp.ProjectCategory = prop.ProjectCategory;
                    temp.DocType = ds.Tables[0].Rows[i]["DocType"].ToString();
                    temp.FolderPath = ds.Tables[0].Rows[i]["FolderPath"].ToString();
                    AddDocumentEntry(temp);
                }
            }
            return true;
        }

        private void AddDocumentEntry(Documents doc)
        {
            string strSql = string.Empty;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.CommandType = CommandType.Text;
                strSql = "insert into SCt_lkpDOcumentType(ProjectID,ProjectCategory,DocType,FolderPath) values(@ProjectID,@ProjectCategory,@DocType,@FolderPath)";
                cmd.CommandText = strSql;
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue("@ProjectID", doc.ProjectId);
                if (doc.ProjectCategory == "1")
                {
                    doc.ProjectCategory = "ENGG";
                }
                else if (doc.ProjectCategory == "2")
                {
                    doc.ProjectCategory = "IS";
                }
                cmd.Parameters.AddWithValue("@ProjectCategory", doc.ProjectCategory);
                cmd.Parameters.AddWithValue("@DocType", doc.DocType);
                cmd.Parameters.AddWithValue("@FolderPath", doc.FolderPath);
                cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception)
            { }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public DataSet getEmployeeProjectsV2(int empId)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            string strSql = string.Empty;
            //strSql = "select distinct SCT_tblProjects.ProjectID,ProjectCode,ProjectName from SCT_tblProjects inner join SCT_tblTeams on SCT_tblProjects.ProjectID = SCT_tblTeams.ProjectID where EmpID=" + empId;
            strSql = "SELECT distinct SCT_tblProjects.ProjectID, SCT_tblProjects.ProjectCode, SCT_tblProjects.ProjectName, SCT_tblProjectModules.ModuleID, SCT_tblProjectModules.ModuleName,SCT_tblTeams.EmpID FROM SCT_tblProjects INNER JOIN SCT_tblProjectModules";
            strSql += " ON SCT_tblProjects.ProjectID = SCT_tblProjectModules.ProjectID INNER JOIN SCT_tblTeams ";
            strSql += " ON SCT_tblProjects.ProjectID = SCT_tblTeams.ProjectID where SCT_tblTeams.EmpID=" + empId;
            adpt = new SqlDataAdapter(strSql, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

        public DataSet IfUserIsQA(int UserID, int ProjectID)
        {
            string strSql = "select MemberRole from SCT_lkpMemberRole inner join SCT_tblTeams on SCT_lkpMemberRole.MemberRoleID = SCT_tblTeams.MemberRoleID where EmpId=" + UserID + " and Projectid=" + ProjectID;
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            adpt = new SqlDataAdapter(strSql, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;            
        }

        public int GetSameProject(int ichkProjectID, int ichkModuleID, int ichkActivityID, int UserID, DateTime ClndMonth)
        {
            return 0;
        }

        public DataSet GetProjectHistory(int intProjectID)
        {
            string strSql = string.Empty;
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            try
            {
                strSql +="SELECT SCT_tblProjectHistory.HistoryID, SCT_tblProjectHistory.PMTID, SCT_tblProjectHistory.IDType, SCT_tblProjectHistory.ValueType, ";
                strSql += " SCT_tblProjectHistory.OldValue, SCT_tblProjectHistory.NewValue, SCT_tblProjectHistory.Remarks, SCT_tblProjectHistory.ModifiedBy as Name,SCT_tblProjectHistory.ModifiedOn, SCT_lkpProjectStatus.ProjectStatus as Status ";
                strSql += " FROM SCT_tblProjects INNER JOIN SCT_tblProjectHistory ON SCT_tblProjects.ProjectID = SCT_tblProjectHistory.PMTID INNER JOIN ";
                strSql += " SCT_lkpProjectStatus ON SCT_tblProjects.Proj_StatusID = SCT_lkpProjectStatus.Proj_StatusID where ProjectID=" + intProjectID;
                //adpt = new SqlDataAdapter("select * from SCT_tblProjectHistory where ProjectID=" + intProjectID, cn);
                //adpt = new SqlDataAdapter("select * from SCT_tblProjectHistory where PMTID=" + intProjectID, cn);
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool AddHistoryForDateChange(int intProjectId, string type, DateTime dtOldEndDate, DateTime dtEndDate, string Reason, int ModifiedBy, DateTime ModifiedOn)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            string strsql = "SCT_spAddHistory";
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strsql;
                cmd.Connection = cn;
                cn.Open();               
                cmd.Parameters.AddWithValue("@PMTID",intProjectId);
                cmd.Parameters.AddWithValue("@IdType", 'P');
                cmd.Parameters.AddWithValue("@ValueType", type);
                cmd.Parameters.AddWithValue("@OldValue", dtOldEndDate.ToShortDateString());
                cmd.Parameters.AddWithValue("@NewValue", dtEndDate.ToShortDateString());
                cmd.Parameters.AddWithValue("@Remarks", Reason);
                cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
                cmd.Parameters.AddWithValue("@ModifiedOn", ModifiedOn);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public bool AddHistoryForStatusChange(int intProjectId, string type, string OldStatus, string NewStatus, string Reason, int ModifiedBy, DateTime ModifiedOn)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            string strsql = "SCT_spAddHistory";
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strsql;
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue("@PMTID", intProjectId);
                cmd.Parameters.AddWithValue("@IdType", 'P');
                cmd.Parameters.AddWithValue("@ValueType", type);
                cmd.Parameters.AddWithValue("@OldValue", OldStatus);
                cmd.Parameters.AddWithValue("@NewValue", NewStatus);
                cmd.Parameters.AddWithValue("@Remarks", Reason);
                cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
                cmd.Parameters.AddWithValue("@ModifiedOn", ModifiedOn);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public DataSet GetProjectSummaryV2(Employee objUser)
        {
            SqlDataAdapter adpt;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = string.Empty;
            try
            {
                strSql += "select ProjectCode, sum(HrsSpent) as TotalHrs from Sct_tblProjects,SCT_tblTimesheets where Sct_tblprojects.ProjectID = SCT_tblTimesheets.ProjectID and ";
                strSql += " Sct_tblProjects.ProjectID in (select projectid from SCT_tblTeams where empid=" + objUser.EmployeeId + ") group by ProjectCode";
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet GetProjectSummaryV2ByDates(Employee objUser,DateTime dt,DateTime dt2)
        {
            SqlDataAdapter adpt;
            int dd1, dd2, mm1, mm2, yy1, yy2;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = string.Empty;
            try
            {
                dd1 = dt.Day;
                dd2 = dt2.Day;

                mm1 = dt.Month;
                mm2 = dt2.Month;

                yy1 = dt.Year;
                yy2 = dt2.Year;

                DateTime d1 = new DateTime(yy1, mm1, dd1);
                DateTime d2 = new DateTime(yy2, mm2, dd2);

                #region "Unused Code"                
                //strSql += "select ProjectCode, sum(HrsSpent) as TotalHrs from Sct_tblProjects,SCT_tblTimesheets where Sct_tblprojects.ProjectID = SCT_tblTimesheets.ProjectID and ";
                //strSql += " Sct_tblProjects.ProjectID in (select projectid from SCT_tblTeams where empid=" + objUser.EmployeeId + ") group by ProjectCode";

                //strSql += "select SUBSTRING(cast(tdate as varchar),0,12) as dates,SCT_tblTimesheets.ProjectID,sum(HrsSpent) as TotalHrs from SCT_tblTimesheets, Sct_tblprojects ";
                //strSql += " where Sct_tblprojects.ProjectID = SCT_tblTimesheets.ProjectID and  Sct_tblProjects.ProjectID in (select projectid from SCT_tblTeams where empid=" + objUser.EmployeeId + ") ";
                //strSql += " group by SUBSTRING(cast(tdate as varchar),0,12),SCT_tblTimesheets.ProjectID";

                // select * from SCT_tblTimesheets where tdate between convert(varchar(11),'04/20/2015',103) and convert(varchar(11),'04/30/2015',103)

                //strSql += "select SUBSTRING(cast(tdate as varchar),0,12) as dates,SCT_tblTimesheets.ProjectID,sum(HrsSpent) as TotalHrs ";
                //strSql += " from SCT_tblTimesheets, Sct_tblprojects  where Sct_tblprojects.ProjectID = SCT_tblTimesheets.ProjectID and  ";
                //strSql += " Sct_tblProjects.ProjectID in (select projectid from SCT_tblTeams where empid=" + objUser.EmployeeId + ")";
                //strSql += " and (tdate between convert(varchar(11),'" + t1 + "',103) and convert(varchar(11),'" + t2 + "',103)) ";
                //strSql += " group by SUBSTRING(cast(tdate as varchar),0,12),SCT_tblTimesheets.ProjectID";
                #endregion

                string t1,t2;
                t1 = mm1.ToString() + "/" + dd1.ToString() + "/" + yy1.ToString();
                t2 = mm2.ToString() + "/" + dd2.ToString() + "/" + yy2.ToString();
               
                strSql += "select SCT_tblProjects.ProjectCode,sum(HrsSpent) as TotalHrs  ";
                strSql += " from SCT_tblTimesheets, Sct_tblprojects  where Sct_tblprojects.ProjectID = SCT_tblTimesheets.ProjectID and  ";
                strSql += " Sct_tblProjects.ProjectID in (select projectid from SCT_tblTeams where empid=" + objUser.EmployeeId + ") and (tdate between convert(varchar(11),'" + t1 + "',103) and convert(varchar(11),'" + t2 + "',103))  group by SCT_tblProjects.ProjectCode";
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}