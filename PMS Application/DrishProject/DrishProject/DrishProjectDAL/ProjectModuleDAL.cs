﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectDAL
{
    public class ProjectModuleDAL
    {
        public ProjectModule GetModule(ProjectModule module1)
        {
            ProjectModule module = new ProjectModule();
            try
            {
                string strsql = "";
                SqlDataReader rdr;
                strsql += "SELECT A.ModuleId, A.ProjectId, A.ModuleName, A.ModuleStatusId, ";
                strsql += "convert(char,nullif(A.Startdate,'1/1/1900'),101) AS StartDate, ";
                strsql += "convert(char,nullif(A.Startdate,'1/1/1900'),106) AS StartDateF, ";
                strsql += "convert(char,nullif(A.Enddate,'1/1/1900'),106) AS EndDateF, ";
                strsql += "convert(char,nullif(A.EndDate,'1/1/1900'),101) AS EndDate, A.AssignedTo, ";
                strsql += "A.assignedBy, A.AssignedOn, A.EstimateHours, B.FirstName, B.EmpId, C.ModuleStatus ";
                strsql += "FROM SCT_tblprojectmodules A LEFT JOIN SCT_tblemployees B ON A.assignedto = B.empid ";
                strsql += "LEFT JOIN SCT_lkpmodulestatus C ON A.modulestatusid = C.modulestatusid WHERE ";
                strsql += "moduleID = ";
                strsql += module1.ModuleId;
                SqlCommand cmd;
                SqlConnection cn = new DataConn().GetConn();
                cmd = new SqlCommand(strsql, cn);
                cn.Open();
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {

                        if (Convert.ToInt32(rdr["ProjectId"]).ToString() != null)
                        {
                            module.ProjectId = Convert.ToInt32(rdr["ProjectId"].ToString());
                        }
                        if (rdr["modulename"].ToString() != null)
                        {
                            module.ModuleName = rdr["modulename"].ToString();
                        }
                        if (Convert.ToInt32(rdr["ModuleStatusId"]).ToString() != null)
                        {
                            module.ModuleStatusId = Convert.ToInt32(rdr["ModuleStatusId"].ToString());
                        }
                        if (rdr["ModuleStatus"].ToString() != null)
                        {
                            module.ModuleStatus = rdr["ModuleStatus"].ToString();
                        }
                        if (Convert.ToDateTime(rdr["startdate"]).ToShortDateString() != null)
                        {
                            module.StartDate = Convert.ToDateTime(rdr["startdate"]);
                        }

                        if (Convert.ToDateTime(rdr["enddate"]).ToShortDateString() != null)
                        {
                            module.EndDate = Convert.ToDateTime(rdr["enddate"]);
                        }
                        if (Convert.ToDateTime(rdr["startdatef"]).ToShortDateString() != null)
                        {
                            module.StartDateF = Convert.ToDateTime(rdr["startdatef"]);
                        }
                        if (Convert.ToDateTime(rdr["enddatef"]).ToShortDateString() != null)
                        {
                            module.EndDateF = Convert.ToDateTime(rdr["enddatef"]);
                        }
                        if (Convert.ToInt32(rdr["AssignedTo"]).ToString() != null)
                        {
                            module.AssignedToId = Convert.ToInt32(rdr["AssignedTo"].ToString());
                        }
                        if (rdr["FirstName"].ToString() != null)
                        {
                            module.AssignedToName = rdr["Firstname"].ToString();
                        }
                        if (Convert.ToInt32(rdr["AssignedBy"].ToString()) != null)
                        {
                            module.AssignedBy = Convert.ToInt32(rdr["AssignedBY"].ToString());
                        }
                        if (rdr["Assignedon"].ToString() != null)
                        {
                            module.AssignedOn = rdr["Assignedon"].ToString();
                        }
                        if (Convert.ToDecimal(rdr["estimatehours"]).ToString() != null)
                        {
                            module.dEstimateHours = Convert.ToDecimal(rdr["estimatehours"].ToString());
                        }
                    }
                }
                rdr.Close();
                return module;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private bool RefExists(ProjectModule module)
        {
            try
            {
                string strSql = "";
                string strRetVal = "";
                bool bVal;
                strSql += "SELECT SUM(ct) FROM (SELECT COUNT(*) ct FROM SCT_tblProjectHistory WHERE (PMTId = ";
                strSql += module.ModuleId;
                strSql += " AND IdType = 'M') ";
                strSql += " UNION SELECT COUNT(*) ct FROM SCT_tblProjectModuleTasks WHERE ModuleId = ";
                strSql += module.ModuleId;
                strSql += " ) AS Table1 ";
                SqlConnection cn = new DataConn().GetConn();
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cn.Open();
                strRetVal = cmd.ExecuteScalar().ToString();
                if (strRetVal != "0")
                {
                    bVal = true;
                }
                else
                {
                    bVal = false;
                }
                return bVal;

            }
            catch (Exception ee)
            {
                throw (ee);
            }
        }

        public string DeleteModule(ProjectModule module)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                string strSql;
                string strRetVal = "";
                if (!this.RefExists(module))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SCT_spDeleteModule";
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.Parameters.AddWithValue("@vmoduleid", module.ModuleId);
                    cmd.ExecuteNonQuery();
                    // ObjDataBase.SCT_spDeleteModule(module.ModuleId);
                }
                else
                {
                    strRetVal = "Reference Exists in Database, Can not be deleted !";
                }
                return strRetVal;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private bool IsModuleExists(ProjectModule module)
        {
            try
            {
                string strsql = "";
                string strRetVal = "";
                bool bVal;
                if (module.ModuleId == 0)
                {
                    strsql += "SELECT COUNT(*) FROM SCT_tblprojectmodules WHERE modulename = '";
                    strsql += module.ModuleName;
                    strsql += "' AND projectid = ";
                    strsql += module.ProjectId;
                }
                else
                {
                    strsql += "SELECT COUNT(*) FROM SCT_tblprojectmodules WHERE modulename = '";
                    strsql += module.ModuleName;
                    strsql += "' AND projectid = ";
                    strsql += module.ProjectId;
                    strsql += " AND moduleId <> ";
                    strsql += module.ModuleId;
                }
                SqlConnection cn = new DataConn().GetConn();
                SqlCommand cmd = new SqlCommand(strsql, cn);
                cn.Open();
                strRetVal = cmd.ExecuteScalar().ToString();
                if (strRetVal != "0")
                {
                    bVal = true;
                }
                else
                {
                    bVal = false;
                }
                return bVal;

            }
            catch (Exception ee)
            {
                throw (ee);
            }
        }


        public string UpdateModule(ProjectModule module)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new DataConn().GetConn();
            string strRetVal;
            try
            {
                //if (IsModuleExists(module))
                //{
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spUpdateModule";
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue("@ProjectID", module.ProjectId);
                cmd.Parameters.AddWithValue("@ModuleId", module.ModuleId);
                cmd.Parameters.AddWithValue("@ModuleName", module.ModuleName);
                cmd.Parameters.AddWithValue("@ModuleStatusId", module.ModuleStatusId);
                cmd.Parameters.AddWithValue("@StartDate", module.StartDate);
                cmd.Parameters.AddWithValue("@endDate", module.EndDate);
                cmd.Parameters.AddWithValue("@EmpId", module.AssignedToId);
                cmd.Parameters.AddWithValue("@AssignedBy", module.AssignedBy);
                cmd.Parameters.AddWithValue("@AssignedOn", module.AssignedOn);
                cmd.Parameters.AddWithValue("@LastModifiedOn", module.LastModifiedOn);
                cmd.Parameters.AddWithValue("@LastModifiedBy", module.LastModifiedBy);
                cmd.Parameters.AddWithValue("@EstimateHours", module.dEstimateHours);

                cmd.ExecuteNonQuery();
                strRetVal = "Module Updated Successfully";
                //}
                //else
                //{
                //    //strSql = new ProjectModuleDAL().InsertModule(module);
                //    // objDataBase.SCT_spUpdateModule(strModuleName, intModuleStatusId, strStartDate, StrEndDate, intAssignedToId, IntAssignedBy, strAssignedOn, intLastModifiedBy, strLastModifiedOn, intProjectId, module.ModuleId, dEstimateHours);
                //    cmd.CommandType = CommandType.StoredProcedure;
                //    cmd.CommandText = "SCT_spInsertModule";
                //    cmd.Connection = cn;
                //    cn.Open();
                //    cmd.Parameters.AddWithValue("@ProjectID", module.ProjectId);
                //    cmd.Parameters.AddWithValue("@ModuleName", module.ModuleName);
                //    cmd.Parameters.AddWithValue("@ModuleStatusId", module.ModuleStatusId);
                //    cmd.Parameters.AddWithValue("@StartDate", module.StartDate);
                //    cmd.Parameters.AddWithValue("@endDate", module.EndDate);
                //    cmd.Parameters.AddWithValue("@AssignedTo", module.AssignedToId);
                //    cmd.Parameters.AddWithValue("@AssignedBy", module.AssignedBy);
                //    cmd.Parameters.AddWithValue("@AssignedOn", module.AssignedOn);
                //    cmd.Parameters.AddWithValue("@LastModifiedOn", module.LastModifiedOn);
                //    cmd.Parameters.AddWithValue("@EmpId", module.LastModifiedBy);
                //    cmd.Parameters.AddWithValue("@EstimateHours", module.dEstimateHours);
                //    cmd.ExecuteNonQuery();
                //    strRetVal = "Module Added Successfully";

                //}
                // return strRetVal;
            }
            catch (Exception ex)
            {
                string ErrMsg = ex.Message + "\n" + ex.StackTrace;
                strRetVal = "Module Not Updated Successfully";
            }
            return strRetVal;
        }

        public string InsertModule(ProjectModule module)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                string strSql;
                string strRetVal;

                if (!IsModuleExists(module))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SCT_spInsertModule";
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.Parameters.AddWithValue("@ProjectID", module.ProjectId);
                    cmd.Parameters.AddWithValue("@ModuleName", module.ModuleName);
                    cmd.Parameters.AddWithValue("@ModuleStatusId", module.ModuleStatusId);
                    cmd.Parameters.AddWithValue("@StartDate", module.StartDate);
                    cmd.Parameters.AddWithValue("@endDate", module.EndDate);
                    cmd.Parameters.AddWithValue("@AssignedTo", module.AssignedToId);
                    cmd.Parameters.AddWithValue("@AssignedBy", module.AssignedBy);
                    cmd.Parameters.AddWithValue("@AssignedOn", Convert.ToDateTime(module.AssignedOn));
                    cmd.Parameters.AddWithValue("@LastModifiedOn", Convert.ToDateTime(module.LastModifiedOn));
                    cmd.Parameters.AddWithValue("@EmpId", module.LastModifiedBy);
                    cmd.Parameters.AddWithValue("@EstimateHours", module.dEstimateHours);
                    cmd.ExecuteNonQuery();
                    strRetVal = "Module Added Successfully";
                }
                else
                {
                    strRetVal = "Module has already been assigned, Could not be saved!";
                }
                return strRetVal;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataSet GetModulesPerProject(ProjectModule module)
        {
            try
            {
                SqlDataAdapter adpt = new SqlDataAdapter();
                SqlConnection cn = new DataConn().GetConn();
                DataSet DTSet = new DataSet();
                string strSql = "";

                strSql += "SELECT B.EmpId, B.EmailID, A.ModuleId, A.ProjectId, A.ModuleName, C.ModuleStatus, ";
                strSql += " A.ModuleStatusId, convert(char,nullif(A.Startdate,'1/1/1900'),106) AS ModuleStartDate, A.StartDate, convert(char,nullif(A.EndDate,'1/1/1900'),106) AS ModuleEndDate, A.EndDate, A.AssignedTo AS AssignedToID, A.EstimateHours, A.assignedBy, A.AssignedOn,u.UserName as [Assigned To] ";
                strSql += " FROM SCT_tblprojectmodules A ";
                strSql += " LEFT JOIN SCT_tblemployees B ON A.assignedto = B.empid LEFT JOIN SCT_lkpmodulestatus C ON ";
                strSql += " A.modulestatusid = C.modulestatusid LEFT JOIN SCT_tblUsers U on u.userId=B.EmpId AND ";
                strSql += " U.ISClient = 0 WHERE ProjectID = ";
                strSql += module.ProjectId;

                #region "Unused Code"                
                /*strSql += "SELECT B.EmpId, B.EmailID, A.ModuleId, A.ProjectId, A.ModuleName, C.ModuleStatus, A.ModuleStatusId, convert(char,nullif(A.Startdate,\'1/1/1900\'),106) AS ModuleStartDate, A.StartDate, convert(char,nullif(A.EndDate,\'1/1/1900\'),106) AS ModuleEndDate, A.EndDate, A.AssignedTo AS AssignedToID, A.EstimateHours, A.assignedBy, A.AssignedOn,u.UserName as [Assigned To] ";
                strSql += "FROM SCT_tblprojectmodules A ";
                strSql += "LEFT JOIN SCT_tblemployees B ON A.assignedto = B.empid LEFT JOIN SCT_lkpmodulestatus C ON ";
                strSql += "A.modulestatusid = C.modulestatusid LEFT JOIN SCT_tblUsers U on u.userId=B.EmpId AND ";
                strSql += "U.ISClient = 0 WHERE ProjectID = ";
                strSql += intProjectId;*/
                /*strSql += " AND C.Priority < ";
                strSql += objDataClass.GetHideStatusValue("ShowDefaultModules");
                strSql += " ORDER BY ";
                strSql += strSortCriteria;
                strSql += " ";
                strSql += strSortDir;*/
                #endregion

                adpt = new SqlDataAdapter(strSql, cn);

                adpt.Fill(DTSet);
                return DTSet;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataSet GetModuleStatus()
        {
            try
            {
                SqlDataAdapter adpt = new SqlDataAdapter();
                SqlConnection cn = new DataConn().GetConn();
                DataSet DTSet = new DataSet();
                string strSql;
                strSql = "SELECT * FROM SCT_lkpmodulestatus ORDER BY Priority";
                adpt = new SqlDataAdapter(strSql, cn);

                adpt.Fill(DTSet);
                return DTSet;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataSet GetModules(ProjectModule module)
        {
            try
            {
                string strSql = "";
                SqlDataAdapter adpt = new SqlDataAdapter();
                SqlConnection cn = new DataConn().GetConn();
                strSql += "SELECT ModuleID, ModuleName FROM SCT_tblProjectModules WHERE ProjectID = ";
                strSql += module.ProjectId;
                strSql += " ORDER BY ModuleName";

                adpt = new SqlDataAdapter(strSql, cn);
                DataSet DTSet = new DataSet();
                adpt.Fill(DTSet);
                return DTSet;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataSet GetModulesPerModuleLead(ProjectModule module)
        {
            try
            {
                SqlDataAdapter adpt = new SqlDataAdapter();
                SqlConnection cn = new DataConn().GetConn();

                string strSql = "";
                strSql += "SELECT ModuleID, ModuleName FROM SCT_tblProjectModules WHERE ProjectID = ";
                strSql += module.ProjectId;
                strSql += "AND AssignedTo = " + module.AssignedToId + " ORDER BY ModuleName";

                adpt = new SqlDataAdapter(strSql, cn);
                DataSet DTSet = new DataSet();
                adpt.Fill(DTSet);
                return DTSet;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataSet GetHideModuleStatus()
        {
            try
            {
                SqlDataAdapter adpt = new SqlDataAdapter();
                SqlConnection cn = new DataConn().GetConn();

                string strSql;
                strSql = "SELECT 'Hide '+ModuleStatus+' and Above' AS [ModuleStatus], Priority FROM SCT_lkpmodulestatus ORDER BY priority";
                adpt = new SqlDataAdapter(strSql, cn);
                DataSet DTSet = new DataSet();
                adpt.Fill(DTSet);
                return DTSet;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        private string SetStrRight(string pstr)
        {
            try
            {
                string strRetVal;

                strRetVal = pstr.Replace("'", "''");
                return strRetVal;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataSet GetSearchModule(ProjectModule module)
        {
            try
            {
                string strSql = "";
                string strSearch = "";
                SqlDataAdapter adpt = new SqlDataAdapter();
                SqlConnection cn = new DataConn().GetConn();

                strSql += "SELECT B.EmpId, B.EmailID, A.ModuleId, A.ProjectId, A.ModuleName, C.ModuleStatus, A.ModuleStatusId, convert(char,nullif(A.Startdate,\'1/1/1900\'),106) AS ModuleStartDate, A.StartDate, convert(char,nullif(A.EndDate,'1/1/1900'),106) AS ModuleEndDate, A.EndDate, A.AssignedTo AS AssignedToID, A.EstimateHours, A.assignedBy, A.AssignedOn,u.UserName as [Assigned To] ";
                strSql += "FROM SCT_tblprojectmodules A LEFT JOIN SCT_tblemployees B ON A.assignedto = B.empid ";
                strSql += "LEFT JOIN SCT_lkpmodulestatus C ON A.modulestatusid = C.modulestatusid left join ";
                strSql += "SCT_tblUsers u on u.UserId = B.EmpId AND U.IsClient = 0 WHERE ProjectID = ";
                strSql += module.ProjectId;
                if (module.ModuleName != "")
                {
                    if (strSearch == "")
                    {
                        strSearch += " AND (A.ModuleName LIKE '%";
                        strSearch += module.ModuleName;
                        strSearch += "%')";
                    }
                    else
                    {
                        strSearch += " AND (A.ModuleName LIKE '%";
                        strSearch += module.ModuleName;
                        strSearch += "%')";
                    }
                }
                if (module.ModuleStatusId != 0)
                {
                    if (strSearch == "")
                    {
                        strSearch += " AND (A.ModuleStatusId = ";
                        strSearch += module.ModuleStatusId;
                        strSearch += ")";
                    }
                    else
                    {
                        strSearch += " AND (A.ModuleStatusId = ";
                        strSearch += module.ModuleStatusId;
                        strSearch += ")";
                    }
                }
                if (module.HideStatusId != 0)
                {
                    if (strSearch == "")
                    {
                        strSearch += " AND (C.Priority < ";
                        strSearch += module.HideStatusId;
                        strSearch += ")";
                    }
                    else
                    {
                        strSearch += " AND (C.Priority <> ";
                        strSearch += module.HideStatusId;
                        strSearch += ")";
                    }
                }
                strSql += strSearch;
                strSql += " ORDER BY 1 ";
                //strSql += module.SortCriteria;
                strSql += "  ASC";
                //strSql += module.SortDir;
                adpt = new SqlDataAdapter(strSql, cn);
                DataSet DTSet = new DataSet();
                adpt.Fill(DTSet);
                return DTSet;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message + "\n" + ex.StackTrace);
            }
        }

        public DataSet GetModuleNames(int ProjectID)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT MODULEID AS [ID],MODULENAME AS [DESC] FROM SCT_tblProjectModules WHERE ProjectID = '" + ProjectID + "' order by MODULENAME", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetModuleHistory(int ProjectID)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT C.ModuleID AS [ID], A.ValueType + ' ' + 'changed' + ' ' + IsNull(CONVERT(char,NullIf(A.OLDVALUE,'1/1/1900'),106),'N/A') + ' ' + 'to' + ' ' + IsNull(CONVERT(char,NullIf(A.NEWVALUE,'1/1/1900'),106),'N/A') AS STATUS, B.FirstName + ' ' + B.LastName AS NAME, LTRIM(RTRIM(CONVERT(CHAR, A.ModifiedOn, 106))) AS MODIFIEDON, LTRIM(RTRIM(A.Remarks)) AS REMARKS FROM SCT_tblProjectHistory A INNER JOIN SCT_tblEmployees B ON A.ModifiedBy = B.EmpID INNER JOIN SCT_tblProjectModules C ON A.PMTID = C.ModuleID WHERE (A.IDType = 'M') AND (C.ProjectID ='" + ProjectID + "') ORDER BY CONVERT(DATETIME, A.ModifiedOn) DESC ", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetModulesList(int ProjectID, int moduleId)
        {
            try
            {
                SqlDataAdapter adpt = new SqlDataAdapter();
                SqlConnection cn = new DataConn().GetConn();
                string strSql;
                if (moduleId > 0)
                {
                    strSql = "SELECT DISTINCT ModuleID AS [ID],ModuleName AS [NAME] FROM SCT_tblProjectModules " + " WHERE ProjectID = \'" + ProjectID + "\' AND moduleid = " + moduleId;
                }
                else
                {
                    strSql = "SELECT DISTINCT ModuleID AS [ID],ModuleName AS [NAME] FROM SCT_tblProjectModules WHERE ProjectID = \'" + ProjectID + "\'";
                }
                adpt = new SqlDataAdapter(strSql, cn);
                DataSet DTSet = new DataSet();
                adpt.Fill(DTSet);
                return DTSet;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataSet GetDistinctModules(int ProjectID)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT DISTINCT ModuleID AS [ID],ModuleName AS [NAME] FROM SCT_tblProjectModules WHERE ProjectID = '" + ProjectID + "'", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }


        public DataSet GetDistinctModules(object ProjectID, object startpage, object pagesize, object tablename)
        {
            try
            {
                DataSet DTSet = new DataSet();
                string strQry;
                strQry = "SELECT DISTINCT ModuleID AS [ID],ModuleName AS [NAME] FROM SCT_tblProjectModules WHERE ProjectID = '" + ProjectID + "'";
                return DTSet;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataSet GetSelectedModule(int ProjectID, int ModuleID)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT ModuleID AS [ID],ModuleName AS [NAME] FROM SCT_tblProjectModules WHERE ProjectID = '" + ProjectID + "' AND ModuleID = '" + ModuleID + "'", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetSelectedModuleHistory(int ProjectID, int ModuleID)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT C.ModuleID AS [ID], A.ValueType + ' ' + 'changed' + ' ' + A.OldValue + ' ' + 'to' + ' ' + A.NewValue AS STATUS, B.FirstName + ' ' + B.LastName AS NAME, LTRIM(RTRIM(CONVERT(CHAR, A.ModifiedOn, 106))) AS MODIFIEDON, LTRIM(RTRIM(A.Remarks)) AS REMARKS FROM SCT_tblProjectHistory A INNER JOIN SCT_tblEmployees B ON A.ModifiedBy = B.EmpID INNER JOIN SCT_tblProjectModules C ON A.PMTID = C.ModuleID WHERE A.IDType = 'M' AND C.ProjectID = '" + ProjectID + "' AND C.ModuleID = '" + ModuleID + "' ORDER BY CONVERT(DATETIME, A.ModifiedOn) DESC", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetActivities()
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("select ActivityID, ActivityName from SCt_lkpActivity", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetTasks(int ProjectID)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT TaskID AS [ID],TaskDesc AS [NAME] FROM SCT_tblProjectModuleTasks WHERE ProjectID = '" + ProjectID + "'", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetTasks(object ProjectID, object startpage, object pagesize, object tablename)
        {
            try
            {
                DataSet DTSet = new DataSet();
                string strQry;
                strQry = "SELECT DISTINCT ModuleID AS [ID],ModuleName AS [NAME] FROM SCT_tblProjectModules WHERE ProjectID = \'" + ProjectID + "\'";
                SqlDataAdapter adpt = new SqlDataAdapter(strQry, new DataConn().GetConn());
                adpt.Fill(DTSet);
                return DTSet;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataSet GetSelectedTaskHistory(int ProjectID)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT C.TaskID AS [ID], A.ValueType + ' ' + 'changed' + ' ' + IsNull(CONVERT(char,NullIf(A.OLDVALUE,'1/1/1900'),106),'N/A') + ' ' + 'to' + ' ' + IsNull(CONVERT(char,NullIf(A.NEWVALUE,'1/1/1900'),106),'N/A') AS STATUS, B.FirstName + ' ' + B.LastName AS NAME, LTRIM(RTRIM(CONVERT(CHAR, A.ModifiedOn, 106))) AS MODIFIEDON, LTRIM(RTRIM(A.Remarks)) AS REMARKS FROM SCT_tblProjectHistory A INNER JOIN SCT_tblEmployees B ON A.ModifiedBy = B.EmpID INNER JOIN SCT_tblProjectModuleTasks C ON A.PMTID = C.TaskID WHERE A.IDType = 'T' AND C.ProjectID = '" + ProjectID + "' ORDER BY CONVERT(DATETIME, A.ModifiedOn) DESC", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }


        public DataSet GetSelectedTask(int ProjectID, int TaskID)
        {

            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT DISTINCT TaskID AS [ID],TaskDesc AS [NAME] FROM SCT_tblProjectModuleTasks WHERE ProjectID = '" + ProjectID + "' AND TaskID = '" + TaskID + "'", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetSelectedTaskHistory(int ProjectID, int TaskID)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT C.TaskID AS [ID], A.ValueType + ' ' + 'changed' +' ' + A.OldValue + ' ' + 'to' + ' ' + A.NewValue AS STATUS, B.FirstName + ' ' + B.LastName AS NAME, LTRIM(RTRIM(CONVERT(CHAR, A.ModifiedOn, 106))) AS MODIFIEDON, LTRIM(RTRIM(A.Remarks)) AS REMARKS FROM SCT_tblProjectHistory A INNER JOIN SCT_tblEmployees B ON A.ModifiedBy = B.EmpID INNER JOIN SCT_tblProjectModuleTasks C ON A.PMTID = C.TaskID WHERE A.IDType = 'T' AND C.ProjectID = '" + ProjectID + "' AND C.TaskID = '" + TaskID + "' ORDER BY CONVERT(DATETIME, A.ModifiedOn) DESC", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetTaskNames(int ProjectID)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT TaskID AS [ID],TaskDesc AS [DESC] FROM SCT_tblProjectModuleTasks WHERE ProjectID = '" + ProjectID + "' order by TaskDesc", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetProjectTeamMembers(Project project)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            adpt = new SqlDataAdapter("select Sct_tblTeams.EmpID,FirstName + ' ' + LastName as Name from Sct_tblEmployees inner join Sct_tblTeams on Sct_tblEmployees.EmpId = Sct_tblTeams.EmpId where ProjectID=" + project.ProjectID + "  and IsReportingManager=0", cn);
            adpt.Fill(ds);
            return ds;
        }

        public DataSet GetProjectTeamManagers(Project project)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            adpt = new SqlDataAdapter("select Sct_tblTeams.EmpID,FirstName + ' ' + LastName as Name from Sct_tblEmployees inner join Sct_tblTeams on Sct_tblEmployees.EmpId = Sct_tblTeams.EmpId where ProjectID=" + project.ProjectID + "  and IsReportingManager=1", cn);
            adpt.Fill(ds);
            return ds;
        }

        public DataSet GetModuleByProjectId(Project project)
        {
            string strSql = string.Empty;
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            strSql += "SELECT B.EmpId, B.EmailID, A.ModuleId, A.ProjectId, A.ModuleName, C.ModuleStatus, A.ModuleStatusId, convert(char,nullif(A.Startdate,'1/1/1900'),106) AS ModuleStartDate, A.StartDate, convert(char,nullif(A.EndDate,'1/1/1900'),106) AS ModuleEndDate, A.EndDate, A.AssignedTo AS AssignedToID, A.EstimateHours, A.assignedBy, A.AssignedOn,u.UserName as [Assigned To] ";
            strSql += " FROM SCT_tblprojectmodules A LEFT JOIN SCT_tblemployees B ON A.assignedto = B.empid ";
            strSql += " LEFT JOIN SCT_lkpmodulestatus C ON A.modulestatusid = C.modulestatusid left join ";
            strSql += " SCT_tblUsers u on u.UserId = B.EmpId AND U.IsClient = 0 WHERE ProjectID = " + project.ProjectID;

            adpt = new SqlDataAdapter(strSql, cn);
            adpt.Fill(ds);
            return ds;
        }

        public DataSet GetModuleByProjectForEmp(Project project, Employee emp, ProjectModule module)
        {
            string strSql = string.Empty;
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            strSql += "SELECT B.EmpId, B.EmailID, A.ModuleId, A.ProjectId, A.ModuleName, C.ModuleStatus, A.ModuleStatusId, convert(char,nullif(A.Startdate,'1/1/1900'),106) AS ModuleStartDate, A.StartDate, convert(char,nullif(A.EndDate,'1/1/1900'),106) AS ModuleEndDate, A.EndDate, A.AssignedTo AS AssignedToID, A.EstimateHours, A.assignedBy, A.AssignedOn,u.UserName as [Assigned To] ";
            strSql += " FROM SCT_tblprojectmodules A LEFT JOIN SCT_tblemployees B ON A.assignedto = B.empid ";
            strSql += " LEFT JOIN SCT_lkpmodulestatus C ON A.modulestatusid = C.modulestatusid left join ";
            strSql += " SCT_tblUsers u on u.UserId = B.EmpId AND U.IsClient = 0 WHERE ProjectID = " + project.ProjectID + " and B.EmpId=" + emp.EmployeeId + " and ModuleId=" + module.ModuleId;

            adpt = new SqlDataAdapter(strSql, cn);
            adpt.Fill(ds);
            return ds;
        }

        public int GetModuleIDByName(string module_name)
        {
            int id = -1;
            string strSql = string.Empty;
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            strSql += "select moduleId from Sct_tblProjectModules where modulename='" + module_name + "'";
            adpt = new SqlDataAdapter(strSql, cn);
            adpt.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                id = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
            }
            return id;
        }

        public DataSet GetProjectTeamDevelopers(Project project)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            string strSql = string.Empty;
            strSql = "SELECT distinct SCT_tblTeams.EmpID, SCT_tblEmployees.FirstName + ' ' + SCT_tblEmployees.LastName as Name, SCT_lkpMemberRole.MemberRole,SCT_tblTeams.MemberRoleID ";
            strSql += " FROM SCT_tblEmployees INNER JOIN SCT_tblTeams ON SCT_tblEmployees.EmpID = SCT_tblTeams.EmpID INNER JOIN SCT_lkpMemberRole ON SCT_tblTeams.MemberRoleID = SCT_lkpMemberRole.MemberRoleID ";
            strSql += " where SCT_tblTeams.MemberRoleID in (1,2,3) and  ProjectID=" + project.ProjectID;
            adpt = new SqlDataAdapter(strSql, cn);
            adpt.Fill(ds);
            return ds;
        }

        public DataSet GetProjectTeamQA(Project project)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            string strSql = string.Empty;
            strSql = "SELECT distinct SCT_tblTeams.EmpID, SCT_tblEmployees.FirstName + ' ' + SCT_tblEmployees.LastName as Name, SCT_lkpMemberRole.MemberRole,SCT_tblTeams.MemberRoleID ";
            strSql += " FROM SCT_tblEmployees INNER JOIN SCT_tblTeams ON SCT_tblEmployees.EmpID = SCT_tblTeams.EmpID INNER JOIN SCT_lkpMemberRole ON SCT_tblTeams.MemberRoleID = SCT_lkpMemberRole.MemberRoleID ";
            strSql += " where SCT_tblTeams.MemberRoleID in (6) and  ProjectID=" + project.ProjectID;
            adpt = new SqlDataAdapter(strSql, cn);
            adpt.Fill(ds);
            return ds;
        }

        public DataSet GetModuleByProjectForUser(Project project, Employee emp)
        {
            string strSql = string.Empty;
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            strSql += "SELECT A.ModuleId, A.ModuleName ";
            strSql += " FROM SCT_tblprojectmodules A LEFT JOIN SCT_tblemployees B ON A.assignedto = B.empid ";
            strSql += " LEFT JOIN SCT_lkpmodulestatus C ON A.modulestatusid = C.modulestatusid left join ";
            strSql += " SCT_tblUsers u on u.UserId = B.EmpId AND U.IsClient = 0 WHERE ProjectID = " + project.ProjectID + " and EmpID = " + emp.EmployeeId;

            adpt = new SqlDataAdapter(strSql, cn);
            adpt.Fill(ds);
            return ds;
        }
    }
}
