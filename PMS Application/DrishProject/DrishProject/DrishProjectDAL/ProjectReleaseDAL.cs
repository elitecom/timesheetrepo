﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectDAL
{
    public class ProjectReleaseDAL
    {
        public DataSet GetReleaseStatus()
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT * FROM SCT_lkpReleaseStatus order by ReleaseStatus", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public ProjectRelease GetRelease(ProjectRelease release)
        {
            SqlDataReader rdr;
            string strsql = "";
            strsql += "SELECT A.ReleaseId, A.releasestatusid, A.ProjectId, B.releasestatus, A.ReleaseTarget, A.DeliveryIssues ,A.Description, ";
            strsql += "convert(char,nullif(A.ReleaseDate,\'1/1/1900\'),106) AS ReleaseDateF, convert(char,nullif(A.ReleaseDate,\'1/1/1900\'),101) AS ReleaseDate,convert(char,nullif(A.ActualDate,\'1/1/1900\'),106) AS ActualDate, A.Deliverable FROM ";
            strsql += "SCT_tblProjectReleases A LEFT JOIN SCT_lkpReleaseStatus B ON ";
            strsql += "A.releasestatusid = B.releasestatusid WHERE ReleaseID = ";
            strsql += release.ReleaseId;

            SqlCommand cmd;

            SqlConnection cn = new DataConn().GetConn();
            cmd = new SqlCommand(strsql, cn);
            ProjectRelease rl = new ProjectRelease();
            cn.Open();
            rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                while (rdr.Read())
                {
                    if (Convert.ToDateTime(rdr["releasedate"]).ToShortDateString() != null)
                    {
                        release.ReleaseDate = Convert.ToDateTime(rdr["releasedate"].ToString());
                    }
                    if (Convert.ToDateTime(rdr["releasedateF"]).ToShortDateString() != null)
                    {
                        release.ReleaseDateF = Convert.ToDateTime(rdr["releasedateF"].ToString());
                    }
                    if (rdr["description"].ToString() != null)
                    {
                        release.Description = rdr["description"].ToString();
                    }
                    if (rdr["deliverable"].ToString() != null)
                    {
                        release.Deliverables = rdr["deliverable"].ToString();
                    }
                    if (Convert.ToInt32(rdr["releasestatusid"]).ToString() != null)
                    {
                        release.ReleaseStatusId = Convert.ToInt32(rdr["releasestatusid"].ToString());
                    }
                    if (rdr["releasestatus"].ToString() != null)
                    {
                        release.ReleaseStatus = rdr["releasestatus"].ToString();
                    }
                    if (rdr["ReleaseTarget"].ToString() != null)
                    {
                        release.ReleaseTarget = rdr["ReleaseTarget"].ToString();
                    }
                    if (rdr["DeliveryIssues"].ToString() != null)
                    {
                        release.DeliveryIssues = rdr["DeliveryIssues"].ToString();
                    }
                    if (Convert.ToDateTime(rdr["ActualDate"]).ToShortDateString() != null)
                    {
                        release.ActualDate = Convert.ToDateTime(rdr["ActualDate"].ToString());
                    }
                }
                rdr.Close();               
            }
            return release;
        }

        public string SetStrRight(string pstr)
        {
            string strRetVal;

            strRetVal = pstr.Replace("\'", "\'\'");
            return strRetVal;

        }

        public bool IsReleaseExists(ProjectRelease release)
        {

            string strsql = "";
            string strRetVal = "";
            bool bVal;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();            
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;
            cn.Open();
            if (release.ReleaseId == 0)
            {
                #region "Unused Code"                
                /*strsql += "SELECT COUNT(*) FROM SCT_tblProjectReleases WHERE ReleaseDate = \'";
                strsql += release.ReleaseDate;
                strsql += "\' AND description = \'";
                strsql += release.Description;
                strsql += "\' AND Projectid = ";
                strsql += release.ProjectId;*/
                #endregion

                strsql += "SELECT COUNT(*) FROM SCT_tblProjectReleases WHERE ";
                strsql += " ReleaseDate = @releaseDate ";
                strsql += " AND description = '' AND Projectid = @projectid";
                cmd.CommandText = strsql;
                cmd.Parameters.AddWithValue("@releaseDate", release.ReleaseDate);
                cmd.Parameters.AddWithValue("@projectid", release.ProjectId);
            }
            else
            {
                #region "Unused Code"                
                //strsql += "SELECT COUNT(*) FROM SCT_tblProjectReleases WHERE ReleaseDate = \'";
                //strsql += release.ReleaseDate;
                //strsql += "\' AND description = \'";
                //strsql += release.Description;
                //strsql += "\' AND ProjectId = ";
                //strsql += release.ProjectId;
                //strsql += " AND Releaseid <> ";
                //strsql += release.ReleaseId;
                #endregion

                strsql += "SELECT COUNT(*) FROM SCT_tblProjectReleases WHERE ";
                strsql += " ReleaseDate = @releaseDate  AND description = @Description  AND Projectid = @ProjectId";
                strsql += " AND ReleaseId <> @ReleaseId";
                cmd.CommandText = strsql;
                cmd.Parameters.AddWithValue("@releaseDate", release.ReleaseDate);
                cmd.Parameters.AddWithValue("@Description", release.Description);
                cmd.Parameters.AddWithValue("@projectid", release.ProjectId);
            }
            
            strRetVal = cmd.ExecuteScalar().ToString();
            if (strRetVal != "0")
            {
                bVal = true;
            }
            else
            {
                bVal = false;
            }
            return bVal;

        }

        public bool UpdateRelease(ProjectRelease release)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            try
            {
                if (!IsReleaseExists(release))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SCT_spUpdateRelease";
                    cmd.Connection = cn;
                    cn.Open();

                    //@pMeetingId int  
                    cmd.Parameters.AddWithValue("@pdtReleaseDate", release.ReleaseDate);
                    cmd.Parameters.AddWithValue("@pDescription", release.Description);
                    cmd.Parameters.AddWithValue("@p@strDeliverable  ", release.Deliverables);
                    cmd.Parameters.AddWithValue("@pstrReleaseTarget", release.ReleaseTarget);
                    cmd.Parameters.AddWithValue("@pstrDeliveryIssues ", release.DeliveryIssues);
                    cmd.Parameters.AddWithValue("@pModifiedBy", release.LastModifiedBy);
                    cmd.Parameters.AddWithValue("@pModifiedOn ", release.LastModifiedOn);
                    cmd.Parameters.AddWithValue("@pintReleaseId", release.ReleaseId);
                    cmd.Parameters.AddWithValue("@ltatusId", release.ReleaseStatusId);
                    cmd.Parameters.AddWithValue("@pdtActualDate", release.ActualDate);

                    pp = new SqlParameter();
                    pp.ParameterName = "@ID";
                    pp.DbType = DbType.Int32;
                    pp.Direction = ParameterDirection.Output;
                    cmd.Parameters.AddWithValue("@ID", 0);
                    int temp;
                    temp = cmd.ExecuteNonQuery();
                    res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                    if (res != -1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                } 
                return true;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public bool InsertRelease(ProjectRelease release)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            try
            {
                if (!IsReleaseExists(release))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SCT_spInsertRelease";
                    cmd.Connection = cn;
                    cn.Open();

                    cmd.Parameters.AddWithValue("@pProjectId", release.ProjectId);
                    cmd.Parameters.AddWithValue("@pDescription", release.Description);
                    cmd.Parameters.AddWithValue("@pReleaseDate", release.ReleaseDate);
                    cmd.Parameters.AddWithValue("@pDeliverable", release.Deliverables);
                    cmd.Parameters.AddWithValue("@pdeliveryIssues", release.DeliveryIssues);
                    cmd.Parameters.AddWithValue("@pReleaseTarget ", release.ReleaseTarget);
                    cmd.Parameters.AddWithValue("@pEnteredBy", release.EnteredBy);
                    cmd.Parameters.AddWithValue("@pEnteredOn", release.EnteredOn);
                    cmd.Parameters.AddWithValue("@pStatusId", release.ReleaseStatusId);
                    cmd.Parameters.AddWithValue("@pActualDate", release.ActualDate);
                    cmd.Parameters.AddWithValue("@pLastModifiedBy", release.LastModifiedBy);
                    //cmd.Parameters.AddWithValue("@pdeliveryIssues", release.ReleaseIssues);
                    pp = new SqlParameter();
                    pp.ParameterName = "@ID";
                    pp.DbType = DbType.Int32;
                    pp.Direction = ParameterDirection.Output;
                    cmd.Parameters.AddWithValue("@ID", 0);
                    int temp;
                    temp = cmd.ExecuteNonQuery();
                    res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                    if (res != -1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                } 
                return true;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public DataSet GetFeatures(ProjectRelease release)
        {
            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();

            string strSql = "";
            strSql += "SELECT A.Taskid, A.Projectid, A.Moduleid, A.StartDate as strStartDate, A.EndDate as strEndDate,B.ModuleName, A.taskDesc AS [Tasks], C.ModuleStatus AS TaskStatus, ";
            strSql += "convert(char,nullif(A.StartDate,\'1/1/1900\'),106) AS StartDate, ";
            strSql += "convert(char,nullif(A.EndDate,\'1/1/1900\'),106) AS EndDate, D.UserName AS Developer, A.EstimateHours, ";
            strSql += "E.Username AS QA, A.EstimateQAHours, A.taskstatusid ";
            strSql += "FROM SCT_tblProjectModuleTasks A LEFT JOIN  SCT_tblprojectmodules B ON A.moduleid = B.moduleid ";
            strSql += "LEFT JOIN SCT_lkpmodulestatus C ON A.taskstatusid = C.modulestatusid LEFT JOIN ";
            strSql += "(select * from SCT_tblusers where isclient=0) AS D On A.assignedto = D.Userid )";
            strSql += "LEFT JOIN (select * from SCT_tblUsers where isclient=0) AS E ON A.qaAssigned = E.Userid ";
            strSql += "WHERE isclosed = 0 AND A.ProjectID = ";
            strSql += release.ProjectId;
            //strSql += " order by ";
            //strSql += release.SortCriteria;
            //strSql += " ";
            //strSql += release.SortDir;
            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }

        public DataSet GetReleaseFeatures(ProjectRelease release)
        {
            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "";
            strSql += "SELECT A.Taskid, A.Projectid, A.Moduleid, A.StartDate AS strStartDate, A.EndDate AS strEndDate,B.ModuleName, A.taskDesc AS [Tasks], C.ModuleStatus AS TaskStatus, ";
            strSql += "convert(char,nullif(A.StartDate,'1/1/1900'),106) AS StartDate, ";
            strSql += "convert(char,nullif(A.EndDate,'1/1/1900'),106) AS EndDate, D.UserName AS Developer, A.EstimateHours, ";
            strSql += "E.Username AS QA, A.EstimateQAHours, A.taskstatusid ";
            strSql += "FROM SCT_tblProjectModuleTasks A LEFT JOIN  SCT_tblprojectmodules B ON A.moduleid = B.moduleid ";
            strSql += "LEFT JOIN SCT_lkpmodulestatus C ON A.taskstatusid = C.modulestatusid LEFT JOIN ";
            strSql += "(select * from SCT_tblusers where isclient =0) AS D On A.assignedto = D.Userid ";
            strSql += "LEFT JOIN (select * from SCT_tblusers where isclient = 0) AS E ON A.qaAssigned = E.userid ";
            strSql += "left join SCT_tblReleaseFeatures R ON R.TaskID = A.TaskID WHERE isclosed = 0 AND A.ProjectID = ";
            strSql += release.ProjectId;
            strSql += " AND R.ReleaseID = ";
            strSql += release.ReleaseId;
            //strSql += " order by ";
            //strSql += release.SortCriteria;
            //strSql += " ";
            //strSql += release.SortDir;
            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }

        public DataSet GetReleaseFeaturesDelivered(ProjectRelease release)
        {
            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "";

            strSql += "SELECT A.Taskid, A.Projectid, A.Moduleid, A.StartDate AS strStartDate, A.EndDate AS strEndDate,B.ModuleName, A.taskDesc AS [Tasks], C.ModuleStatus AS TaskStatus, ";
            strSql += "convert(char,nullif(A.StartDate,\'1/1/1900\'),106) AS StartDate, ";
            strSql += "convert(char,nullif(A.EndDate,\'1/1/1900\'),106) AS EndDate, D.UserName AS Developer, A.EstimateHours, ";
            strSql += "E.Username AS QA, A.EstimateQAHours, A.taskstatusid ";
            strSql += "FROM SCT_tblProjectModuleTasks A LEFT JOIN  SCT_tblprojectmodules B ON A.moduleid = B.moduleid ";
            strSql += "LEFT JOIN SCT_lkpmodulestatus C ON A.taskstatusid = C.modulestatusid LEFT JOIN ";
            strSql += "(select * from SCT_tblusers where isclient =0) AS D On A.assignedto = D.Userid ";
            strSql += "LEFT JOIN (select * from SCT_tblusers where isclient = 0) AS E ON A.qaAssigned = E.userid ";
            strSql += "left join SCT_tblReleaseFeatures R ON R.TaskID = A.TaskID WHERE  A.ProjectID = ";
            strSql += release.ProjectId;
            strSql += " AND R.ReleaseID = ";

            strSql += release.ReleaseId;
            //strSql += " order by ";
            //strSql += release.SortCriteria;
            //strSql += " ";
            //strSql += release.SortDir;
            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }

        public SqlDataReader GetPlannedFeatures(ProjectRelease release)
        {
            SqlDataReader rdr;
            string strSql;
            bool bVal;
            strSql = "SELECT A.Taskid, A.Releaseid FROM SCT_tblReleaseFeatures A WHERE A.releaseid = " + release.ReleaseId;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strSql, cn);
            cn.Open();
            rdr = cmd.ExecuteReader();
            //string strRetVal = "";
            //strRetVal = cmd.ExecuteScalar().ToString();
            //if (strRetVal != "0")
            //{
            //    bVal = true;
            //}
            //else
            //{
            //    bVal = false;
            //}
            //return bVal;
            return rdr;
        }

        public DataSet GetReleasesPerProject(ProjectRelease release)
        {
            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "";

            strSql += "SELECT A.Description, A.releasestatusid, A.ReleaseId,A.DeliveryIssues,A.ReleaseTarget, ";
            strSql += "convert(char,nullif(A.ReleaseDate,\'1/1/1900\'),106) AS ReleaseDateFormated, A.ReleaseDate, ";
            strSql += "convert(char,nullif(A.ActualDate,\'1/1/1900\'),106) As ActualDateFormated, A.ActualDate, ";
            strSql += "A.Deliverable, B.releasestatus FROM SCT_tblProjectReleases A LEFT JOIN ";
            strSql += "SCT_lkpReleaseStatus B ON  A.releasestatusid = B.releasestatusid ";
            strSql += "WHERE ProjectID = ";
            strSql += release.ProjectId;
            //strSql += " ORDER BY ";
            //strSql += release.SortCriteria;
            //strSql += " ";
            //strSql += release.SortDir;
            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }

        public DataSet GetLatestRelease(ProjectRelease release)
        {

            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "";

            strSql += "SELECT TOP 1 A.Description, A.releasestatusid, A.ReleaseId,A.DeliveryIssues,A.ReleaseTarget, ";
            strSql += "convert(char,nullif(A.ReleaseDate,\'1/1/1900\'),106) AS ReleaseDateFormated, A.ReleaseDate, ";
            strSql += "convert(char,nullif(A.ActualDate,\'1/1/1900\'),106) As ActualDateFormated, A.ActualDate, ";
            strSql += "A.Deliverable, B.releasestatus FROM SCT_tblProjectReleases A LEFT JOIN ";
            strSql += "SCT_lkltatus B ON  A.releasestatusid = B.releasestatusid ";
            strSql += "WHERE ProjectID = ";
            strSql += release.ProjectId;
            strSql += " ORDER BY ";
            strSql += "A.ReleaseDate DESC";
            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }

        public DataSet AddTasktoFeatures(ProjectRelease release)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT MAX(TaskID) AS MaxID FROM SCT_tblProjectModuleTasks", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                SqlCommand cmd = new SqlCommand();
                SqlParameter pp = new SqlParameter();
                int res;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spInsertFeatures";
                cmd.Connection = cn;
                cn.Open();

                cmd.Parameters.AddWithValue("@pReleaseId ", release.ReleaseId);
                cmd.Parameters.AddWithValue("@pTaskId", release.TaskId);
                pp = new SqlParameter();
                pp.ParameterName = "@ID";
                pp.DbType = DbType.Int32;
                pp.Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@ID", 0);
                int temp;
                temp = cmd.ExecuteNonQuery();
                res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                //if (res != -1)
                //{
                //    return true;
                //}
                //else
                //{
                //    return false;
                //}

                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);

            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                //cmd.Dispose();
                cn.Dispose();
            }
        }

        public bool CleanFeatures(ProjectRelease release)
        {

            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spDeleteFeatures";
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue("@pReleaseId", release.ReleaseId);
                pp = new SqlParameter();
                pp.ParameterName = "@ID";
                pp.DbType = DbType.Int32;
                pp.Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@ID", 0);
                int temp;
                temp = cmd.ExecuteNonQuery();
                res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                if (res != -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public bool InsertFeatures(ProjectRelease release)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();            
            int res;
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spInsertFeatures";
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue("@pReleaseId", release.ReleaseId);
                cmd.Parameters.AddWithValue("@pTaskId", release.TaskId);
                pp = new SqlParameter();
                pp.ParameterName = "@ID";
                pp.DbType = DbType.Int32;
                pp.Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@ID", 0);
                int temp;
                temp = cmd.ExecuteNonQuery();
                res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                if (res != -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public DataSet GetReleaseDate(ProjectRelease release)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT TOP 1 ReleaseID,LTRIM(RTRIM(CONVERT(CHAR,ReleaseDate,106))) AS ReleaseDate FROM SCT_tblProjectReleases where ProjectID = \'" + release.ProjectId + "\' ORDER BY CONVERT(DATETIME,ReleaseDate) DESC", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetReleaseDateV2(ProjectRelease release)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT TOP 1 ReleaseID,LTRIM(RTRIM(CONVERT(CHAR,ReleaseDate,106))) AS ReleaseDate,ActualDate FROM SCT_tblProjectReleases where ProjectID = \'" + release.ProjectId + "\' ORDER BY CONVERT(DATETIME,ReleaseDate) DESC", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetNoOfReleaseFeatures(ProjectRelease release)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT COUNT(*) AS NoOfFeatures FROM SCT_tblReleaseFeatures WHERE ReleaseID = " + release.ReleaseId , cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetNoOfCompletedFeatures(ProjectRelease release)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT COUNT(*) AS CompletedFeatures FROM SCT_tblReleaseFeatures A INNER JOIN SCT_tblProjectModuleTasks B ON A.Taskid = B.TaskId JOIN SCT_lkpModuleStatus C ON B.TaskStatusId = C.ModuleStatusID WHERE A.ReleaseId = " + release.ReleaseId + " AND C.Priority >= 4", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public DataSet GetReleaseHistory(ProjectRelease release)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT A.VALUETYPE+\' \'+\'changed\'+\' \'+ IsNull(CONVERT(char,NullIf(A.OLDVALUE,\'1/1/1900\'),106),\'N/A\') + \' \' + \'to\' + \' \' + IsNull(CONVERT(char,NullIf(A.NEWVALUE,\'1/1/1900\'),106),\'N/A\') AS STATUS, B.FIRSTNAME+\' \'+B.LASTNAME AS [NAME], LTRIM(RTRIM(CONVERT(CHAR,A.MODIFIEDON,106))) AS MODIFIEDON,LTRIM(RTRIM(A.REMARKS)) AS REMARKS FROM SCT_tblPROJECTHISTORY A JOIN SCT_tblEMPLOYEES B ON A.MODIFIEDBY = B.EMPID JOIN SCT_tblProjectReleases C ON C.ReleaseID = A.PMTID WHERE C.ProjectID = \'" + release.ProjectId + "\' AND A.IDTYPE = \'R\' ORDER BY CONVERT(DATETIME, A.ModifiedOn) DESC", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }        
    }
}
