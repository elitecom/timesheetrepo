﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectDAL
{
    public class DocumentsDAL
    {
        public string GetFolderPath(Documents doc)
        {
            string strsql;
            string strRetVal = "";
            SqlDataReader rdr;
            SqlCommand cmd;
            SqlConnection cn = new DataConn().GetConn();
            strsql = "(SELECT * from SCT_lkpDocumentType where DocTypeId = " + doc.DocTypeId + ")";
            cmd = new SqlCommand(strsql, cn);
            cn.Open();
            rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                rdr.Read();
                strRetVal = rdr.GetString(4).ToString();
            }
            rdr.Close();
            return strRetVal;
        }


        public void GetDoc(Documents doc)
        {
            Documents docs = new Documents();
            string strsql = "";
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                strsql += "SELECT A.DocId, A.DocTypeId, A.DocDesc, A.ProjectId, A.DocName, A.DocFileName, ";
                strsql += "A.UploadedBy, convert(char,nullif(A.UploadedOn,'1/1/1900'),101) as UploadedOn, ";
                strsql += "B.DocType, C.UserName from SCT_tblDocuments A left join SCT_lkpDocumentType B on ";
                strsql += "A.DocTypeId = B.DocTypeId left join SCT_tblusers C on A.UPloadedBy = C.userid AND ";
                strsql += "C.IsClient = 0 WHERE ";
                strsql += "A.DocID = ";
                strsql += doc.DocId; ;
                SqlDataReader rdr;
                SqlCommand cmd;
                cmd = new SqlCommand(strsql, cn);
                cn.Open();
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    rdr.Read();
                    if (rdr["DocName"].ToString() != null)
                    {
                        docs.DocName = rdr["DocName"].ToString();
                    }
                    if (Convert.ToInt32(rdr["doctypeid"]).ToString() != null)
                    {
                        docs.DocTypeId = Convert.ToInt32(rdr["doctypeid"]);
                    }
                    if (rdr["doctype"].ToString() != null)
                    {
                        docs.DocType = rdr["DocType"].ToString();
                    }
                    if (rdr["docdesc"].ToString() != null)
                    {
                        docs.DocDesc = rdr["docdesc"].ToString();
                    }
                    if (rdr["docfilename"].ToString() != null)
                    {
                        docs.DocFileName = rdr["docfilename"].ToString();
                    }
                    if (rdr["UserName"].ToString() != null)
                    {
                        docs.UploadedBy = rdr["UserName"].ToString();
                    }
                }
            }
            catch (Exception ee)
            {
                throw new Exception(ee.Message);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
            }
        }

        public DataSet GetDocTypes(int ProjectID)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT DocTypeId, DocType from SCT_lkpDocumentType WHERE DocType NOT IN (SELECT SHOWDESC FROM SCT_tblPriorityStatus where ShowDesc != 'Releases' and ShowDesc != 'Downloads') AND (ProjectID = '" + ProjectID + "' OR ProjectID IS NULL) AND ProjectCategory = (SELECT Ltrim(Rtrim(ProjectCategory)) AS ProjectCategory FROM SCT_tblProjects WHERE ProjectID = '" + ProjectID + "') ORDER BY DOCTYPE", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }
        public DataSet GetDocType(string txtFolderPath)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT DocTypeID, LTRIM(RTRIM(DocType)) AS DocType from SCT_lkpDocumentType WHERE FolderPath = \'" + txtFolderPath + "\'", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }


        public bool UpdateDoc(Documents doc)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            bool ans = true;
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spUpdateDocument";
                cmd.Connection = cn;
                cn.Open();


                cmd.Parameters.AddWithValue("@pintDocTypeId", doc.DocTypeId);
                cmd.Parameters.AddWithValue("@pDocDesc", doc.DocDesc);
                cmd.Parameters.AddWithValue("@pDocName", doc.DocName);
                cmd.Parameters.AddWithValue("@pUploadedOn", doc.UploadedOn.ToShortDateString());
                cmd.Parameters.AddWithValue("@pUploadedBY", doc.UploadedBy);
                cmd.Parameters.AddWithValue("@pDocID", doc.DocId);
                pp = new SqlParameter();
                pp.ParameterName = "@ID";
                pp.DbType = DbType.Int32;
                pp.Direction = ParameterDirection.Output;

                cmd.Parameters.AddWithValue("@ID", 0);
                int temp;
                temp = cmd.ExecuteNonQuery();

                res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                if (res != -1)
                {
                    return true;
                }
                else
                {
                    //string strRetVal;
                    //strRetVal = "Document already recorded, Could not be added"; ;
                    ans = false;
                }
                return ans;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        private bool IsDocExists(Documents doc)
        {
            string strsql = "";
            int strRetVal;
            bool bVal;
            if (doc.DocId == 0)
            {
                strsql += "select count(*) from SCT_tblDocuments where DocName = \'";
                strsql += doc.DocName;
                strsql += "\' and doctypeid = ";
                strsql += doc.DocTypeId;
                strsql += " AND ProjectID = ";
                strsql += doc.ProjectId;
            }
            else
            {
                strsql += "select count(*) from SCT_tblDocuments where DocName = \'";
                strsql += doc.DocName;
                strsql += "\' and docid <> ";
                strsql += doc.DocId;
                strsql += " and doctypeid = ";
                strsql += doc.DocTypeId;
                strsql += " AND ProjectID = ";
                strsql += doc.ProjectId;
            }
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strsql, cn);
            cn.Open();

            strRetVal = Convert.ToInt32(cmd.ExecuteScalar());
            if (strRetVal != 0)
            {
                bVal = true;
            }
            else
            {
                bVal = false;
            }
            return bVal;
        }
        public bool IfFilesExists(Documents doc)
        {
            int iCount;
            string strsql = "";
            strsql += "SELECT COUNT(*) AS iCount FROM SCT_tblDocuments WHERE ProjectID = \'" + doc.ProjectId + "\' AND DocTypeID = \'" + doc.DocTypeId + "\'";
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strsql, cn);
            cn.Open();
            iCount = Convert.ToInt32(cmd.ExecuteScalar());
            if (iCount > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool InsertDoc(Documents doc)
        {
            bool ans;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res = 0;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SCT_spInsertDocument";
            cmd.Connection = cn;
            cn.Open();
            if (!IsDocExists(doc))
            {
                try
                {
                    cmd.Parameters.AddWithValue("@pintProjectId", doc.ProjectId);
                    cmd.Parameters.AddWithValue("@pintDocTypeId", doc.DocType);
                    cmd.Parameters.AddWithValue("@pstrDocName", doc.DocName);
                    cmd.Parameters.AddWithValue("@pstrDocDesc", doc.DocDesc);
                    cmd.Parameters.AddWithValue("@pstrDocFileName", doc.DocFileName);
                    cmd.Parameters.AddWithValue("@pintUploadedBy", doc.UploadedBy);
                    cmd.Parameters.AddWithValue("@pdtUploadedOn", doc.UploadedOn.ToShortDateString());
                    pp = new SqlParameter();
                    pp.ParameterName = "@ID";
                    pp.DbType = DbType.Int32;
                    pp.Direction = ParameterDirection.Output;

                    cmd.Parameters.AddWithValue("@ID", 0);
                    int temp;
                    temp = cmd.ExecuteNonQuery();

                    res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                    if (res != -1)
                    {
                        ans = true;
                    }
                    else
                    {
                        ans = false;
                    }
                    return ans;
                }
                catch (Exception ee)
                {
                    throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
                }
                finally
                {
                    if (cn.State == ConnectionState.Open)
                    {
                        cn.Close();
                    }
                    cmd.Dispose();
                    cn.Dispose();
                }
            }
            else
            {
                return false;
            }
        }
        public bool InsertDocComment(Documents doc)
        {

            bool RetVal;
            string StrRetVal;
            RetVal = IsdocCommentExists(doc);
            if (RetVal == false)
            {
                SqlConnection cn = new DataConn().GetConn();
                SqlCommand cmd = new SqlCommand();
                SqlParameter pp = new SqlParameter();
                int res = 0;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spInsertDocumentComments";
                cmd.Connection = cn;
                cn.Open();
                try
                {
                    /*@ProjectId,@DocTypeId, @Comments*/
                    cmd.Parameters.AddWithValue("@pProjectId", doc.ProjectId);
                    cmd.Parameters.AddWithValue("@pDocTypeId", doc.DocTypeId);
                    cmd.Parameters.AddWithValue("@pComments", doc.Comments);
                    pp = new SqlParameter();
                    pp.ParameterName = "@ID";
                    pp.DbType = DbType.Int32;
                    pp.Direction = ParameterDirection.Output;

                    cmd.Parameters.AddWithValue("@ID", 0);
                    int temp;
                    temp = cmd.ExecuteNonQuery();

                    res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                    if (res != -1)
                    {
                        StrRetVal = "Comments Added Successfully";
                    }
                    else
                    {
                        return false;
                    }
                    return true;
                }
                catch (Exception ee)
                {
                    throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
                }
                finally
                {
                    if (cn.State == ConnectionState.Open)
                    {
                        cn.Close();
                    }
                    cmd.Dispose();
                    cn.Dispose();
                }
            }
            else
            {
                SqlConnection cn = new DataConn().GetConn();
                SqlCommand cmd = new SqlCommand();
                SqlParameter pp = new SqlParameter();
                int res = 0;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spUpdateDocumentComments";
                cmd.Connection = cn;
                cn.Open();
                try
                {
                    cmd.Parameters.AddWithValue("@pintProjectId", doc.ProjectId);
                    cmd.Parameters.AddWithValue("@pintDocTypeId", doc.DocTypeId);
                    cmd.Parameters.AddWithValue("@pstrComments", doc.Comments);
                    pp = new SqlParameter();
                    pp.ParameterName = "@ID";
                    pp.DbType = DbType.Int32;
                    pp.Direction = ParameterDirection.Output;

                    cmd.Parameters.AddWithValue("@ID", 0);
                    int temp;
                    temp = cmd.ExecuteNonQuery();

                    res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                    if (res != -1)
                    {
                        return true;
                        // StrRetVal = "Comments Added Successfully";
                    }
                    else
                    {
                        return false;
                    }
                }

                catch (Exception ee)
                {
                    throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
                }
                finally
                {
                    if (cn.State == ConnectionState.Open)
                    {
                        cn.Close();
                    }
                    cmd.Dispose();
                    cn.Dispose();
                }
            }
        }
        #region "Unused Code"
        //public  bool InsertFolderDetails(int ProjectID, string DocType, string FolderPath, string ProjectCategory)
        //     {

        //         if (IfFolderPathExists(ProjectID, DocType, FolderPath, ProjectCategory) == false)
        //         {
        //            SqlConnection cn = new DataConn().GetConn();
        //             SqlCommand cmd = new SqlCommand();
        //             SqlParameter pp = new SqlParameter();
        //             int res = 0;
        //             string StrRetVal = "";
        //             DocumentsClass doc = new DocumentsClass();
        //             cmd.CommandType = CommandType.StoredProcedure;
        //             cmd.CommandText = "SCT_spInsertFolderDetails";
        //             cmd.Connection = cn;
        //             cn.Open();
        //             try
        //             {   cmd.Parameters.AddWithValue("@pProjectId", doc.ProjectId);
        //                 cmd.Parameters.AddWithValue("@pDocTypeId", doc.DocTypeId);
        //                 cmd.Parameters.AddWithValue("@pFolderPath", doc.FolderPath);
        //                 cmd.Parameters.AddWithValue("@pProjectCategory",doc.ProjectCategory);
        //                 pp = new SqlParameter();
        //                 pp.ParameterName = "@ID";
        //                 pp.DbType = DbType.Int32;
        //                 pp.Direction = ParameterDirection.Output;

        //                 cmd.Parameters.AddWithValue("@ID", 0);
        //                 int temp;
        //                 temp = cmd.ExecuteNonQuery();

        //                 res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
        //                 if (res != -1)
        //                 {
        //                     StrRetVal = "Comments Added Successfully";
        //                 }
        //                 else
        //                 {
        //                     return false;
        //                 }
        //                 return true;
        //             }

        //             catch (Exception ee)
        //             {
        //                 throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
        //             }
        //             finally
        //             {
        //                 if (cn.State == ConnectionState.Open)
        //                 {
        //                     cn.Close();
        //                 }
        //                 cmd.Dispose();
        //                 cn.Dispose();
        //             }				
        //         }			
        //     }
        #endregion

        public bool IfFolderPathExists(Documents doc)
        {
            DataSet DTS = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT COUNT(*) AS iCount FROM SCT_lkpDocumentType WHERE ProjectID = \'" + doc.ProjectId + "\' AND DocType = \'" + doc.DocType + "\' AND FolderPath = \'" + doc.FolderPath + "\' AND ProjectCategory = \'" + doc.ProjectCategory + "\'", cn);
                DTS = new DataSet();
                adpt.Fill(DTS);
                if (Convert.ToInt32(DTS.Tables[0].Rows[0]["iCount"].ToString()) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }

        }
        public bool IsdocCommentExists(Documents doc)
        {
            bool bVal;
            DataSet DTS = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strDescription = string.Empty;
            try
            {
                adpt = new SqlDataAdapter("Select Comments from SCT_tblDocumentComments where ProjectID = " + doc.ProjectId + " and DocTypeID = " + doc.DocTypeId + ")", cn);
                DTS = new DataSet();
                adpt.Fill(DTS);
                if (DTS.Tables[0].Rows.Count > 0)
                {
                    strDescription = DTS.Tables[0].Rows[0][0].ToString();
                    bVal = true;
                }
                else
                {
                    bVal = false;
                }
                return bVal;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }

        }
        public bool GetComments(Documents doc)
        {
            bool bVal;
            DataSet DTS = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strDescription = string.Empty;
            try
            {
                adpt = new SqlDataAdapter("select Comments from SCT_tblDocumentComments where ProjectID=" + doc.ProjectId + " and DoctypeID=" + doc.DocTypeId + ")", cn);
                DTS = new DataSet();
                adpt.Fill(DTS);
                if (DTS.Tables[0].Rows.Count > 0)
                {
                    strDescription = DTS.Tables[0].Rows[0][0].ToString();
                    bVal = true;
                }
                else
                {
                    bVal = false;
                }
                return bVal;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        private string SetStrRight(string pstr)
        {
            String strRetVal;
            strRetVal = pstr.Replace("'", "''");
            return strRetVal;
        }


        public DataSet GetDocsPerProject(Documents doc)
        {
            DataSet DTSet = new DataSet();
            string strSql = "";
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();

            strSql += "SELECT A.DocId, A.DocTypeId, A.DocDesc, A.ProjectId, A.DocName, A.DocFileName, A.UploadedBy, ";
            strSql += "convert(char,nullif(A.UPloadedOn,'1/1/1900'),106) as UploadedOn, B.DocType, B.FolderPath, C.UserName ";
            strSql += " from SCT_tblDocuments A left join SCT_lkpDocumentType B on A.DocTypeId = B.DocTypeId ";
            strSql += "left join SCT_tblusers C on A.UPloadedBy = C.userid AND C.IsClient = 0 WHERE ";
            strSql += "A.DocTypeId in ( select D.DocTypeId from SCT_lkpdocumentType D where folderpath = '";
            strSql += doc.FolderName;
            strSql += "') and A.Projectid = ";
            strSql += doc.ProjectId;
            strSql += " order by CONVERT(DATETIME, A.UPloadedOn) DESC";

            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;


        }

        public DataSet GetFolderList(int intProjectID)
        {
            DataSet DTSet = new DataSet();
            string strSql;
            string strProjectPath;

            Project objProject = new Project();
            objProject.ProjectID = intProjectID;
            objProject = new DrishProjectDAL.ProjectDAL().GetProject(intProjectID);

            strProjectPath = System.Configuration.ConfigurationManager.AppSettings["ProjectFolders"].ToString() + "" + objProject.ProjectCode + "";
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT DISTINCT LTRIM(RTRIM(CAST(B.DocTypeID AS CHAR)))+';'+LTRIM(RTRIM(CAST(B.ProjectID AS CHAR))) AS KeyField, B.FolderPath, '" + strProjectPath + "' + B.FolderPath as [FolderLink], RTRIM(LTRIM(B.FolderPath + ' - (' + CONVERT(CHAR,(SELECT COUNT(*) from SCT_tblDocuments where  ProjectID = '" + intProjectID + "' and DocTypeID in (select doctypeid from SCT_lkpDocumentType C where folderpath=(select folderpath from SCT_lkpDocumentType D where D.doctypeid=B.doctypeid))))))+ ')' AS FolderPathDocNo, replace(B.FolderPath,'',';') + ';' AS repFolderPath from SCT_lkpDocumentType B WHERE (B.ProjectID ='" + intProjectID + "' OR B.ProjectID IS NULL) AND B.ProjectCategory = (SELECT Ltrim(Rtrim(ProjectCategory)) AS ProjectCategory FROM SCT_tblProjects WHERE ProjectID = '" + intProjectID + "') ORDER BY B.FolderPath", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }


        }
        public DataSet GetFolderList(string txtDocName, int intProjectID)
        {
            DataSet DTSet = new DataSet();
            string strSql;
            string strProjectPath;
            Project objProject = new Project();
            objProject.ProjectID = intProjectID;
            objProject = new DrishProjectDAL.ProjectDAL().GetProject(intProjectID);
            strProjectPath = System.Configuration.ConfigurationManager.AppSettings["ProjectFolders"].ToString() + "\\" + objProject.ProjectCode + "\\";
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT DISTINCT LTRIM(RTRIM(CAST(B.DocTypeID AS CHAR)))+\';\'+LTRIM(RTRIM(CAST(B.ProjectID AS CHAR))) AS KeyField, B.FolderPath, \'" + strProjectPath + "\' + B.FolderPath as [FolderLink], RTRIM(LTRIM(B.FolderPath + \' - (\' + CONVERT(CHAR,(SELECT COUNT(*) from SCT_tblDocuments where ProjectID = \'" + intProjectID + "\' AND DocTypeID IN (select doctypeid from SCT_lkpDocumentType C where folderpath=(select folderpath from SCT_lkpDocumentType D where D.doctypeid=B.doctypeid))))))+ \')\' AS FolderPathDocNo, REPLACE(B.FolderPath,\'\\\',\';\') + \';\' AS repFolderPath from SCT_lkpDocumentType B LEFT JOIN SCT_tblDocuments A ON A.DocTypeID = B.DocTypeID WHERE A.ProjectID = \'" + intProjectID + "\' AND A.DocName LIKE \'%" + txtDocName + "%\' AND B.ProjectCategory = (SELECT Ltrim(Rtrim(ProjectCategory)) AS ProjectCategory FROM SCT_tblProjects WHERE ProjectID = \'" + intProjectID + "\') ORDER BY B.FolderPath", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }


        public bool DeleteDoc(Documents doc)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res = 0;
            string StrRetVal = "";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SCT_spDeleteDocument";
            cmd.Connection = cn;
            cn.Open();
            try
            {
                cmd.Parameters.AddWithValue("@pDocId", doc.DocId);
                pp = new SqlParameter();
                pp.ParameterName = "@ID";
                pp.DbType = DbType.Int32;
                pp.Direction = ParameterDirection.Output;

                cmd.Parameters.AddWithValue("@ID", 0);
                int temp;
                temp = cmd.ExecuteNonQuery();

                res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                if (res != -1)
                {
                    StrRetVal = "Deleted  Successfully";
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public string GetDocName(Documents doc)
        {
            string strSql;
            string strFileName1 = "";
            string strFileName = "";
            string strFolderPath = "";
            SqlConnection cn = new DataConn().GetConn();
            SqlDataReader rdr;
            SqlCommand cmd;
            strSql = "(select * from SCT_tbldocuments A left join SCT_lkpdocumenttype B on  A.doctypeid = B.doctypeid where docid = " + doc.DocId + ")";
            cmd = new SqlCommand(strSql, cn);
            cn.Open();
            rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                if (rdr.HasRows)
                {
                    rdr.Read();
                    strFileName1 = rdr["docfilename"].ToString();
                    strFolderPath = rdr["folderpath"].ToString();
                }
                if (strFileName1.ToString().Trim() != null)
                {
                    strFileName += System.Configuration.ConfigurationManager.AppSettings["ProjectFolders"].ToString();
                    strFileName += "/";
                    strFileName += doc.ProjectCode;
                    strFileName += "/";
                    strFileName += strFolderPath.ToString();
                    strFileName += "/";
                    strFileName += strFileName1;
                }
                return strFileName.ToString();
            }
            return "";
        }

        public int GetNextId()
        {
            string strSql;
            string strRetVal = "";
            int intNextId;

            strSql = "select isnull(MAX(DocId),0) from SCT_tbldocuments";
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strSql, cn);
            cn.Open();
            strRetVal = cmd.ExecuteScalar().ToString();
            intNextId = (Convert.ToInt32(strRetVal)) + 1;
            return intNextId;

        }

        public DataSet GetDocsListPerProjectLevel(Documents doc)
        {
            DataSet DTSet = new DataSet();
            string strSql = "";
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();

            strSql += " SELECT A.DocTypeId, ";
            strSql += " A.LevelId, B.DocType,  ";
            strSql += " isnull((select TOP 1 RTRIM(D.ProjectCode) +\'\\\'+ B.FolderPath +\'\\\'+ isnull(C.DocFileName,\'\') from SCT_tblDocuments C where projectid = \'" + doc.ProjectId + "\' AND A.DocTypeId = C.DocTypeId ORDER BY UploadedOn DESC, DOCID DESC),\'\') as FilePath,";
            strSql += " isnull((select TOP 1 isnull(C.DocName,\'\') from SCT_tblDocuments C where projectid = \'" + doc.ProjectId + "\' AND A.DocTypeId = C.DocTypeId ORDER BY UploadedOn DESC, DOCID DESC),\'\') as DocName, ";
            strSql += " (select TOP 1 CONVERT(CHAR,nullif(C.UPloadedOn,\'1/1/1900\'),106) from SCT_tblDocuments C LEFT JOIN SCT_tblUsers E ON E.IsClient = 0 AND C.UploadedBy = E.UserID where projectid = \'" + doc.ProjectId + "\' AND A.DocTypeId = C.DocTypeId ORDER BY UploadedOn DESC, DOCID DESC) as UploadedOn ,";
            strSql += " (select TOP 1 e.username from SCT_tblDocuments C LEFT JOIN SCT_tblUsers E ON E.IsClient = 0 AND C.UploadedBy = E.UserID where projectid = \'" + doc.ProjectId + "\' AND A.DocTypeId = C.DocTypeId ORDER BY UploadedOn DESC, DOCID DESC) as UserName";
            strSql += " from SCT_lkpCheckList A ";
            strSql += " left join SCT_lkpDocumentType B on A.DocTypeId = B.DocTypeId ";
            strSql += " left join sct_tblProjects D ON D.ProjectID = ";
            strSql += doc.ProjectId;
            strSql += " WHERE A.levelid = ";
            strSql += doc.ProjectLevelId;
            strSql += " order by ";
            strSql += doc.SortCriteria;
            strSql += " ";
            strSql += doc.SortDir;

            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;


        }

        public DataSet GetDocsList(Documents doc)
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "";
            strSql += "SELECT A.DocTypeId,A.LevelId, B.DocType,isnull((select TOP 1 RTRIM(D.ProjectCode) +''+ B.FolderPath +''+isnull(C.DocFileName,'') from SCT_tblDocuments C where projectid =" + doc.ProjectId;
            strSql += "AND A.DocTypeId = C.DocTypeId ORDER BY UploadedOn DESC, DOCID DESC),'') as FilePath,isnull((select TOP 1 isnull(C.DocName,'') from SCT_tblDocuments C ";
            strSql += "where projectid = '" + doc.ProjectId + "' AND A.DocTypeId = C.DocTypeId ORDER BY UploadedOn DESC, DOCID DESC),'') as DocName, ";
            strSql += "(select TOP 1 CONVERT(CHAR,nullif(C.UPloadedOn,'1/1/1900'),106) from SCT_tblDocuments C LEFT JOIN SCT_tblUsers E ON E.IsClient = 0 AND C.UploadedBy = E.UserID where projectid = " + doc.ProjectId + " AND A.DocTypeId = C.DocTypeId ";
            strSql += "ORDER BY UploadedOn DESC, DOCID DESC) as UploadedOn ,(select TOP 1 e.username from SCT_tblDocuments C LEFT JOIN SCT_tblUsers E ON E.IsClient = 0 AND C.UploadedBy = E.UserID where projectid = " + doc.ProjectId + " AND ";
            strSql += "A.DocTypeId = C.DocTypeId ORDER BY UploadedOn DESC, DOCID DESC) as UserName from SCT_lkpCheckList A left join SCT_lkpDocumentType B on A.DocTypeId = B.DocTypeId left join sct_tblProjects D ON D.ProjectID = " + doc.ProjectId + "'WHERE A.levelid =" + doc.ProjectLevelId;
            adpt = new SqlDataAdapter(strSql, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;


        }

        public int getDocTypeID(string FolderName)
        {
            int DTID;
            string strSql = "";
            strSql += "SELECT doctypeid FROM SCT_lkpDocumentType where Folderpath=\'";
            strSql += FolderName;
            strSql += "\'";
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strSql, cn);
            cn.Open();
            DTID = Convert.ToInt32(cmd.ExecuteScalar());
            return DTID;
        }

        public bool IsDocExists_Sync(Documents doc)
        {
            string strsql = "";
            string strRetVal = "";
            bool bVal;
            if (doc.DocId == 0)
            {
                strsql += "select count(*) from SCT_tblDocuments where DocFileName = \'";
                strsql += doc.DocFileName;
                strsql += "\' and doctypeid = ";
                strsql += doc.DocTypeId;
                strsql += " AND ProjectID = ";
                strsql += doc.ProjectId;
            }
            else
            {
                strsql += "select count(*) from SCT_tblDocuments where DocFileName = \'";
                strsql += doc.DocFileName;
                strsql += "\' and docid <> ";
                strsql += doc.DocId;
                strsql += " and doctypeid = ";
                strsql += doc.DocTypeId;
                strsql += " AND ProjectID = ";
                strsql += doc.ProjectId;
            }

            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strsql, cn);
            cn.Open();
            strRetVal = cmd.ExecuteScalar().ToString();
            if (strRetVal != "0")
            {
                bVal = true;
            }
            else
            {
                bVal = false;
            }
            return bVal;
        }

        public bool DeleteFolder(Documents doc)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res = 0;
            string StrRetVal = "";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SCT_spDeleteFolder";
            cmd.Connection = cn;
            cn.Open();
            try
            {
                cmd.Parameters.AddWithValue("@pDocTypeID", doc.DocTypeId);
                cmd.Parameters.AddWithValue("@pProjectID", doc.ProjectId);
                pp = new SqlParameter();
                pp.ParameterName = "@ID";
                pp.DbType = DbType.Int32;
                pp.Direction = ParameterDirection.Output;

                cmd.Parameters.AddWithValue("@ID", 0);
                int temp;
                temp = cmd.ExecuteNonQuery();

                res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                if (res != -1)
                {
                    StrRetVal = "Deleted  Successfully";
                }
                else
                {
                    return false;
                }
                return true;
            }

            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }


            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }


        }

         public DataSet GetDocumentTypes()
        {
            string strSql = "select * from SCT_lkpDocumentType";
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds = new DataSet();
            adpt = new SqlDataAdapter(strSql, cn);
            adpt.Fill(ds);
            return ds;
        }
    }
}
