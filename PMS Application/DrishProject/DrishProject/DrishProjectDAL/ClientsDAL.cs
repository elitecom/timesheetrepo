﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectDAL
{
    public class ClientsDAL
    {
        public bool DeleteContact(Clients client)
        {
            string strSql = "";
            string strRetVal = "";
            bool bVal;
            strSql = "delete from SCT_tblProjectClients where contactid = " + client.ClientID;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strSql, cn);
            cn.Open();
            strRetVal = cmd.ExecuteScalar().ToString();
            if (strRetVal != "0")
            {
                bVal = true;
            }
            else
            {
                bVal = false;
            }
            return bVal;
        }

        public bool UpdateContact(Clients client)
        {
            string strSql = "";
            string strRetVal = "";
            bool bVal;
            strSql += "UPDATE SCT_tblProjectClients SET contactid = " + client.ContactId;
            strSql += " WHERE projectID = ";
            strSql += client.ProjectID;
            strSql += " and mapid = ";
            strSql += client.MapId;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strSql, cn);
            cn.Open();
            strRetVal = cmd.ExecuteNonQuery().ToString();
            if (strRetVal != "0")
            {
                bVal = true;
            }
            else
            {
                bVal = false;
            }
            return bVal;
        }

        public bool IfClientExists(Clients client)
        {
            bool RetVal;
            string strSql;

            RetVal = false;
            strSql = "select * from SCT_tblProjectClients where projectid = " + client.ProjectID + " and contactid = " + client.ContactId + "";
            SqlDataReader rdr;
            SqlCommand cmd;
            Project PP = new Project();
            SqlConnection con = new DataConn().GetConn();

            cmd = new SqlCommand(strSql, con);
            con.Open();
            rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                RetVal = true;
            }
            return RetVal;
        }

        public bool ExistsContact(Clients client)
        {
            try
            {
                SqlConnection cn = new DataConn().GetConn();
                SqlDataAdapter adpt = new SqlDataAdapter("select * from SCT_tblclients where ClientID=" + client.ClientID, cn);
                DataSet ds = new DataSet();
                adpt.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool InsertContact(Clients client)
        {
            string strSql;
            string strRetVal;
            bool bVal;

            if (!(new ClientsDAL().ExistsContact(client)))
            {
                strSql = "INSERT INTO SCT_tblProjectClients (Projectid, contactid) VALUES (" + client.ProjectID + ", " + client.ContactId + " )";
                SqlConnection cn = new DataConn().GetConn();
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cn.Open();
                strRetVal = cmd.ExecuteNonQuery().ToString();
                if (strRetVal != "0")
                {
                    bVal = true;
                }
                else
                {
                    bVal = false;
                }
                return bVal;
            }
            else
            {
                strRetVal = "Contact already exists!";
            }
            return true;
        }

        public DataSet GetContactsPerProject(int intProjectId)
        {
            DataSet DTSet = new DataSet();
            string strSql = "";
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            strSql += "select distinct clientid, clientname, companyname, address1+', '+address2 AS address1, city as place " + "from SCT_tblclients A where exists ( select * from SCT_tblcontacts B join SCT_tblprojectclients C on B.contactid = C.contactid where B.clientid = A.clientid and projectid = " + intProjectId + " )";
            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }

        public DataSet GetAllClients()
        {
            DataSet DTSet = new DataSet();
            string strSql = "";
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            strSql += "SELECT A.ClientId, A.ClientName from SCT_tblclients A order by ClientName ";
            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }

        public DataSet GetContactsPerClients(Clients client)
        {
            DataSet DTSet = new DataSet();
            string strSql = "";
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            strSql += "SELECT A.Mapid, A.ProjectId, A.contactId, B.FirstName + ' ' + B.MiddleName + ' ' + B.LastName as name, " + "B.email, B.phoneno, B.mobileno, B.faxno from SCT_tblProjectClients A left join SCT_tblcontacts B on " + "A.contactid = B.contactid where projectid = " + client.ProjectID + " and clientid = " + client.ClientID;
            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }

        public DataSet GetProjectsPerClient(Clients client)
        {
            DataSet DTSet = new DataSet();
            string strSql = "";
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            strSql += "SELECT A.ProjectId, A.ContactId, B.FirstName + ' ' + B.MiddleName + ' ' + B.LastName as Name, " + " C.ProjectName, C.Proj_StatusId, C.Proj_StartDate, convert(char, nullif(C.Proj_StartDate, '1/1/1900'), 106) as startdate, " + " C.Proj_EndDate, convert(char, nullif(C.Proj_EndDate, '1/1/1900'), 106) as enddate, D.ProjectStatus from " + " SCT_tblProjectClients A left join SCT_tblContacts B on A.contactid = B.Contactid left join " + " SCT_tblProjects C on A.ProjectId = C.ProjectId left join SCT_lkpProjectStatus D on " + " C.Proj_StatusId = D.Proj_statusid where B.clientid = " + client.ClientID + " order by 1 ";
            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }

        public bool UpdateClientInfo(Clients client)
        {
            bool res;
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "update SCT_tblClients set CompanyName=@CompanyName,Address1=@Address1,Address2=@Address2,City=@City,State=@State,Country=@Country,ZipCode=@ZipCode,Notes=@Notes where ClientId=@ClientId";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@CompanyName", client.CompanyName);
                cmd.Parameters.AddWithValue("@Address1",client.Address1);
                cmd.Parameters.AddWithValue("@Address2",client.Address2);
                cmd.Parameters.AddWithValue("@City",client.City);
                cmd.Parameters.AddWithValue("@State",client.State);
                cmd.Parameters.AddWithValue("@Country",client.Country);
                cmd.Parameters.AddWithValue("@ZipCode", client.Zip);
                cmd.Parameters.AddWithValue("@Notes", client.Notes);
                cmd.Parameters.AddWithValue("@ClientId", client.ClientID);
                cn.Open();
                cmd.ExecuteNonQuery();
                res = true;
                return res;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public int AddNewClient(Clients client)
        {
            bool res;
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spAddNewClient";
                cmd.Connection = cn;                
                cmd.Parameters.AddWithValue("@ClientName", client.ClientName);
                cmd.Parameters.AddWithValue("@CompanyName", client.CompanyName);
                cmd.Parameters.AddWithValue("@Address1",client.Address1);
                cmd.Parameters.AddWithValue("@Address2",client.Address2);
                cmd.Parameters.AddWithValue("@City",client.City);
                cmd.Parameters.AddWithValue("@State",client.State);
                cmd.Parameters.AddWithValue("@Zip", client.Zip);
                cmd.Parameters.AddWithValue("@Country",client.Country);                
                cmd.Parameters.AddWithValue("@Notes", client.Notes);

                SqlParameter pp = new SqlParameter();
                pp.ParameterName = "@ID";
                pp.DbType = DbType.Int32;
                pp.Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@ID", 0);
                cn.Open();
                cmd.ExecuteNonQuery();
                int ans;
                ans = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                if (ans > 0)
                {
                    res = true;
                }
                else
                {
                    ans = 0;
                    res = false;
                }
                return ans;
            }
            catch (Exception)
            {
                return 0;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public Clients GetClientDetails(int clientID)
        {
            Clients client = new Clients();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            DataSet ds;
            adpt = new SqlDataAdapter("select * from SCT_tblClients where ClientID=" + clientID, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                client.ClientID = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                client.ClientName = Convert.ToString(ds.Tables[0].Rows[0][1].ToString());
                client.CompanyName = Convert.ToString(ds.Tables[0].Rows[0][2].ToString());
                client.Address1 = Convert.ToString(ds.Tables[0].Rows[0][3].ToString());
                client.Address2 = Convert.ToString(ds.Tables[0].Rows[0][4].ToString());
                client.City = Convert.ToString(ds.Tables[0].Rows[0][5].ToString());
                client.State = Convert.ToString(ds.Tables[0].Rows[0][6].ToString());
                client.Country = Convert.ToString(ds.Tables[0].Rows[0][7].ToString());
                client.Zip = Convert.ToString(ds.Tables[0].Rows[0][8].ToString());
                client.Notes = Convert.ToString(ds.Tables[0].Rows[0][9].ToString());

                return client;
            }
            else
            {
                return null;
            }
        }

        public DataSet GetCountryList()
        {
            SqlDataAdapter adpt;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            adpt = new SqlDataAdapter("select * from SCT_lkpCountries", cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }
    }
}
