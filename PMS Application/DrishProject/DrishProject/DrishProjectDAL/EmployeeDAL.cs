﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;
using System.IO;
using System.Configuration;
using System.Security.Cryptography;

namespace DrishProjectDAL
{
    public class EmployeeDAL
    {
        Employee emp = new Employee();
        public DataSet GetDepartments()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            try
            {
                adpt = new SqlDataAdapter("select * from Sct_lkpDepartment", cn);
                ds = new DataSet();
                adpt.Fill(ds, "Sct_lkpDepartment");
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cn.Dispose();
            }
        }

        public DataSet GetDesignations()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            try
            {
                adpt = new SqlDataAdapter("select * from Sct_lkpDesignation", cn);
                ds = new DataSet();
                adpt.Fill(ds, "Sct_lkpDesignation");
                return ds;

            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();

                }
                cn.Dispose();
            }
        }

        public Employee GetProfileDetails(Employee emp)
        {
            Employee employee = new Employee();
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string qry = string.Empty;
            DataSet ds = new DataSet();
            try
            {
                qry += "SELECT     SCT_tblEmployees.FirstName, SCT_tblEmployees.LastName, SCT_tblEmployees.EmpCode, SCT_tblEmployees.EmailId, SCT_lkpDepartment.DeptName, ";
                qry += "SCT_lkpDesignation.Designation, SCT_tblEmployees.DOB, SCT_tblEmployees.DOJ, SCT_tblEmployees.CorrAddress, SCT_tblEmployees.PerAddress, ";
                qry += "SCT_tblEmployees.Phone, SCT_tblEmployees.Mobile, SCT_tblEmployees.EduQualification, SCT_tblEmployees.SkillSet, SCT_tblEmployees.Hobbies, SCT_tblEmployees.EmpID, Sct_tblUSers.UserName, SCT_tblEmployees.IsReportingManager FROM  SCT_tblEmployees INNER JOIN ";
                qry += "SCT_lkpDesignation ON SCT_tblEmployees.DesgID = SCT_lkpDesignation.DesgID INNER JOIN ";
                qry += "SCT_tblUsers ON SCT_tblEmployees.EmpID = SCT_tblUsers.userid INNER JOIN ";
                qry += "SCT_lkpDepartment ON SCT_tblEmployees.DeptID = SCT_lkpDepartment.DeptID where Sct_tblEmployees.EmpCode =(select EmpCode from Sct_tblEmployees inner join Sct_tblUsers on SCT_tblEmployees.EmpID = SCT_tblUsers.userid where UserName='" + emp.UserName + "')";

                adpt = new SqlDataAdapter(qry, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    employee.FirstName = dr[0].ToString();
                    employee.LastName = dr[1].ToString();
                    employee.EmployeeCode = dr[2].ToString();
                    employee.EmailId = dr[3].ToString();
                    employee.DepartmentName = dr[4].ToString();
                    employee.Designation = dr[5].ToString();
                    employee.DateOfBirth = Convert.ToDateTime(dr[6].ToString());
                    employee.DateOfJoining = Convert.ToDateTime(dr[7].ToString());
                    employee.CorrespondanceAddress = dr[8].ToString();
                    employee.PermanentAddress = dr[9].ToString();
                    employee.PhoneNumber = dr[10].ToString();
                    employee.MobileNumber = dr[11].ToString();
                    employee.EducationalQualification = dr[12].ToString();
                    employee.SkillSet = dr[13].ToString();
                    employee.Hobbies = dr[14].ToString();
                    employee.EmployeeId = Convert.ToInt32(dr[15].ToString());
                    employee.UserName = Convert.ToString(dr[16].ToString());
                    employee.IsReportingManager = (dr[17].ToString().ToLower() == "true" ? 1 : 0);
                }
                return employee;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Employee GetProfileDetailsByUserName(Employee emp)
        {
            Employee employee = new Employee();
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string qry = string.Empty;
            DataSet ds = new DataSet();
            try
            {
                qry += "SELECT     SCT_tblEmployees.FirstName, SCT_tblEmployees.LastName, SCT_tblEmployees.EmpCode, SCT_tblEmployees.EmailId, SCT_lkpDepartment.DeptName, ";
                qry += "SCT_lkpDesignation.Designation, SCT_tblEmployees.DOB, SCT_tblEmployees.DOJ, SCT_tblEmployees.CorrAddress, SCT_tblEmployees.PerAddress, ";
                qry += "SCT_tblEmployees.Phone, SCT_tblEmployees.Mobile, SCT_tblEmployees.EduQualification, SCT_tblEmployees.SkillSet, SCT_tblEmployees.Hobbies, SCT_tblEmployees.EmpID, Sct_tblUSers.UserName, SCT_tblEmployees.IsReportingManager FROM  SCT_tblEmployees INNER JOIN ";
                qry += "SCT_lkpDesignation ON SCT_tblEmployees.DesgID = SCT_lkpDesignation.DesgID INNER JOIN ";
                qry += "SCT_tblUsers ON SCT_tblEmployees.EmpID = SCT_tblUsers.userid INNER JOIN ";
                qry += "SCT_lkpDepartment ON SCT_tblEmployees.DeptID = SCT_lkpDepartment.DeptID where Sct_tblUsers.UserName ='" + emp.UserName + "'";

                adpt = new SqlDataAdapter(qry, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    employee.FirstName = dr[0].ToString();
                    employee.LastName = dr[1].ToString();
                    employee.EmployeeCode = dr[2].ToString();
                    employee.EmailId = dr[3].ToString();
                    employee.DepartmentName = dr[4].ToString();
                    employee.Designation = dr[5].ToString();
                    employee.DateOfBirth = Convert.ToDateTime(dr[6].ToString());
                    employee.DateOfJoining = Convert.ToDateTime(dr[7].ToString());
                    employee.CorrespondanceAddress = dr[8].ToString();
                    employee.PermanentAddress = dr[9].ToString();
                    employee.PhoneNumber = dr[10].ToString();
                    employee.MobileNumber = dr[11].ToString();
                    employee.EducationalQualification = dr[12].ToString();
                    employee.SkillSet = dr[13].ToString();
                    employee.Hobbies = dr[14].ToString();
                    employee.EmployeeId = Convert.ToInt32(dr[15].ToString());
                    employee.UserName = Convert.ToString(dr[16].ToString());
                    employee.IsReportingManager = (dr[17].ToString().ToLower() == "true" ? 1 : 0);
                }
                return employee;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet getdetail()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            try
            {
                adpt = new SqlDataAdapter("Select * from SCT_tblEmployees", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cn.Dispose();
            }
        }

        public DataSet SearchEmployee(int desgId, string name, string sdob, string sdoj)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string strSql = "";
            try
            {
                strSql += "SELECT  Distinct SCT_tblEmployees.EmpCode, SCT_tblEmployees.FirstName, SCT_tblEmployees.LastName, SCT_tblEmployees.DOB, ";
                strSql += "SCT_tblEmployees.Phone, SCT_tblEmployees.Mobile, SCT_tblEmployees.EmailId, SCT_lkpDepartment.DeptName, ";
                strSql += "SCT_lkpDesignation.Designation, SCT_tblEmployees.DOJ FROM SCT_lkpDesignation INNER JOIN ";
                strSql += "SCT_tblEmployees ON SCT_lkpDesignation.DesgID = SCT_tblEmployees.DesgID INNER JOIN SCT_lkpDepartment ON SCT_tblEmployees.DeptID = SCT_lkpDepartment.DeptID WHERE ";

                if (desgId != -1)
                {
                    strSql += " SCT_tblEmployees.DesgID=" + desgId;
                }
                else
                {
                    strSql += "SCT_tblEmployees.DesgID = -1";
                }
                if (!String.IsNullOrEmpty(name))
                {
                    strSql += " OR ((SCT_tblEmployees.FirstName LIKE '%";
                    strSql += name;
                    strSql += "%') OR (SCT_tblEmployees.LastName LIKE '%";
                    strSql += name;
                    strSql += "%') OR (SCT_tblEmployees.Empcode LIKE '%";
                    strSql += name;
                    strSql += "%'))";
                }
                if (!String.IsNullOrEmpty(sdob))
                {
                    //cast('12/12/1980' as smalldatetime)
                    strSql += " OR dob like cast('" + sdob + "' as smalldatetime)";
                }
                if (!String.IsNullOrEmpty(sdoj))
                {
                    strSql += " OR doj like cast('" + sdoj + "' as smalldatetime)";
                }

                adpt = new SqlDataAdapter(strSql, cn);
                DataSet ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        #region "Old Code"
        //public DataSet SearchEmployee(int desgId, string name, string dob, string doj)
        //{
        //    SqlConnection cn = new DataConn().GetConn();
        //    SqlDataAdapter adpt;
        //    string strSql = "";
        //    string strSearch = "";
        //    string strtxt = "";
        //    try
        //    {
        //        strSql += "SELECT  Distinct SCT_tblEmployees.EmpCode, SCT_tblEmployees.FirstName, SCT_tblEmployees.LastName, SCT_tblEmployees.DOB, ";
        //        strSql += "SCT_tblEmployees.Phone, SCT_tblEmployees.Mobile, SCT_tblEmployees.EmailId, SCT_lkpDepartment.DeptName, ";
        //        strSql += "SCT_lkpDesignation.Designation, SCT_tblEmployees.DOJ FROM SCT_lkpDesignation INNER JOIN ";
        //        strSql += "SCT_tblEmployees ON SCT_lkpDesignation.DesgID = SCT_tblEmployees.DesgID INNER JOIN SCT_lkpDepartment ON SCT_tblEmployees.DeptID = SCT_lkpDepartment.DeptID ";


        //        //strSql += "SELECT A.EmpId, A.EmpCode, C.UserName,A.FirstName, A.LastName,A.FirstName + '' + A.LastName as Name,A.DOB as strDOB, A.DOJ as strDOJ, B.Designation, CONVERT(char,nullif(A.DOB,'1/1/1900'),106) AS DOB, CONVERT(char,nullif(A.DOJ,'1/1/1900'),106) AS DOJ, A.CorrAddress, A.Mobile, A.Phone, A.EmailID";
        //        //strSql += " FROM SCT_tblemployees A LEFT JOIN SCT_lkpdesignation B ON A.desgid = B.desgid ";
        //        //strSql += " LEFT JOIN SCT_tblusers C ON A.empid = C.userid AND C.IsClient = 0 ";
        //        if (name != "")
        //        {
        //            if (name.ToString() == "")
        //            {
        //                strSearch += " WHERE ((SCT_tblEmployees.FirstName LIKE '%";
        //                strSearch += name;
        //                strSearch += "%') OR (SCT_tblEmployees.LastName LIKE '%";
        //                strSearch += name;
        //                strSearch += "%') OR (SCT_tblEmployees.Empcode LIKE '%";
        //                strSearch += name;
        //                strSearch += "%'))";
        //            }
        //            else
        //            {
        //                strSearch += " AND ((SCT_tblEmployees.FirstName LIKE '%";
        //                strSearch += name;
        //                strSearch += "%') OR (SCT_tblEmployees.LastName LIKE '%";
        //                strSearch += name;
        //                strSearch += "%') OR (SCT_tblEmployees.Empcode LIKE '%";
        //                strSearch += name;
        //                strSearch += "%'))";
        //            }
        //        }
        //        if (dob != "")
        //        {
        //            if (strSearch.ToString() == "")
        //            {
        //                strSearch += " WHERE (SCT_tblEmployees.DOB = '";
        //                strSearch += dob;
        //                strSearch += "')";
        //            }
        //            else
        //            {
        //                strSearch += " AND (SCT_tblEmployees.DOB = '";
        //                strSearch += dob;
        //                strSearch += "')";
        //            }
        //        }
        //        if (doj != "")
        //        {
        //            if (strSearch.ToString() == "")
        //            {
        //                strSearch += " WHERE (SCT_tblEmployees.DOJ = '";
        //                strSearch += doj;
        //                strSearch += "')";
        //            }
        //            else
        //            {
        //                strSearch += " AND (SCT_tblEmployees.DOJ = '";
        //                strSearch += doj;
        //                strSearch += "')";
        //            }
        //        }
        //        if (desgId == -1)
        //        {
        //            strSearch += "WHERE Sct_tblEMployees.DesgID =" + desgId;
        //        }
        //        else
        //        {
        //            strSearch += "AND  SCT_tblEmployees.DesgID=" + desgId;
        //        }
        //        strSql += strSearch.ToString();
        //        strSql += " ORDER BY EmpCode";

        //        adpt = new SqlDataAdapter(strSql, cn);
        //        DataSet ds = new DataSet();
        //        adpt.Fill(ds);
        //        return ds;
        //    }
        //    catch (Exception ee)
        //    {
        //        return null;
        //    }
        //}
        #endregion

        public DataSet GetReportingManagers()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            try
            {
                adpt = new SqlDataAdapter("select EmpId,FirstName + ' ' + LastName as Name from Sct_tblEmployees  where IsReportingManager=1 and status='A'", cn);
                ds = new DataSet();
                adpt.Fill(ds, "Sct_tblEmployees");
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cn.Dispose();
            }
        }

        public bool AddEmployee(Employee emp)
        {
            bool res;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.CommandText = "SCT_spInsertEmployee";
                cmd.Parameters.AddWithValue("@pUserName", emp.UserName);
                cmd.Parameters.AddWithValue("@pPwd", emp.Password);
                //cmd.Parameters.AddWithValue("@pPwd", Encrypt(emp.Password));
                cmd.Parameters.AddWithValue("@pFirstName", emp.FirstName);
                cmd.Parameters.AddWithValue("@pLastName", emp.LastName);
                cmd.Parameters.AddWithValue("@pEmpCode", emp.EmployeeCode);
                cmd.Parameters.AddWithValue("@pDOB", emp.DateOfBirth);
                cmd.Parameters.AddWithValue("@pDOJ", emp.DateOfJoining);
                cmd.Parameters.AddWithValue("@pCorrAddr", emp.CorrespondanceAddress);
                cmd.Parameters.AddWithValue("@pPerAddr", emp.PermanentAddress);
                cmd.Parameters.AddWithValue("@pPhone", emp.PhoneNumber);
                cmd.Parameters.AddWithValue("@pMobile", emp.MobileNumber);
                cmd.Parameters.AddWithValue("@pEmailID", emp.EmailId);
                cmd.Parameters.AddWithValue("@pDeptId", emp.DepartmentId);
                cmd.Parameters.AddWithValue("@pDesigId", emp.DesgID);
                cmd.Parameters.AddWithValue("@pEduQual", emp.EducationalQualification);
                cmd.Parameters.AddWithValue("@pSkillSet", emp.SkillSet);
                cmd.Parameters.AddWithValue("@pHobbies", emp.Hobbies);
                cmd.Parameters.AddWithValue("@pRemarks", emp.Remarks);
                cmd.Parameters.AddWithValue("@pStatus", emp.Status);
                cmd.Parameters.AddWithValue("@pIsAdmin", emp.IsAministrator);
                cmd.Parameters.AddWithValue("@pSeat", emp.SeatLocation);
                cmd.Parameters.AddWithValue("@pICard", emp.IdentityCard);
                cmd.Parameters.AddWithValue("@pReporting", emp.IsReportingManager);
                cmd.Parameters.AddWithValue("@pProject", emp.ProjectId);
                cmd.Parameters.AddWithValue("@pRoleID", emp.RoleId);
                cmd.Parameters.AddWithValue("@pShortName", emp.ShortName);
                cmd.Parameters.AddWithValue("@pIsReportingManager", emp.IsReportingManager);

                SqlParameter pp = new SqlParameter();
                pp.ParameterName = "@ID";
                pp.DbType = DbType.Int32;
                pp.Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@ID", 0);

                cn.Open();
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception ee)
            {
                res = false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
            return res;

        }

        public bool updatedata(Employee emp)
        {
            bool res;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sct_spupdateemployee";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pUserName", emp.UserName);
                cmd.Parameters.AddWithValue("@pPwd", emp.Password);
                //cmd.Parameters.AddWithValue("@pPwd", Encrypt(emp.Password));
                cmd.Parameters.AddWithValue("@pFirstName", emp.FirstName);
                cmd.Parameters.AddWithValue("@pLastName", emp.LastName);
                cmd.Parameters.AddWithValue("@pEmpCode", emp.EmployeeCode);
                cmd.Parameters.AddWithValue("@pDOB", emp.DateOfBirth);
                cmd.Parameters.AddWithValue("@pDOJ", emp.DateOfJoining);
                cmd.Parameters.AddWithValue("@pCorrAddr", emp.CorrespondanceAddress);
                cmd.Parameters.AddWithValue("@pPerAddr", emp.PermanentAddress);
                cmd.Parameters.AddWithValue("@pPhone", emp.PhoneNumber);
                cmd.Parameters.AddWithValue("@pMobile", emp.MobileNumber);
                cmd.Parameters.AddWithValue("@pEmailID", emp.EmailId);
                cmd.Parameters.AddWithValue("@pDeptId", emp.DepartmentId);
                cmd.Parameters.AddWithValue("@pDesigId", emp.DesgID);
                cmd.Parameters.AddWithValue("@pEduQual", emp.EducationalQualification);
                cmd.Parameters.AddWithValue("@pSkillSet", emp.SkillSet);
                cmd.Parameters.AddWithValue("@pHobbies", emp.Hobbies);
                cmd.Parameters.AddWithValue("@pRemarks", emp.Remarks);
                cmd.Parameters.AddWithValue("@pStatus", emp.Status);
                cmd.Parameters.AddWithValue("@pIsAdmin", emp.IsAministrator);
                cmd.Parameters.AddWithValue("@pSeat", emp.SeatLocation);
                cmd.Parameters.AddWithValue("@pICard", emp.IdentityCard);
                cmd.Parameters.AddWithValue("@pReporting", emp.IsReportingManager);
                cmd.Parameters.AddWithValue("@pProject", emp.ProjectId);
                cmd.Parameters.AddWithValue("@pRoleID", emp.RoleId);
                cmd.Parameters.AddWithValue("@pShortName", emp.ShortName);
                cmd.Parameters.AddWithValue("@pIsReportingManager", emp.IsReportingManager);

                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ee)
            {
                res = false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
            return res;
        }

        public DataSet GetProjects()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            try
            {
                adpt = new SqlDataAdapter("select * from Sct_tblProjects", cn);
                ds = new DataSet();
                adpt.Fill(ds, "Sct_tblProjects");
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cn.Dispose();
            }
        }

        public bool IsAdministrator(Employee emp)
        {
            int roleId = -1;
            //bool res = false;
            SqlDataAdapter adpt;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            //adpt = new SqlDataAdapter("select IsReportingManager from SCT_tblEmployees inner join SCT_tblUsers on SCT_tblEmployees.EmpID=SCT_tblUsers.userid and SCT_tblUsers.username='" + emp.UserName + "'", cn);
            adpt = new SqlDataAdapter("select RoleId from SCT_tblEmployees inner join SCT_tblUsers on SCT_tblEmployees.EmpID=SCT_tblUsers.userid and SCT_tblUsers.username='" + emp.UserName + "'", cn);
            ds = new DataSet();
            adpt.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                roleId = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                if (roleId == 1 || roleId == 2)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            //if (ds.Tables[0].Rows[0][0].ToString() == "1")
            //{
            //    res = true;
            //}
            //else
            //{
            //    res = false;
            //}
            //return res;
        }

        public bool AuthenticateUser(Employee emp)
        {
            SqlConnection cn = new DataConn().GetConn();
            string qry = "";
            SqlDataAdapter adpt;
            DataSet ds;
            try
            {
                qry = "select * from Sct_tblUsers where username='" + emp.UserName + "' and UserPwd='" + emp.Password + "'";
                adpt = new SqlDataAdapter(qry, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ee)
            {
                return false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cn.Dispose();
            }
        }

        public DataSet getdata()
        {
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "SELECT SCT_tblEmployees.FirstName, SCT_tblEmployees.LastName, SCT_tblEmployees.DOB, SCT_tblEmployees.EmpCode,";
            strSql += " SCT_tblEmployees.Phone, SCT_tblEmployees.Mobile, SCT_tblEmployees.EmailId, SCT_lkpDepartment.DeptName, ";
            strSql += " SCT_lkpDesignation.Designation, SCT_tblEmployees.DOJ FROM SCT_lkpDesignation INNER JOIN ";
            strSql += " SCT_tblEmployees ON SCT_lkpDesignation.DesgID = SCT_tblEmployees.DesgID INNER JOIN ";
            strSql += " SCT_tblEmployeeDetails ON SCT_tblEmployees.EmpID = SCT_tblEmployeeDetails.EmpID INNER JOIN ";
            strSql += " SCT_tblUsers ON SCT_tblEmployees.EmpID = SCT_tblUsers.userid INNER JOIN ";
            strSql += " SCT_lkpDepartment ON SCT_tblEmployees.DeptID = SCT_lkpDepartment.DeptID";
            //SqlDataAdapter adpt = new SqlDataAdapter("select * from SCT_tblEmployees inner join SCT_lkpDesignation on SCT_lkpDesignation.DesgID=SCT_tblEmployees.DesgID inner join Sct_lkpDepartment on SCT_lkpDepartment.DeptId=SCT_tblEmployees.DeptId ", cn);
            SqlDataAdapter adpt = new SqlDataAdapter(strSql, cn);
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            return (ds);
        }

        public DataSet ProjectCategory()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            try
            {
                adpt = new SqlDataAdapter("SELECT [ProjectCategory] FROM [SCT_lkpProjectCategory]", cn);
                ds = new DataSet();
                adpt.Fill(ds, "Sct_lkpProjectCategory");
                return ds;

            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();

                }
                cn.Dispose();
            }
        }

        public DataSet searchdata(Employee emp)
        {
            string qry = string.Empty;

            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            qry += " select * from SCT_tblEmployees inner join SCT_lkpDesignation on SCT_lkpDesignation.DesgID=SCT_tblEmployees.DesgID inner join Sct_lkpDepartment on SCT_lkpDepartment.DeptId=SCT_tblEmployees.DeptId where";
            if (emp.DesgID > 0)
            {

                qry += "DesgID=" + emp.DesgID + " ";
            }
            //if (emp.FirstName !=null)
            //{
            //   qry += "FirstName=" + emp.FirstName + " ";
            //}

            DataSet ds = new DataSet();
            adpt = new SqlDataAdapter(qry, cn);
            adpt.Fill(ds, qry);

            return (ds);
        }
        public Employee SearchEmployeeByCode(string empCode)
        {
            Employee employee = new Employee();
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string qry = string.Empty;
            DataSet ds = new DataSet();
            try
            {
                qry += "SELECT SCT_tblEmployees.FirstName, SCT_tblEmployees.LastName, SCT_tblEmployees.EmpCode, SCT_tblEmployees.EmailId,SCT_tblEmployees.DeptID, ";
                qry += " SCT_lkpDepartment.DeptName,SCT_tblEmployees.DesgID, SCT_lkpDesignation.Designation, SCT_tblEmployees.DOB, SCT_tblEmployees.DOJ, ";
                qry += " SCT_tblEmployees.CorrAddress, SCT_tblEmployees.PerAddress, SCT_tblEmployees.Phone, SCT_tblEmployees.Mobile, SCT_tblEmployees.EduQualification,";
                qry += " SCT_tblEmployees.SkillSet, SCT_tblEmployees.Hobbies, username, seatlocation, ICard,Remarks, SCT_tblEmployees.RM_ID, ProjectAssigned, IsReportingManager,";
                qry += " EmailId, Remarks  FROM  SCT_tblEmployees INNER JOIN SCT_lkpDesignation ON SCT_tblEmployees.DesgID = SCT_lkpDesignation.DesgID INNER JOIN SCT_tblUsers ON ";
                qry += " SCT_tblEmployees.EmpID = SCT_tblUsers.userid INNER JOIN SCT_lkpDepartment ON SCT_tblEmployees.DeptID = SCT_lkpDepartment.DeptID ";
                qry += " INNER JOIN SCT_tblEmployeeDetails on SCT_tblEmployees.EmpID = SCT_tblEmployeeDetails.EmpID where Sct_tblEmployees.EmpCode ='" + empCode + "'";

                adpt = new SqlDataAdapter(qry, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    employee.FirstName = dr[0].ToString();
                    employee.LastName = dr[1].ToString();
                    employee.EmployeeCode = dr[2].ToString();
                    employee.EmailId = dr[24].ToString();
                    employee.DepartmentId = Convert.ToInt32(dr[4].ToString());
                    employee.DepartmentName = dr[5].ToString();
                    employee.DesgID = Convert.ToInt32(dr[6].ToString());
                    employee.Designation = dr[7].ToString();
                    employee.DateOfBirth = Convert.ToDateTime(dr[8].ToString());
                    employee.DateOfJoining = Convert.ToDateTime(dr[9].ToString());
                    employee.CorrespondanceAddress = dr[10].ToString();
                    employee.PermanentAddress = dr[11].ToString();
                    employee.PhoneNumber = dr[12].ToString();
                    employee.MobileNumber = dr[13].ToString();
                    employee.EducationalQualification = dr[14].ToString();
                    employee.SkillSet = dr[15].ToString();
                    employee.Hobbies = dr[16].ToString();
                    employee.SeatLocation = dr[18].ToString();
                    employee.Remarks = dr[25].ToString();
                    employee.UserName = dr[17].ToString();
                    employee.IsReportingManager = (Convert.ToString(dr[23].ToString().ToLower()) == "true" ? 1 : 0);
                    employee.IdentityCard = dr[19].ToString();
                    employee.ProjectId = Convert.ToInt32(dr[22].ToString());
                    employee.RM_ID = Convert.ToInt32(dr[21].ToString());

                }
                return employee;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string GetEmailId(Employee emp)
        {
            string email = string.Empty;
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string strSql = string.Empty;
            strSql += "select EmailId from Sct_tblEmployees where EmpCode = '" + emp.EmployeeCode + "'";
            adpt = new SqlDataAdapter(strSql, cn);
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                email = Convert.ToString(ds.Tables[0].Rows[0]["EmailId"].ToString());
            }
            return email;
        }

        public string Encrypt(string clearText)
        {
            //string EncryptionKey = "MAKV2SPBNI99212";
            string EncryptionKey = System.Configuration.ConfigurationManager.AppSettings["EncryptionKey"].ToString();
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public string Decrypt(string cipherText)
        {
            //string EncryptionKey = "MAKV2SPBNI99212";
            string EncryptionKey = System.Configuration.ConfigurationManager.AppSettings["EncryptionKey"].ToString();
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public string GetUserRole(string username)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            string strSql = string.Empty;
            string role = string.Empty;
            DataSet ds;
            try
            {
                strSql = "select Designation from SCT_tblEmployees,SCT_tblUsers,SCT_lkpDesignation ";
                strSql += " where SCT_tblEmployees.EmpID = SCT_tblUsers.userid and SCT_tblEmployees.DesgID = SCT_lkpDesignation.DesgID ";
                strSql += " and SCT_tblUsers.username='" + username + "'";
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    role = Convert.ToString(ds.Tables[0].Rows[0][0].ToString());
                }
                return role;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool IsActiveMember(int EmpId, int Projectid)
        {
            string strsql;
            DataSet ds = new DataSet();
            try
            {
                strsql = ("SELECT Availability FROM SCT_tblTEAMS WHERE EmpID = " + EmpId + " and ProjectId = " + Projectid + "");
                if (strsql == "N" || strsql == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public Employee EmployeeDetailForMail(Employee employee)
        {
            Employee E1 = new Employee();
            try
            {
                string sb = "";
                sb += "SELECT isnull(A.EmpId,0) EmpId,isnull(A.FirstName,'') FirstName,isnull(A.LastName,'') LastName, isnull(A.EmpCode,'') EmpCode, isnull(D.UserName,'') UserName,isnull(D.UserPwd,'')Password, D.IsAdmin, convert(char,nullif(A.DOB,'1/1/1900'),101) AS DOB,";
                sb += " convert(char,nullif(A.DOJ,'1/1/1900'),101) AS DOJ,convert(char,nullif(A.DOL,'1/1/1900'),101) AS DOL, isnull(A.CorrAddress,'') CorrAddress,isnull(A.PerAddress,'') PerAddress, isnull(A.Phone,'') Phone,isnull(A.mobile,'') Mobile, isnull(A.DeptId,0) Deptid,";
                sb += " isnull(A.DesgId,0) Desgid, isnull(A.RoleId,0) RoleId,isnull(A.EduQualification,'') EduQualification, isnull(A.SkillSet,'') SkillSet,isnull(A.EmailId,'') EmailId, isnull(A.Hobbies,'') Hobbies, isnull(A.Remarks,'') Remarks,isnull(B.Designation,'') Designation, isnull(C.DeptName,'') DeptName";
                sb += " FROM SCT_tblemployees A LEFT JOIN SCT_lkpdesignation B ON A.desgid = B.desgid LEFT JOIN SCT_lkpdepartment C ON A.deptid = C.deptid LEFT JOIN SCT_tblusers D ON A.empid = D.userid AND D.IsClient = 0 WHERE A.EmpId = ";
                sb += employee.EmployeeId;
                SqlConnection cn = new DataConn().GetConn();
                SqlDataReader rdr;
                SqlCommand cmd;

                cmd = new SqlCommand(sb, cn);
                cn.Open();
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        if (rdr["FirstName"].ToString() != null)
                        {
                            E1.FirstName = rdr["FirstName"].ToString();
                        }
                        if (rdr["LastName"].ToString() != null)
                        {
                            E1.LastName = rdr["LastName"].ToString();
                        }
                        if (rdr["EmpCode"].ToString() != null)
                        {
                            E1.EmployeeCode = rdr["EmpCode"].ToString();
                        }
                        if (rdr["UserName"].ToString() != null)
                        {
                            E1.UserName = rdr["UserName"].ToString();
                        }
                        if (rdr["Password"].ToString() != null)
                        {
                            E1.Password = rdr["Password"].ToString();
                        }
                        if (Convert.ToInt32(rdr["IsAdmin"]).ToString() != null)
                        {
                            E1.IsAministrator = Convert.ToInt32(rdr["IsAdmin"].ToString());
                        }
                        if (Convert.ToDateTime(rdr["DOB"]).ToShortDateString() != null)
                        {
                            E1.DateOfBirth = Convert.ToDateTime(rdr["DOB"]);
                        }
                        if (Convert.ToDateTime(rdr["DOJ"]).ToShortDateString() != null)
                        {
                            E1.DateOfJoining = Convert.ToDateTime(rdr["DOJ"]);
                        }
                        //if (Convert.ToDateTime(rdr["DOL"]).ToShortDateString() != null)
                        //{
                        //    E1.DateOfLeaving = Convert.ToDateTime(rdr["DOL"]);
                        //}
                        E1.CorrespondanceAddress = rdr["corrAddress"].ToString();
                        E1.PermanentAddress = rdr["PerAddress"].ToString();
                        E1.PhoneNumber = rdr["Phone"].ToString();
                        E1.MobileNumber = rdr["Mobile"].ToString();
                        E1.DepartmentId = Convert.ToInt32(rdr["DeptID"].ToString());
                        E1.DesgID = Convert.ToInt32(rdr["DesgId"].ToString());
                        E1.RoleId = Convert.ToInt32(rdr["RoleId"].ToString());
                        E1.EducationalQualification = rdr["EduQualification"].ToString();
                        E1.SkillSet = rdr["SkillSet"].ToString();
                        E1.EmailId = rdr["EmailId"].ToString();
                        E1.Hobbies = rdr["Hobbies"].ToString();
                        E1.Remarks = rdr["Remarks"].ToString();
                        E1.Designation = rdr["designation"].ToString();
                        E1.DepartmentName = rdr["DeptName"].ToString();
                    }
                }
                return E1;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public int GetEmployeeId(Employee employee)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            int empId = -1;
            adpt = new SqlDataAdapter("select empid from SCT_tblEmployees inner join Sct_tblUsers on Sct_tblEmployees.EmpId = Sct_tblUsers.UserId where username='" + employee.UserName + "'", cn);
            ds = new DataSet();
            adpt.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                empId = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
            }
            return empId;
        }

        public DataSet GetAllEmployees()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            adpt = new SqlDataAdapter("select EmpId, FirstName + ' ' + LastName as Name from Sct_tblEmployees where IsReportingManager=0 and Status='A'", cn);
            adpt.Fill(ds, "Sct_tblEMployees");
            return ds;
        }

        public DataSet GetAllReportingManagers()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            adpt = new SqlDataAdapter("select EmpId, FirstName + ' ' + LastName as Name from Sct_tblEmployees where IsReportingManager=1 and Status='A'", cn);
            adpt.Fill(ds, "Sct_tblEMployees");
            return ds;
        }

        public DataSet GetMemberRolesList()
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT MemberRoleID,MemberRole FROM SCT_lkpMemberRole order by MemberRole", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }

        public bool ChangePassword(Employee emp)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spUpdateUser";
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue("@pUserName", emp.UserName);
                cmd.Parameters.AddWithValue("@pOldPwd", emp.OldPassword);
                cmd.Parameters.AddWithValue("@pNewPwd", emp.NewPassword);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ee)
            {
                return false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public DataSet GetEmployeeList()
        {
            string strSql = string.Empty;
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            try
            {
                strSql = " SELECT A.EmpId, A.EmpCode, C.UserName, A.FirstName, A.LastName, A.FirstName+' '+A.LastName AS EMPNAME, B.Designation, A.EmailID ";
                strSql += "  FROM SCT_tblemployees A LEFT JOIN SCT_lkpdesignation B ON A.desgid = B.desgid LEFT JOIN SCT_tblusers C ";
                strSql += "  ON A.empid = C.UserId  AND C.IsClient = 0 WHERE A.STATUS = 'A' ORDER BY A.FirstName";
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet GetEmployeeListPerTeamLead(Employee emp)
        {
            string strSql = string.Empty;
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            try
            {
                //strSql = " SELECT A.EmpId, A.EmpCode, C.UserName, A.FirstName, A.LastName, A.FirstName+' '+A.LastName AS EMPNAME, B.Designation, A.EmailID ";
                //strSql += "  FROM SCT_tblemployees A LEFT JOIN SCT_lkpdesignation B ON A.desgid = B.desgid LEFT JOIN SCT_tblusers C ";
                //strSql += "  ON A.empid = C.UserId  AND C.IsClient = 0 WHERE A.STATUS = 'A' and A.RM_ID=" + emp.EmployeeId + "  ORDER BY A.FirstName";
                strSql += "SELECT A.EmpId, A.FirstName + ' ' +  A.LastName as Name FROM SCT_tblemployees A  WHERE A.STATUS = 'A' and A.RM_ID=" + emp.EmployeeId + "  ORDER BY A.FirstName";
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataSet GetEmployeeRoles(Employee emp)
        {
            string strSql = string.Empty;
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            strSql = "select itemname from SCT_lkpitems where itemid in (select itemid from SCT_lkproleitem where roleid in (select MemberRoleID from SCT_tblteams where empid =" + emp.EmployeeId + "))";
            adpt = new SqlDataAdapter(strSql, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

        public bool CheckUserRole(Employee emp, string screen)
        {
            SqlDataAdapter adpt;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = string.Empty;
            strSql = "select * from SCT_lkpitems where itemid in (select itemid from SCT_lkproleitem where roleid in (select MemberRoleID from SCT_tblteams where empid =" + emp.EmployeeId + ")) and ItemName = '" + screen + "'";
            adpt = new SqlDataAdapter(strSql, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataSet GetDetailedEmployeeList()
        {
            string strSql = string.Empty;
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt;
            DataSet ds;
            try
            {
                strSql = "SELECT SCT_tblEmployees.EmpID, SCT_tblEmployees.EmpCode, SCT_tblEmployees.FirstName, SCT_tblEmployees.LastName,";
                strSql += " convert(char,nullif(DOB,'1/1/1900'),101) as DOB,convert(char,nullif(DOJ,'1/1/1900'),101) as DOJ,SCT_lkpDesignation.Designation, SCT_lkpDepartment.DeptName, SCT_lkpMemberRole.MemberRole,";
                strSql += " SCT_tblEmployees.EmailId FROM SCT_tblEmployees INNER JOIN SCT_lkpDesignation ON SCT_tblEmployees.DesgID = SCT_lkpDesignation.DesgID INNER JOIN ";
                strSql += " SCT_tblEmployeeDetails ON SCT_tblEmployees.EmpID = SCT_tblEmployeeDetails.EmpID INNER JOIN ";
                strSql += " SCT_lkpDepartment ON SCT_tblEmployees.DeptID = SCT_lkpDepartment.DeptID INNER JOIN SCT_lkpMemberRole ON SCT_tblEmployees.RoleID = SCT_lkpMemberRole.MemberRoleID order by 1";
                adpt = new SqlDataAdapter(strSql, cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool IsClient(Employee emp)
        {
            SqlDataAdapter adpt;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            adpt = new SqlDataAdapter("select IsClient from SCT_tblUsers where username='" + emp.UserName + "'", cn);
            ds = new DataSet();
            int ctr;
            if (ds.Tables[0].Rows.Count > 0)
            {
                ctr = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
            }
            else
            {
                ctr = 0;
            }
            if (ctr == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public DataSet GetListEmailAllEmployees()
        {
            SqlDataAdapter adpt;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            adpt = new SqlDataAdapter("select emailid from SCT_tblEmployees where Status='A'", cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

        public DataSet GetEmailListPerProject(int ProjectId)
        {
            SqlDataAdapter adpt;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            adpt = new SqlDataAdapter("select emailid from SCT_tblEmployees where EmpID In (select empid from SCT_tblTeams where ProjectID =" + ProjectId + ")", cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }
    }
}
