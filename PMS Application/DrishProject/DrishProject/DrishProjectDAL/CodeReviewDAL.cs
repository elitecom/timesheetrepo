﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectDAL
{
    public class CodeReviewDAL
    {
        public DataSet GetReviewsPerProject(Project project)
        {
            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT A.ReviewId, C.AcceptabilityCode, A.Acceptability, A.ReviewDate, convert(char,nullif(A.ReviewDate,\'1/1/1900\'),106) AS strReviewDate, A.ReviewerId, A.ReviewRemarks, B.UserName as ReviewerName from SCT_tblCodeReview A left join (SELECT * FROM SCT_tblUsers WHERE IsClient=0) AS B ON A.ReviewerId = B.UserId LEFT JOIN SCT_lkpAcceptabilityReview C ON A.Acceptability = C.AcceptabilityID where ProjectId = " + project.ProjectID + " ORDER BY A.ReviewDate DESC", cn);
                DTSet = new DataSet();
                adpt.Fill(DTSet, "SCT_lkpAcceptabilityReview");
                return DTSet;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }

        }
        private bool RefExists(CodeReview review)
        {
            string strSql = "";
            string strRetVal = "";
            strSql += "SELECT SUM(ct) FROM (SELECT COUNT(*) ct FROM SCT_tblProjectHistory WHERE (PMTId = ";
            strSql += review.ReviewerId;
            strSql += " AND IdType = \'C\') ";
            strSql += " UNION SELECT COUNT(Ă) ct FROM SCT_tblCodeReview WHERE ReviewId = ";
            strSql += review.ReviewId;
            strSql += " ) AS Table1";
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strSql, cn);
            cn.Open();

            strRetVal = cmd.ExecuteScalar().ToString();
            if (strRetVal != "0")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool DeleteReview(CodeReview review)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spDeleteReview";
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue("@pReviewId", review.ReviewId);
                pp = new SqlParameter();
                pp.ParameterName = "@ID";
                pp.DbType = DbType.Int32;
                pp.Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@ID", 0);
                int temp;
                temp = cmd.ExecuteNonQuery();
                res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                if (res != -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }
        public DataSet GetReviewDetail(CodeReview review)
        {
            DataSet DTSet = new DataSet();

            SqlDataAdapter adpt;

            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT A.AgendaId, A.ObservationId, A.ReviewId, A.Observation, A.SLId, A.FollowUpplan, A.PlanDate, A.Status, A.ReviewerRemarks, B.AgendaPoint from SCT_tblCodeReviewDetail A LEFT JOIN SCT_lkpAgendaList B ON A.AgendaId = B.AgendaId where ReviewId = " + review.ReviewId, cn);
                DTSet = new DataSet();
                adpt.Fill(DTSet, "SCT_lkpAgendaList");
                return DTSet;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }
        public void GetReview(CodeReview review)
        {
            SqlDataReader rdr;
            SqlConnection cn = new DataConn().GetConn();
            string strsql = "";
            strsql += "SELECT A.ReviewId, A.ProjectId, A.ReviewDate, ";
            strsql += "convert(char,nullif(A.ReviewDate,\'1/1/1900\'),101) AS strReviewDate, convert(char,nullif(A.ReviewDate,\'1/1/1900\'),106) AS strReviewDateF, ";
            strsql += "A.ReviewerId, A.ReviewRemarks, A.Acceptability, B.UserName ";
            strsql += "FROM SCT_tblCodeReview A LEFT JOIN (SELECT * from SCT_tblUsers WHERE IsClient = 0 ) AS B ON A.ReviewerId = B.Userid ";
            strsql += "WHERE ReviewID = ";
            strsql += review.ReviewId;
            SqlCommand cmd;
            cmd = new SqlCommand(strsql, cn);
            cn.Open();
            rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                rdr.Read();
                if (Convert.ToInt32(rdr["ProjectId"]).ToString() != null)
                {
                    review.ProjectId = Convert.ToInt32(rdr["ProjectId"].ToString());
                }
                if (Convert.ToDateTime(rdr["ReviewDate"]).ToShortDateString() != null)
                {
                    review.dtReviewDate = Convert.ToDateTime(rdr["ReviewDate"].ToString());
                }
                if (Convert.ToDateTime(rdr["strReviewDate"]).ToShortDateString() != null)
                {
                    review.dtReviewDate = Convert.ToDateTime(rdr["strReviewDate"].ToString());
                }
                if (rdr["strReviewDateF"].ToString() != null)
                {
                    review.ReviewDateF = rdr["strReviewDateF"].ToString();
                }
                if (rdr["UserName"].ToString() != null)
                {
                    review.ReviewerName = rdr["UserName"].ToString();
                }
                if (Convert.ToInt32(rdr["ReviewerId"]).ToString() != null)
                {
                    review.ReviewerId = Convert.ToInt32(rdr["ReviewerId"].ToString());
                }
                if (rdr["ReviewRemarks"].ToString() != null)
                {
                    review.ReviewRemarks = rdr["ReviewRemarks"].ToString();
                }
                if (Convert.ToInt32(rdr["Acceptability"]).ToString() != null)
                {
                    review.Acceptability = Convert.ToInt32(rdr["Acceptability"].ToString());
                }
            }
            rdr.Close();

        }
        public bool UpdateReview(CodeReview review)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            try
            {
                if (!IsReviewExists(review))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SCT_spUpdateReview";
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.Parameters.AddWithValue("@pReviewId", review.ReviewId);
                    cmd.Parameters.AddWithValue("@pReviewDate", review.ReviewDate);
                    cmd.Parameters.AddWithValue("@pAcceptability", review.Acceptability);
                    cmd.Parameters.AddWithValue("@pReviewRemarks", review.ReviewRemarks);
                    cmd.Parameters.AddWithValue("@pLastModifiedBy ", review.LastModifiedBy);
                    cmd.Parameters.AddWithValue("@pLastModifiedOn ", review.dtLastModifiedOn.ToShortDateString());


                    pp = new SqlParameter();
                    pp.ParameterName = "@ID";
                    pp.DbType = DbType.Int32;
                    pp.Direction = ParameterDirection.Output;
                    cmd.Parameters.AddWithValue("@ID", 0);
                    int temp;
                    temp = cmd.ExecuteNonQuery();
                    res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                    if (res != -1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                else
                {
                    string strRetVal = "";
                    strRetVal = "Review has already been saved, Could not be saved!";
                }
                return true;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();

            }

        }
        public bool InsertReview(CodeReview review)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spInsertReview";
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue("@pProjectId", review.ProjectId);
                cmd.Parameters.AddWithValue("@pReviewDate", review.ReviewDate);
                cmd.Parameters.AddWithValue("@pAcceptability", review.Acceptability);
                cmd.Parameters.AddWithValue("@pReviewRemarks", review.ReviewRemarks);
                cmd.Parameters.AddWithValue("@pReviewerId", review.ReviewerId);


                pp = new SqlParameter();
                pp.ParameterName = "@ID";
                pp.DbType = DbType.Int32;
                pp.Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@ID", 0);
                int temp;
                temp = cmd.ExecuteNonQuery();
                res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                if (res != -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        private bool IsReviewExists(CodeReview review)
        {
            string strsql = "";
            string strRetVal = "";
            bool bVal;
            if (review.ReviewerId == 0)
            {
                strsql += "SELECT COUNT(*) FROM SCT_tblcodereview WHERE ReviewDate = \'";
                strsql += review.ReviewDate;
                strsql += "\' AND ReviewerId = ";
                strsql += review.ReviewerId;
                strsql += " AND ProjectID = ";
                strsql += review.ProjectId;
            }
            else
            {
                strsql += "SELECT COUNT(*) FROM SCT_tblCodeReview WHERE ReviewDate = \'";
                strsql += review.ReviewDate;
                strsql += "\' and ReviewerId = ";
                strsql += review.ReviewerId;
                strsql += " AND ReviewId <> ";
                strsql += review.ReviewId;
                strsql += " AND ProjectID = ";
                strsql += review.ProjectId;
            }

            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strsql, cn);
            cn.Open();
            strRetVal = cmd.ExecuteScalar().ToString();
            if (strRetVal != "0")
            {
                bVal = true;
            }
            else
            {
                bVal = false;
            }
            return bVal;
        }
        public DataSet GetReviewDetailsPerReview(CodeReview review)
        {
            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT A.ReviewId,A.AgendaId, A.ObservationId, A.Observation, A.Slid, A.FollowUpPlan, A.AssignedTo, A.PlanDate, convert(char,nullif(A.PlanDate,\'1/1/1900\'),106) AS strPlanDate, A.Status, A.ReviewerRemarks, B.UserName as NameResponsibility, C.AgendaPoint, D.ReviewerId from SCT_tblCodeReviewDetail A left join (SELECT * FROM SCT_tblUsers WHERE IsClient=0) AS B ON A.Assignedto = B.UserId LEFT JOIN SCT_lkpAgendaList C ON A.AgendaId = C.AgendaID LEFT JOIN SCT_tblCodeReview D ON A.ReviewId=D.ReviewID where A.ReviewId = " + review.ReviewerId + " ORDER BY C.AgendaPoint", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }
        public bool DeleteObservation(CodeReview review)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spDeleteObservation";
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue("@pObservationId ", review.ObservationId);
                pp = new SqlParameter();
                pp.ParameterName = "@ID";
                pp.DbType = DbType.Int32;
                pp.Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@ID", 0);
                int temp;
                temp = cmd.ExecuteNonQuery();
                res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                if (res != -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }

        }
        public void GetObservation(int ObservationId)
        {
            SqlDataReader rdr;
            SqlConnection cn = new SqlConnection();
            string strsql = "";
            strsql += "SELECT A.ReviewId, A.AgendaId, A.ObservationId, A.Observation, A.SLId, A.FollowUpPlan, A.AssignedTo, ";
            strsql += "convert(char,nullif(A.PlanDate,\'1/1/1900\'),101) AS strPlanDate,convert(char,nullif(A.PlanDate,\'1/1/1900\'),106) AS strPlanDateF, A.Status, ";
            strsql += "A.ReviewerRemarks, B.UserName, C.AgendaPoint ";
            strsql += "FROM SCT_tblCodeReviewDetail A LEFT JOIN (SELECT * from SCT_tblUsers WHERE IsClient = 0 ) AS B ON A.AssignedTo = B.Userid ";
            strsql += "LEFT JOIN SCT_lkpAgendaList C ON A.AgendaId = C.AgendaId WHERE ObservationID = ";
            strsql += ObservationId;
            SqlCommand cmd;
            CodeReview review = new CodeReview();
            cmd = new SqlCommand(strsql, cn);
            cn.Open();
            rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                rdr.Read();
                if (Convert.ToInt32(rdr["ReviewId"]).ToString() != null)
                {
                    review.ReviewId = Convert.ToInt32(rdr["DocName"].ToString());
                }
                if (Convert.ToInt32(rdr["AgendaId"]).ToString() != null)
                {
                    review.AgendaId = Convert.ToInt32(rdr["AgendaId"].ToString());
                }
                if (rdr["AgendaPoint"].ToString() != null)
                {
                    review.AgendaPoint = rdr["AgendaPoint"].ToString();
                }
                if (Convert.ToInt32(rdr["ObservationId"]).ToString() != null)
                {
                    review.ObservationId = Convert.ToInt32(rdr["ObservationId"].ToString());
                }
                if (rdr["Observation"].ToString() != null)
                {
                    review.Observation = rdr["Observation"].ToString();
                }
                if (rdr["SLId"].ToString() != null)
                {
                    review.SLId = rdr["SLId"].ToString();
                }
                if (rdr["FollowUpPlan"].ToString() != null)
                {
                    review.FollowUpPlan = rdr["FollowUpPlan"].ToString();
                }
                if (Convert.ToInt32(rdr["AssignedTo"]).ToString() != null)
                {
                    review.ResponsibilityID = Convert.ToInt32(rdr["AssignedTo"].ToString());
                }
                if (rdr["UserName"].ToString() != null)
                {
                    review.Responsibility = rdr["UserName"].ToString();
                }
                if (Convert.ToDateTime(rdr["strPlanDate"]).ToShortDateString() != null)
                {
                    review.PlanDate = Convert.ToDateTime(rdr["strPlanDate"]).ToShortDateString();
                }
                if (Convert.ToDateTime(rdr["strPlanDateF"]).ToShortDateString() != null)
                {
                    review.PlanDateF = Convert.ToDateTime(rdr["strPlanDateF"]).ToShortDateString();
                }
                if (rdr["status"].ToString() != null)
                {
                    review.Status = rdr["status"].ToString();
                }
                if (rdr["ReviewerRemarks"].ToString() != null)
                {
                    review.ReviewerRemarks = rdr["ReviewerRemarks"].ToString();
                }
            }
            rdr.Close();


        }
        public bool UpdateObservation(CodeReview review)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            try
            {
                if (!IsObservationExists(review))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SCT_spUpdateObservation";
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.Parameters.AddWithValue("@pObservationId", review.ObservationId);
                    cmd.Parameters.AddWithValue("@pAgendaId", review.AgendaId);
                    cmd.Parameters.AddWithValue("@pObservation", review.Observation);
                    cmd.Parameters.AddWithValue("@pSLId", review.SLId);
                    cmd.Parameters.AddWithValue("@pFollowUpPlan", review.FollowUpPlan);
                    cmd.Parameters.AddWithValue("@pResponsibility", review.Responsibility);
                    cmd.Parameters.AddWithValue("@pPlanDate n", review.PlanDate);
                    cmd.Parameters.AddWithValue("@pStatus", review.Status);
                    cmd.Parameters.AddWithValue("@pReviewerRemarks", review.ReviewerRemarks);
                    pp = new SqlParameter();
                    pp.ParameterName = "@ID";
                    pp.DbType = DbType.Int32;
                    pp.Direction = ParameterDirection.Output;
                    cmd.Parameters.AddWithValue("@ID", 0);
                    int temp;
                    temp = cmd.ExecuteNonQuery();
                    res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                    if (res != -1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;

            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }

        }
        public bool InsertObservation(CodeReview review)
        {

            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            try
            {

                if (!IsObservationExists(review))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SCT_spInsertObservation";
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.Parameters.AddWithValue("@p@ReviewId", review.ReviewId);
                    cmd.Parameters.AddWithValue("@pAgendaId", review.AgendaId);
                    cmd.Parameters.AddWithValue("@pObservation", review.Observation);
                    cmd.Parameters.AddWithValue("@pSLId", review.SLId);
                    cmd.Parameters.AddWithValue("@pFollowUpPlan", review.FollowUpPlan);
                    cmd.Parameters.AddWithValue("@pResponsibility", review.Responsibility);
                    cmd.Parameters.AddWithValue("@pPlanDate n", review.PlanDate);
                    cmd.Parameters.AddWithValue("@pStatus", review.Status);
                    cmd.Parameters.AddWithValue("@pReviewerRemarks", review.ReviewerRemarks);
                    pp = new SqlParameter();
                    pp.ParameterName = "@ID";
                    pp.DbType = DbType.Int32;
                    pp.Direction = ParameterDirection.Output;
                    cmd.Parameters.AddWithValue("@ID", 0);
                    int temp;
                    temp = cmd.ExecuteNonQuery();
                    res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                    if (res != -1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;

            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }


        }
        private bool IsObservationExists(CodeReview review)
        {
            string strsql = "";
            string strRetVal = "";
            bool bVal;
            if (review.ObservationId == 0)
            {
                strsql += "SELECT COUNT(*) FROM SCT_tblcodereviewdetail WHERE Observation = \'";
                strsql += review.Observation;
                strsql += "\' and ReviewId = ";
                strsql += review.ReviewId;
                strsql += " and AgendaId = ";
                strsql += review.AgendaId;
            }
            else
            {
                strsql += "SELECT COUNT(*) FROM SCT_tblCodeReviewdetail WHERE Observation = \'";
                strsql += review.Observation;
                strsql += "\' and ReviewId = ";
                strsql += review.ReviewId;
                strsql += " and AgendaId = ";
                strsql += review.AgendaId;
                strsql += " AND ObservationId <> ";
                strsql += review.ObservationId;
            }
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strsql, cn);
            cn.Open();
            strRetVal = cmd.ExecuteScalar().ToString();
            if (strRetVal != "0")
            {
                bVal = true;
            }
            else
            {
                bVal = false;
            }
            return bVal;

        }
        public DataSet GetTeam(Project prj)
        {
            string sqlstr = "";
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();

            sqlstr += "SELECT A.ProjectId, A.EmpId, B.UserName ";
            sqlstr += "FROM SCT_tblteams A left join (select username, userid from SCT_tblEmployees left join SCT_tblUsers on SCT_tblEmployees.empid = SCTĂtblUsers.UserId where isclient=0 and status = \'A\') B on A.empid = B.Userid ";
            sqlstr += " WHERE A.Availability = \'Y\' and B.UserName != \'\' AND A.ProjectId = ";
            sqlstr += prj.ProjectID;
            adpt = new SqlDataAdapter(sqlstr, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }
        public DataSet GetAgendaList()
        {
            string sqlstr = "";
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            sqlstr += "SELECT A.AgendaId, A.AgendaPoint ";
            sqlstr += "FROM SCT_lkpAgendaList A ";

            adpt = new SqlDataAdapter(sqlstr, cn);
            ds = new DataSet();
            adpt.Fill(ds);
            return ds;


        }
        public DataSet GetSeverityLevels()
        {

            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT SEVERITYLEVEL, SEVERITYCODE FROM SCT_lkpSeverityLevel ORDER BY SEVERITYLEVEL", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }

        }
        public DataSet GetAcceptabilityList()
        {

            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT AcceptabilityID, Acceptability FROM SCT_lkpAcceptabilityReview ORDER BY AcceptabilityID", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }
        private string SetStrRight(string pstr)
        {
            string strRetVal;

            strRetVal = pstr.Replace("\'", "\'\'");
            return strRetVal;
        }		
    }
}
