﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;
using System.Collections;

namespace DrishProjectDAL
{
    public class SkillSetsDAL
    {
        public DataSet GetQuarter()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt = new SqlDataAdapter("select * from SCT_lkpQuarter", cn);
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }
        public DataSet GetDropDownValue()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt = new SqlDataAdapter("select * from SCT_lkpDropDownValue", cn);
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }
        public DataSet GetYear()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt = new SqlDataAdapter("select * from SCT_lkpYear", cn);
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            return ds;
        }

        public Hashtable GetSkillSetTechValue()
        {
            Hashtable table = new Hashtable();
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt = new SqlDataAdapter("select * from SCT_lkpSkillSetTech", cn);
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            string key = string.Empty;
            int value;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = ds.Tables[0].Rows[i];
                    key = dr[1].ToString();
                    value = Convert.ToInt32(dr[0].ToString());
                    table.Add(key, value);
                }
            }
            return table;
        }

        public Hashtable GetSkillSetGeneralValue()
        {
            Hashtable table = new Hashtable();
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt = new SqlDataAdapter("select * from SCT_lkpSkillSetGeneral", cn);
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            string key = string.Empty;
            int value;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = ds.Tables[0].Rows[i];
                    key = dr[1].ToString();
                    value = Convert.ToInt32(dr[0].ToString());
                    table.Add(key, value);
                }
            }
            return table;
        }

        public bool InsertTblSkillset(SkillSet skillset)
        {
            return true;
        }
    }
}
