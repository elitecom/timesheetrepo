﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectDAL
{
    public class ForumDAL
    {
        public int GetNextTopicId(int res)
        {
            string strSql;
            string strRetVal = "";
            int intNextId;
            bool bVal;
            strSql = "select isnull(MAX(TopicId),0) from SCT_tblTopics";
            intNextId = Convert.ToInt32(strRetVal) + 1;
            res = intNextId;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strSql, cn);
            cn.Open();
            strRetVal = cmd.ExecuteScalar().ToString();
            if (strRetVal != "0")
            {
                bVal = true;
            }
            else
            {
                bVal = false;
            }
            res = Convert.ToInt32(strRetVal);
            return res;
        }

        public int GetNextReplyId(int res)
        {
            string strSql;
            string strRetVal = String.Empty;
            int intNextId;
            bool bVal=false;
            SqlCommand cmd;
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                strSql = "select isnull(MAX(ReplyId),0) from SCT_tblReply";
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.CommandText = strSql;
                cn.Open();
                strRetVal = Convert.ToString(cmd.ExecuteScalar());
                cn.Close();
                intNextId = Convert.ToInt32(strRetVal) + 1;
                
                if (strRetVal != "0")
                {
                    res = intNextId;
                    bVal = true;
                }
                else
                {
                    res = 0;
                    bVal = false;
                }
            }
            catch (Exception)
            {
                bVal = false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
              
                cn.Dispose();
            }
            return res;
        }


        public Forum GetTopics(int id)
        {
            Forum forum = new Forum();
            string strsql;
            strsql = "SELECT A.TopicId, A.TopicSummery, A.TopicDesc, A.Attachment, A.CreatedBy, convert(char,nullif(A.CreatedOn,\'1/1/1900\'),106) AS DateCreated, A.LastReplyBY, convert(char,nullif(A.LastReplyOn,\'1/1/1900\'),101) AS DateLastReply, B.UserName as NameCreatedBY, C.UserName as NameLastReplyBy FROM SCT_tblTopics A LEFT JOIN (SELECT * FROM SCT_tblUsers WHERE isClient=0) AS B ON A.CreatedBY = B.UserId LEFT JOIN (SELECT * FROM SCT_tblUsers WHERE isClient=0) AS C ON A.LastReplyBy = C.UserId " + " WHERE TopicID = " + id;
            SqlConnection cn = new DataConn().GetConn();
            SqlDataAdapter adpt = new SqlDataAdapter(strsql, cn);
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                forum.TopicId = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                forum.TopicSummary = ds.Tables[0].Rows[0][1].ToString();
                forum.Attachment = ds.Tables[0].Rows[0][2].ToString();
                forum.CreatedBy = Convert.ToInt32(ds.Tables[0].Rows[0][3].ToString());
                forum.CreatedOn = Convert.ToDateTime(ds.Tables[0].Rows[0][4].ToString());
                forum.LastReplyBy = Convert.ToInt32(ds.Tables[0].Rows[0][5].ToString());
                forum.LastReployOn = Convert.ToDateTime(ds.Tables[0].Rows[0][6].ToString());
                forum.NameLastReplyBy = ds.Tables[0].Rows[0][7].ToString();
            }
            return forum;
        }

        public bool DeleteTopic()
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            Forum fr = new Forum();
            int res;
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spDeleteTopic";
                cmd.Connection = cn;
                cn.Open();

                return true;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }            
        }

        public int GetNextReplyId()
        {
            return 1;
        }
        public bool bln_ToSelectTopic(int intTopicId)
        {
            return true;
        }
        public string InsertReply(Forum forum)
        {
            return "";
        }
        public Forum GetTopicInfo(int intTopicId)
        {
            Entities.Forum forum = new Entities.Forum();
            return forum;
        }
        public DataSet GetReplies(int topicId)
        {
            DataSet ds = new DataSet();
            return ds;
        }
        public string GetReplyDocName(int intReplyId)
        {
            return "";
        }
        public string DeleteReply(int intReplyId)
        {
            return "";
        }
    }
}
