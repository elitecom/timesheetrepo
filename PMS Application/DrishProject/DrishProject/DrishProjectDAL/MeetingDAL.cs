﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectDAL
{
    public class MeetingDAL
    {
        public Meeting GetMeeting(Meeting meeting1)
        {
            Meeting meeting = new Meeting();
            string strsql = "";
            strsql += "SELECT A.MeetingId, A.ProjectId, A.Recap, A.ToMembers, A.CcMembers, A.Attachment, Convert(char,nullif(A.DateTime,'1/1/1900'),106) AS MeetingDate,";
            strsql += " CASE WHEN MeetingHr < 10 THEN '0'+RTRIM(LTRIM(convert(char,MeetingHr))) ELSE RTRIM(LTRIM(convert(char, MeetingHr))) END+ ':' +  ";
            strsql += " CASE WHEN MeetingMin < 10 THEN '0'+ RTRIM(LTRIM(convert(char, MeetingMin))) ELSE RTRIM(LTRIM(convert(char, MeetingMin))) END ";
            strsql += "+ ' ' + LTRIM(RTRIM(A.MeetingAMPM)) AS MeetingTime, CASE WHEN MeetingHr < 10 THEN '0'+ LTRIM(RTRIM(convert(char, MeetingHr))) ";
            strsql += " ELSE LTRIM(RTRIM(convert(char, MeetingHr))) END AS MeetingHr, CASE WHEN MeetingMin < 10 THEN '0'+";
            strsql += " LTRIM(RTRIM(convert(char, MeetingMin))) ELSE LTRIM(RTRIM(convert(char, MeetingMin))) END AS MeetingMin, A.MeetingAmPm, ";
            strsql += "A.Duration, A.MediaId,B.Media, A.subject, A.calledby, A.MeetingStatus, ";
            strsql += "C.FirstName + ' ' + C.LastName AS Username FROM SCT_tblProjectMeetings A LEFT JOIN SCT_lkpMeetingMedia B ON A.MediaId = B.MediaId ";
            strsql += "LEFT JOIN SCT_tblEmployees C ON A.CalledBy = C.empid ";
            strsql += "WHERE ";
            strsql += "meetingID = ";
            strsql += meeting1.MeetingId;
            SqlDataReader rdr;
            SqlCommand cmd;
            SqlConnection cn = new DataConn().GetConn();
            cmd = new SqlCommand(strsql, cn);
            cn.Open();
            rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                while (rdr.Read())
                {
                    if (Convert.ToInt32(rdr["ProjectId"]).ToString() != null)
                    {
                        meeting.ProjectId = Convert.ToInt32(rdr["ProjectId"].ToString());
                    }
                    else
                    {
                        meeting.ProjectId = 0;
                    }

                    if (rdr["Recap"].ToString() != null)
                    {
                        meeting.Recap = rdr["Recap"].ToString();
                    }
                    else
                    {
                        meeting.Recap = "";
                    }
                    if (rdr["MeetingDate"].ToString() != null)
                    {
                        meeting.MeetingDate = rdr["meetingdate"].ToString();
                    }
                    else
                    {
                        meeting.MeetingDate = "";
                    }
                    if (rdr["MeetingTime"].ToString() != null)
                    {
                        meeting.MeetingTime = rdr["MeetingTime"].ToString();
                    }
                    else
                    {
                        meeting.MeetingDate = "";
                    }

                    if (rdr["MeetingHr"].ToString() != null)
                    {
                        meeting.MeetingHr = rdr["Meetinghr"].ToString();
                    }
                    else
                    {
                        meeting.MeetingHr = "";
                    }
                    if (rdr["MeetingMin"].ToString() != null)
                    {
                        meeting.MeetingMin = rdr["meetingmin"].ToString();
                    }
                    else
                    {
                        meeting.MeetingMin = "";
                    }
                    if (rdr["MeetingAmPm"].ToString() != null)
                    {
                        meeting.MeetingAmPm = rdr["meetingAmPm"].ToString(); ;
                    }
                    else
                    {
                        meeting.MeetingAmPm = "";
                    }
                    if (rdr["duration"].ToString() != null)
                    {
                        meeting.Duration = rdr["duration"].ToString();
                    }
                    else
                    {
                        meeting.Duration = "";
                    }
                    if (Convert.ToInt32(rdr["MediaId"]).ToString() != null)
                    {
                        meeting.MediaId = Convert.ToInt32(rdr["MediaId"].ToString());
                    }
                    else
                    {
                        meeting.MediaId = 0;
                    }
                    if (rdr["media"].ToString() != null)
                    {
                        meeting.Media = rdr["media"].ToString();
                    }
                    else
                    {
                        meeting.Media = "";
                    }
                    if (rdr["subject"].ToString() != null)
                    {
                        meeting.Subject = rdr["subject"].ToString();
                    }
                    else
                    {
                        meeting.Subject = "";
                    }
                    if (Convert.ToInt32(rdr["CalledBy"]).ToString() != null)
                    {
                        meeting.CalledBy = Convert.ToInt32(rdr["calledby"].ToString());
                    }
                    else
                    {
                        meeting.CalledBy = 0;
                    }
                    if (rdr["UserName"].ToString() != null)
                    {
                        meeting.CalledByName = rdr["UserName"].ToString();
                    }
                    else
                    {
                        meeting.CalledByName = "";
                    }
                    if (Convert.ToInt32(rdr["MeetingStatus"]).ToString() != null)
                    {
                        meeting.MeetingStatusId = Convert.ToInt32(rdr["MeetingStatus"].ToString());
                    }
                    else
                    {
                        meeting.MeetingStatusId = 0;
                    }
                    if (rdr["ToMembers"].ToString() != null)
                    {
                        meeting.To = rdr["ToMembers"].ToString();
                    }
                    else
                    {
                        meeting.To = "";
                    }
                    if (rdr["CcMembers"].ToString() != null)
                    {
                        meeting.Cc = rdr["CcMembers"].ToString();
                    }
                    else
                    {
                        meeting.Cc = "";
                    }
                    if (rdr["Attachment"].ToString() != null)
                    {
                        meeting.Attachment = rdr["Attachment"].ToString();
                    }
                    else
                    {
                        meeting.Attachment = "";
                    }
                    if (Convert.ToInt32(rdr["calledby"]).ToString() != null)
                    {
                        meeting.CalledBy = Convert.ToInt32(rdr["calledby"].ToString());
                    }
                    else
                    {
                        meeting.CalledBy = 0;
                    }
                    //if (Convert.ToInt32(rdr["VenueId"]).ToString()!=null)
                    //{
                    //    meeting.VenueId = Convert.ToInt32(rdr["VenueId"].ToString());
                    //}
                    //else
                    //{
                    //    meeting.VenueId = 0;
                    //}
                }
                rdr.Close();

            }
            return meeting;
        }
        
        public DataSet GetEmpList(Meeting meeting)
        {

            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string sqlStr = "";

            if (meeting.Criteria.ToUpper() == "EMPLOYEES")
            {
                sqlStr += "SELECT A.EmpId, A.FirstName+ ' ' + A.LastName AS username, A.emailid, B.Designation ";
                sqlStr += "FROM SCT_tblemployees A join SCT_lkpdesignation B ON A.desgid = B.desgid ";
                sqlStr += "WHERE status = 'A' AND EmpId ";
                sqlStr += "NOT IN (SELECT empid FROM SCT_tblMeetingMembers WHERE meetingid = ";
                sqlStr += meeting.MeetingId;
                sqlStr += " and isClient = 0)";
            }
            else
            {
                sqlStr += "SELECT A.ProjectId, A.EmpId, B.FirstName+ ' ' + B.LastName AS username, B.emailid, ";
                sqlStr += "C.Designation FROM SCT_tblteams A left join SCT_tblemployees B on A.empid = B.empid ";
                sqlStr += "left join SCT_lkpdesignation C ON B.desgid = C.desgid ";
                sqlStr += "WHERE B.status = 'A' AND A.EmpId ";
                sqlStr += "NOT IN (SELECT empid FROM SCT_tblMeetingMembers WHERE meetingid = ";
                sqlStr += meeting.MeetingId;
                sqlStr += " and isClient = 0) and A.ProjectId = ";
                sqlStr += meeting.ProjectId;
            }
            adpt = new SqlDataAdapter(sqlStr, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }

        public DataSet GetMeetingId(int intProjectId)
        {

            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string sqlStr = "";

            sqlStr += "Select MeetingId from SCT_tblProjectMeetings where ProjectId= ";
            sqlStr += intProjectId;
            adpt = new SqlDataAdapter(sqlStr, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }
        
        public DataSet GetMemberList(Meeting meeting)
        {
            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "";

            strSql += "SELECT B.Empid, B.FirstName + ' ' + B.LastName AS UserName FROM SCT_tblMeetingMembers A LEFT JOIN ";
            strSql += " SCT_tblemployees B ON A.empid = B.empid ";
            strSql += "WHERE meetingid = ";
            strSql += meeting.MeetingId;
            strSql += " ORDER BY B.EmpCode";
            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }
        
        public DataSet GetMemberListByMeetingID(int meetingID)
        {
            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "";

            strSql += "SELECT B.Empid, B.FirstName + ' ' + B.LastName AS UserName FROM SCT_tblMeetingMembers A LEFT JOIN ";
            strSql += " SCT_tblemployees B ON A.empid = B.empid ";
            strSql += "WHERE meetingid = ";
            strSql += meetingID;
            strSql += " ORDER BY B.EmpCode";
            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }
        public DataSet GetSelectedClientList(Meeting meeting)
        {
            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "";

            strSql += "SELECT B.Contactid, LTRIM(RTRIM(B.FirstName)) + \' \' + LTRIM(RTRIM(B.MiddleName)) + CASE WHEN LTRIM(RTRIM(ISNULL(B.MiddleName,\'\'))) <> \'\' THEN \' \' ELSE \'\' END + LTRIM(RTRIM(B.LastName)) AS ClientName  FROM SCT_tblMeetingMembers A LEFT JOIN ";
            strSql += " SCT_tblContacts B ON A.empid = B.ContactId ";
            strSql += "WHERE meetingid = ";
            strSql += meeting.MeetingId;
            strSql += " and isClient = 1 ORDER BY B.firstname, B.Lastname";
            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }

        public DataSet GetClientList(Meeting meeting)
        {

            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "";

            strSql += "SELECT A.ContactId, ltrim(rtrim(A.FirstName)) + ' ' + ltrim(rtrim(A.MiddleName)) +  CASE WHEN ltrim(rtrim(A.MiddleName)) <> '' THEN ' ' ELSE '' + ltrim(rtrim(LastName)) END AS ClientName  FROM SCT_tblContacts A LEFT JOIN ";
            strSql += " SCT_tblProjectClients B ON A.ContactId = B.ContactId ";
            strSql += " WHERE Projectid = ";
            strSql += meeting.ProjectId;
            strSql += " ORDER BY A.FirstName, A.MiddleName, A.LastName";
            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;

        }
        
        //public DataSet GetMediaList()
        //{
        //    SqlDataAdapter adpt;
        //    DataSet ds = new DataSet();
        //    SqlConnection cn = new DataConn().GetConn();
        //    try
        //    {
        //        adpt = new SqlDataAdapter("SELECT * FROM SCT_lkpMeetingMedia ORDER BY media", cn);
        //        ds = new DataSet();
        //        adpt.Fill(ds);
        //        return ds;
        //    }
        //    catch (Exception ee)
        //    {
        //        throw new ArgumentException(ee.Message);
        //    }

        //}

        public bool DeleteMeeting(Meeting meeting)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            Meeting ms = new Meeting();
            int res;
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spDeleteMeeting";
                cmd.Connection = cn;
                cn.Open();

                //@pMeetingId int  
                cmd.Parameters.AddWithValue("@pMeetingId", meeting.MeetingId);

                pp = new SqlParameter();
                pp.ParameterName = "@ID";
                pp.DbType = DbType.Int32;
                pp.Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@ID", 0);
                int temp;
                temp = cmd.ExecuteNonQuery();
                res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                if (res != -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }
        
        private string SetStrRight(string pstr)
        {
            string strRetVal;

            strRetVal = pstr.Replace("'", "''");
            return strRetVal;

        }
        
        private bool IsMeetingExists(Meeting meeting)
        {
            string strsql = "";
            string strRetVal;
            bool bVal;
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            if (meeting.MeetingId == 0)
            {
                #region "Unused Code"                
                //strsql += "SELECT COUNT(*) FROM SCT_tblProjectMeetings WHERE datetime = '";
                //strsql += Convert.ToDateTime(meeting.MeetingDate.ToString());
                //strsql += "' AND subject = '";
                //strsql += meeting.Subject;
                //strsql += "' AND calledby = ";
                //strsql += meeting.CalledBy;
                //strsql += " AND meetinghr = ";
                //strsql += meeting.MeetingHr;
                //strsql += " AND meetingmin = ";
                //strsql += meeting.MeetingMin;
                //strsql += " AND meetingampm = '";
                //strsql += meeting.MeetingAmPm;
                //strsql += "'";
                #endregion
                strsql += "SELECT COUNT(*) FROM SCT_tblProjectMeetings WHERE datetime = @datetime";
                strsql += " AND subject = @subject AND calledby = @calledby AND meetinghr = @meetinghr";
                strsql += " AND meetingmin = @meetingmin  AND meetingampm = @meetingampm";
                cmd.CommandText = strsql;
            }
            else
            {
                #region "Unused Code"                
                //strsql += "SELECT count(*) FROM SCT_tblProjectMeetings WHERE datetime = '";
                //strsql += Convert.ToDateTime(meeting.MeetingDate);
                //strsql += "' AND subject = '";
                //strsql += meeting.Subject;
                //strsql += "' AND calledby = ";
                //strsql += meeting.CalledBy;
                //strsql += " AND meetinghr = ";
                //strsql += meeting.MeetingHr;
                //strsql += " AND meetingmin = ";
                //strsql += meeting.MeetingMin;
                //strsql += " AND meetingampm = '";
                //strsql += meeting.MeetingAmPm;
                //strsql += "' AND meetingid <> ";
                //strsql += meeting.MeetingId;
                #endregion

                strsql += "SELECT count(*) FROM SCT_tblProjectMeetings WHERE datetime = @datetime ";
                strsql += " AND subject = @subject  AND calledby = @calledby  AND meetinghr = @meetinghr ";
                strsql += " AND meetingmin = @meetingmin  AND meetingampm = @meetingampm  AND meetingid <> @meetingid";               
                cmd.CommandText = strsql;
                cmd.Parameters.AddWithValue("@meetingid", meeting.MeetingId);
            }
            cn.Open();
            cmd.Parameters.AddWithValue("@datetime", Convert.ToDateTime(meeting.MeetingDate));
            cmd.Parameters.AddWithValue("@subject", meeting.Subject);
            cmd.Parameters.AddWithValue("@calledby", meeting.CalledBy);
            cmd.Parameters.AddWithValue("@meetinghr", meeting.MeetingHr);
            cmd.Parameters.AddWithValue("@meetingmin", meeting.MeetingMin);
            cmd.Parameters.AddWithValue("@meetingampm", meeting.MeetingAmPm);

            strRetVal = cmd.ExecuteScalar().ToString();
            if (strRetVal != "0")
            {
                bVal = true;
            }
            else
            {
                bVal = false;
            }
            return bVal;
        }
        
        public bool UpdateMeeting(Meeting meeting)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            try
            {
                if (!IsMeetingExists(meeting))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SCT_spUpdateMeeting";
                    cmd.Connection = cn;
                    cn.Open();                  
                    cmd.Parameters.AddWithValue("@strRecap", meeting.Recap);
                    cmd.Parameters.AddWithValue("@DateTime", meeting.MeetingDate);
                    cmd.Parameters.AddWithValue("@MeetingHr", meeting.MeetingHr);
                    cmd.Parameters.AddWithValue("@Meetingmin", meeting.MeetingMin);
                    cmd.Parameters.AddWithValue("@MeetingAmPm", meeting.MeetingAmPm);
                    cmd.Parameters.AddWithValue("@strDuration", meeting.Duration);
                    cmd.Parameters.AddWithValue("@intMediaId", meeting.MediaId);
                    cmd.Parameters.AddWithValue("@strSubject", meeting.Subject);
                    cmd.Parameters.AddWithValue("@intCalledBy", meeting.CalledBy);
                    cmd.Parameters.AddWithValue("@intMeetingID", meeting.MeetingId);
                    cmd.Parameters.AddWithValue("@intStatusId", meeting.MeetingStatusId); 
                    cmd.Parameters.AddWithValue("@strTo", meeting.To);
                    cmd.Parameters.AddWithValue("@strCc", meeting.Cc);
                    cmd.Parameters.AddWithValue("@strAttachment", meeting.Attachment);
                    cmd.ExecuteNonQuery();                    
                }
                return true;
            }
            catch (Exception ee)
            {
                return false;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }

        }
        
        public bool InsertMeeting(Meeting meeting)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            Meeting ms = new Meeting();
            try
            {                
                if (!IsMeetingExists(meeting))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SCT_spInsertMeeting";
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.Parameters.AddWithValue("@pProjectId", meeting.ProjectId);
                    cmd.Parameters.AddWithValue("@pintHrs", meeting.MeetingHr);
                    cmd.Parameters.AddWithValue("@pstrRecap", meeting.Recap);
                    cmd.Parameters.AddWithValue("@pstrAmPm", meeting.MeetingAmPm);
                    cmd.Parameters.AddWithValue("@pstrDuration", meeting.Duration);
                    cmd.Parameters.AddWithValue("@pintMediaId", meeting.MediaId);
                    cmd.Parameters.AddWithValue("@pstrSubject", meeting.Subject);
                    cmd.Parameters.AddWithValue("@pintCalledBy", meeting.CalledBy);
                    cmd.Parameters.AddWithValue("@pintStatusId", meeting.MeetingStatusId); 
                    cmd.Parameters.AddWithValue("@pMeetingId", meeting.MeetingId);
                    cmd.Parameters.AddWithValue("@pMeetingDate", Convert.ToDateTime(meeting.MeetingDate));
                    cmd.Parameters.AddWithValue("@pintMins", meeting.MeetingMin);
                    cmd.Parameters.AddWithValue("@pToMembers", meeting.To);
                    cmd.Parameters.AddWithValue("@pCCMembers", meeting.Cc);
                    cmd.Parameters.AddWithValue("@pAttachement", meeting.Attachment);
                    cmd.ExecuteNonQuery();                    
                }
                return true;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }
       

        public bool DeleteMembers(Meeting meeting)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            Meeting ms = new Meeting();
            int res;
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spDeleteMembers";
                cmd.Connection = cn;
                cn.Open();

                //@pMeetingId int  
                cmd.Parameters.AddWithValue("@pMeetingId", meeting.MeetingId);
                cmd.ExecuteNonQuery();
                return true;
                //pp = new SqlParameter();
                //pp.ParameterName = "@ID";
                //pp.DbType = DbType.Int32;
                //pp.Direction = ParameterDirection.Output;
                //cmd.Parameters.AddWithValue("@ID", 0);
                //int temp;
                //temp = cmd.ExecuteNonQuery();
                //res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                //if (res != -1)
                //{
                //    return true;
                //}
                //else
                //{
                //    return false;
                //}
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }
       

        public bool InsertMembers(Meeting meeting)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            try
            {                
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spInsertMembers";
                cmd.Connection = cn;
                cn.Open();
 
                cmd.Parameters.AddWithValue("@pMeetingId", meeting.MeetingId);
                cmd.Parameters.AddWithValue("@pEmpId", meeting.CalledBy);

                return true;
                //pp = new SqlParameter();
                //pp.ParameterName = "@ID";
                //pp.DbType = DbType.Int32;
                //pp.Direction = ParameterDirection.Output;
                //cmd.Parameters.AddWithValue("@ID", 0);
                //int temp;
                //temp = cmd.ExecuteNonQuery();
                //res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                //if (res != -1)
                //{
                //    return true;
                //}
                //else
                //{
                //    return false;
                //}
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }
       

        public DataSet GetMeetingsPerProject(Meeting meeting)
        {
            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "";
            

            strSql += "SELECT A.MeetingId, A.ProjectId, A.CalledBy, A.Recap, Convert(char,nullif(A.DateTime,'1/1/1900'),106) ";
            strSql += "AS MeetingDate, CASE WHEN Meetinghr < 10 THEN '0' + convert(char,MeetingHr) ELSE ";
            strSql += "convert(char,MeetingHr) END + ':' + CASE WHEN MeetingMin < 10 THEN '0' + ";
            strSql += "convert(char, MeetingMin) ELSE convert(char, MeetingMin) END + ' ' + MeetingAMPM  ";
            strSql += " AS MeetingTime, A.Duration, A.MediaId, A.MeetingStatus, CASE A.MeetingStatus WHEN 1 THEN 'Planned' WHEN 2 THEN 'Executed' ELSE 'Cancelled' END AS strMeetingStatus, B.Media, A.subject, ";
            strSql += "C.FirstName + ' ' + C.LastName AS Username FROM SCT_tblProjectMeetings A LEFT JOIN ";
            strSql += "SCT_lkpMeetingMedia B ON A.MediaId = B.MediaId LEFT JOIN SCT_tblEmployees C ON ";
            strSql += "A.CalledBy = C.EmpId WHERE projectID = ";
            strSql += meeting.ProjectId;
            //strSql += " ORDER BY ";
            //strSql += meeting.SortCriteria;
            //strSql += " ";
            //strSql += meeting.SortDir;

            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }
        
        public DataSet GetMeetingStatus()
        {
            SqlDataAdapter adpt;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            adpt = new SqlDataAdapter ("select * from SCT_lkpMeetingStatus",cn);
            ds = new DataSet ();
            adpt.Fill(ds,"SCT_lkpMeetingStatus");
            return ds;
        }
        public DataSet GetMeetingCount(Meeting meeting)
        {

            DataSet DTSet = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "";
            strSql += "SELECT A.MeetingId, A.ProjectId, A.CalledBy, A.Recap, Convert(char,nullif(A.DateTime,\'1/1/1900\'),106) ";
            strSql += "AS MeetingDate, CASE WHEN Meetinghr < 10 THEN \'0\' + convert(char,MeetingHr) ELSE ";
            strSql += "convert(char,MeetingHr) END + \':\' + CASE WHEN MeetingMin < 10 THEN \'0\' + ";
            strSql += "convert(char, MeetingMin) ELSE convert(char, MeetingMin) END + \' \' + MeetingAMPM  ";
            strSql += " AS MeetingTime, A.Duration, A.MediaId, A.MeetingStatus, CASE A.MeetingStatus WHEN 1 THEN \'Planned\' WHEN 2 THEN \'Executed\' ELSE \'Cancelled\' END AS strMeetingStatus, B.Media, A.subject, ";
            strSql += "C.FirstName + \' \' + C.LastName AS Username FROM SCT_tblProjectMeetings A LEFT JOIN ";
            strSql += "SCT_lkpMeetingMedia B ON A.MediaId = B.MediaId LEFT JOIN SCT_tblEmployees C ON ";
            strSql += "A.CalledBy = C.EmpId WHERE projectID = ";
            strSql += meeting.ProjectId;
            strSql += " and A.subject like \'%Kick Off%\' OR A.subject like \'%KickOff\'";
            //strSql+=strSortCriteria)
            //strSql+=" ")
            //strSql+=strSortDir)

            adpt = new SqlDataAdapter(strSql, cn);
            DTSet = new DataSet();
            adpt.Fill(DTSet);
            return DTSet;
        }
      

        public bool GetCalledByName(int intMeetingID)
        {
            string strSql = "";
            strSql = "SELECT LTRIM(RTRIM(A.FirstName + \' \' + A.LastName)) AS [CALLEDBY] FROM SCT_tblProjectMeetings B LEFT JOIN SCT_tblEmployees A ON A.EmpID = B.CalledBy WHERE B.MeetingID = \'" + intMeetingID + "\'";

            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strSql, cn);
            cn.Open();
            bool bVal;
            string strRetVal = "";
            strRetVal = cmd.ExecuteScalar().ToString();
            if (strRetVal != "0")
            {
                bVal = true;
            }
            else
            {
                bVal = false;
            }
            return bVal;
        }

        public bool UpdateMeetingAttachment(Meeting meeting)
        {
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            Meeting ms = new Meeting();
            int res;
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SCT_spUpdateMeetingAttachment";
                cmd.Connection = cn;
                cn.Open();

                //@pMeetingId int  
                cmd.Parameters.AddWithValue("@pMeetingId", meeting.MeetingId);

                pp = new SqlParameter();
                pp.ParameterName = "@ID";
                pp.DbType = DbType.Int32;
                pp.Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@ID", 0);
                int temp;
                temp = cmd.ExecuteNonQuery();
                res = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                if (res != -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }

        public DataSet GetMeetingVenue()
        {
            SqlDataAdapter adpt;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter(" select * from SCT_lkpVenues", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }
         public DataSet GetMediaList()
        {
            SqlDataAdapter adpt;
            DataSet ds;
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter(" select * from SCT_lkpMedia", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string GetMaxMeetingID()
         {
             SqlCommand cmd = new SqlCommand();
             SqlConnection cn = new DataConn().GetConn();
             string temp = string.Empty;
             try
             {
                 cmd.CommandType = CommandType.Text;
                 cmd.Connection = cn;
                 cmd.CommandText = "select max(MeetingId) from SCT_tblProjectMeetings";
                 cn.Open();
                 temp = cmd.ExecuteScalar().ToString();
                 return temp;
             }
             catch (Exception ee)
             {
                 return "0";
             }
             finally
             {
                 if (cn.State == ConnectionState.Open)
                 {
                     cn.Close();
                 }
                 cmd.Dispose();
                 cn.Dispose();
             }
         }
    }
}
