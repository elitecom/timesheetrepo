﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectDAL
{
    public class ProjectNotesDAL
    {
        public ProjectNotes GetNote(ProjectNotes notes)
        {
            ProjectNotes prj = new ProjectNotes();
            SqlDataReader rdr;
            SqlConnection cn = new DataConn().GetConn();
            string strsql;
            strsql = "SELECT A.NoteId, A.ProjectId, A.NoteTypeId, A.Note, A.Reference FROM SCT_tblProjectNotes A WHERE " + "NoteID = " + notes.NoteId;
            SqlCommand cmd;
            Documents ObjDoc = new Documents();
            cmd = new SqlCommand(strsql, cn);
            cn.Open();
            rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                rdr.Read();
                if (Convert.ToInt32(rdr["ProjectId"].ToString()) != null)
                {
                    prj.ProjectId = Convert.ToInt32(rdr["ProjectId"]);
                }
                if (Convert.ToInt32(rdr["NoteTypeId"].ToString()) != null)
                {
                    prj.NoteTypeId = Convert.ToInt32(rdr["NoteTypeId"]);
                }
                if (rdr["Note"].ToString() != null)
                {
                    prj.Note = rdr["note"].ToString();
                }
                if (rdr["reference"].ToString() != null)
                {
                    prj.Reference = rdr["reference"].ToString();
                }
            }
            return prj;
        }
       
        public DataSet GetNoteTypes()
        {
            SqlDataAdapter adpt;
            DataSet ds = new DataSet();
            SqlConnection cn = new DataConn().GetConn();
            try
            {
                adpt = new SqlDataAdapter("SELECT * FROM SCT_lkpNoteTypes ORDER BY NoteType", cn);
                ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message);
            }
        }
       
        public bool UpdateNote(ProjectNotes pn)
        {           
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            bool ans = true;
            try
            {
                if (!IsNoteExists(pn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SCT_spupdateNote";
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.Parameters.AddWithValue("@pnotetypeId", pn.NoteTypeId);
                    cmd.Parameters.AddWithValue("@pnote", pn.Note);
                    cmd.Parameters.AddWithValue("@preference", pn.Reference);
                    cmd.Parameters.AddWithValue("@pLastModifiedOn", pn.LastModifiedOn.ToShortDateString());
                    cmd.Parameters.AddWithValue("@pLastModifiedBy", pn.LastModifiedBy);
                    cmd.Parameters.AddWithValue("@pnoteID", pn.NoteId);
                    pp = new SqlParameter();
                    pp.ParameterName = "@ID";
                    pp.DbType = DbType.Int32;
                    pp.Direction = ParameterDirection.Output;

                    cmd.Parameters.AddWithValue("@ID", 0);
                    int temp;
                    temp = cmd.ExecuteNonQuery();

                    res = Convert.ToInt32(cmd.Parameters["@ID"].Value);

                    if (res != -1)
                    {
                        return true;
                    }
                    else
                    {
                        string strRetVal;
                        strRetVal = "Note already exists, could not be saved!";
                        ans = false;
                    }
                }
                return ans;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }
       

        private string SetStrRight(string pstr)
        {
            string strRetVal;
            strRetVal = pstr.Replace("'", "''");
            return strRetVal;

        }
      
        private bool IsNoteExists(ProjectNotes notes)
        {
            string strsql = "";
            string strRetVal;
            bool bVal;
            if (notes.NoteId == 0)
            {
                strsql += "SELECT COUNT(*) FROM SCT_tblProjectNotes WHERE Note = '";
                strsql += SetStrRight(notes.Note);
                strsql += "' AND notetypeId = ";
                strsql += notes.NoteTypeId;
                strsql += " AND projectid = ";
                strsql += notes.ProjectId;
            }
            else
            {
                strsql += "SELECT COUNT(*) FROM SCT_tblProjectnotes WHERE Note = '";
                strsql += SetStrRight(notes.Note);
                strsql += "' AND NoteTypeId = ";
                strsql += notes.NoteTypeId;
                strsql += " AND projectid = ";
                strsql += notes.ProjectId;
                strsql += " AND noteid <> ";
                strsql += notes.NoteId;
            }
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand(strsql, cn);
            cn.Open();
            strRetVal = (cmd.ExecuteScalar().ToString());
            if (strRetVal != "0")
            {
                bVal = true;
            }
            else
            {
                bVal = false;
            }
            return bVal;
        }
       
        public bool InsertNote(ProjectNotes notes)
        {
            ProjectNotes pn = new ProjectNotes();
            SqlConnection cn = new DataConn().GetConn();
            SqlCommand cmd = new SqlCommand();
            SqlParameter pp = new SqlParameter();
            int res;
            bool ans = true;
            try
            {
                if (!IsNoteExists(notes))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SCT_spInsertNote";
                    cmd.Connection = cn;
                    cn.Open();

                    cmd.Parameters.AddWithValue("@ProjectId", notes.ProjectId);
                    cmd.Parameters.AddWithValue("@NoteType", notes.NoteTypeId);
                    cmd.Parameters.AddWithValue("@Note", notes.Note);
                    cmd.Parameters.AddWithValue("@Reference", notes.Reference);
                    cmd.Parameters.AddWithValue("@PostedBy", notes.PostedBy);
                    cmd.Parameters.AddWithValue("@PostedOn", notes.PostedOn);

                    res = cmd.ExecuteNonQuery();
                    
                    if (res != -1)
                    {
                        return true;
                    }
                    else
                    {
                        string strRetVal;
                        strRetVal = "Note already exists, could not be saved!";
                        ans = false;
                    }
                }
                return ans;
            }
            catch (Exception ee)
            {
                throw new ArgumentException(ee.Message + "\n" + ee.StackTrace);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
                cn.Dispose();
            }
        }
        
        public DataSet GetNotesPerProject(ProjectNotes notes)
        {

            DataSet DTS = new DataSet();
            SqlDataAdapter adpt;
            SqlConnection cn = new DataConn().GetConn();
            string strSql = "";
            if (notes.SortCriteria != "")
            {
                strSql += "SELECT A.NoteId,A.NoteTypeId, A.Note, A.Reference, B.NoteType, ";
                strSql += "convert(char,nullif(A.PostedOn,'1/1/1900'),106) AS Datepostedon, ";
                strSql += "convert(char,nullif(A.LastModifiedOn,'1/1/1900'),106) AS DateLastModifiedon FROM ";
                strSql += "SCT_tblProjectNotes A LEFT JOIN SCT_lkpNoteTypes B ON A.NoteTypeId = B.NoteTypeId ";
                strSql += "WHERE ProjectID = ";
                strSql += notes.ProjectId;
                //strSql += " ORDER BY ";
                //strSql += notes.SortCriteria;
                //strSql += " ";
                //strSql += notes.SortDir;
            }
            else
            {
                strSql += "SELECT A.NoteId,A.NoteTypeId, A.Note, A.Reference, B.NoteType, ";
                strSql += "convert(char,nullif(A.PostedOn,'1/1/1900'),106) AS postedon, ";
                strSql += "convert(char,nullif(A.LastModifiedOn,'1/1/1900'),106) AS LastModifiedon ";
                strSql += "FROM SCT_tblProjectNotes A LEFT JOIN SCT_lkpNoteTypes B ON A.NoteTypeId = B.NoteTypeId ";
                strSql += "WHERE ProjectID = ";
                strSql += notes.ProjectId;
                //strSql += " ORDER BY B.NoteType";
            }

            adpt = new SqlDataAdapter(strSql, new DataConn().GetConn());
            DTS = new DataSet();
            adpt.Fill(DTS);
            return DTS;
        }

        public bool IfAccessible(int NoteID, int ProjectID, string Note)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new DataConn().GetConn();
            int res;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from Sct_tblProjectNotes where projectid=" + ProjectID;
                cmd.Connection = cn;
                cn.Open();
                res = Convert.ToInt32(cmd.ExecuteScalar());
                if (res != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                cmd.Dispose();
            }
        }
    }
}
