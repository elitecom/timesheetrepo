﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using System.Data;
using System.Data.SqlClient;

namespace DrishProjectBAL
{
    public class ForumBAL
    {
        public int GetNextTopicId(int res)
        {
            return new DrishProjectDAL.ForumDAL().GetNextTopicId(res);
        }
        public int GetNextReplyId(int res)
        {
            return new DrishProjectDAL.ForumDAL().GetNextReplyId(res);
        }

        public Forum GetTopics(int id)
        {
            return new DrishProjectDAL.ForumDAL().GetTopics(id);
        }

        public int GetNextReplyId()
        {
            return new DrishProjectDAL.ForumDAL().GetNextReplyId();
        }
        public bool bln_ToSelectTopic(int intTopicId)
        {
            return new DrishProjectDAL.ForumDAL().bln_ToSelectTopic(intTopicId);
        }
        public string InsertReply(Forum forum)
        {
            return new DrishProjectDAL.ForumDAL().InsertReply(forum);
        }
        public Forum GetTopicInfo(int intTopicId)
        {
            return new DrishProjectDAL.ForumDAL().GetTopicInfo(intTopicId);
        }
        public DataSet GetReplies(int topicId)
        {
            return new DrishProjectDAL.ForumDAL().GetReplies(topicId);
        }
        public string GetReplyDocName(int intReplyId)
        {
            return new DrishProjectDAL.ForumDAL().GetReplyDocName(intReplyId);
        }
        public string DeleteReply(int intReplyId)
        {
            return new DrishProjectDAL.ForumDAL().DeleteReply(intReplyId);
        }
    }
}
