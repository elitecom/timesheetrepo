﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectDAL;

namespace DrishProjectBAL
{
    public class ProjectNotesBAL
    {
        public ProjectNotes GetNote(ProjectNotes notes)
        {
            return new ProjectNotesDAL().GetNote(notes);
        }
        public DataSet GetNoteTypes()
        {
            return new ProjectNotesDAL().GetNoteTypes();
        }
        public bool UpdateNote(ProjectNotes pn)
        {
            return new ProjectNotesDAL().UpdateNote(pn);
        }
        public bool InsertNote(ProjectNotes notes)
        {
            return new ProjectNotesDAL().InsertNote(notes);
        }
        public DataSet GetNotesPerProject(ProjectNotes notes)
        {
            return new ProjectNotesDAL().GetNotesPerProject(notes);
        }
        public bool IfAccessible(int NoteID, int ProjectID, string Note)
        {
            return new ProjectNotesDAL().IfAccessible(NoteID, ProjectID, Note);
        }
    }
}
