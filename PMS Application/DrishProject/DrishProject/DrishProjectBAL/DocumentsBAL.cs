﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectDAL;

namespace DrishProjectBAL
{
    public class DocumentsBAL
    {
        public string GetFolderPath(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().GetFolderPath(doc);
        }
        public void GetDoc(Documents doc)
        {
            new DrishProjectDAL.DocumentsDAL().GetDoc(doc);
        }
        public DataSet GetDocTypes(int ProjectID)
        {
            return new DrishProjectDAL.DocumentsDAL().GetDocTypes(ProjectID);
        }
        public DataSet GetDocType(string txtFolderPath)
        {
            return new DrishProjectDAL.DocumentsDAL().GetDocType(txtFolderPath);
        }
        public bool UpdateDoc(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().UpdateDoc(doc);
        }        
        public bool IfFilesExists(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().IfFilesExists(doc);
        }
        public bool InsertDoc(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().InsertDoc(doc);
        }
        public bool InsertDocComment(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().InsertDocComment(doc);
        }
        public bool IfFolderPathExists(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().IfFolderPathExists(doc);
        }
        public bool IsdocCommentExists(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().IsdocCommentExists(doc);
        }
        public bool GetComments(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().GetComments(doc);
        }
        public DataSet GetDocsPerProject(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().GetDocsPerProject(doc);
        }
        public DataSet GetFolderList(int intProjectID)
        {
            return new DrishProjectDAL.DocumentsDAL().GetFolderList(intProjectID);
        }
        public DataSet GetFolderList(string txtDocName, int intProjectID)
        {
            return new DrishProjectDAL.DocumentsDAL().GetFolderList(txtDocName, intProjectID);
        }
        public bool DeleteDoc(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().DeleteDoc(doc);
        }
        public string GetDocName(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().GetDocName(doc);
        }
        public int GetNextId()
        {
            return new DrishProjectDAL.DocumentsDAL().GetNextId();
        }
        public DataSet GetDocsListPerProjectLevel(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().GetDocsListPerProjectLevel(doc);
        }
        public DataSet GetDocsList(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().GetDocsList(doc);
        }
        public int getDocTypeID(string FolderName)
        {
            return new DrishProjectDAL.DocumentsDAL().getDocTypeID(FolderName);
        }
        public bool IsDocExists_Sync(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().IsDocExists_Sync(doc);
        }
        public bool DeleteFolder(Documents doc)
        {
            return new DrishProjectDAL.DocumentsDAL().DeleteFolder(doc);
        }
        public DataSet GetDocumentTypes()
        {
            return new DocumentsDAL().GetDocumentTypes();
        }
    }
}
