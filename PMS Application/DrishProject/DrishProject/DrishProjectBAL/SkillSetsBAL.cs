﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Entities;
using DrishProjectDAL;
using System.Collections;


namespace DrishProjectBAL
{
    public class SkillSetsBAL
    {
        public DataSet GetQuarter()
        {
            return new SkillSetsDAL().GetQuarter();
        }
        public DataSet GetDropDownValue()
        {
            return new SkillSetsDAL().GetDropDownValue();
        }
        public DataSet GetYear()
        {
            return new SkillSetsDAL().GetYear();
        }

        public Hashtable GetSkillSetTechValue()
        {
            return new SkillSetsDAL().GetSkillSetTechValue();
        }

        public Hashtable GetSkillSetGeneralValue()
        {
            return new SkillSetsDAL().GetSkillSetGeneralValue();
        }

        public bool InsertTblSkillset(SkillSet skillset)
        {
            return new SkillSetsDAL().InsertTblSkillset(skillset);
        }
    }
}
