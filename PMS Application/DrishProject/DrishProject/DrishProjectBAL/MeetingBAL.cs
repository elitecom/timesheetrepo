﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectDAL;

namespace DrishProjectBAL
{
    public class MeetingBAL
    {
        public Meeting GetMeeting(Meeting meeting)
        {
            return new DrishProjectDAL.MeetingDAL().GetMeeting(meeting);
        }
        public DataSet GetEmpList(Meeting meeting)
        {
            return new DrishProjectDAL.MeetingDAL().GetEmpList(meeting);
        }
        public DataSet GetMeetingId(int intProjectId)
        {
            return new DrishProjectDAL.MeetingDAL().GetMeetingId(intProjectId);
        }
        public DataSet GetMemberList(Meeting meeting)
        {
            return new DrishProjectDAL.MeetingDAL().GetMemberList(meeting);
        }
        public DataSet GetSelectedClientList(Meeting meeting)
        {
            return new DrishProjectDAL.MeetingDAL().GetSelectedClientList(meeting);
        }
        public DataSet GetClientList(Meeting meeting)
        {
            return new DrishProjectDAL.MeetingDAL().GetClientList(meeting);
        }
        //public DataSet GetMediaList()
        //{
        //    return new DrishProjectDAL.MeetingDAL().GetMediaList();
        //}
        public bool DeleteMeeting(Meeting meeting)
        {
            return new DrishProjectDAL.MeetingDAL().DeleteMeeting(meeting);
        }
        public bool UpdateMeeting(Meeting meeting)
        {
            return new DrishProjectDAL.MeetingDAL().UpdateMeeting(meeting);
        }
        public bool InsertMeeting(Meeting meeting)
        {
            return new DrishProjectDAL.MeetingDAL().InsertMeeting(meeting);
        }
        public bool DeleteMembers(Meeting meeting)
        {
            return new DrishProjectDAL.MeetingDAL().DeleteMembers(meeting);
        }
        public bool InsertMembers(Meeting meeting)
        {
            return new DrishProjectDAL.MeetingDAL().InsertMembers(meeting);
        }
        public DataSet GetMeetingsPerProject(Meeting meeting)
        {
            return new DrishProjectDAL.MeetingDAL().GetMeetingsPerProject(meeting);
        }
        public DataSet GetMeetingCount(Meeting meeting)
        {
            return new DrishProjectDAL.MeetingDAL().GetMeetingCount(meeting);
        }
        public bool GetCalledByName(int intMeetingID)
        {
            return new DrishProjectDAL.MeetingDAL().GetCalledByName(intMeetingID);
        }
        public bool UpdateMeetingAttachment(Meeting meeting)
        {
            return new DrishProjectDAL.MeetingDAL().UpdateMeetingAttachment(meeting);
        }
        public DataSet GetMeetingVenue()
        {
            return new DrishProjectDAL.MeetingDAL().GetMeetingVenue();
        }
        public DataSet GetMediaList()
        {
            return new DrishProjectDAL.MeetingDAL().GetMediaList();
        }
        public DataSet GetMemberListByMeetingID(int meetingID)
        {
            return new DrishProjectDAL.MeetingDAL().GetMemberListByMeetingID(meetingID);
        }
        public DataSet GetMeetingStatus()
        {
            return new DrishProjectDAL.MeetingDAL().GetMeetingStatus();
        }
        public string GetMaxMeetingID()
        {
            return new DrishProjectDAL.MeetingDAL().GetMaxMeetingID();
        }
    }
}
