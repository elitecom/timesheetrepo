﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrishProjectBAL
{
    public class RBACScreenBAL
    {
        public bool IsValidRole(string screenname)
        {
            return new DrishProjectDAL.RBACScreenDAL().IsValidRole(screenname);
        }
    }
}
