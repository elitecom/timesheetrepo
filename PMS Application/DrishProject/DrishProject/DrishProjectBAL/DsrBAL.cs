﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectBAL
{
    public class DsrBAL
    {
        public bool SaveSentDSRContents(DSR dsr)
        {
            return new DrishProjectDAL.DsrDAL().SaveSentDSRContents(dsr);
        }
        public bool SaveDSRContents(DSR dsr)
        {
            return new DrishProjectDAL.DsrDAL().SaveDSRContents(dsr);
        }
        public DataSet GetDSRContentsForDate(DSR dsr)
        {
            return new DrishProjectDAL.DsrDAL().GetDSRContentsForDate(dsr);
        }
        public bool SendFinalDSR(DSR dsr)
        {
            return new DrishProjectDAL.DsrDAL().SendFinalDSR(dsr);
        }
        public DataSet GetMonthSummaryV2(int EmpID, int tMonth, int tYear)
        {
            return new DrishProjectDAL.DsrDAL().GetMonthSummaryV2(EmpID, tMonth, tYear);
        }
        public DataSet GetOldDSR(int EmpID, DateTime dDate)
        { 
            return new DrishProjectDAL.DsrDAL().GetOldDSR(EmpID, dDate);
        }
        public DataSet getPreviousDates()
        {
            return new DrishProjectDAL.DsrDAL().getPreviousDates();
        }
        public string GetPreviousTime(int EmpId, int ProjectId, DateTime FromDate)
        {
            return new DrishProjectDAL.DsrDAL().GetPreviousTime(EmpId, ProjectId, FromDate);
        }
        public decimal GetPreviousTotal(int EmpId, DateTime FromDate)
        { 
            return new DrishProjectDAL.DsrDAL().GetPreviousTotal(EmpId,FromDate);
        }
        public string GetProjectCode(int projectid)
        { 
            return new DrishProjectDAL.DsrDAL().GetProjectCode(projectid); 
        }
        public Project GetProjectInfo(int ProjectID, DateTime FromDate)
        { 
            return new DrishProjectDAL.DsrDAL().GetProjectInfo(ProjectID,FromDate);
        }
        public string getProjectStage(int ProjectID)
        { 
            return new DrishProjectDAL.DsrDAL().getProjectStage(ProjectID);
        }
        public DataSet GetProjectSummary(int EmpID, DateTime FromDate, DateTime ToDate)
        { 
            return new DrishProjectDAL.DsrDAL().GetProjectSummary(EmpID, FromDate,ToDate);
        }
        public DataSet GetProjectSummaryV2(int EmpID)
        { 
            return new DrishProjectDAL.DsrDAL().GetProjectSummaryV2(EmpID); 
        }
        public DataSet GetProjectSummaryV2(int EmpID, DateTime FromDate, DateTime ToDate)
        { 
            return new DrishProjectDAL.DsrDAL().GetProjectSummaryV2(EmpID, FromDate,ToDate);
        }
        public DSR GetProjTimeSheet(int ProjectId)
        { 
            return new DrishProjectDAL.DsrDAL().GetProjTimeSheet(ProjectId);
        }
        public DSR GetProjTimeSheetV2(int ProjectId)
        { 
            return new DrishProjectDAL.DsrDAL().GetProjTimeSheetV2(ProjectId);
        }
        public int getQANum(int ProjectID, DateTime FromDate, DateTime Todate)
        { 
            return new DrishProjectDAL.DsrDAL().getQANum(ProjectID,FromDate, Todate);
        }
        public DataSet getReleaseCodes(int ProjectID, DateTime FromDate, DateTime Todate)
        { 
            return new DrishProjectDAL.DsrDAL().getReleaseCodes(ProjectID,FromDate, Todate);
        }
        public DataSet GetResourceHours(int ProjectID, int EmpId, DateTime FromDate, DateTime Todate)
        { 
            return new DrishProjectDAL.DsrDAL().GetResourceHours(ProjectID, EmpId, FromDate, Todate); 
        }
        public int GetSameProject(int projectid, int moduleid, int activityid, int EmpID, DateTime dDate)
        { 
            return new DrishProjectDAL.DsrDAL().GetSameProject(projectid, moduleid, activityid, EmpID, dDate);
        }
        public DataSet GetSearchedTimesheets(int tYear, int tMonth, string ProjectCode)
        { 
            return new DrishProjectDAL.DsrDAL().GetSearchedTimesheets(tYear, tMonth, ProjectCode);
        }
        public DataSet GetSearchedTimesheets(int EmpID, int tYear, int tMonth, string ProjectCode)
        { 
           return new DrishProjectDAL.DsrDAL().GetSearchedTimesheets(EmpID, tYear, tMonth, ProjectCode);
        }
        public DataSet GetSearchedTimesheetsV2(int tYear, int tMonth, string ProjectCode)
        { 
            return new DrishProjectDAL.DsrDAL().GetSearchedTimesheetsV2(tYear, tMonth, ProjectCode);
        }
        public DataSet GetSearchedTimesheetsV2(int EmpID, int tYear, int tMonth, string ProjectCode)
        { 
            return new DrishProjectDAL.DsrDAL().GetSearchedTimesheetsV2(EmpID, tYear, tMonth, ProjectCode);
        }
        public DataSet GetSearchedTimesheetV2(int tYear, int tMonth, string ProjectCode)
        { 
            return new DrishProjectDAL.DsrDAL().GetSearchedTimesheetV2(tYear, tMonth, ProjectCode);
        }
        public DataSet GetSearchedTimesheetV2(int EmpID, int tYear, int tMonth, string ProjectCode)
        { 
            return new DrishProjectDAL.DsrDAL().GetSearchedTimesheetV2(EmpID, tYear, tMonth, ProjectCode);
        }
        public string getTeamLead(int ProjectID)
        { 
            return new DrishProjectDAL.DsrDAL().getTeamLead(ProjectID); 
        }
        public DataSet GetTimesheetEmployeeSummary(int ProjectID, int tMonth, int tYear)
        { 
            return new DrishProjectDAL.DsrDAL().GetTimesheetEmployeeSummary(ProjectID, tMonth, tYear);
        }
        public DataSet GetTimesheetEmployeeSummaryV2(int ProjectID, int tMonth, int tYear)
        { 
            return new DrishProjectDAL.DsrDAL().GetTimesheetEmployeeSummaryV2(ProjectID,  tMonth, tYear);
        }
        public DataSet GetTimesheets()
        {
            return new DrishProjectDAL.DsrDAL().GetTimesheets(); 
        }
        public DataSet GetTimesheets(int EmpID)
        { 
            return new DrishProjectDAL.DsrDAL().GetTimesheets(EmpID);
        }
        public DataSet GetTimesheetSummary()
        {
            return new DrishProjectDAL.DsrDAL().GetTimesheetSummary(); 
        }
        public DataSet GetTimesheetSummary(int EmpID)
        {
            return new DrishProjectDAL.DsrDAL().GetTimesheetSummary(EmpID);
        }
        public DataSet GetTimesheetSummaryV2()
        {
            return new DrishProjectDAL.DsrDAL().GetTimesheetSummaryV2();
        }
        public DataSet GetTimesheetSummaryV2(int EmpID)
        { 
            return new DrishProjectDAL.DsrDAL().GetTimesheetSummaryV2(EmpID);
        }
        public DataSet GetTimesheetV2()
        {
            return new DrishProjectDAL.DsrDAL().GetTimesheetV2();
        }
        public DataSet GetTimesheetV2(int EmpID)
        { 
            return new DrishProjectDAL.DsrDAL().GetTimesheetV2(EmpID);
        }
        public decimal GetTimeSpent(int pintProjectId, int pintModuleId, int pintEmpId)
        { 
            return new DrishProjectDAL.DsrDAL().GetTimeSpent(pintProjectId, pintModuleId, pintEmpId);
        }
        public DataSet GetTodayDSR(int EmpID, DateTime dDate)
        { 
            return new DrishProjectDAL.DsrDAL().GetTodayDSR(EmpID, dDate); 
        }
        public DataSet GetTodayDSRHours(int EmpID, DateTime dDate)
        { 
            return new DrishProjectDAL.DsrDAL().GetTodayDSR( EmpID,  dDate);
        }
        public decimal getTotalHours(int EmpId, DateTime FromDate, DateTime Todate)
        {
            return new DrishProjectDAL.DsrDAL().getTotalHours(EmpId, FromDate, Todate);
        }
        public string GetTotalProjectHrs(int ProjectID, int tMonth, int tYear)
        { 
            return new DrishProjectDAL.DsrDAL().GetTotalProjectHrs(ProjectID, tMonth, tYear);
        }
        public DataSet GetValuesToEdit(int DSRHistID)
        { 
            return new DrishProjectDAL.DsrDAL().GetValuesToEdit(DSRHistID);
        }
        public bool InsertMonth(int EmpID)
        { 
            return new DrishProjectDAL.DsrDAL().InsertMonth(EmpID);
        }
        public bool InsertMonthV2(int EmpID)
        { 
            return new DrishProjectDAL.DsrDAL().InsertMonthV2(EmpID); 
        }
        public bool saveDSR(int EmpID, int ProjectID, int ModuleID, double HrsSpent, string workdone, DateTime dDate, DateTime dtModifiedOn)
        { 
            return new DrishProjectDAL.DsrDAL().saveDSR(EmpID, ProjectID, ModuleID, HrsSpent, workdone, dDate, dtModifiedOn); 
        }   
        public string saveDSRV2(int EmpID, int ProjectID, int ModuleID, int ActivityID, double HrsSpent, string workdone, DateTime dDate, DateTime dtModifiedOn)
        { 
            return new DrishProjectDAL.DsrDAL().saveDSRV2(EmpID, ProjectID, ModuleID, ActivityID, HrsSpent, workdone, dDate, dtModifiedOn); 
        }
        public bool ToCheckIfDSRSentV2(int EmpID, DateTime dDate)
        { 
            return new DrishProjectDAL.DsrDAL().ToCheckIfDSRSentV2(EmpID, dDate);
        }
        public int update(DateTime dDate, int empID, int DSRSent)
        { 
            return new DrishProjectDAL.DsrDAL().update(dDate, empID, DSRSent);
        }
        public int UpdateDSRGrid(int ProjectID, int ModuleID, int ActivityID, double HrsSpent, string workdone, DateTime dtModifiedOn, int DSRHistID)
        { 
            return new DrishProjectDAL.DsrDAL().UpdateDSRGrid(ProjectID, ModuleID, ActivityID, HrsSpent, workdone, dtModifiedOn, DSRHistID);
        }
        public bool UpdateIfDSRSent(int EmpID, DateTime tDate)
        { 
            return new DrishProjectDAL.DsrDAL().UpdateIfDSRSent(EmpID, tDate);
        }
        public int UpdateIFSendDSR(DateTime dDate, int empID)
        { 
            return new DrishProjectDAL.DsrDAL().UpdateIFSendDSR(dDate, empID);
        }
        public bool UpdateTimeAllocated(int ProjectID, int EmpId, DateTime FromDate, string Time)
        { 
            return new DrishProjectDAL.DsrDAL().UpdateTimeAllocated(ProjectID, EmpId, FromDate, Time);
        }
        public bool checkIfPresentNew(int EmpID, DateTime date)
        {
            return new DrishProjectDAL.DsrDAL().checkIfPresentNew(EmpID, date);
        }
        public bool checkIfPresent(int EmployeeId, DateTime date)
        {
            return new DrishProjectDAL.DsrDAL().checkIfPresent(EmployeeId, date);
        }
        public bool checkIfDSRSentNew(int EmpID,DateTime date)
        {
            return new DrishProjectDAL.DsrDAL().checkIfDSRSentNew(EmpID, date);
        }    
        public DataSet GetActivities()
        {
            return new DrishProjectDAL.DsrDAL().GetActivities();
        }
        public DataSet getDSNewModule(int projectID)
        {
            return new DrishProjectDAL.DsrDAL().getDSNewModule(projectID);
        }
        public int DeleteDSRNew(int dsrId)
        {
            return new DrishProjectDAL.DsrDAL().DeleteDSRNew(dsrId);
        }
        public DataSet GetCompleteDSR(int UserID, DateTime ClndMonth)
        {
            return new DrishProjectDAL.DsrDAL().GetCompleteDSR(UserID, ClndMonth);
        }
        public DataSet  GetModulesToEdit(string ProjName)
        {
            return new DrishProjectDAL.DsrDAL().GetModulesToEdit(ProjName);
        }
        public DataSet getDSRV2(int UserID, DateTime ClndMonth)
        {
            return new DrishProjectDAL.DsrDAL().getDSRV2(UserID, ClndMonth);
        }
        public DataSet getDSRNewModule(int iProjectID)
        {
            return new DrishProjectDAL.DsrDAL().getDSRNewModule(iProjectID);
        }
        public DataSet GetDSRMonths(int empId)
        {
            return new DrishProjectDAL.DsrDAL().GetDSRMonths(empId);
        }
        public DataSet GetDSRModule(int employeeId,DateTime selectedDate, int projectid)
        {
            return new DrishProjectDAL.DsrDAL().GetDSRModule(employeeId,selectedDate,projectid);
        }
        public bool DeleteDSR(int employeeId, int projectid, int moduleid, DateTime selectedDate)
        {
            return new DrishProjectDAL.DsrDAL().DeleteDSR(employeeId, projectid, moduleid, selectedDate);
        }
        public DataSet getDSRModuleOld(int employeeId, DateTime selectedDate, int projectid)
        {
            return new DrishProjectDAL.DsrDAL().getDSRModuleOld(employeeId, selectedDate, projectid);
        }
        public DataSet getDSRModule(int employeeId, DateTime selectedDate, int projectid)
        {
            return new DrishProjectDAL.DsrDAL().getDSRModule(employeeId, selectedDate, projectid);
        }
        public DataSet getDSR(int employeeId, DateTime selectedDate)
        {
            return new DrishProjectDAL.DsrDAL().getDSR(employeeId, selectedDate);
        }
        public DataSet getDSROldAvailable(int employeeId, DateTime selectedDate)
        {
            return new DrishProjectDAL.DsrDAL().getDSROldAvailable(employeeId, selectedDate);
        }
        //public bool UpdateIfDSRSent(int employeeId, DateTime time)
        //{
        //    return new DrishProjectDAL.DsrDAL().UpdateIfDSRSent(employeeId, time);
        //}
    }
}
