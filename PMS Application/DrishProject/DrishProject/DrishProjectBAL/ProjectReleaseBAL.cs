﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DrishProjectDAL;
using Entities;

namespace DrishProjectBAL
{
    public class ProjectReleaseBAL
    {
        public DataSet GetReleaseStatus()
        {
            return new DrishProjectDAL.ProjectReleaseDAL().GetReleaseStatus();
        }
        public ProjectRelease GetRelease(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().GetRelease(release);
        }
        public string SetStrRight(string pstr)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().SetStrRight(pstr);
        }
        public bool IsReleaseExists(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().IsReleaseExists(release);
        }
        public bool UpdateRelease(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().UpdateRelease(release);
        }
        public bool InsertRelease(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().InsertRelease(release);
        }
        public DataSet GetFeatures(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().GetFeatures(release);
        }
        public DataSet GetReleaseFeatures(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().GetReleaseFeatures(release);
        }
        public DataSet GetReleaseFeaturesDelivered(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().GetReleaseFeaturesDelivered(release);
        }
        public SqlDataReader GetPlannedFeatures(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().GetPlannedFeatures(release);
        }
        public DataSet GetReleasesPerProject(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().GetReleasesPerProject(release);
        }
        public DataSet GetLatestRelease(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().GetLatestRelease(release);
        }
        public DataSet AddTasktoFeatures(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().AddTasktoFeatures(release);
        }
        public bool CleanFeatures(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().CleanFeatures(release);
        }
        public bool InsertFeatures(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().InsertRelease(release);
        }
        public DataSet GetReleaseDate(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().GetReleaseDate(release);
        }
        public DataSet GetReleaseDateV2(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().GetReleaseDateV2(release);
        }
        public DataSet GetNoOfReleaseFeatures(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().GetNoOfReleaseFeatures(release);
        }
        public DataSet GetNoOfCompletedFeatures(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().GetNoOfCompletedFeatures(release);
        }
        public DataSet GetReleaseHistory(ProjectRelease release)
        {
            return new DrishProjectDAL.ProjectReleaseDAL().GetReleaseHistory(release);
        }
    }
}
