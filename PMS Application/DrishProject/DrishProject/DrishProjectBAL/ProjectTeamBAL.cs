﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Entities;
using DrishProjectDAL;

namespace DrishProjectBAL
{
    public class ProjectTeamBAL
    {
         public DataSet GetDevListPerProject(ProjectTeam projectTeam)
        {
            return new DrishProjectDAL.ProjectTeamDAL().GetDevListPerProject(projectTeam);
        }
        public DataSet GetEmployeeListPerProject(ProjectTeam projectTeam,string strSortCriteria, string strSortDir)
         {
            return new DrishProjectDAL.ProjectTeamDAL().GetEmployeeListPerProject(projectTeam,strSortCriteria,strSortDir);
         }
        public DataSet GetAllEmployeeList()
        {
            return new DrishProjectDAL.ProjectTeamDAL().GetAllEmployeeList();
        }
        public DataSet GetEmployeeListPerTeamLead(int EmpId)
        {
            return new DrishProjectDAL.ProjectTeamDAL().GetEmployeeListPerTeamLead(EmpId);
        }
        public DataSet GetMemberRolesList()
        {
            return new DrishProjectDAL.ProjectTeamDAL().GetMemberRolesList();
        }
        public DataSet GetSelectedMemberID(ProjectTeam projectTeam)
        {
            return new DrishProjectDAL.ProjectTeamDAL().GetSelectedMemberID(projectTeam);
        }
        public bool MemberExist(ProjectTeam member)
        {
            return new DrishProjectDAL.ProjectTeamDAL().MemberExist(member);
        }
        public bool UpdateTeamMember(ProjectTeam ptr)
        {
            return new DrishProjectDAL.ProjectTeamDAL().UpdateTeamMember(ptr);
        }
        public bool InsertTeamMember(ProjectTeam ptr)
        {
            return new DrishProjectDAL.ProjectTeamDAL().InsertTeamMember(ptr);
        }
         public bool DeleteTeamMember(ProjectTeam ptr)
        {
            return new DrishProjectDAL.ProjectTeamDAL().DeleteTeamMember(ptr);
        }
        public int GetSelectedEmpID(ProjectTeam projectTeam)
        {
            return new DrishProjectDAL.ProjectTeamDAL().GetSelectedEmpID(projectTeam);
        }
        public bool TeamMemberRefExists(int pEmpId, int Projectid)
        {
            return new DrishProjectDAL.ProjectTeamDAL().TeamMemberRefExists(pEmpId, Projectid);
        }
        public bool IsActiveMember(int EmpId, int Projectid)
        {
            return new DrishProjectDAL.ProjectTeamDAL().IsActiveMember(EmpId, Projectid);
        }
        public DataSet GetEmployeeListPerProject(int ProjectId)
        {
            return new DrishProjectDAL.ProjectTeamDAL().GetEmployeeListPerProject(ProjectId);
        }
        public DataSet GetTeamMemberDetails(int ProjectId,int empId)
        {
            return new DrishProjectDAL.ProjectTeamDAL().GetTeamMemberDetails(ProjectId, empId);
        }
    }
}
