﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;
using DrishProjectDAL;

namespace DrishProjectBAL
{
    public class CodeReviewBAL
    {
        public DataSet GetReviewsPerProject(Project project)
        {
            return new DrishProjectDAL.CodeReviewDAL().GetReviewsPerProject(project);
        }
        public bool DeleteReview(CodeReview review)
        {
            return new DrishProjectDAL.CodeReviewDAL().DeleteReview(review);
        }
        public DataSet GetReviewDetail(CodeReview review)
        {
            return new DrishProjectDAL.CodeReviewDAL().GetReviewDetail(review);
        }
        public void GetReview(CodeReview review)
        {
            new DrishProjectDAL.CodeReviewDAL().GetReview(review);
        }
        public bool UpdateReview(CodeReview review)
        {
            return new DrishProjectDAL.CodeReviewDAL().UpdateReview(review);
        }
        public bool InsertReview(CodeReview review)
        {
            return new DrishProjectDAL.CodeReviewDAL().InsertReview(review);
        }
        public DataSet GetReviewDetailsPerReview(CodeReview review)
        {
            return new DrishProjectDAL.CodeReviewDAL().GetReviewDetailsPerReview(review);
        }
        public bool DeleteObservation(CodeReview review)
        {
            return new DrishProjectDAL.CodeReviewDAL().DeleteObservation(review);
        }
        public void GetObservation(int ObservationId)
        {
            new DrishProjectDAL.CodeReviewDAL().GetObservation(ObservationId);
        }
        public bool UpdateObservation(CodeReview review)
        {
            return new DrishProjectDAL.CodeReviewDAL().UpdateObservation(review);
        }
        public bool InsertObservation(CodeReview review)
        {
            return new DrishProjectDAL.CodeReviewDAL().InsertObservation(review);
        }
        public DataSet GetTeam(Project prj)
        {
            return new DrishProjectDAL.CodeReviewDAL().GetTeam(prj);
        }
        public DataSet GetAgendaList()
        {
            return new DrishProjectDAL.CodeReviewDAL().GetAgendaList();
        }
        public DataSet GetSeverityLevels()
        {
            return new DrishProjectDAL.CodeReviewDAL().GetSeverityLevels();
        }
        public DataSet GetAcceptabilityList()
        {
            return new DrishProjectDAL.CodeReviewDAL().GetAcceptabilityList();
        }
    }
}
