﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DrishProjectDAL;
using Entities;

namespace DrishProjectBAL
{
    public class ProjectModuleBAL
    {
        public ProjectModule GetModule(ProjectModule module)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetModule(module);
        }
        public string DeleteModule(ProjectModule module)
        {
            return new DrishProjectDAL.ProjectModuleDAL().DeleteModule(module);
        }
        public string UpdateModule(ProjectModule module)
        {
            return new DrishProjectDAL.ProjectModuleDAL().UpdateModule(module);
        }
        public string InsertModule(ProjectModule module)
        {
            return new DrishProjectDAL.ProjectModuleDAL().InsertModule(module);
        }
        public DataSet GetModulesPerProject(ProjectModule module)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetModulesPerProject(module);
        }
        public DataSet GetModuleStatus()
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetModuleStatus();
        }
        public DataSet GetModules(ProjectModule module)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetModules(module);
        }
        public DataSet GetModulesPerModuleLead(ProjectModule module)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetModulesPerModuleLead(module);
        }
        public DataSet GetHideModuleStatus()
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetHideModuleStatus();
        }
        public DataSet GetSearchModule(ProjectModule module)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetSearchModule(module);
        }
        public DataSet GetModuleNames(int ProjectID)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetModuleNames(ProjectID);
        }
        public DataSet GetModuleHistory(int ProjectID)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetModuleHistory(ProjectID);
        }
        public DataSet GetModulesList(int ProjectID, int moduleId)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetModulesList(ProjectID, moduleId);
        }
        public DataSet GetDistinctModules(int ProjectID)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetDistinctModules(ProjectID);
        }
        public DataSet GetDistinctModules(object ProjectID, object startpage, object pagesize, object tablename)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetDistinctModules(ProjectID, startpage, pagesize, tablename);
        }
        public DataSet GetSelectedModule(int ProjectID, int ModuleID)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetSelectedModule(ProjectID, ModuleID);
        }
        public DataSet GetSelectedModuleHistory(int ProjectID, int ModuleID)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetSelectedModuleHistory(ProjectID, ModuleID);
        }
        public DataSet GetActivities()
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetActivities();
        }
        public DataSet GetTasks(int ProjectID)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetTasks(ProjectID);
        }
        public DataSet GetTasks(object ProjectID, object startpage, object pagesize, object tablename)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetTasks(ProjectID, startpage, pagesize, tablename);
        }
        public DataSet GetSelectedTaskHistory(int ProjectID)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetSelectedTaskHistory(ProjectID);
        }
        public DataSet GetSelectedTask(int ProjectID, int TaskID)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetSelectedTask(ProjectID, TaskID);
        }
        public DataSet GetSelectedTaskHistory(int ProjectID, int TaskID)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetSelectedTaskHistory(ProjectID, TaskID);
        }
        public DataSet GetTaskNames(int ProjectID)
        {
            return new DrishProjectDAL.ProjectModuleTaskDAL().GetTasks(ProjectID);
        }
        public DataSet GetTaskNamesByTaskID(int TaskID)
        {
            return new DrishProjectDAL.ProjectModuleTaskDAL().GetTaskNamesByTaskID(TaskID);
        }
        public DataSet GetProjectTeamMembers(Project project)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetProjectTeamMembers(project);
        }
        public DataSet GetProjectTeamManagers(Project project)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetProjectTeamManagers(project);
        }
        public DataSet GetModuleByProjectId(Project project)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetModuleByProjectId(project);
        }
        public int GetModuleIDByName(string module_name)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetModuleIDByName(module_name);
        }
        public DataSet GetProjectTeamDevelopers(Project project)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetProjectTeamDevelopers(project);
        }
        public DataSet GetProjectTeamQA(Project project)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetProjectTeamQA(project);
        }
        public DataSet GetModuleByProjectForUser(Project project, Employee emp)
        {
            return new DrishProjectDAL.ProjectModuleDAL().GetModuleByProjectForUser(project, emp);
        }
        public DataSet GetModuleByProjectForEmp(Project project, Employee emp, ProjectModule module)
        {
            return new ProjectModuleDAL().GetModuleByProjectForEmp(project, emp, module);
        }
    }
}
