﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Entities;
using DrishProjectDAL;

namespace DrishProjectBAL
{
    public class ClientsBAL
    {
        public bool DeleteContact(Clients client)
        {
            return new ClientsDAL().DeleteContact(client);
        }
        public bool UpdateContact(Clients client)
        {
            return new ClientsDAL().UpdateContact(client);
        }
        public bool IfClientExists(Clients client)
        {
            return new ClientsDAL().IfClientExists(client);
        }
        public bool InsertContact(Clients client)
        {
            return new ClientsDAL().InsertContact(client);
        }
        public DataSet GetContactsPerProject(int intProjectId)
        {
            return new ClientsDAL().GetContactsPerProject(intProjectId);
        }
        public DataSet GetAllClients()
        {
            return new ClientsDAL().GetAllClients();
        }
        public DataSet GetContactsPerClients(Clients client)
        {
            return new ClientsDAL().GetContactsPerClients(client);
        }
        public DataSet GetProjectsPerClient(Clients client)
        {
            return new ClientsDAL().GetProjectsPerClient(client);
        }
        public bool UpdateClientInfo(Clients client)
        {
            return new ClientsDAL().UpdateClientInfo(client);
        }
        public int AddNewClient(Clients client)
        {
            return new ClientsDAL().AddNewClient(client);
        }
        public Clients GetClientDetails(int clientID)
        {
            return new ClientsDAL().GetClientDetails(clientID);
        }
        public DataSet GetCountryList()
        {
            return new ClientsDAL().GetCountryList();
        }
    }
}
