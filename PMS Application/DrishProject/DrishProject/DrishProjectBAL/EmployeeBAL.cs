﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectBAL
{
    public class EmployeeBAL
    {
        public bool IsAdministartor(Employee emp)
        {
            return new DrishProjectDAL.EmployeeDAL().IsAdministrator(emp);
        }
        public DataSet GetDepartments()
        {
            return new DrishProjectDAL.EmployeeDAL().GetDepartments();
        }
        public DataSet GetDesignations()
        {
            return new DrishProjectDAL.EmployeeDAL().GetDesignations();
        }
        public DataSet getdata()
        {
            return new DrishProjectDAL.EmployeeDAL().getdata();
        }
        public DataSet GetReportingManagers()
        {
            return new DrishProjectDAL.EmployeeDAL().GetReportingManagers();
        }
        public DataSet SearchEmployee(int desgId, string name, string dob, string doj)
        {
            return new DrishProjectDAL.EmployeeDAL().SearchEmployee(desgId, name, dob, doj);
        }
        //public DataSet getdetails()
        //{
        //    return new DrishProjectDAL.EmployeeDAL().getdata();
        //}
        public DataSet getdetail()
        {
            return new DrishProjectDAL.EmployeeDAL().getdetail();
        }
        public bool AddEmployee(Employee emp)
        {
            return new DrishProjectDAL.EmployeeDAL().AddEmployee(emp);
        }
        public bool updatedata(Employee emp)
        {
            return new DrishProjectDAL.EmployeeDAL().updatedata(emp);
        }
        public Employee GetProfileDetails(Employee emp)
        {
            return new DrishProjectDAL.EmployeeDAL().GetProfileDetails(emp);
        }
        public Employee GetProfileDetailsByUserName(Employee emp)
        {
            return new DrishProjectDAL.EmployeeDAL().GetProfileDetailsByUserName(emp);
        }
        public DataSet GetProjects()
        {
            return new DrishProjectDAL.EmployeeDAL().GetProjects();
        }
        public bool AuthenticateUser(Employee emp)
        {
            return new DrishProjectDAL.EmployeeDAL().AuthenticateUser(emp);
        }
        public DataSet searchdata(Employee emp)
        {
            return new DrishProjectDAL.EmployeeDAL().searchdata(emp);
        }
        public DataSet ProjectCategory()
        {
            return new DrishProjectDAL.EmployeeDAL().ProjectCategory();
        }
        public Employee SearchEmployeeByCode(string empCode)
        {
            return new DrishProjectDAL.EmployeeDAL().SearchEmployeeByCode(empCode);
        }
        public string GetEmailId(Employee emp)
        {
            return new DrishProjectDAL.EmployeeDAL().GetEmailId(emp);
        }
        public string Encrypt(string clearText)
        {
            return new DrishProjectDAL.EmployeeDAL().Encrypt(clearText);
        }
        public string Decrypt(string cipherText)
        {
            return new DrishProjectDAL.EmployeeDAL().Decrypt(cipherText);
        }
        public string GetUserRole(string username)
        {
            return new DrishProjectDAL.EmployeeDAL().GetUserRole(username);
        }
        public bool IsActiveMember(int EmpId, int Projectid)
        {
            return new DrishProjectDAL.EmployeeDAL().IsActiveMember(EmpId, Projectid);
        }
        public Employee EmployeeDetailForMail(Employee employee)
        {
            return new DrishProjectDAL.EmployeeDAL().EmployeeDetailForMail(employee);
        }
        public int GetEmployeeId(Employee employee)
        {
            return new DrishProjectDAL.EmployeeDAL().GetEmployeeId(employee);
        }
        public DataSet GetAllEmployees()
        {
            return new DrishProjectDAL.EmployeeDAL().GetAllEmployees();
        }
        public DataSet GetAllReportingManagers()
        {
            return new DrishProjectDAL.EmployeeDAL().GetAllReportingManagers();
        }
        public DataSet GetMemberRolesList()
        {
            return new DrishProjectDAL.EmployeeDAL().GetMemberRolesList();
        }
        public bool ChangePassword(Employee emp)
        {
            return new DrishProjectDAL.EmployeeDAL().ChangePassword(emp);
        }
        public DataSet GetEmployeeList()
        {
            return new DrishProjectDAL.EmployeeDAL().GetEmployeeList();
        }
        public DataSet GetEmployeeListPerTeamLead(Employee emp)
        {
            return new DrishProjectDAL.EmployeeDAL().GetEmployeeListPerTeamLead(emp);
        }
        public DataSet GetEmployeeRoles(Employee emp)
        {
            return new DrishProjectDAL.EmployeeDAL().GetEmployeeRoles(emp);
        }
        public bool CheckUserRole(Employee emp, string screen)
        {
            return new DrishProjectDAL.EmployeeDAL().CheckUserRole(emp, screen);
        }
       public DataSet GetDetailedEmployeeList()
        {
            return new DrishProjectDAL.EmployeeDAL().GetDetailedEmployeeList();
        }
        public bool IsClient(Employee emp)
       {
           return new DrishProjectDAL.EmployeeDAL().IsClient(emp);
       }
        public DataSet GetListEmailAllEmployees()
        {
            return new DrishProjectDAL.EmployeeDAL().GetListEmailAllEmployees();
        }
        public DataSet GetEmailListPerProject(int ProjectId)
        {
            return new DrishProjectDAL.EmployeeDAL().GetEmailListPerProject(ProjectId);
        }
    }
}


