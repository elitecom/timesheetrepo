﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DrishProjectDAL;
using Entities;

namespace DrishProjectBAL
{
    public class ProjectBAL
    {
        public DataSet GetProjectDetailsByEmpId(int empid)
        {
            return new ProjectDAL().GetProjectDetailsByEmpId(empid);
        }
        public DataSet GetProjectDetails()
        {
            return new ProjectDAL().GetProjectDetails();
        }
        public DataSet GetProjectCategories()
        {
            return new ProjectDAL().GetProjectCategories();
        }
        public bool AddNewProject( Project prop)
        {
            return new DrishProjectDAL.ProjectDAL().AddNewProject(prop);
        }
        public DataSet GetProjectTypes()
        {
            return new ProjectDAL().GetProjectTypes();
        }
        public DataSet GetProjectLevels()
        {
            return new ProjectDAL().GetProjectLevels();
        }
        public DataSet GetProjectStatus()
        {
            return new ProjectDAL().GetProjectStatus();
        }
        public DataSet GetProjectPhase()
        {
            return new ProjectDAL().GetProjectPhase();
        }
        public DataSet GetProjectFolders()
        {
            return new ProjectDAL().GetProjectFolders();
        }
        public Project ProjectDetails(Project prop)
        {
            return new DrishProjectDAL.ProjectDAL().ProjectDetails(prop);
        }

        public int GetProjectIDByCode(Project prop)
        {
            return new DrishProjectDAL.ProjectDAL().GetProjectIDByCode(prop);
        }

        public DataSet projectdata(string ProjectName, int Proj_TypeID,int Proj_StatusID)
        {
            return new DrishProjectDAL.ProjectDAL().SearchProject( ProjectName,Proj_TypeID,Proj_StatusID);
        }

        public DataSet SearchProject(Project prj)
        {
            return new DrishProjectDAL.ProjectDAL().SearchProject(prj);
        }

        public DataSet SearchProject(Project prj, int empID)
        {
            return new DrishProjectDAL.ProjectDAL().SearchProject(prj,empID);
        }
        public bool UpdateProject(Project prj)
        {
            return new DrishProjectDAL.ProjectDAL().UpdateProject(prj);
        }

        public Project GetProject(int projectId)
        {
            return new DrishProjectDAL.ProjectDAL().GetProject(projectId);
        }
        public bool IsActiveMember(int EmployeeId,int ProjectID)
        {
            return new DrishProjectDAL.ProjectDAL().IsActiveMember(EmployeeId, ProjectID);
        }
        public string GetProjectStatus(string projectCode)
        {
            return new DrishProjectDAL.ProjectDAL().GetProjectStatus(projectCode);
        }

        public bool AddProjectDocumentTypes(Documents prop)
        {
            return new DrishProjectDAL.ProjectDAL().AddProjectDocumentTypes(prop);
        }
        public DataSet getEmployeeProjectsV2(int empId)
        {
            return new DrishProjectDAL.ProjectDAL().getEmployeeProjectsV2(empId);
        }
        public DataSet IfUserIsQA(int UserID, int ProjectID)
        {
            return new DrishProjectDAL.ProjectDAL().IfUserIsQA(UserID, ProjectID);
        }
        public int GetSameProject(int ichkProjectID, int ichkModuleID, int ichkActivityID, int UserID, DateTime ClndMonth)
        {
            return new DrishProjectDAL.ProjectDAL().GetSameProject(ichkProjectID, ichkModuleID, ichkActivityID, UserID, ClndMonth);
        }
        public DataSet GetProjectHistory(int intProjectID)
        {
            return new DrishProjectDAL.ProjectDAL().GetProjectHistory(intProjectID);
        }
        public bool AddHistoryForDateChange(int intProjectId, string type, DateTime dtOldEndDate, DateTime dtEndDate, string Reason, int ModifiedBy, DateTime ModifiedOn)
        {
            return new DrishProjectDAL.ProjectDAL().AddHistoryForDateChange(intProjectId, type, dtOldEndDate, dtEndDate, Reason, ModifiedBy, ModifiedOn);
        }
        public bool AddHistoryForStatusChange(int intProjectId, string type, string OldStatus, string NewStatus, string Reason, int ModifiedBy, DateTime ModifiedOn)
        {
            return new DrishProjectDAL.ProjectDAL().AddHistoryForStatusChange(intProjectId, type, OldStatus, NewStatus, Reason, ModifiedBy, ModifiedOn);
        }
        public DataSet GetProjectSummaryV2(Employee objUser)
        {
            return new DrishProjectDAL.ProjectDAL().GetProjectSummaryV2(objUser);
        }
        public DataSet GetProjectSummaryV2ByDates(Employee objUser,DateTime dt1,DateTime dt2)
        {
            return new DrishProjectDAL.ProjectDAL().GetProjectSummaryV2ByDates(objUser,dt1,dt2);
        }
    }
}
