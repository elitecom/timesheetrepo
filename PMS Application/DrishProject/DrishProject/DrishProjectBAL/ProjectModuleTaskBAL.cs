﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DrishProjectDAL;
using Entities;


namespace DrishProjectBAL
{
    public class ProjectModuleTaskBAL
    {
        public DataSet GetStatusList()
        {
            return new ProjectModuleTaskDAL().GetStatusList();
        }
        public DataSet GetHideStatusList()
        {
            return new ProjectModuleTaskDAL().GetHideStatusList();
        }
        public DataSet GetAllEmployeeList()
        {
            return new ProjectModuleTaskDAL().GetAllEmployeeList();
        }
        public DataSet GetEmployeeListPerTeamLead(Employee emp)
        {
            return new ProjectModuleTaskDAL().GetEmployeeListPerTeamLead(emp);
        }
        public DataSet GetMyTaskList(Employee emp)
        {
            return new ProjectModuleTaskDAL().GetMyTasksList(emp);
        }
        public DataSet GetMyProjectTaskList(Employee emp,int ProjectID)
        {
            return new ProjectModuleTaskDAL().GetMyProjectTaskList(emp,ProjectID);
        }
        public DataSet GetSearchMyTasks(ProjectModuleTasks tasks)
        {
            return new ProjectModuleTaskDAL().GetSearchMyTasks(tasks);
        }
        public ProjectModuleTasks GetNoOfMyTasks(Employee emp)
        {
            return new ProjectModuleTaskDAL().GetNoOfMyTasks(emp);
        }
        public bool AddProjectModulesTask(ProjectModuleTasks task)
        {
            return new ProjectModuleTaskDAL().AddProjectModulesTask(task);            
        }
        public DataSet GetTaskActivity()
        {
            return new ProjectModuleTaskDAL().GetTaskActivity();
        }
        public decimal GetModuleHoursForEmp(int AssignedTo, int ModuleId)
        {
            return new ProjectModuleTaskDAL().GetModuleHoursForEmp(AssignedTo, ModuleId);
        }
        public DataSet GetSelectedTask(int ProjectID, int TaskID)
        {
            return new ProjectModuleTaskDAL().GetSelectedTask(ProjectID, TaskID);
        }
        public DataSet GetSelectedTaskHistory(int ProjectID, int TaskID)
        {
            return new ProjectModuleTaskDAL().GetSelectedTaskHistory(ProjectID, TaskID);
        }

        public DataSet GetTasks(int ProjectID)
        {
            return new ProjectModuleTaskDAL().GetTasks(ProjectID);
        }

        public DataSet GetSelectedTaskHistory(int ProjectID)
        {
            return new ProjectModuleTaskDAL().GetSelectedTaskHistory(ProjectID);
        }
    }
}
