﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DrishProjectBAL
{
    public class HistoryBAL
    {
        public DataSet GetSelectedTaskHistory(int ProjectID, int TaskID)
        {
            return new DrishProjectDAL.HistoryDAL().GetSelectedTaskHistory(ProjectID, TaskID);
        }
    }
}
